package com.gxwzu.app.project;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmTask;
import com.gxwzu.business.service.project.XmTaskService;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewTask;
import com.gxwzu.sysVO.ViewXmTask;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmTaskAction extends BaseAction implements Preparable,ModelDriven<XmTask>,ServletContextAware{

	private static final long serialVersionUID = 5791927981200342232L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmTask model=new XmTask();
	@Override
	public XmTask getModel() {
		return model;
	}
	public void setModel(XmTask model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmTask();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmTaskService xmTaskService;
	
	public void setXmTaskService(XmTaskService xmTaskService) {
		this.xmTaskService = xmTaskService;
	}
	/***********************声明参数******************************/
	private String actionState;
	private String jsonXmTask;
	private String jsonViewXmTask;
	
	private List<XmTask> listXmTask;
	
	private String thisYear;
	private String thisUserId;
	private String thisLandNumber;
	private String thisXmTaskId;
	
	/**************************方法类**************************/
	/**
	 * 查询任务完成记录，通过征地编号，年份：http://127.0.0.1:8080/dxxy/appf/appXmTask_findXmTaskByNum.action?
	 * 参数：thisYear（年份）
	 * 		thisLandNumber（项目编号）
	 * 返回值：json（com.gxwzu.business.model.XmTask对象的List）
	 */
	public String findXmTaskByNum() {
		
		List<ViewTask> listViewTask = new ArrayList<ViewTask>();
		try {
			XmTask findXmTask = new XmTask();
			findXmTask.setTaskYear(thisYear);
			findXmTask.setLandNumber(thisLandNumber);
			List<XmTask> listXmTask = xmTaskService.findByExample(findXmTask);
			
			for (int i = 0; i < listXmTask.size(); i++) {
				findXmTask = listXmTask.get(i);
				
				ViewTask viewTask = new ViewTask();
				
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateCreateTime = sdf.format(findXmTask.getCreateTime());
				
				viewTask.setId(findXmTask.getId());
				viewTask.setTask(findXmTask.getTask());
				viewTask.setUserId(findXmTask.getUserId());
				viewTask.setUserName(findXmTask.getUserName());
				viewTask.setCreateTime(dateCreateTime);
				viewTask.setLandNumber(findXmTask.getLandNumber());
				viewTask.setTaskYear(findXmTask.getTaskYear());
				viewTask.setTaskMonth(findXmTask.getTaskMonth());
				viewTask.setTaskType(findXmTask.getTaskType());
				
				listViewTask.add(viewTask);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewTask);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 添加任务完成记录：http://127.0.0.1:8080/dxxy/appf/appXmTask_add.action?
	 * 关键数据（com.gxwzu.sysVO.ViewXmTask，封装json对象：jsonViewXmTask）
	 *        ：landNumber（项目编号）
	 *          taskYear（年份）
	 *          taskMonth（月份）
	 *          userId（上传人编号）
	 *          userName（上传人姓名）
	 *          landTask（土地征收（亩））
	 *          houseTask（房屋征收（m2））
	 *          mountainTask（坟山迁移）
	 * 返回：actionState：1成功；0失败
	 */
	public String add() {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			Gson gson = new Gson();
			ViewXmTask viewXmTask = gson.fromJson(jsonViewXmTask, ViewXmTask.class);
			if(viewXmTask.getLandNumber() != null && !"".equals(viewXmTask.getLandNumber())){
				if(viewXmTask.getLandTask() != null && !"".equals(viewXmTask.getLandTask())){
					XmTask landTask = new XmTask();
					
					landTask.setTask(viewXmTask.getLandTask());
					landTask.setUserId(viewXmTask.getUserId());
					landTask.setUserName(viewXmTask.getUserName());
					landTask.setCreateTime(Timestamp.valueOf(dateStr));
					landTask.setLandNumber(viewXmTask.getLandNumber());
					landTask.setTaskYear(viewXmTask.getTaskYear());
					landTask.setTaskMonth(viewXmTask.getTaskMonth());
					landTask.setTaskType("01");
					
					xmTaskService.save(landTask);
				}
				
				if(viewXmTask.getHouseTask() != null && !"".equals(viewXmTask.getHouseTask())){
					XmTask houseTask = new XmTask();
					
					houseTask.setTask(viewXmTask.getHouseTask());
					houseTask.setUserId(viewXmTask.getUserId());
					houseTask.setUserName(viewXmTask.getUserName());
					houseTask.setCreateTime(Timestamp.valueOf(dateStr));
					houseTask.setLandNumber(viewXmTask.getLandNumber());
					houseTask.setTaskYear(viewXmTask.getTaskYear());
					houseTask.setTaskMonth(viewXmTask.getTaskMonth());
					houseTask.setTaskType("02");
					
					xmTaskService.save(houseTask);
				}
				
				if(viewXmTask.getMountainTask() != null && !"".equals(viewXmTask.getMountainTask())){
					XmTask mountainTask = new XmTask();
					
					mountainTask.setTask(viewXmTask.getMountainTask());
					mountainTask.setUserId(viewXmTask.getUserId());
					mountainTask.setUserName(viewXmTask.getUserName());
					mountainTask.setCreateTime(Timestamp.valueOf(dateStr));
					mountainTask.setLandNumber(viewXmTask.getLandNumber());
					mountainTask.setTaskYear(viewXmTask.getTaskYear());
					mountainTask.setTaskMonth(viewXmTask.getTaskMonth());
					mountainTask.setTaskType("03");
					
					xmTaskService.save(mountainTask);
				}
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除任务完成记录：http://127.0.0.1:8080/dxxy/appf/appXmTask_del.action?
	 * 参数： thisXmTaskId（任务完成记录ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String  del() {
		try {
			if(thisXmTaskId != null && !"".equals(thisXmTaskId)){
				XmTask delXmTask = xmTaskService.findById(Integer.parseInt(thisXmTaskId));
				if(delXmTask != null){
					xmTaskService.remove(delXmTask);
					actionState="1";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/****************参数的getter和setter方法****************/
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	public String getThisYear() {
		return thisYear;
	}
	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}
	public String getActionState() {
		return actionState;
	}
	public void setActionState(String actionState) {
		this.actionState = actionState;
	}
	public String getJsonXmTask() {
		return jsonXmTask;
	}
	public void setJsonXmTask(String jsonXmTask) {
		this.jsonXmTask = jsonXmTask;
	}
	public List<XmTask> getListXmTask() {
		return listXmTask;
	}
	public void setListXmTask(List<XmTask> listXmTask) {
		this.listXmTask = listXmTask;
	}
	public String getThisLandNumber() {
		return thisLandNumber;
	}
	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}
	public String getThisXmTaskId() {
		return thisXmTaskId;
	}
	public void setThisXmTaskId(String thisXmTaskId) {
		this.thisXmTaskId = thisXmTaskId;
	}
	public String getJsonViewXmTask() {
		return jsonViewXmTask;
	}
	public void setJsonViewXmTask(String jsonViewXmTask) {
		this.jsonViewXmTask = jsonViewXmTask;
	}
	
}
