//定义添加新网格的基础点
var lng=0;
var lat=0;

//行政划区
function getBoundary(){     
		for(var i=0;i<area.length;i++){
			var bdary = new BMap.Boundary();
			bdary.get(area[i], function(rs){       //获取行政区域
			//map.clearOverlays();        //清除地图覆盖物       
			var count = rs.boundaries.length; //行政区域的点有多少个
			if (count === 0) {
				//alert('未能获取当前输入行政区域');
				return ;
			}
          	var pointArray = [];
			for (var i = 0; i < count; i++) {
				var ply = new BMap.Polygon(rs.boundaries[i], {strokeWeight: 2, strokeColor: "#ff0000",fillOpacity:0.1}); //建立多边形覆盖物
				
				//创建右键菜单
				var markerMenu=new BMap.ContextMenu();
				markerMenu.addItem(new BMap.MenuItem('添加网格',addGrid.bind()));
				ply.addContextMenu(markerMenu);
				
				map.addEventListener("rightclick", function (e) {
				  lat=e.point.lat;
				  lng=e.point.lng;
				});
				
				map.addOverlay(ply);  //添加覆盖物
				ply.disableMassClear();//禁止覆盖物在map.clearOverlays方法中被清除(自 1.1 新增)
				pointArray = pointArray.concat(ply.getPath());
			}    
			map.setViewport(pointArray);    //调整视野                 
			});   
		}  
		
	}
	setTimeout(function(){
		getBoundary();
	}, 0);//原为2000setTimeout(function(){
	//备注：先加载区域，后加载网格，否则区域会挡住网格，使网格右键菜单效
	
	
	//在右键点击点添加网格
	function addGrid(){
		var newPly= new BMap.Polygon([
						new BMap.Point(lng,lat),
						new BMap.Point(lng+0.01078,lat-0.004375),
						new BMap.Point(lng+0.005102,lat-0.009347)
					    ], {strokeColor:"Green", strokeWeight:2, strokeOpacity:0.5});  //创建多边形
		addLay(newPly);//添加多边形
	}