// 面编辑
	var polygon = null;
	var addLay=function(newPolygon) { 
				var polygon;//声明多边形（如果newPolygon为空，则创建一个新的）
				if(newPolygon!=null){
					polygon=newPolygon;
				}else{
					polygon = new BMap.Polygon([
						new BMap.Point(111.280832,23.488972),
						new BMap.Point(111.291612,23.484597),
						new BMap.Point(111.285934,23.479625)
					    ], {strokeColor:"Green", strokeWeight:2, strokeOpacity:0.3});  //创建多边形
				}
				
				
				map.addOverlay(polygon);   //增加多边形
				polygon.enableEditing();   //开启面编辑
				polygon.enableMassClear();  //允许覆盖物在map.clearOverlays方法中被清除(自 1.1 新增)
				
				
				var removeMarker = function(e,ee,polygon){
					map.removeOverlay(polygon);
				};
				
				//创建右键菜单
				var markerMenu=new BMap.ContextMenu();
				markerMenu.addItem(new BMap.MenuItem('删除',removeMarker.bind(polygon)));
				polygon.addContextMenu(markerMenu);
				
				polygon.addEventListener("dblclick", function showOverlayInfo2(e){   //添加监听事件,单击时触发
						var graph = this.getPath();
						var bgSqua=gridSuq(polygon);
					  	var ZB="";
					  	for(var i=0;i<graph.length;i++){
					      	ZB +=graph[i].lng+","+graph[i].lat+"_";
					  	}
 						var index = layer.open({
    						type: 2,
    						area: ['50%', '70%'],
    						fix: false, //不固定
    						maxmin: true,
    						content: '/szSystem/sjzy/grid_openPage.action?view=add&zb='+ZB+'&bgSuqa='+bgSqua,
				    		end: function(){
				    		    //如果添加成功，则刷新页面
				    		    if($("#flag").val()==1){
				                    location.reload();//刷新页面  
				                    $("#flag").val(0);  		    
				    		    }
    		                }	
    						
 						}); 
				});
	};
	var removeLay=function() { 
				map.clearOverlays(polygon);   //清除多边形
	};
	
	var ajaxAreaList=function(){
	$.ajax({
		url : "/szSystem/sjzy_json/grid_findAllAjax_ajax.action",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			var obj = eval(data);
			for ( var i in obj) {
				var gridId = obj[i].gridId;
				var gridName = obj[i].gridName;
				var bgcode = obj[i].bgcode;
				var bgsqua = obj[i].bgsqua;
				var ordate = obj[i].ordate;
				var chdate = obj[i].chdate;
				var coordinate = obj[i].coordinate;
				var areaId = obj[i].areaId;
				var girdType = obj[i].girdType;
				var note = obj[i].note;
	
				var zb2 = coordinate.split("_");	
				var p = [];
				var ply1;
				for(var j=0;j<zb2.length;j++){
					var zb3 = zb2[j].split(",");
	                p.push(new BMap.Point(zb3[0], zb3[1]));
	            }
	
				ply1 = new BMap.Polygon(p, {strokeWeight: 2, strokeColor: "Green", fillColor: "Green", fillOpacity:0.2});
				
				//创建右键菜单
				var markerMenu=new BMap.ContextMenu();
				markerMenu.addItem(new BMap.MenuItem('删除',deletePly.bind(ply1,gridId)));
				markerMenu.addItem(new BMap.MenuItem('查看',detailsPly.bind(ply1,gridId)));
				
				ply1.addContextMenu(markerMenu);
				
				
				map.addOverlay(ply1);  //添加覆盖物
				ply1.disableMassClear()  ;  //禁止覆盖物在map.clearOverlays方法中被清除(自 1.1 新增) 
				
			}
		}
	});
	};
	
	setTimeout(function(){
		ajaxAreaList();
	}, 1000);//原为2000
	
	//删除已经存在的覆盖物
    var deletePly=function(gridId,e,ee,ply1){
		layer.confirm('确认删除该网格?', function(index){
			$.post("/szSystem/sjzy/grid_del.action?view=details",{"id":gridId},function(json){
			if(json='true'){
				layer.msg('删除成功', {
			        shade: 0.3,
			        time: 1000
			   });
			   map.removeOverlay(ply1);//从地图上删除
			}
		  });
		}); 
	};
	
	//查看已经存在的覆盖物
    var detailsPly=function(gridId,e,ee,ply1){
    	//dialog('300px','400px','查看网格详细信息','/szSystem/sjzy/grid_openedit.action?view=details&id='+gridId, 'true', '10%', '10%');
    	
		var index = layer.open({
    						type: 2,
    						title:'查看网格详细信息',
    						area: ['300px', '400px'],
    						fix: false, //不固定
    						content: ['/szSystem/sjzy/grid_openedit.action?view=details&id='+gridId]
 		}); 
	};
    
    //测面积
    function gridSuq(polygon){
    	var area = BMapLib.GeoUtils.getPolygonArea(polygon);
   		return area.toFixed(2);
    	   
	 };
  	