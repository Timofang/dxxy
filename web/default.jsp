<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>工作台</title>
<base href="<%=basePath %>" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=path%>/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path %>/third/jquery.ba-resize.min.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
  
	$(".select3").uedSelect({
		width : 150
	});
});
function chartForm(){
	$("#chartForm").submit();
}
</script>
<!-- ECharts单文件引入 -->
		<script src="<%=path%>/third/echarts-2.2.7/dist/echarts.js"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="<%=path %>/sys/Home_home.action">首页</a></li>
     <li><a href="#">工作台</a></li>
       <li style="font-weight: blod;">系统角色：${session.fullName}</li>
    
    </ul>
    </div>
    
    
    <div class="mainbox">
    
    <div class="mainleft">
    
    
    <div class="leftinfo">
     <form action="<%=path%>/sys/Home_home.action" id="chartForm" method="post">
    <div class="listtitle"><a href="#" class="more1">更多</a>各乡镇八有一超完成动态(单位：户)</div>
    <div class="seachform">
   
     <ul class="seachform " style="margin-top:5px;">
    

    <li><label>登记年份</label>  
    <div class="vocation">
 <s:select name="param_yyyy"  list="yearList" listKey="key" listValue="value" value="param_yyyy" cssClass="select3" onchange="chartForm();"></s:select>
    </div>
    </li>
 
    </ul></div>
    <div class="maintj" id="maintj_echart" style="height:400px;">  
    
    
    </div>
    </form>
    
    </div>
    <!--leftinfo end-->
    
    
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="<%=path %>/biz/Notice_listNoticeType.action?resUri=view&thisNoticeType=01&thisNoticeType=02" class="more1">更多</a>公告、通告</div>    
    <ul class="newlist">
    <s:iterator value="recenNoticeList2.data">
    <li>
    	<c:if test="${theFormat=='01'}">
			<a href="<%=path %>/biz/Notice_details.action?model.id=${id}" title="${noticeTitle}" target="_blank" >${noticesubTitle }</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
		<c:if test="${theFormat=='02'}">
			<a href="<%=path %>/third/pdfjs-1.6.210-dist/web/viewer.html?name=<%=path %>${enclosure}" target="_blank" alt="标题" title="点击公告详情">${noticeTitle}</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
    </li>
    </s:iterator>
    </ul>   
    
    </div>
    
    <div class="inforight">
    
    <div class="listtitle"><a href="<%=path %>/biz/Notice_listNoticeType.action?resUri=view&thisNoticeType=03" class="more1">更多</a>政策</div>    
    <ul class="newlist">
    <s:iterator value="recenNoticeList3.data">
      <li>
      	<c:if test="${theFormat=='01'}">
			<a href="<%=path %>/biz/Notice_details.action?model.id=${id}" title="${noticeTitle}" target="_blank" >${noticesubTitle }</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
		<c:if test="${theFormat=='02'}">
			<a href="<%=path %>/third/pdfjs-1.6.210-dist/web/viewer.html?name=<%=path %>${enclosure}" target="_blank" alt="标题" title="点击公告详情">${noticeTitle}</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
      </li>
    </s:iterator>
    </ul>   
    
    </div>
    

    </div>
        
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="<%=path %>/biz/Notice_listNoticeType.action?resUri=view&thisNoticeType=04" class="more1">更多</a>扶贫动态</div>    
    <ul class="newlist">
    <s:iterator value="recenNoticeList.data">
    <li>
		<c:if test="${theFormat=='01'}">
			<a href="<%=path %>/biz/Notice_details.action?model.id=${id}" title="${noticeTitle}" target="_blank" >${noticesubTitle }</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
		<c:if test="${theFormat=='02'}">
			<a href="<%=path %>/third/pdfjs-1.6.210-dist/web/viewer.html?name=<%=path %>${enclosure}" target="_blank" alt="标题" title="点击公告详情">${noticeTitle}</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
	</li>
    </s:iterator>
    </ul>   
    
    </div>
    
    <div class="inforight">
    
    <div class="listtitle"><a href="<%=path %>/biz/Notice_listNoticeType.action?resUri=view&thisNoticeType=05" class="more1">更多</a>扶贫风采</div>    
    <ul class="newlist">
    <s:iterator value="recenNoticeList4.data">
      <li>
		<c:if test="${theFormat=='01'}">
			<a href="<%=path %>/biz/Notice_details.action?model.id=${id}" title="${noticeTitle}" target="_blank" >${noticesubTitle }</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
		<c:if test="${theFormat=='02'}">
			<a href="<%=path %>/third/pdfjs-1.6.210-dist/web/viewer.html?name=<%=path %>${enclosure}" target="_blank" alt="标题" title="点击公告详情">${noticeTitle}</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
	</li>
    </s:iterator>
    </ul>   
    
    </div>
    
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="<%=path %>/biz/Notice_listNoticeType.action?resUri=view&thisNoticeType=06" class="more1">更多</a>督查报告</div>    
    <ul class="newlist">
    <s:iterator value="recenNoticeList5.data">
      <li>
      	<c:if test="${theFormat=='01'}">
			<a href="<%=path %>/biz/Notice_details.action?model.id=${id}" title="${noticeTitle}" target="_blank" >${noticesubTitle }</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
		<c:if test="${theFormat=='02'}">
			<a href="<%=path %>/third/pdfjs-1.6.210-dist/web/viewer.html?name=<%=path %>${enclosure}" target="_blank" alt="标题" title="点击公告详情">${noticeTitle}</a><b><s:date name="releaseTime" format="yyyy-MM-dd" /></b>
		</c:if>
	</li>
    </s:iterator>
    </ul>   
    
    </div>
    </div>
    
    
    

    </div>
    
    
    </div>
    <!--mainleft end-->
    
    
    
    <!--mainright end-->
    
    
    </div>


	<script type="text/javascript">
       // 路径配置
       var echarts_path="<%=path%>/third/echarts-2.2.7/dist";
        require.config({
            paths: {
                echarts: echarts_path
		}
	});

	// 使用
	require([ 'echarts', 'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
	], function(ec) {
		// 基于准备好的dom，初始化echarts图表
		var myChart = ec.init(document.getElementById('maintj_echart'));

		var zrColor = require('zrender/tool/color');
		var colorList = [ '#ff7f50', '#87cefa', '#da70d6', '#32cd32',
				'#6495ed', '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500',
				'#40e0d0' ];
		var itemStyle = {
			normal : {
				color : function(params) {
					if (params.dataIndex < 0) {
						// for legend
						return zrColor.lift(colorList[colorList.length - 1],
								params.seriesIndex * 0.1);
					} else {
						// for bar
						return zrColor.lift(colorList[params.dataIndex],
								params.seriesIndex * 0.1);
					}
				}
			}
		};
	
		var x_data=[
		  <s:iterator value="viewOvestepModel.results" id="iterm"  status='st'>
		   <s:if test="#st.first"> 
					{
						name : '${iterm[0]}',
						type : 'bar',
						itemStyle : itemStyle,
						data : [ ${iterm[3]},${iterm[4]}, ${iterm[5]}, ${iterm[6]}, ${iterm[7]}, ${iterm[8]},
								${iterm[9]}, ${iterm[10]},${iterm[11]},${iterm[12]} ]
					}
		   </s:if><s:else>
		   ,
		   	{
						name : '${iterm[0]}',
						type : 'bar',
						itemStyle : itemStyle,
						data : [ ${iterm[3]},${iterm[4]}, ${iterm[5]}, ${iterm[6]}, ${iterm[7]}, ${iterm[8]},
								${iterm[9]}, ${iterm[10]},${iterm[11]},${iterm[12]} ]
					}
		   </s:else>
		 </s:iterator>
					];
		var xAxis_data=[ {
				type : 'category',
				axisLabel :{  interval:0   },
				data : [ '完成双认定', '人均年收入', '有稳固住房', '有饮用水', '有电用', '有路通自然村',
						'有义务教育保障', '有医疗保障','有电视看','有收入来源' ]}];
		
		option = {
	
			tooltip : {
				trigger : 'axis',
				backgroundColor : 'rgba(255,255,255,0.7)',
				axisPointer : {
					type : 'shadow'
				},
				formatter : function(params) {
					// for text color
					var color = colorList[params[0].dataIndex];
					var res = '<div style="color:' + color + '">';
					res += '<strong>' + params[0].name + '(户)</strong>';
					for ( var i = 0, l = params.length; i < l; i++) {
						res += '<br/>' + params[i].seriesName + ' : '
								+ params[i].value;
					}
					res += '</div>';
					return res;
				}
			},
			legend : {
				x : 'right',
				data : [ 
				<s:iterator value="viewOvestepModel.results" id="iterm"  status='st'>
		   <s:if test="#st.first"> 
				'${iterm[0]}'
		   </s:if><s:else>,
		   	'${iterm[0]}'
		   </s:else>
		 </s:iterator>
				]
			},
			toolbox : {
				show : true,
				orient : 'vertical',
				y : 'center',
				feature : {
					mark : {
						show : false
					},
					dataView : {
						show : false,
						readOnly : true
					},
					restore : {
						show : true
					},
					saveAsImage : {
						show : true
					}
				}
			},
			calculable : true,
			grid : {
				y : 80,
				y2 : 40,
				x2 : 40
			},
			xAxis : [ {
				type : 'category',
				data : ['' ]
			} ],
			yAxis : [ {
				type : 'value'
			} ],
			series : [{}]
		};
		//设置横坐标
		option.xAxis=xAxis_data;
		option.series=x_data;
		// 为echarts对象加载数据 
		myChart.setOption(option);

	});
	//设置窗口大小
	setWidth();
	$(window).resize(function(){
		setWidth();	
	});
	function setWidth(){
		var width = ($('.leftinfos').width()-12)/2;
		$('.infoleft,.inforight').width(width);
	}
</script>
</body>

</html>
