<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=path %>/css/style.css" rel="stylesheet" type="text/css" />
<script language="javaScript" src="<%=path %>/third/jquery.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>

<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson .header").click(function(){
		var $parent = $(this).parent();
		$(".menuson>li.active").not($parent).removeClass("active open").find('.sub-menus').hide();
		
		$parent.addClass("active");
		if(!!$(this).next('.sub-menus').size()){
			if($parent.hasClass("open")){
				$parent.removeClass("open").find('.sub-menus').hide();
			}else{
				$parent.addClass("open").find('.sub-menus').show();	
			}
		}
	});
	//一级菜单
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	// 三级菜单点击
	$('.sub-menus li').click(function(e) {
        $(".sub-menus li.active").removeClass("active")
		$(this).addClass("active");
    });
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('.menuson').slideUp();
		if($ul.is(':visible')){
			$(this).next('.menuson').slideUp();
		}else{
			$(this).next('.menuson').slideDown();
		}
	});
});
</script>
</head>
<body style="background:#f0f9fd;" >
<div class="lefttop"><span></span>导航菜单</div>
<dl class="leftmenu">

    <dd>
    <div class="title">
    <span><img src="<%=path %>/images/leftico01.png" /></span>任务管理
    </div>
    	<ul class="menuson">
    		<c:if test='${fn:contains(UserType,"1") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Xm_openPage.action?resUri=list" target="rightFrame">学院任务</a><i></i></li>
    		</c:if>
    		<c:if test='${fn:contains(UserType,"2") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Xm_openProjectList.action?resUri=projectList" target="rightFrame">任务列表</a><i></i></li>
    		</c:if>
    		<c:if test='${fn:contains(UserType,"3") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Xm_openMyProjectList.action?resUri=myProjectList" target="rightFrame">我的任务</a><i></i></li>
    		</c:if>
    		<c:if test='${fn:contains(UserType,"4") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Xm_openMyIssueList.action?resUri=myIssue" target="rightFrame">我的下发</a><i></i></li>
    		</c:if>
    			<c:if test='${fn:contains(UserType,"4") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Xm_xmStatistics.action?resUri=xmStatistics" target="rightFrame">任务统计</a><i></i></li>
    		</c:if>
    		<%-- <li><cite></cite><a href="<%=path %>/biz/Xm_openUnitProjectIssue.action?resUri=openUnitProjectIssue" target="rightFrame">教研室任务</a><i></i></li> --%>
        </ul>
    
    <div class="title">
    <c:if test='${fn:contains(UserType,"1") }'>
    	<span><img src="<%=path %>/images/leftico01.png" /></span>系统管理
    </c:if>
    </div>
    	<ul class="menuson">
    		
    		<c:if test='${fn:contains(UserType,"6") }'>
    			<li><cite></cite><a href="<%=path %>/sys/User_list.action?view=list&resUri=list" target="rightFrame">用户管理</a><i></i></li>
    		</c:if>
    		<c:if test='${fn:contains(UserType,"7") }'>
    			<li><cite></cite><a href="<%=path %>/biz/Unit_list.action?view=list&resUri=list" target="rightFrame">教研室管理</a><i></i></li>
    		</c:if>
    		<c:if test='${fn:contains(UserType,"8") }'>
    			<li><cite></cite><a href="<%=path %>/biz/XmType_list.action?view=list&resUri=list" target="rightFrame">任务类型管理</a><i></i></li>
    		</c:if>
    		

        </ul>
        
    </dd>
    </dl>
</body>
</html>
