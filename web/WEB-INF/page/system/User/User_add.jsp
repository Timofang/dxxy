<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>添加用户</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

    <script type="text/javascript">
        $(function(){
            //校验手机号码
            $.validator.addMethod("mobile", function(value, element) {
                var length = value.length;
                var mobile = /^(13\d|14[57]|15[^4,\D]|17[678]|18\d)\d{8}|170[059]\d{7}$/;
                return this.optional(element) || (length == 11 && mobile.test(value));
            }, "手机号码格式错误");

            //校验QQ
            $.validator.addMethod("qq", function(value, element) {
                var length = value.length;
                var qq = /^\d{5,12}$/;
                return this.optional(element) || (qq.test(value));
            }, "QQ格式错误");

            //校验身份证号码
            $.validator.addMethod("card", function(value, element) {
                var length = value.length;
                var card = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/;
                return this.optional(element) || (card.test(value));
            }, "身份证号码格式错误");

            $("#userFrom").validate({
                errorClass:"errorInfo", //默认为错误的样式类为：error
                errorElement:"em",
                focusInvalid: false, //当为false时，验证无效时，没有焦点响应
                onkeyup: false,
                submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                    form.submit();   //提交表单
                },
                rules:{
                    "model.jobNumber":{
                        required:true
                    },
                    "model.helpUserName":{
                        required:true,
                        remote: {
                            type: "post",
                            url: "<%=path %>/sys/User_checkLoginName.action",
                            data: {
                                "model.helpUserName": function() {
                                    return $("#helpUserName").val();
                                }
                            },
                            dataType: "html",
                            dataFilter: function(data, type) {
                                if (data == "true"){
                                    return true;
                                }else{
                                    return false;
                                }
                            }
                        },
                        rangelength:[1,100]
                    },
                    "model.helpPassword":{
                        required:true
                    },
                    "model.helpName":{
                        required:true
                    }
                },
                messages:{
                    "model.jobNumber":{
                        required:"请输入员工工号"
                    },
                    "model.helpUserName":{
                        required:"请输入工号",
                        remote:"该工号已经存在"
                    },
                    "model.helpPassword":{
                        required:"请输入密码"
                    },
                    "model.helpName":{
                        required:"请输入名称"
                    }
                }
            });
        });

        $(function(){
            var flag="${model.id}";
            if(flag!='' && flag!=undefined){
                $('#submit').attr('disabled','disabled');
                layer.msg('添加成功');
                setTimeout(function(){
                    parent.location.reload();
                    layer.close(index);
                },1000);
            }
        });

    </script>

    <script type="text/javascript">
        function saveRec(){
            var _areaDistrictID = $("#areaDistrictID").val();
            var _areaTownshipID = $("#areaTownshipID").val();
            var _areaVillageID = $("#areaVillageID").val();
            var _thisHelpUserType = $("input[name='thisHelpUserType']:checked").val();
            if(_thisHelpUserType != undefined){


                var strChecks2 = "";
                var checks2 = $("input[name='thisHelpUserType']:checked");

                for(var i=0; i<checks2.length; i++){
                    if(i == 0){
                        strChecks2 += checks2[i].value;
                    }else{
                        strChecks2 += "_"+checks2[i].value;
                    }
                }
                if(strChecks2.indexOf("02")!=-1 || strChecks2 == '02' || strChecks2.indexOf("03")!=-1 || strChecks2 == '03' || strChecks2.indexOf("04")!=-1 || strChecks2 == '04'){
                    if(_areaDistrictID==""){
                        alert("县（市、区）必填!!");
                        $("#areaDistrictID").focus();
                        return false;
                    }else{
                        if(_areaTownshipID==""){
                            alert("乡（镇）必填!!");
                            $("#areaTownshipID").focus();
                            return false;
                        }else{
                            if(_areaVillageID==""){
                                alert("行政村必填!!");
                                $("#areaVillageID").focus();
                                return false;
                            }else{
                                $("#userFrom").submit();
                            }
                        }
                    }
                }else if(strChecks2.indexOf("05")!=-1 || strChecks2 == '05'){
                    if(_areaDistrictID==""){
                        alert("县（市、区）必填!!");
                        $("#areaDistrictID").focus();
                        return false;
                    }else{
                        if(_areaTownshipID==""){
                            alert("乡（镇）必填!!");
                            $("#areaTownshipID").focus();
                            return false;
                        }else{
                            $("#userFrom").submit();
                        }
                    }
                }else if(strChecks2.indexOf("06")!=-1 || strChecks2 == '06'){
                    if(_areaDistrictID==""){
                        alert("县（市、区）必填!!");
                        $("#areaDistrictID").focus();
                        return false;
                    }else{
                        $("#userFrom").submit();
                    }
                }else{
                    $("#userFrom").submit();
                }
            }else{
                $("#userFrom").submit();
            }

        }
    </script>

</head>
<body>
<div class="main">
    <form action="<%=path %>/sys/User_add.action?view=add&resUri=add" method="post"  name="form" id="userFrom" enctype="multipart/form-data">
        <ul>
            <li><strong><font color="red">用户信息：</font></strong></li>
            <li>
                <table width="100%" border="10" cellspacing="0" cellpadding="0" class="formTable">
                    <tr>
                    	
                        <!-- <td>用户名：</td>
                        <td >
                            <input class="dfinput" type="text" id="helpUserName" name="model.helpUserName">
                        </td> -->
                        <%--<td>个人照片：</td>
                        <td colspan="3">
                            <input type="file" name="upload" id="upload"/>
                        </td>--%>
                        <td>工号：</td>
                        <td>
                            <input class="dfinput" type="text" name="model.helpUserName" id="helpUserName"/>
                        </td>
                        
                        <%----%>
                        <td>密码：</td>
                        <td>
                            <input class="dfinput" type="text" id="model.helpPassword" name="model.helpPassword">
                        </td>
                          <td style="border-right: none;"></td><td style="border-left: none;"></td>
                    </tr>
                    <tr>
                        <td>领导级别:</td>
                        <td>
                            <%--<input class="dfinput" type="text" name="model.leaderType" id="model.leaderType" />--%>
                            <select class="dfinput" name="model.leaderTypeId" id="model.leaderTypeId">
                                <option value="1">学院领导</option>
                                <option value="2">教研室/实验室负责人</option>
                                <option value="3">普通教师</option>
                            </select>
                        </td>
                        <%----%>
                        <td>姓名：</td>
                        <td>
                            <input class="dfinput" type="text" id="model.helpName" name="model.helpName">
                        </td>
                        <td>职务：</td>
                            <td><input class="dfinput" type="text" name="model.helpPost" id="model.helpPost">
                        </td>
                    </tr>
                    <tr>
                        <td>联系方式:</td>
                        <td>
                            <input class="dfinput" type="text" name="model.helpTelephone" id="model.helpTelephone">
                        </td>
                        <td>性别:</td>
                        <td>
                            <select class="dfinput" name="model.helpSex" id="model.helpSex">
                                <option value="01">男</option>
                                <option value="02">女</option>
                            </select>
                        </td>
                        <td style="border-right: none;"></td><td style="border-left: none;"></td>
                    </tr>
                    <tr>
                        <td>所属教研室：</td>
                        <td colspan="6">
                            <c:forEach items="${requestScope.listUnit}" var="key">
                                <input type="checkbox" name="model.unitId" id="model.unitId" value="${key.unitId}"
                                       <c:if test="${fn:contains(model.unitId,key.unitId)}">checked="checked"</c:if>>${key.unitId}：${key.unitName} &nbsp;&nbsp;
                            </c:forEach>
                                <%--<input type="checkbox" name="model.unitId" id="model.unitId" value="A001"--%>
                                       <%--<c:if test="${fn:contains(model.unitId,'A001')}">checked="checked"</c:if>>A001：行政 &nbsp;&nbsp;--%>
                                <%--<input type="checkbox" name="model.unitId" id="model.unitId" value="A002"--%>
                                       <%--<c:if test="${fn:contains(model.unitId,'A002')}">checked="checked"</c:if>>A002：软件工程&nbsp;&nbsp;--%>
                        </td>
                    </tr>
                    <%-- <tr>
                       <td>是否有批示权限：</td>
                       <td colspan="5">
                          <input type="radio" name="model.isReply" value="00" checked="checked"/> 否
                          <input type="radio" name="model.isReply" value="03"/> 是
                       </td>
                     </tr>--%>
                </table>
            <li>
                <div style="margin:10px 50%;">
                    <input name="" id="submit1" type="button" class="scbtn" value="提交" onClick="saveRec();"/>
                </div>
            </li>
        </ul>
    </form>
</div>
</body>
</html>