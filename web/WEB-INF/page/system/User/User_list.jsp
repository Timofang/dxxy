<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>用户管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>

    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />


    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>

    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" /><script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript">
        function openAdd(_isSecretary){
            layer.open({
                type: 2,
                area: ['90%', '90%'],
                fix: false, //不固定
                title:'添加用户',
                maxmin: true,
                content: '<%=path%>/sys/User_openAdd.action?view=add&resUri=openAdd&isSecretary='+_isSecretary
            });
        }

    </script>
</head>
<body>
<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="<%=path %>/sys/Home_home.action">首页</a></li>
        <li>用户管理</li>
    </ul>
</div>
<div class="rightinfo">
    <div class="itab" style="margin-bottom:5px;">
        <ul>
            <li><a class="selected" href="<%=path %>/sys/User_list.action?view=list&resUri=list&isOneAction=1&thisUserDuty=${thisUserDuty}" target="rightFrame">系统用户管理</a></li>
        </ul>
    </div>
    <form action="<%=path%>/sys/User_findByExample.action?view=list&resUri=list&isSecretary=${isSecretary}" method="post" id="household">
        <ul>
            <li>
                <table border="1" cellspacing="0" cellpadding="0" >
                    <tr>
                        <td valign="middle">用户姓名：</td>
                        <td >
                            <input class="dfinput" type="text" id="model.helpName" name="model.helpName" value="${model.helpName }"/>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <input name="" id="queryBtn" type="submit" class="scbtn" value="查询"/>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                </table>
            </li>
        </ul>
    </form>
    <div class="tools">
        <ul class="toolbar">
            <li class="click"><span><img src="<%=path%>/images/t01.png" /></span>
                <a href="#" onclick="openAdd('${isSecretary}')">添加用户</a></li>
        </ul>
    </div>

    <table class="tablelist">
        <thead>
        <tr>
            <th>序号</th>
            <th>工号</th>
            <th>姓名</th>
            <th>性别</th>
            <%--<th>单位</th>--%>
            <%--<th>职务</th>--%>
            <%--<th>联系方式</th>--%>
            <%--<th>是否有批示权限</th>--%>
            <th>所属教研室</th>
            <th>领导级别</th>
            <th>职务名称</th>
            <th>联系电话</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:if test="${isExample==1 }">
            <s:iterator id="p" value="listUser" status="pp">
                <tr  id="tr_${id}" align="center" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                    <td align="center"><s:property value="#pp.count"/></td>
                    <td align="center">${helpUserName}</td>
                    <td align="center">${helpName}</td>
                    <td align="center">
                        <c:if test="${helpSex=='01' }">男</c:if>
                        <c:if test="${helpSex=='02' }">女</c:if>
                        <c:if test="${helpSex=='00' }">其他</c:if>
                    </td>
                        <%--<td align="center">${helpUnit}</td>--%>
                        <%--<td align="center">${helpPost}</td>--%>
                        <%--<td align="center">${helpTelephone}</td>--%>
                        <%--<td align="center">--%>
                        <%--<s:if test="%{isReply=='03'}">是</s:if>--%>
                        <%--<s:else>否</s:else>--%>
                        <%--</td>  		--%>
                    <td align="center">
                        <c:forEach items="${requestScope.listUnit}" var="key">
                            <c:if test="${fn:contains(unitId,key.unitId)}">${key.unitName}，</c:if>
                        </c:forEach>
                        <%--<c:if test="${fn:contains(unitId,'A001')}">行政,</c:if>--%>
                        <%--<c:if test="${fn:contains(unitId,'A002')}">软件工程,</c:if>--%>
                    </td>
                    <td align="center">
                        <c:if test="${leaderTypeId=='1'}">学院领导</c:if>
                        <c:if test="${leaderTypeId=='2'}">教研室/实验室负责人</c:if>
                        <c:if test="${leaderTypeId=='3'}">普通教师</c:if>
                    </td>
                    <td align="center">${helpPost}</td>
                    <td align="center">${helpTelephone}</td>
                    <td align="center">
                        <a href="<%=path%>/sys/User_del.action?view=list&resUri=del&delUserId=${id }" target="rightFrame" onclick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
                        <a href="javascript:dialog('90%','90%','修改用户信息','<%=path%>/sys/User_openEdit.action?view=edit&thisUserId=${id }&resUri=openEdit', 'true', '5%', '5%');" ><font color="red">修改</font></a>
                    </td>
                </tr>
            </s:iterator>
        </c:if>
        <c:if test="${isExample==0 }">
            <s:iterator id="p" value="pageResult.data" status="pp">
                <tr id="tr_${id}" align="center" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                    <td align="center"><s:property value="#pp.count"/></td>
                    <td align="center">${helpUserName}</td>
                    <td align="center">${helpName}</td>
                    <td align="center">
                        <c:if test="${helpSex=='01'}">男</c:if>
                        <c:if test="${helpSex=='02'}">女</c:if>
                        <c:if test="${helpSex=='00'}">其他</c:if>
                    </td>
                        <%--<td align="center">${helpUnit}</td>--%>
                        <%--<td align="center">${helpPost}</td>--%>
                        <%--<td align="center">${helpTelephone}</td>--%>
                        <%--<td align="center">--%>
                        <%--<s:if test="%{isReply=='03'}">是</s:if>--%>
                        <%--<s:else>否</s:else>--%>
                        <%--</td> 	--%>
                    <td>
                        <c:forEach items="${requestScope.listUnit}" var="key">
                        <c:if test="${fn:contains(unitId,key.unitId)}">${key.unitName}，</c:if>
                    </c:forEach>
                    </td>
                    <td align="center">
                        <c:if test="${leaderTypeId=='1'}">学院领导</c:if>
                        <c:if test="${leaderTypeId=='2'}">教研室/实验室负责人</c:if>
                        <c:if test="${leaderTypeId=='3'}">普通教师</c:if>
                    </td>
                    <td align="center">${helpPost}</td>
                    <td align="center">${helpTelephone}</td>
                    <td align="center">
                        <a href="<%=path%>/sys/User_del.action?view=list&resUri=del&delUserId=${id }" target="rightFrame" onclick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
                        <a href="javascript:dialog('90%','90%','修改用户信息','<%=path%>/sys/User_openEdit.action?view=edit&thisUserId=${id }&resUri=openEdit', 'true', '5%', '5%');" ><font color="red">修改</font></a>
                    </td>
                </tr>
            </s:iterator>
        </c:if>


        </tbody>
    </table>
    <c:if test="${isExample==0 }">
        <%@ include file="/WEB-INF/common/pagination.jsp"%>
    </c:if>
</div>
<br/>
</body>
</html>
