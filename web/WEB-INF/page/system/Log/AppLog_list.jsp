<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>用户管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>

<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />


<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>

<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" /><script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/Home_home.action">首页</a></li>
<li>用户管理</li>
</ul>
</div>
		<div class="rightinfo">
     
        <table  class="tablelist">
    	<thead>
    	<tr>
        <th>序号</th>
   		<th>用户</th>
   		<th>报错时间</th>
   		<th>操作</th>
        </tr>
        </thead>
        <tbody>
        	<s:iterator id="p" value="listApplog" status="pp">
        	<tr  id="tr_${id}">
        <td align="center"><s:property value="#pp.count"/></td>
        <td align="center">${userName}</td>
   		<td align="center">${logTime}</td>
   		<td align="center">
           	<a href="javascript:dialog('90%','90%','查看App报错信息','<%=path%>/appf/appLogin_appLogInfo.action?resUri=appLogInfo&ApplogId=${id }', 'true', '5%', '5%');" ><font color="red">查看</font></a>
       </td>
</tr>
</s:iterator>       
          
        </tbody>
</table>
</div>
<br/>
</body>
</html>
