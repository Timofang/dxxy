<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body onload="msg('${msg}')">
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>任务管理（列表）</li>
</ul>
</div>
<script type="text/javascript">
	
    function openAdd(){
		layer.open({
	    	type: 2,
	    	area: ['90%', '90%'],
	    	fix: false, //不固定
	    	maxmin: true,
			title:'新增任务立项信息填写',
	    	content: '<%=path%>/biz/Xm_openAdd.action?resUri=openAdd&thisResUri=list'
		});
	}
	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'学院任务修改',
    	content: '<%=path%>/biz/Xm_openEdit.action?resUri=openedit&thisXmNumber='+_xmNumber,
	});
}

function excute(){
	$("#formXm").submit();
}

function findPage(_url){
	_url += "&resUri=list"
	$("#formXm").attr("action",_url);
	$("#formXm").submit();
}

/* 提示信息 */
function msg(msg){
if(msg!=null&msg!=""){
layer.msg(msg, {icon: 6});
}
}

	</script>
	<br>

	
	<form id="formXm" action="<%=path %>/biz/Xm_findExample.action?resUri=list" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right">任务类型：</td>
        		<td>
        			<select name="xm.type" class="dfinput4" >
        				<option value="">--请选择--</option>
        				<option value="01">类型一</option>
        				<option value="02">类型一</option>
        				<option value="04">类型一</option>
        				<option value="03">类型一</option>
        			</select>
        		</td>
        		<td align="right">任务名称：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xm.name" id="xm.name" value="${xm.name }">
        			<input type="hidden" name="Xm.budgetType" id="Xm.budgetType" value="${thisBudgetType }">
        			<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">
        		</td>
      		</tr>
      		<tr>
        		<td align="right">是否已经完成：</td>
        		<td colspan="5">
        			<input type="radio" name="xm.status" value="00" <s:if test="%{xm.status==0}">checked="checked"</s:if>/> 未完成
					<input type="radio" name="xm.status" value="01" <s:if test="%{xm.status==1}">checked="checked"</s:if>/> 已完成
        		</td>
      		</tr>
    	</table>
  </form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="openAdd();"><span><img src="<%=path%>/images/t01.png" /></span>发布任务</li>
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<%-- <ul class="toolbar1">
        	<li><a href="<%=path %>/biz/Xm_findViewXm.action?resUri=findViewXm&thisBudgetType=${thisBudgetType}""><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>任务信息统计</strong></font></a></li>
       </ul> --%>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="15%">任务名称</th>
              <th width="7%">任务类型</th>
              <th width="7%">发布人</th>
              <th width="7%">发布时间</th>
              <th width="7%">截止时间</th>
              <th width="6%">完成状态</th>
              <th width="15%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${name}</td>
                <td align="center">
                 	${type}
				</td>
                <td align="center">
                	<c:forEach items="${requestScope.users}" var="users">
	                		<c:if test="${fn:contains(username,users.jobNumber)}">${users.helpName}</c:if>
	                </c:forEach>
                </td>
				<td align="center"><fmt:formatDate value="${publishTime}" pattern="yyyy-MM-dd"/></td>
                <td align="center"><fmt:formatDate value="${deadLine}" pattern="yyyy-MM-dd"/></td>
                <td align="center">
                	<s:if test="%{status==0}">未完成</s:if>
                	<s:if test="%{status==1}">已完成</s:if>
                </td>
                <td align="center">
                	<a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/Xm_openDetail.action?resUri=detail&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="red"><strong>任务详情</strong></font></a>|
                  	<%-- <a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${xmNumber}');"><strong>修改</strong></a>| --%>
					<a id="delXm" href="<%=path%>/biz/Xm_delete.action?xm.xmNumber=${xmNumber}&resUri=list" onClick="javascript:if(confirm('删除该任务后，任务对应的所有信息以及相关文件都将被删除且无法恢复，确定要删除吗？')){return true;}return false;"><strong>删除</strong></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>