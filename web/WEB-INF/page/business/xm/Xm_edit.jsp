<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>项目库维护</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>

</head>
<s:iterator value="#request['xm']" id="xm">
<body onload="radio('${type}','${receiver}','${receiverUnit}')">
 <div class="formbody" >	
<form id="form1" action="<%=path%>/biz/Xm_Edit.action?resUri=list&thisXmNumber=${xmNumber}" target="rightFrame" method="post">
<span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+修改项目基本信息</strong>
<strong style="color:red">(温馨提示：项目一旦修改之前的项目将会被重置)</strong></span>
<table width="100%" border="1" class="formTable">
  <tr >
    <td align="right" width="10%">项目名称:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xm.name" id="name" value="${name}"/>
    </td>
  </tr>
  <tr>
    <td align="right">项目类型:</td>
    <td>
    	<input type="radio" name="xm.type" id="01" value="1"/>类型一
		<input type="radio" name="xm.type" id="02" value="2"/>类型二
		<input type="radio" name="xm.type" id="03" value="3"/>类型三
		<input type="radio" name="xm.type" id="04" value="4"/>类型四
		<input type="radio" name="xm.type" id="05" value="5"/>其他
    </td>
  </tr>
   
   <tr>
   	<td align="right">完成截止时间:</td>
    <td>
    	<input type="text" class="dfinput4 Wdate required" name="xm.deadLine" id="deadLine" onFocus="WdatePicker()" value="${deadLine}" />
    </td>
   </tr>
   
  <tr>
  	<td align="right">项目要求说明:</td>
  	<td>
  		<textarea name="xm.requires" cols="100" id="requires" rows="7" >${requires}</textarea>
  	</td>
  	
  </tr>
  </table>
  </s:iterator>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+任务指派</strong></span>
  
    <div class="radio-inline" style="margin-left: 40px;margin-top: 30px">
      <label for="killOrder1"><strong>指派给教研室</strong></label>
	  <input type="radio" onclick="shift(0)" name="receiverStatus" id="receiveunit" value="0"/>
	</div>
	<table>
	<li id="unitList" style="display: none" >
    	<ul style="margin-left:80px;margin-top:10px;">
	    	<s:iterator value="unitList" id="id">
	    		<li style="display: inline;">
		    		<label for="${userId}" style="width:150px;">
		    			<input onclick="return false;" name="unitIdList" id="${unitId}" type="checkbox" class="dfinput1" value="${unitId}" />${unitName}
		    		</label>
	    		</li>&nbsp;&nbsp;&nbsp;&nbsp;
	    	</s:iterator>
    	</ul>
    </li>
	</table>
	
	<div class="radio-inline" style="margin-left: 40px;margin-top: 30px">
      <label for="killOrder1"><strong>指派给教职工</strong></label>
	  <input type="radio" onclick="shift(1)" name="receiverStatus" id="receivepeople" value="1"/>
	</div>
	<table>
	<li id="userList" style="display: none" >
    	<ul style="margin-left:80px;margin-top:10px;">
	    	<s:iterator value="userList" id="id">
	    		<li style="display: inline;">
		    		<label for="${userId}" style="width:150px;">
		    			<input onclick="return false;" name="userIdList" id="${helpUserName}" type="checkbox" class="dfinput1" value="${helpUserName}" />${helpName}
		    		</label>
	    		</li>&nbsp;&nbsp;&nbsp;&nbsp;
	    	</s:iterator>
    	</ul>
    </li>
	</table>
    </br>
    
    <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+需上传材料</strong></span>  
    <span style="display:block; margin-bottom:5px; margin-top:5px; margin-left:80px;">请输入需上传材料个数(上限未10个)
    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input size="5" maxlength="2" name="xm.fileCount" type="text" id="fileCount" value="${xm.fileCount }"/></span>
    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input type="button" onclick="makeFile()" value="重新选择"/></span>
     <label style="display:block; margin-top:5px; margin-left: 80px;"></label>
     <input style="display:block; margin-top:5px; margin-left: 80px;" type="text" name="xm.fileName" id="fileName" value="${xm.fileName}"/>
    <label id="tip" style="color:red">温馨提示：如果多个文件,文件名以" , "隔开如"XXX.doc,XXX.doc"</label>
    </span>
    <div id="file">
    	<div id="fileC">
    	
    	</div>
    </div>
    
    </br>
    </br>
  <div align="center" style="margin-top:5px;"><input type="submit" id="save" name="add_btn" class="scbtn"   value="保存"/></div>
</form>

<script>
	
	//生成文件名输入框
	function makeFile(){
		//清除原有框
		$("#fileC").remove();
		$("#fileName").remove();
		$("#tip").remove();
		//加入div
		$("#file").append("<div id='fileC'></div>");
		var fileCount = $("#fileCount").val();
		if(fileCount != null && !isNaN(fileCount) && fileCount <= 10){
			$("#fileC").append('<label id="nameLabel" style="margin-top:5px; margin-left: 80px;">请分别输入文件名称</label>');
            for(var i = 0; i < fileCount; i++){
            	
            	$("#nameLabel").append('<label style="display:block; margin-top:5px; margin-left: 80px;"></label><input style="display:block; margin-top:5px; margin-left: 80px;" type="text" name="xm.fileName" id="file' + i + '" value=""/><br/>');
            }
		}else{
			alert("输入有误，请重新输入")
		}
	}



	//转换点击指派单位/个人时复选框状态
    function shift(temp){
    	//点击指派教研室
    	if(temp == 0){
    		//显示教研室列表
    		$("#unitList").show();
    		//隐藏教职工列表
    		$("#userList").hide();
    		
    	    //获取复选框中所有教研室
            $("input[name='unitIdList']").each(function(){
                //移除点击失效事件
                $(this).removeAttr('onclick');
            });
            
            //获取复选框中所有教职工
            $("input[name='userIdList']").each(function(){
                //添加点击失效事件
                $(this).attr('onclick', 'return false;');
            });
    	}
    	
    	//点击指派教职工
    	if(temp ==1){
    		//显示教职工列表
    		$("#userList").show();
    		//隐藏教研室列表
    		$("#unitList").hide();
    		//获取复选框中所有教职工
            $("input[name='userIdList']").each(function(){
                //移除点击失效事件
                $(this).removeAttr('onclick');
            });
            
            //获取复选框中所有教研室
            $("input[name='unitIdList']").each(function(){
                //添加点击失效事件
                $(this).attr('onclick', 'return false;');
            });
    	}
    	
    	
    }
    
    //表单校验
	var form = document.forms[0],
    submit = $("#save")
    form.onsubmit = function(){
        if($("#name").val() == ""){
            alert("项目名不能为空！");
            return false;
        }else if($("#type").val() == ""){
            alert("请选择项目类型！");
            return false;
        }else if(inputBtn[2].value == ""){
            alert("请您再次输入密码");
            return false;
        }else if(inputBtn[1].value != inputBtn[2].value){
            alert("两次密码输入不匹配，请更正！");
            return false;
        }   
		         
	}
  
  
  
  //选择框自动选择
function radio(type,receiver,unitList){
$("#0"+type).attr("checked","checked");
if(receiver!=null&&receiver!=""){
var receivers=receiver.split(',');
for(i=0;i<receivers.length-1;i++){
	$("#"+receivers[i]).attr("checked","checked");}
	$("#receivepeople").click();
	}
		else{
		$("#receiveunit").click();
		var unitLists=unitList.split(',');
		for(i=0;i<unitLists.length-1;i++){
		$("#"+unitLists[i]).attr("checked","checked");}
		    }
}

</script>
	

</div>
</body>
