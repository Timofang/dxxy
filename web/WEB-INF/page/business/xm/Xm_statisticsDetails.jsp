<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="8%">任务接收</th>
              <th width="8%">任务名称</th>
              <th width="20%">任务内容</th>
              <th width="4%">完成标识</th>
              <th width="8%">下发人</th>
              <th width="8%">难度系数</th>
              <th width="8%">完成质量</th>
           
            </tr>
          </thead>
          <tbody>
          <s:iterator id="p2" value="userPageResult.data" status="pp">
              <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
        <!--   院下发任务 -->
                <!-- 2任务接收 -->
                <td align="center">
					${receiver}
				</td>
				<!-- 3任务名称-->
                 <td align="center">
                 	${name}
				</td>
				<!-- 4-任务内容-->
                <td align="center">
                ${requires}
                </td>
                <!-- 5完成标识-->
				<td align="center">
				<div style="color:red">
					<s:if test="%{status == 0}">
						未完成
					</s:if>
				 </div>
				<s:if test="%{status == 1}">
						已完成
				</s:if>
				</td>
				<!-- 6下发人-->
				<td align="center">
				${Lowerrunner}
				</td>
				<!-- 7难度系数-->
				<td align="center">
					${degree}
				</td>
					<!-- 8完成质量-->
                <td align="center">
                	${quality}
                </td>
              </tr>
      <!--    教研室下发任务 -->
         </s:iterator>
          </tbody>
        </table>
     ${footer}
        
</body>
</html>