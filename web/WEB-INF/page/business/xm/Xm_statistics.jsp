<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>我的任务列表</li>
</ul>
</div>
	<script type="text/javascript">

    function openDetail(helpUserName){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'任务统计详情',
    	content: '<%=path%>/biz/Xm_statisticsDetails.action?resUri=statisticsDetails&thisUserName='+helpUserName
	});
}
	</script>
	<br>
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="15%">姓名</th>
              <th width="10%">个人任务总数</th>
              <th width="10%">完成带权任务总数</th>
              <th width="10%">未完成带权任务总数</th>
              <th width="10%">完成比例</th>
              <th width="12%">操作</th>
            </tr>
          </thead>
          <tbody>
          	<s:iterator id="p2" value="userPageResult.data" status="pp">
              <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${helpName}</td>
                <td align="center">${allCount}</td>
                <td align="center">${finishCount}</td>
                <td align="center">${unfinishedCount}</td>
                <td align="center">${proportion}</td>
                <td align="center">
                	<a href="javascript:void 0" onclick="openDetail('${helpUserName}')"><font color="red">详情<strong>
                	</font></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer}
        
</body>
</html>