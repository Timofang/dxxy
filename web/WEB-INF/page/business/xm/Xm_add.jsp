<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>任务库维护</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
	.label{display: inline-block;}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>

</head>
<body>
 <div class="formbody">

<form id="form1" action="<%=path%>/biz/Xm_add.action?resUri=${thisResUri}" target="rightFrame" method="post" enctype="multipart/form-data">
<span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+发布任务基本信息</strong></span>
<table width="100%" border="1" class="formTable">
  <tr>
    <td align="right" width="10%">任务名称:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xm.name" id="name"/>
    </td>
  </tr>
  <tr>
    <td align="right">任务类型:</td>
    <!-- 遍历所有项目类型 -->
    <td>
    <c:forEach items="${typeList}" var="xmType">
    	<input type="radio" name="xm.type" id="type" value="${xmType.typeName}"/>${xmType.typeName}
    </c:forEach>
    </td>
  </tr>
  
  <tr>
    <td align="right">难度系数:</td>
    <td>
    	<select name="xm.degree">
    		<option value="0.1">0.1</option>
    		<option value="0.2">0.2</option>
    		<option value="0.3">0.3</option>
    		<option value="0.4">0.4</option>
    		<option value="0.5">0.5</option>
    		<option value="0.6">0.6</option>
    		<option value="0.7">0.7</option>
    		<option value="0.8">0.8</option>
    		<option value="0.9">0.9</option>
    		<option value="1.0">1.0</option>
    	</select>
    </td>
  </tr>
   
   <tr>
   	<td align="right">完成截止时间:</td>
    <td>
    	<input type="text" class="dfinput4 Wdate required" name="xm.deadLine" id="deadLine" onFocus="WdatePicker()"  />
    </td>
   </tr>
   
  <tr>
  	<td align="right">任务要求说明:</td>
  	<td>
  		<textarea name="xm.requires" cols="100" id="requires" rows="7"></textarea>
  	</td>
  	
  </tr>
  </table>
  
  <table  width="100%" border="1" class="formTable">
  <span style="display:block; margin-bottom:5px; margin-top:20px;"><strong>+任务附件</strong></span>
	<tr>
		<td>
			<div class="radio-inline" style="margin-left: 40px;" id="fileDiv1" >
			  <input id="file1" type="file" onclick="delFile()" name="myfile"/>&nbsp;&nbsp;&nbsp;&nbsp;
			  <input class="scbtn" type="button" onclick="addFile()" value="继续添加" />
			</div>
   		</td>
    </tr>
	</table> <br />
  
  
  <table  width="100%" border="1" class="formTable">
  <span style="display:block; margin-bottom:5px; margin-top:20px;"><strong>+任务指派</strong></span>
  
  <tr>
  	<td style="border-bottom: none;">
  		<div class="radio-inline" style="margin-left: 40px; margin-top: 10px">
	      <label for="killOrder1"><strong>指派给教研室</strong></label>
		  <input type="radio" onclick="shift(0)" checked="checked" name="receiverStatus" value="0" style="margin-right: 50px" />
		  
	      <label for="killOrder1"><strong>指派给教职工</strong></label>
		  <input type="radio" onclick="shift(1)" name="receiverStatus" value="1"/>
		</div>
    </td>
  </tr>
  
  <tr>
  	<td>
	  	<li id="unitList">
	    	<ul style="margin-left:80px;margin-top:10px;">
		    	<s:iterator value="unitList" id="id">
		    		<li style="display: inline;">
			    		<label for="${userId}" style="width:130px; vertical-align: middle;">
			    			<input name="unitIdList" id="${unitId}" type="checkbox" class="dfinput1" value="${unitId}" />
			    			<lable>${unitName}</lable>
			    		</label>
		    		</li>&nbsp;&nbsp;&nbsp;&nbsp;
		    	</s:iterator>
	    	</ul>
		</li>
		
			<li id="userList" style="display: none">
		    	<ul style="margin-left:80px;margin-top:10px;">
			    	<s:iterator value="userList" id="id">
			    		<li style="display: inline;">
				    		<label for="${userId}" style="width:130px;vertical-align: middle;" class="label">
				    			<input onclick="return false;" name="userIdList" id="${userId}" type="checkbox" class="dfinput1" value="${helpUserName}" />${helpName}
				    		</label>
			    		</li>&nbsp;&nbsp;&nbsp;&nbsp;
			    	</s:iterator>
		    	</ul>
		    </li>
   		</td>
    </tr>
    
	</table> <br />
   
	<span style="display:block; margin-bottom:5px; margin-top:20px;"><strong>+任务需上传材料</</strong></span> 
	 <table  width="100%" border="1" class="formTable">
    	<tr >
    		<td>
			    <span style=" margin-bottom:5px; margin-top:5px; margin-left:80px; font-size: 14px;">请输入该任务需要上传的文件个数：(上限为10个)
			    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input class="dfinput4 required" size="5" maxlength="2" name="xm.fileCount" value="${xm.fileCount}" type="text" id="fileCount"/></span>
			    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input class="scbtn" type="button" onclick="makeFile()" value="确定"/></span>
			    </span>
			    <div id="file">
			    	<div id="fileC">
			    	
			    	</div>
			    </div>
	    	</td>
	    </tr>
    </table>
    </br>
    </br>
  <div align="center" style="margin-top:5px;"><input type="submit" id="save" name="add_btn" class="scbtn" value="保存"/></div>
</form>

<script>
	
	var i = 1;
	function addFile(){
		 i++;
		$("#fileDiv1").append("<div id="+i+"><tr><td><input id=file"+i+" type='file' onclick='' name='myfile'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class='scbtn' type='button' onclick='delFile("+i+")' value='删除' /></td></tr></div>");
	}
	
	function delFile(id){
		$("#"+id+"").remove();
	}
	
	//生成文件名输入框
	function makeFile(){
		//清除原有框
		$("#fileC").remove();
		//加入div
		$("#file").append("<div id='fileC'></div>");
		var fileCount = $("#fileCount").val();
		if(fileCount != null && !isNaN(fileCount) && fileCount <= 10 && fileCount != ""){
			$("#fileC").append('<label id="nameLabel" style="margin-top:5px; margin-left: 80px; font-size: 14px;">请分别输入文件名称：</label>');
            for(var i = 0; i < fileCount; i++){
            	var j = i+1;
            	$("#nameLabel").append('<div style="margin-left: 80px;"><label style="display:block; margin-top:5px; margin-left: 80px; font-size: 14px;"></label>'+j+'：  <input class="dfinput4 required" style=" margin-top:5px; " type="text" name="xm.fileName" id="file' + i + '" value=""/></div><br/>');
            }
		}else{
			alert("输入有误，请重新输入!")
		}
	}



	//转换点击指派单位/个人时复选框状态
    function shift(temp){
    	//点击指派教研室
    	if(temp == 0){
    		//显示教研室列表
    		$("#unitList").show();
    		//隐藏教职工列表
    		$("#userList").hide();
    		
    	    //获取复选框中所有教研室
            $("input[name='unitIdList']").each(function(){
                //移除点击失效事件
                $(this).removeAttr('onclick');
            });
            
            //获取复选框中所有教职工
            $("input[name='userIdList']").each(function(){
                //添加点击失效事件
                $(this).attr('onclick', 'return false;');
            });
    	}
    	
    	//点击指派教职工
    	if(temp ==1){
    		//显示教职工列表
    		$("#userList").show();
    		//隐藏教研室列表
    		$("#unitList").hide();
    		//获取复选框中所有教职工
            $("input[name='userIdList']").each(function(){
                //移除点击失效事件
                $(this).removeAttr('onclick');
            });
            
            //获取复选框中所有教研室
            $("input[name='unitIdList']").each(function(){
                //添加点击失效事件
                $(this).attr('onclick', 'return false;');
            });
    	}
    	
    	
    }
    
    //表单校验
	var form = document.forms[0],
    submit = $("#save")
    form.onsubmit = function(){
        if($("#name").val() == ""){
            alert("任务名不能为空！");
            return false;
        }else if($("#type").val() == ""){
            alert("请选择任务类型！");
            return false;
        }else if(inputBtn[2].value == ""){
            alert("请您再次输入密码");
            return false;
        }else if(inputBtn[1].value != inputBtn[2].value){
            alert("两次密码输入不匹配，请更正！");
            return false;
        }   
		         
	}
    

</script>
	

</div>
</body>
</html>