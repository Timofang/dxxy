<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>项目实施-验收</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
		</style>
	
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>
        <link href="<%=path%>/third/layer-v2.4/layer/skin/layer.css" rel="stylesheet" type="text/css">

<style type="text/css">

    .image1{ 
        margin-top: 25px; 
        width:50px; 
        height:50px; 
        border-radius:50px; 
    }

    </style>
    
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
		<script src="<%=path %>/js/baiduMap_util/GeoUtils.js" type="text/javascript"></script>
		<script src="<%=path %>/js/baiduMap_util/AreaRestriction.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer-v2.4/layer/layer.js"></script>

	</head>

	<body>
		<div class="formBody">
        	<span style="font-weight:bold;margin-bottom:5px;display:block">+征地基本情况</span>
				<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
					<tr>
    					<td><strong>项目名称：${xmLandproj.name }</strong>
    						<input type="hidden" name="coordinate" id="coordinate" value="${xmLandproj.coordinate }">
    						<input type="hidden" name="coordinatePly" id="coordinatePly" value="${xmLandproj.coordinatePly }">
    						<input type="hidden" name="laName" id="laName" value="${xmLandproj.name }">
    						<input type="hidden" name="laYear" id="laYear" value="${xmLandproj.laYear }">
    					</td>
	    				<td>建设年份：${xmLandproj.laYear }</td>
	    				<td>报告周期：${xmLandproj.cycle }天</td>
	    				<td>批文情况：${xmLandproj.approval }</td>
  					</tr>
  					<tr>
  						<td>大额投资委通过情况：${xmLandproj.largeInvest }</td>
  						<td>项目业主：${xmLandproj.owner }</td>
    					<td colspan="2">项目建设地址：${xmLandproj.address }</td>
  					</tr>
  					<tr>
  						<td colspan="6">征地类型：
							<c:if test='${fn:contains(xmLandproj.landType,"01") }'>分片区；</c:if>
                			<c:if test='${fn:contains(xmLandproj.landType,"02") }'>片区外重点；</c:if>
                			<c:if test='${fn:contains(xmLandproj.landType,"03") }'>片区外统筹；</c:if>
                			<c:if test='${fn:contains(xmLandproj.landType,"04") }'>征拆大会战；</c:if>
						</td>
  					</tr>
				</table>
				<span style="font-weight:bold;margin-bottom:5px;display:block">+土地征收（亩）</span>
				<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
					<tr>
	    				<td width="33%">项目总任务：${xmLandproj.landTotal }</td>
	    				<td width="33%">剩余待完成任务：${xmLandproj.landSurplus }</td>
	    				<td width="33%">本年任务：${xmLandproj.landCurrent }</td>
  					</tr>
				</table>
				<span style="font-weight:bold;margin-bottom:5px;display:block">+房屋征收（m2）</span>
				<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
					<tr>
	    				<td width="33%">项目总任务：${xmLandproj.houseTotal }</td>
	    				<td width="33%">剩余待完成任务：${xmLandproj.houseSurplus }</td>
	    				<td width="33%">本年任务：${xmLandproj.houseCurrent }</td>
  					</tr>
				</table>
				<span style="font-weight:bold;margin-bottom:5px;display:block">+坟山迁移（座）</span>
				<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
					<tr>
	    				<td width="33%">项目总任务：${xmLandproj.mountainTotal }</td>
	    				<td width="33%">剩余待完成任务：${xmLandproj.mountainSurplus }</td>
	    				<td width="33%">本年任务：${xmLandproj.mountainCurrent }</td>
  					</tr>
				</table>
				
			<div class="itab" style="margin-top:5px;">
				<ul>
					<li><a href="<%=path%>/biz/XmSchedule_findScheduleByNumInLandproj.action?resUri=findScheduleByNumInLandproj&thisLandNumber=${xmLandproj.landNumber}" class="selected">工作进度/存在问题</a></li>
                	<li><a href="<%=path%>/biz/XmTask_findTaskByNum.action?resUri=findTaskByNum&thisLandNumber=${xmLandproj.landNumber}">征地完成进度</a></li>
                	<li><a href="<%=path%>/biz/XmDuty_findDutyByNumInLandproj.action?resUri=findDutyByNumInLandproj&thisLandNumber=${xmLandproj.landNumber}">责任单位/责任人</a></li>
				</ul>
			</div>
            <div style="clear:both; margin-top:5px;">
				<table class="tablelist" id="editTable" width="100%" style="margin-top:2px;">
					<tr>
              			<th>序号</th>
              			<th>上传人</th>
              			<th>类型</th>
              			<th>内容</th>
              			<th>证明图片</th>
              			<th>批示</th>
            		</tr>
					<s:iterator id="p2" value="resultViewXmSchedule.data" status="pp">
          				<tr>
          					<td height="21" align="center"><s:property value="#pp.count"/></td>
          					<td width="8%" align="center">
          						<img src="<%=path %>${portrait }" class="image1"><br>${userName }
          					</td>
          					<td width="8%">
          						<s:if test="%{type=='01'}"><font color="black"><strong>工作进度：</strong></font></s:if>
    							<s:if test="%{type=='02'}"><font color="black"><strong>存在问题：</strong></font></s:if>
          					</td>
          					<td>
          						${content }
          					</td>
          					<td width="250px">
          						<div id="layer-photos-demo-<s:property value="#pp.count"/>" class="layer-photos-demo" onmousedown="openPIC(<s:property value="#pp.count"/>);">
    							<s:iterator id="p3" value="listXmSchedulepic" status="pp3">
    								<img layer-src="<%=path%>${picLink }" layer-pid="<%=path%>${picLink }" src="<%=path%>${picLink }" width="80px"  />
    							</s:iterator>
    							</div>
          					</td>
          					<td>
    							<s:iterator id="p3" value="listXmReply" status="pp4">
    								<strong>${reUserName }</strong>：${content }（${createTime }）
    							</s:iterator>
          					</td>
          				</tr>
          			</s:iterator>
        		</table>
        		${footer }
          </div>
          <div id="allmap" style="overflow:hidden;zoom:1;position:relative;">	
				<div id="map" style="height:20%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
			</div>
			<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map.js"></script>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map_navigationControl.js"></script>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map_PanoramaControl.js"></script>
       </div>
	<script type="text/javascript">
	$(function(){
	
		var _coordinate = document.getElementById("coordinate").value;
		var _coordinatePly = document.getElementById("coordinatePly").value;
		var _laName = document.getElementById("laName").value;
		var _laYear = document.getElementById("laYear").value;
		
		//显示锚点
		var pc = _coordinate.split(",");
		var point = new BMap.Point(pc[0],pc[1]);
		
		map.centerAndZoom(point, 17);  // 初始化地图,设置中心点坐标和地图级别
			 		
		var label = new BMap.Label(_laName+"（"+_laYear+"年）",{offset:new BMap.Size(20,-10)});//添加信息提示框
					
		var marker = new BMap.Marker(point);
		map.addOverlay(marker);
		marker.setLabel(label);
		
		//显示面
		var p = [];//线的数组
		var ply1;
		var zb2=_coordinatePly.split("_");
		//面的数组
		for(var j=0;j<zb2.length;j++){
			var zb3 = zb2[j].split(",");
			p.push(new BMap.Point(zb3[0], zb3[1]));
		}
		ply1 = new BMap.Polygon(p, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
		map.addOverlay(ply1);  //添加覆盖物
		ply1.setFillOpacity(0.3);
		ply1.setFillColor('green');
	});
</script>
<script type="text/javascript">
function openPIC(_pp){
	//调用示例
	layer.photos({
	photos: '#layer-photos-demo-'+_pp,
	anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
	}); 
}
</script>
	</body>
</html>