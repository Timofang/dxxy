<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>项目实施-验收</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
		</style>
	
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>
        <link href="<%=path%>/third/layer-v2.4/layer/skin/layer.css" rel="stylesheet" type="text/css">

<style type="text/css">

    .image1{ 
        width:50px; 
        height:50px; 
        border-radius:50px; 
    }

    </style>
    
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
		<script src="<%=path %>/js/baiduMap_util/GeoUtils.js" type="text/javascript"></script>
		<script src="<%=path %>/js/baiduMap_util/AreaRestriction.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer-v2.4/layer/layer.js"></script>

	</head>

	<body>
		<div class="formBody">
        	<span style="font-weight:bold;margin-bottom:5px;display:block">+项目基本情况</span>
				<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
					<tr>
    					<td><strong>项目名称：${viewXmInfo.name }</strong>
    						<input type="hidden" name="coordinate" id="coordinate" value="${viewXmInfo.coordinate }">
    						<input type="hidden" name="coordinatePly" id="coordinatePly" value="${viewXmInfo.coordinatePly }">
    						<input type="hidden" name="xmName" id="xmName" value="${viewXmInfo.name }">
    						<input type="hidden" name="xmYear" id="xmYear" value="${viewXmInfo.xmYear }">
    					</td>
	    				<td>建设年份：${viewXmInfo.xmYear }</td>
	    				<td>项目层次：
	    					<c:if test='${fn:contains(viewXmInfo.budgetType,"01") }'>中央预算；</c:if>
	    					<c:if test='${fn:contains(viewXmInfo.budgetType,"02") }'>自治区层面；</c:if>
	    					<c:if test='${fn:contains(viewXmInfo.budgetType,"03") }'>市级层面；</c:if>
	    					<c:if test='${fn:contains(viewXmInfo.budgetType,"04") }'>项目建设丰收年；</c:if>
	    					<c:if test='${fn:contains(viewXmInfo.budgetType,"05") }'>现代服务业重点项目；</c:if>
	    				</td>
	    				<td>项目性质：
	    					<s:if test="%{viewXmInfo.projectType=='01'}">新开工项目</s:if>
	    					<s:if test="%{viewXmInfo.projectType=='02'}">续建项目</s:if>
	    					<s:if test="%{viewXmInfo.projectType=='03'}">重大前期工作</s:if>
	    					<s:if test="%{viewXmInfo.projectType=='04'}">竣工或部分竣工</s:if>
	    				</td>
  					</tr>
  					<tr>
  						<td>起止年限：${viewXmInfo.period }</td>
  						<td>行业（所属）：${viewXmInfo.industry }</td>
  						<td>项目业主：${viewXmInfo.owner }</td>
    					<td>所在县市区：${viewXmInfo.county }</td>
  					</tr>
  					<tr>
  						<td colspan="4">建设内容及规模：${viewXmInfo.scale }</td>
  					</tr>
  					<tr>
    					<td>是否已经开工：
    						<s:if test="%{viewXmInfo.isState=='00'}"><font color="red">未开工</font></s:if>
    						<s:if test="%{viewXmInfo.isState=='01'}"><font color="green">已开工</font></s:if>
    					</td>
    					<td>是否已经竣工：
    						<s:if test="%{viewXmInfo.isCompleted=='00'}"><font color="red">未竣工</font></s:if>
    						<s:if test="%{viewXmInfo.isCompleted=='01'}"><font color="green">已竣工</font></s:if>
    					</td>
    					<td>计划开工时间：<s:date name="viewXmInfo.startTime" format="yyyy-MM-dd" /></td>
    					<td>计划开工时间：<s:date name="viewXmInfo.completionTime" format="yyyy-MM-dd" /></td>
  					</tr>
  					<tr>
    					<td colspan="4">项目建设地址：${viewXmInfo.address }</td>
  					</tr>
				</table>
			<span style="font-weight:bold; margin-bottom:5px; display:block">+项目投资情况</span>
                <table width="100%" class="formTable">
					<tr>
    					<td><strong>总投资：${viewXmInfo.investTotal }</strong></td>
    					<td>资金来源：${viewXmInfo.investSource }</td>
  					</tr>
  					<tr>
  						<td>当年计划投资：${viewXmInfo.investPlan }</td>
    					<td>去年已完成投资：${viewXmInfo.investComplete }</td>
					</tr>
				</table>
				
			<div class="itab" style="margin-top:5px;">
				<ul>
					<li><a href="<%=path%>/biz/XmSchedule_findScheduleByNum.action?resUri=findScheduleByNum&thisXmNumber=${viewXmInfo.xmNumber}" class="selected">工作进度/存在问题</a></li>
                	<li><a href="<%=path%>/biz/XmInvest_findInvestByNum.action?resUri=findInvestByNum&thisXmNumber=${viewXmInfo.xmNumber}">资金投资进度</a></li>
                	<li><a href="<%=path%>/biz/XmDuty_findDutyByNum.action?resUri=findDutyByNum&thisXmNumber=${viewXmInfo.xmNumber}">责任单位/责任人</a></li>
				</ul>
			</div>
            <div style="clear:both; margin-top:5px;">
				<table class="tablelist" id="editTable" width="100%" style="margin-top:2px;">
					<tr>
              			<th>序号</th>
              			<th>上传人</th>
              			<th>类型</th>
              			<th>内容</th>
              			<th>证明图片</th>
              			<th>批示</th>
            		</tr>
					<s:iterator id="p2" value="resultViewXmSchedule.data" status="pp">
          				<tr>
          					<td height="21" align="center"><s:property value="#pp.count"/></td>
          					<td width="8%" align="center">
          						<img src="<%=path %>${portrait }" class="image1"><br>${userName }
          					</td>
          					<td width="8%">
          						<s:if test="%{type=='01'}"><font color="black"><strong>工作进度：</strong></font></s:if>
    							<s:if test="%{type=='02'}"><font color="black"><strong>存在问题：</strong></font></s:if>
          					</td>
          					<td>
          						${content }
          					</td>
          					<td width="250px">
          						<div id="layer-photos-demo-<s:property value="#pp.count"/>" class="layer-photos-demo" onmousedown="openPIC(<s:property value="#pp.count"/>);">
    							<s:iterator id="p3" value="listXmSchedulepic" status="pp3">
    								<img layer-src="<%=path%>${picLink }" layer-pid="<%=path%>${picLink }" src="<%=path%>${picLink }" width="80px"  />
    							</s:iterator>
    							</div>
          					</td>
          					<td>
    							<s:iterator id="p3" value="listXmReply" status="pp4">
    								<strong>${reUserName }</strong>：${content }（${createTime }）
    							</s:iterator>
          					</td>
          				</tr>
          			</s:iterator>
        		</table>
        		${footer }
          </div>
          <div id="allmap" style="overflow:hidden;zoom:1;position:relative;">	
				<div id="map" style="height:20%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
			</div>
			<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map.js"></script>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map_navigationControl.js"></script>
			<script type="text/javascript" src="<%=path %>/js/baiduMap/map_PanoramaControl.js"></script>
       </div>
	<script type="text/javascript">
	$(function(){
	
		var _coordinate = document.getElementById("coordinate").value;
		var _coordinatePly = document.getElementById("coordinatePly").value;
		var _xmName = document.getElementById("xmName").value;
		var _xmYear = document.getElementById("xmYear").value;
		
		//显示锚点
		var pc = _coordinate.split(",");
		var point = new BMap.Point(pc[0],pc[1]);
		
		map.centerAndZoom(point, 17);  // 初始化地图,设置中心点坐标和地图级别
			 		
		var label = new BMap.Label(_xmName+"（"+_xmYear+"年）",{offset:new BMap.Size(20,-10)});//添加信息提示框
					
		var marker = new BMap.Marker(point);
		map.addOverlay(marker);
		marker.setLabel(label);
		
		//显示面
		var p = [];//线的数组
		var ply1;
		var zb2=_coordinatePly.split("_");
		//面的数组
		for(var j=0;j<zb2.length;j++){
			var zb3 = zb2[j].split(",");
			p.push(new BMap.Point(zb3[0], zb3[1]));
		}
		ply1 = new BMap.Polygon(p, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
		map.addOverlay(ply1);  //添加覆盖物
		ply1.setFillOpacity(0.3);
		ply1.setFillColor('green');
	});
</script>
<script type="text/javascript">
function openPIC(_pp){
	//调用示例
	layer.photos({
	photos: '#layer-photos-demo-'+_pp,
	anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
	}); 
}
</script>
	</body>
</html>