<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>员工个人任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>员工个人任务（列表）</li>
</ul>
</div>
<script type="text/javascript">
//查看项目任务+上传文件
   function openViewXm(xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'查看项目任务',
    	content: '<%=path%>/biz/Xm_openViewUserTask.action?xmNumber='+xmNumber
	});
}
</script>
	<br>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="15%">项目名称</th>
              <th width="10%">项目编号</th>
              <th width="7%">发布教研室</th> 
              <th width="7%">截止时间</th>
              <th width="7%">完成状态</th> 
              <th width="15%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="XmChildPageResult.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmName}</td>
                <td align="center" class="td-overflow" title="${xmNumber}" width="20%">${xmNumber}</td>
                <td align="center">${unitParent}</td>
				<td align="center">截止时间（待填）</td>
                <td align="center">
                    <s:if test="%{status==0}">未完成</s:if>
                	<s:if test="%{status==1}">已完成</s:if>                
                </td>
                <td align="center">
                	<a href="javascript:void(0)" style="font-weight:bold;" onclick="openViewXm('${xmNumber}');"><strong>查看</strong></a>|
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer}
</body>
</html>