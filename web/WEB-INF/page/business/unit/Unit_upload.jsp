<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>上传文件</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body, html, #allmap {
	width: 100%;
	height: 85%;
	margin: 0;
	font-family: "微软雅黑";
}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css" />

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.js"></script>

		<script type="text/javascript">

			/**
			 * @author soldier（黄结）
			 * layerweb弹层组件
			 */
			$(function(){
				var flag="${isClose}";
				if(flag!='' && flag!=undefined){
					$('#submit').attr('disabled','disabled');
					layer.msg('修改成功', {//样式
						icon:1
					});
					/* 此处用setTimeout演示ajax的回调*/
					setTimeout(function(){
						parent.location.reload();/*刷新父页面*/
						layer.close(index);/*关闭当前layer弹出层*/
					},1000);
				}
			});
		</script>
</head>
<body>
<form action="<%=path%>/biz/Materials_submitMaterials.action?view=upload&resUri=fileUpload" method="post" enctype="multipart/form-data">
<strong><span style="display:block; margin-bottom:5px; margin-top:5px; font-size: 15px;">+任务基本信息</span></strong>
	<table width="100%" border="1" class="formTable">
		<tr>
			<!-- 取项目主表的文件信息 -->
			<c:if test="${xm != null}">
				<c:if test="${xm.name !=null}">
					<td align="right" width="15%" style="font-size: 15px;">任务名称:</td>
					<td style="font-size: 15px;" >${xm.name}</td>
					<input type="hidden" name="xmId" value="${xm.xmNumber}" />
				</c:if>
			</c:if>
			<!-- 取项目子表的文件信息 -->
			<c:if test="${xmChild != null}">
				<c:if test="${xmChild.xmChildName !=null}">
					<td align="right" width="15%" style="font-size: 15px;">任务名称:</td>
					<td style="font-size: 15px;" >${xmChild.xmChildName}</td>
					<input type="hidden" name="xcId" value="${xmChild.xmChildNumber}" />
					<input type="hidden" name="materialsId" value="${xmChild.materialsId}" />
					<input type="hidden" name="userName" value="${xmChild.userName}" />
				</c:if>
			</c:if>
		</tr>
	</table><br />
	<strong><span style="display:block; margin-bottom:5px; margin-top:5px; font-size: 15px;">+任务需要上传的文件</span></strong>
	
  		<s:iterator value="#request['fileName']" var="fileName" status="status">
			<table width="100%" border="1" class="formTable">
				<tr>
					<td align="right" width="15%" style="font-size: 15px;">
						<s:property value="#fileName" />
						<input type="hidden" name="myfileFileName" value="#fileName"/>
					</td>
					<td>
						<s:file name="myfile" label="请选择文件"></s:file>
					</td>
				</tr>
 	 		</table>
 		</s:iterator>
  			<div align="center" style="margin-top: 20px">
				<s:submit value="上传" name="add_btn" cssClass="scbtn"></s:submit>
			</div>
	</form>
</body>
</html>