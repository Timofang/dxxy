<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>任务实施</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
		</style>
	
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
		<script src="<%=path %>/js/baiduMap_util/GeoUtils.js" type="text/javascript"></script>
		<script src="<%=path %>/js/baiduMap_util/AreaRestriction.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

		<script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

		<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
        <script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
       
	</head>
	
	<body>
		<div class="formBody">
        	<span style="font-weight:bold;margin-bottom:5px;display:block">+任务基本情况</span>
			<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
				<tr>
   					<td><strong>任务名称：${xmChild.xmChildName }</strong></td>
    				<td style="border-right:none;">任务类型： 
						无类型
    				</td>
    				<td style="border-left:none;border-right:none;"></td>
 					</tr>
 					<tr>
 						<td>发布时间：<fmt:formatDate value="${xmChild.xmChild.publishTime}" pattern="yyyy-MM-dd"/></td>
 						<td>完成截止时间：<fmt:formatDate value="${xmChild.xmChild.deadLine}" pattern="yyyy-MM-dd"/></td>
 						<td>发布人：${xmChild.helpName }</td>
 					</tr>
 					<tr>
   					<td style="border-right:none;">是否已经完成：
   						<s:if test="%{xmChild.status==0}"><font color="red">未完成</font></s:if>
   						<s:if test="%{xmChild.status==1}"><font color="green">已完成</font></s:if>
   					</td>
   					<td style="border-left:none;border-right:none;"></td>
					<td style="border-left:none;"></td>
 					</tr>
			</table><br />
			
			<span style="font-weight:bold;margin-bottom:5px;display:block">+材料要求及指派情况</span>
			<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
				<tr>
   					<td style="border-right:none;">需上交材料数量：${xmChild.xmChild.fileCount }</td>
    				<td style="border-left:none;border-right:none;"></td>
					<td style="border-left:none;"></td>
 				</tr>
				<tr>
					<td style="border-right:none;">材料名称：${xmChild.xmChild.fileName} </td>
					<td style="border-left:none;border-right:none;"></td>
					<td style="border-left:none;"></td>
				</tr>
				<tr>
					<td style="border-right:none;">指派到个人：
						<c:forEach items="${xmChild.xmChild.userList }" var="user">
							${user.helpName}
						</c:forEach>
					</td>
					<td style="border-left:none;border-right:none;"></td>
					<td style="border-left:none;"></td>
				</tr>
				
			</table><br />
			
			<span style="font-weight:bold;margin-bottom:5px;display:block">+任务要求</span>
			<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
				<tr>
   					<td><textarea readonly="readonly" name="xm.requires" cols="180" id="requires" rows="7">${xmChild.xmChild.requires}</textarea> </td>
 				</tr>
			</table><br />
			
			<!-- 教研室任务批示 -->
			<c:forEach items="${instructionsList}" var="instructions" varStatus="vs"> 
				<c:if test="${(fn:length(instructionsList)) > 1}">
					<span style="font-weight:bold;margin-bottom:5px;display:block">+任务批示${vs.count}</span>
				</c:if>
				<c:if test="${(fn:length(instructionsList)) == 1}">
					<span style="font-weight:bold;margin-bottom:5px;display:block">+任务批示</span>
				</c:if>
				<table width="100%" cellspacing="0" cellpadding="0" class="formTable">
					<tr>
	   					<td>
	   						<textarea id="instructions.instructionsContent" name="instructions.instructionsContent" readonly="readonly" cols="180" rows="7" >${instructions.instructionsContent}</textarea>
						</td>
	 				</tr>
				</table><br />
			</c:forEach>		
			
			<span style="font-weight:bold;margin-:5px;display:block">+任务附件</span>
			<table width="100%" cellspacing="0" cellpadding="0" class="formTable">
			<c:forEach items="${attachmentList}" var="attachment">
				<tr>
					<td><a href="<%=path%>/biz/Attachment_attachmentDownload.action?downloadFilePath=${attachment.path}&downloadFileName=${attachment.fileName}&resUri=download">${attachment.fileName}</a></td>
				</tr>
			</c:forEach>
			<c:if test="${attachmentList == null}">
					<tr><td>无</td></tr>
			</c:if>
			</table><br />
			
		<span style="font-weight:bold;margin-bottom:5px;display:block">+材料完成情况</span>
			<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
				<c:forEach items="${xmChild.xmChild.userList}" var="user">
                          <tr>
                              <td>${user.helpName}</td>
                               <c:if test="${xmChild.helpName = thisUserHelpName}">
	                              <c:if test="${xmChild.quality == null }">
	                              	<td>
		                          		完成质量：
		                              	<select id="${user.helpUserName }" name="${user.helpUserName }">
								    		<option value="A+">A+</option>
								    		<option value="A">A</option>
								    		<option value="A-">A-</option>
								    		<option value="B+">B+</option>
								    		<option value="B">B</option>
								    		<option value="B-">B-</option>
								    		<option value="C+">C+</option>
								    		<option value="C">C</option>
								    		<option value="C-">C-</option>
								    		<option value="D">D</option>
								    	</select>
		                             	<input type="button" onclick="degree('${user.helpUserName}','${user.xmChild.xmChildNumber}')" style="color: #378888" value="提交"/>
	                            	</td>
	                              </c:if>
                              </c:if>
		                      <c:if test="${xmChild.quality != null }">
		                      	<td>完成质量：${xmChild.quality}</td>
		                      </c:if>
		                      <c:if test="${xmChild.helpName != thisUserHelpName && xmChild.quality == null }">
		                      	<td>完成质量：待评分</td>
		                      </c:if>
                              <!-- 判断材料是否提交 -->
                              <c:if test="${empty user.materialsList[0].path}">
		                              	<td><font color="red">未完成</font></td>
		                      </c:if>
		                      <c:if test="${not empty user.materialsList[0].path}">
		                             <c:forEach items="${user.materialsList}" var="m">
			                             <c:if test="${not empty m.path}">
			                              	<td align="center">
	                              				<%--<a href="${m.path}">${m.filename}</a>--%>
												 <a href="<%=path%>/biz/Materials_downloadMaterials.action?downloadFilePath=${m.path}&downloadFileName=${m.fileName}&resUri=download">${m.fileName}</a>
			                            	</td>
			                             </c:if>
                              		 </c:forEach>
		                      </c:if>
                              
                          </tr>
				</c:forEach>
			</table><br />
       	 </div>
       
       
	<script type="text/javaript">

</script>
	</body>
</html>