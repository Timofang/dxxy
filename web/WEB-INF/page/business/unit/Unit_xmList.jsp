<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>我的任务列表</li>
</ul>
</div>
	<script type="text/javascript">
		
	// 任务下发
	function xmAdd(xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增任务信息填写',
    	content: '<%=path%>/biz/Xm_getXmByXmNumber.action?resUri=unitXmAdd&thisResUri=list&thisXmNumber='+xmNumber
	});
}
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增任务信息填写',
    	content: '<%=path%>/biz/User_findUserByUnit.action?resUri=unitXmAdd&thisResUri=list'
	});
}

	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'任务信息修改',
    	content: '<%=path%>/biz/Xm_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

function excute(){
	$("#formXm").submit();
}

function findPage(_url){
	_url += "&resUri=list"
	$("#formXm").attr("action",_url);
	$("#formXm").submit();
}

    function openUpload(xmNumber,userName){	
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'文件上传',
    	content: '<%=path%>/biz/Xm_fileUpload.action?resUri=openUpload&thisResUri=list&thisXmNumber= +xmNumber&thisUserName='+userName
	});
}
	</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/Xm_openPage.action?resUri=list&thisBudgetType=01" target="rightFrame" <s:if test="%{thisBudgetType=='01'}">class="selected"</s:if>>任务类型一</a></li>
			<li><a href="<%=path %>/biz/Xm_openPage.action?resUri=list&thisBudgetType=02" target="rightFrame" <s:if test="%{thisBudgetType=='02'}">class="selected"</s:if>>任务类型二</a></li>
			<li><a href="<%=path %>/biz/Xm_openPage.action?resUri=list&thisBudgetType=03" target="rightFrame" <s:if test="%{thisBudgetType=='03'}">class="selected"</s:if>>任务类型三</a></li>
			<li><a href="<%=path %>/biz/Xm_openPage.action?resUri=list&thisBudgetType=04" target="rightFrame" <s:if test="%{thisBudgetType=='04'}">class="selected"</s:if>>任务类型四</a></li>
			<li><a href="<%=path %>/biz/Xm_openPage.action?resUri=list&thisBudgetType=05" target="rightFrame" <s:if test="%{thisBudgetType=='05'}">class="selected"</s:if>>其他</a></li>
		</ul>
	</div>
	
	<form id="formXm" action="<%=path %>/biz/Xm_findExample.action?resUri=unitXmList" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right">任务类型：</td>
        		<td>
        			<select name="xm.type" class="dfinput4" >
        				<option value="">--请选择--</option>
        				<option value="01">类型一</option>
        				<option value="02">类型一</option>
        				<option value="04">类型一</option>
        				<option value="03">类型一</option>
        			</select>
        		</td>
        		<td align="right">任务名称：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="Xm.name" id="Xm.name" value="${Xm.name }">
        			<input type="hidden" name="Xm.budgetType" id="Xm.budgetType" value="${thisBudgetType }">
        			<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">
        		</td>
      		</tr>
      		<tr>
        		<td align="right">是否已经完成：</td>
        		<td colspan="5">
        			<input type="radio" name="xm.status" value="00" <s:if test="%{xm.status==0}">checked="checked"</s:if>/> 未完成
					<input type="radio" name="xm.status" value="01" <s:if test="%{xm.status==1}">checked="checked"</s:if>/> 已完成
        		</td>
      		</tr>
    	</table>
  </form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<%-- <ul class="toolbar1">
        	<li><a href="<%=path %>/biz/Xm_findViewXm.action?resUri=findViewXm&thisBudgetType=${thisBudgetType}""><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>任务信息统计</strong></font></a></li>
       </ul> --%>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <!-- <th width="10%">任务编号</th> -->
              <th width="10%">任务名称</th>
              <th width="6%">任务类型</th>
              <th width="6%">发布人</th>
              <th width="6%">发布时间</th>
              <th width="6%">截止时间</th>
              <th width="10%">院领导要求</th>
              <!-- <th width="10%">教研室要求</th> -->
              <th width="10%">文件名</th>
              <th width="6%">文件数</th>
              <th width="10%">接收单位/个人</th>
              <th width="6%">下发状态</th>
              <th width="6%">完成状态</th>
              <th width="10%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${xmNumber}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmName}</td>
                 <td align="center">
					<c:if test='${fn:contains(type, 0) }'>类型一</c:if>
                	<c:if test='${fn:contains(type, 1) }'>类型二</c:if>
                	<c:if test='${fn:contains(type, 2) }'>类型三</c:if>
                	<c:if test='${fn:contains(type, 3) }'>类型四</c:if>
                	<c:if test='${fn:contains(type, 4) }'>其他</c:if>
				</td>
                <td align="center">${username}</td>
				<td align="center"><fmt:formatDate value="${publishTime}" pattern="yyyy-MM-dd"/></td>
				<td align="center"><fmt:formatDate value="${deadLine}" pattern="yyyy-MM-dd"/></td>
				<td align="center">${requires}</td>
                <td align="center">${fileName}</td>
                <td align="center">${fileCount}</td>
                <td align="center">${receiver}</td>
                <td align="center">
                	未下发
               	 	<s:if test="%{issue==0}">未下发</s:if>
                	<s:if test="%{issue==1}">已下发</s:if>
                </td>
                <td align="center">
                	<s:if test="%{status==0}">未完成</s:if>
                	<s:if test="%{status==1}">已完成</s:if>
                </td>
                <td align="center">
                

                	<a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/Xm_openDetail.action?resUri=unitXmDetail&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="red"><strong>详情</strong></font></a>
                	<a href="javascript:void(0)" id="${xmNumber}" style="font-weight:bold;" onclick="xmAdd('${xmNumber}');"><span style="color: green"><strong>下发</strong></span></a>
                	<a href="javascript:void(0)" onclick="openUpload('${xmNumber}','${userName}');" style="font-weight:bold;"><strong>上传</strong></a>

                		<!-- 院领导发任务到教研室 -->
                	<c:if test="${unitId == userinfo.unitId}">
	                    <c:if test="${userinfo.leaderTypeId==2}">
		                  	| <a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${xmNumber}');"><strong>修改</strong></a> |
							<a  href="<%=path%>/biz/Xm_delete.action?resUri=unitXmDelete&xmNumber=${id}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><strong>删除</strong></a> |
							<a href="javascript:void(0)" onclick="openUpload('${xmNumber}','${fileName}','${name}');" style="font-weight:bold;"><strong>上传</strong></a>
					</c:if>
					</c:if> 
					
					<!-- 院领导发任务到个人 -->
					<c:if test="${unitId == null && userName == userinfo.helpUserName}">
	                    <c:if test="${userinfo.leaderTypeId==1}">
		                  	| <a href="javascript:void(0)" onclick="openUpload('${xmNumber}','${fileName}','${name}');" style="font-weight:bold;"><strong>上传</strong></a>
						</c:if>
					</c:if>
					
					<!-- 教研室发任务到个人 -->
					<c:if test="${unitParent == userinfo.unitId}">
		                  	| <a href="javascript:void(0)" onclick="openUpload('${xmNumber}','${fileName}','${name}');" style="font-weight:bold;"><strong>上传</strong></a>
					</c:if> 
					
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
        
</body>
</html>