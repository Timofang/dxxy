<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

    <script type="text/javascript">


        /**
         * @author soldier（黄结）
         * layerweb弹层组件
         */
        $(function(){
            var flag="${thisState}";
            if(flag!='' && flag!=undefined){
                $('#submit').attr('disabled','disabled');
                layer.msg('修改成功', {//样式
                    icon:1
                });
                /* 此处用setTimeout演示ajax的回调*/
                setTimeout(function(){
                    parent.location.reload();/*刷新父页面*/
                    layer.close(index);/*关闭当前layer弹出层*/
                },1000);
            }
        });

    </script>

    <script type="text/javascript">
        function saveRec(){
            var _areaDistrictID = $("#areaDistrictID").val();
            var _areaTownshipID = $("#areaTownshipID").val();
            var _areaVillageID = $("#areaVillageID").val();
            var _thisHelpUserType = $("input[name='thisHelpUserType']:checked").val();
            var _unitId = $("#model.unitId").val();
            var _unitName = $("#model.unitName").val();
            var _unitPerson = $("#model.unitPerson").val();

            if(_unitId==""){
                alert("教研室代码必填!!");
                $("#unitId").focus();
                return false;
            }
            if(_unitName==""){
                alert("教研室名称必填!!");
                $("#unitName").focus();
                return false;
            }
            $("#unitFrom").submit();
        }
    </script>

</head>

<body>
<div class="main">
    <form action="<%=path %>/biz/Unit_edit.action?view=edit&resUri=edit" method="post"  name="form" id="unitFrom" enctype="multipart/form-data">
         <input class="dfinput" type="hidden" id="model.unitId" name="model.unitId" value="${model.unitId}">
        <ul>
            <li><strong>教研室信息：</strong></li>
            <li>
                <table width="100%" border="10" cellspacing="0" cellpadding="0" class="formTable">
                    <tr>
                      <%--   <td>教研室编号：</td>
                        <td >
                            <input class="dfinput" type="text" id="model.unitId" name="model.unitId" value="${model.unitId}">
                        </td> --%>
                        <td>名称：</td>
                        <td>
                            <input class="dfinput" type="text" id="model.unitName" name="model.unitName" value="${model.unitName}"/>
                        </td>
                        <td>
                        	负责人：
                            <select class="dfinput" name="model.unitPerson" id="unitPerson">
                                <c:forEach items="${requestScope.usersList}" var="key">
                                    <%--负责人用工号代替--%>
                                    <%--<option <c:if test="${model.unitPerson==key.jobNumber }">selected="selected"</c:if> value="${key.jobNumber}">${key.helpName}</option>--%>

                                    <%--直接是用户的用户名--%>
                                    <option <c:if test="${model.unitPerson==key.helpUserName }">selected="selected"</c:if> value="${key.helpUserName}">${key.helpName}</option>
                                </c:forEach>
                            </select>
                            <%-- 负责人不可改，出现bug看 https://blog.csdn.net/wangqing84411433/article/details/81026411--%>
                            <%--<input class="dfinput" type="hidden" id="model.unitPerson" name="model.unitPerson" value="${model.unitPerson}"/>--%>
                            <%--<c:forEach items="${requestScope.usersList}" var="userKey_1">--%>
                                  <%--<c:if test="${fn:contains(model.unitPerson,userKey_1.jobNumber)}">--%>
                                      <%--<input type="text" readonly="readonly"/>${userKey_1.helpName}--%>
                                  <%--</c:if>--%>
                            <%--</c:forEach>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>成员：</td>
                        <td colspan="6">
                            <c:forEach items="${requestScope.usersList}" var="userKey_2">
                                <input type="checkbox" name="jobNumber_1" value="${userKey_2.jobNumber}"
                                       <c:if test="${fn:contains(userKey_2.unitId,model.unitId)}">checked="checked"</c:if>
                                        <%--教研室负责人不能取消勾选--%>
                                       <c:if test="${model.unitPerson==userKey_2.helpUserName}">disabled="disabled"</c:if>
                                >${userKey_2.helpName} &nbsp;&nbsp;
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </li>
            <li>
                <div style="margin:10px 50%;">
                    <input name="" id="submit1" type="button" class="scbtn" value="保存记录" onClick="saveRec();"/>
                </div>
            </li>
        </ul>
    </form>
</div>
</body>
</html>