<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>添加教研室</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#unitFrom").validate({
                errorClass:"errorInfo", //默认为错误的样式类为：error
                errorElement:"em",
                focusInvalid: false, //当为false时，验证无效时，没有焦点响应
                onkeyup: false,
                submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                    form.submit();   //提交表单
                },
                rules:{
                    "model.unitId":{
                        required:true,
                        remote: {
                            type: "post",   //数据发送方式
                            url: "<%=path %>/biz/Unit_checkUnitId.action",
                            data: {
                                "model.unitId": function() {
                                    return $("#unitId").val();
                                }
                            },
                            dataType: "html",
                            dataFilter: function(data, type) {
                                if (data == "true"){
                                    return true;
                                }else{
                                    return false;
                                }
                            }
                        },
                        rangelength:[1,100]
                    },
                    "model.unitName":{
                        required:true,
                    },
                    "model.unitPerson":{
                        required:true
                    }
                },
                messages:{
                    "model.unitId":{
                        required:"请输入教研室编号",
                        remote:"该教研室编号已经存在"
                    },
                    "model.unitName":{
                        required:"请输入教研室名称",
                        remote:"该教研室名称已经存在"
                    }
                }
            });
        });


        /**
         * @author soldier（黄结）
         * layerweb弹层组件
         */
        $(function(){
            var flag_1="${model.unitId}";
            var flag_2="${model.unitName}";
            if(flag_1!='' && flag_1!=undefined) {
                if (flag_2 != '' && flag_2 != undefined) {
                    $('#submit').attr('disabled', 'disabled');
                    layer.msg('添加成功', {//样式
                        icon:1
                    });
                    /* 此处用setTimeout演示ajax的回调*/
                    setTimeout(function () {
                        parent.location.reload();/*刷新父页面*/
                        layer.close(index);/*关闭当前layer弹出层*/
                    }, 1000);
                }
            }

        });
    </script>

</head>
<body>
<div class="main">
    <form action="<%=path %>/biz/Unit_add.action?view=add&resUri=add" method="post"  name="form" id="unitFrom" enctype="multipart/form-data">
        <ul>
            <li><strong><font color="red">教研室信息：</font></strong></li>
            <li>
                <table width="100%" border="10" cellspacing="0" cellpadding="0" class="formTable">
                    <tr>
                        <td>名称：</td>
                        <td>
                            <input class="dfinput" type="text" id="model.unitName" name="model.unitName"/>
                        </td>
                        <td>负责人：</td>
                        <td>
                            <select class="dfinput" id="model.unitPerson" name="model.unitPerson">
                                <c:forEach items="${requestScope.usersLists}" var="key_1">
                                    <%--负责人用工号代替--%>
                                    <%--<option value="${key_1.jobNumber}">${key_1.helpName}</option>--%>

                                    <%--直接是用户的用户名--%>
                                    <option value="${key_1.helpUserName}">${key_1.helpName}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>请选择成员：</td>
                        <td colspan="6">
                            <c:forEach items="${requestScope.usersList}" var="key_2">
                                <input type="checkbox" name="jobNumber_1" id="jobNumber_1" value="${key_2.jobNumber}"
                                       <%--<c:if test="${fn:contains(model.unitId,key_2.unitId)}">checked="checked"</c:if>--%>>${key_2.helpName} &nbsp;&nbsp;
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            <li>
                <div style="margin:10px 50%;">
                    <input name="add_btn" id="save" type="submit" class="scbtn" value="保存记录"/>
                </div>
            </li>
        </ul>
    </form>
</div>
</body>
</html>