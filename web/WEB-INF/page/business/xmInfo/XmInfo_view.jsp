<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>项目管理（列表）</li>
</ul>
</div>
	<script type="text/javascript">
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增项目立项信息填写',
    	content: '<%=path%>/biz/XmInfo_openAdd.action?resUri=openAdd&thisResUri=list'
	});
}
	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'立项信息修改',
    	content: '<%=path%>/biz/XmInfo_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

	function excute(){
		var _url;
		_url="<%=path%>/biz/HelpRecord_downDetail.action";
		$("#helpRecForm").attr("action", _url);
		$("#helpRecForm").submit();
	}
	</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=01" target="rightFrame" <s:if test="%{thisBudgetType=='01'}">class="selected"</s:if>>重大项目（五千万以上）</a></li>
			<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=02" target="rightFrame" <s:if test="%{thisBudgetType=='02'}">class="selected"</s:if>>城镇建设年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=03" target="rightFrame" <s:if test="%{thisBudgetType=='03'}">class="selected"</s:if>>工业振兴年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=04" target="rightFrame" <s:if test="%{thisBudgetType=='04'}">class="selected"</s:if>>服务业提速年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=05" target="rightFrame" <s:if test="%{thisBudgetType=='05'}">class="selected"</s:if>>其他</a></li>
		</ul>
	</div>
	<div class="tools" style="margin-top:5px;">
  		<ul class="toolbar">
			<li><a href="<%=path%>/biz/XmInfo_downViewXmInfo.action?thisBudgetType=${thisBudgetType}"><span><img src="<%=path%>/images/t06.png" /></span><strong>导出数据</strong></a></li>
		</ul>
  		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=${thisBudgetType}"><span><img src="<%=path%>/images/ico04.png" /></span><font color="blue"><strong>项目管理列表</strong></font></a></li>
       </ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="2%">序号</th>
              <th>建设年份</th>
              <th>所在县市区</th>
              <th width="6%">项目名称</th>
              <th>行业（所属）</th>
              <th>项目层次</th>
              <th>项目性质</th>
              <th width="6%">建设内容及规模</th>
              <th>起止年限</th>
              <th width="6%">项目业主</th>
              <th width="6%">项目地址</th>
              <th>总投资（万元）</th>
              <th width="6%">资金来源</th>
              <th>当年计划投资（万元）</th>
              <th>去年已完成投资（万元）</th>
              <th>本年投资总金额（万元）</th>
              <th>投资完成率（%）</th>
              <th>计划开工时间</th>
              <th>计划竣工时间</th>
              <th>是否已经开工</th>
              <th>是否已经竣工</th>
              <th>报告周期（天）</th>
              <th>是否超期</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="resultViewXmInfo.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmYear}</td>
                <td align="center">${county}</td>
                <td align="center" class="td-overflow" title="${name}" width="10%">${name}</td>
                <td align="center">${industry}</td>
                <td align="center">
					<c:if test='${fn:contains(budgetType,"01") }'>重大项目（五千万以上）；</c:if>
                	<c:if test='${fn:contains(budgetType,"02") }'>城镇建设年；</c:if>
                	<c:if test='${fn:contains(budgetType,"03") }'>工业振兴年；</c:if>
                	<c:if test='${fn:contains(budgetType,"04") }'>服务业提速年；</c:if>
                	<c:if test='${fn:contains(budgetType,"05") }'>其他；</c:if>
				</td>
                <td align="center">
                	<s:if test="%{projectType=='01'}">新开工项目</s:if>
                	<s:if test="%{projectType=='02'}">续建项目</s:if>
                	<s:if test="%{projectType=='03'}">重大前期工作</s:if>
                	<s:if test="%{projectType=='04'}">竣工或部分竣工</s:if>
                </td>
                <td align="center" class="td-overflow" title="${scale}" width="10%">${scale}</td>
                <td align="center">${period}</td>
                <td align="center" class="td-overflow" title="${owner}" width="10%">${owner}</td>
                <td align="center" class="td-overflow" title="${address}" width="10%">${address}</td>
                <td align="center">${investTotal}</td>
                <td align="center" class="td-overflow" title="${investSource}" width="6%">${investSource}</td>
                <td align="center">${investPlan}</td>
                <td align="center">${investComplete}</td>
                <td align="center">${sInvestment}</td>
                <td align="center">${cRate}</td>
                <td align="center">${startTime }</td>
                <td align="center">${completionTime }</td>
                <td align="center">
                	<s:if test="%{isState=='00'}">未开工</s:if>
                	<s:if test="%{isState=='01'}">已开工</s:if>
                </td>
                <td align="center">
                	<s:if test="%{isCompleted=='00'}">未竣工</s:if>
                	<s:if test="%{isCompleted=='01'}">已竣工</s:if>
                </td>
                <td align="center">${cycle}</td>
                <td align="center">
					<s:if test="%{isOverdue==0}">未超期</s:if>
                	<s:if test="%{isOverdue==1}">超期</s:if>
				</td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>


