<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>项目管理（列表）</li>
</ul>
</div>
	<script type="text/javascript">
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增项目立项信息填写',
    	content: '<%=path%>/biz/XmInfo_openAdd.action?resUri=openAdd&thisResUri=list'
	});
}
	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'立项信息修改',
    	content: '<%=path%>/biz/XmInfo_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

function excute(){
	$("#formXmInfo").submit();
}

function findPage(_url){
	_url += "&resUri=list"
	$("#formXmInfo").attr("action",_url);
	$("#formXmInfo").submit();
}
	</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=01" target="rightFrame" <s:if test="%{thisBudgetType=='01'}">class="selected"</s:if>>重大项目（五千万以上）</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=02" target="rightFrame" <s:if test="%{thisBudgetType=='02'}">class="selected"</s:if>>城镇建设年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=03" target="rightFrame" <s:if test="%{thisBudgetType=='03'}">class="selected"</s:if>>工业振兴年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=04" target="rightFrame" <s:if test="%{thisBudgetType=='04'}">class="selected"</s:if>>服务业提速年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType=05" target="rightFrame" <s:if test="%{thisBudgetType=='05'}">class="selected"</s:if>>其他</a></li>
		</ul>
		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmInfoMap&thisBudgetType=${thisBudgetType}""><span><img height="30px" src="<%=path%>/images/du002.jpg" /></span>切换（地理信息）</a></li>
       </ul>
	</div>
	
	<form  id="formXmInfo" action="<%=path %>/biz/XmInfo_findExample.action?resUri=list" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right">项目性质：</td>
        		<td>
        			<select name="xmInfo.projectType" class="dfinput4" >
        				<option value="" <s:if test="%{xmInfo.projectType==''}">selected="selected"</s:if>>--请选择--</option>
        				<option value="01" <s:if test="%{xmInfo.projectType=='01'}">selected="selected"</s:if>>新开工项目</option>
        				<option value="02" <s:if test="%{xmInfo.projectType=='02'}">selected="selected"</s:if>>续建项目</option>
        				<option value="04" <s:if test="%{xmInfo.projectType=='04'}">selected="selected"</s:if>>竣工或部分竣工</option>
        				<option value="03" <s:if test="%{xmInfo.projectType=='03'}">selected="selected"</s:if>>重大前期工作</option>
        			</select>
        		</td>
        		<td align="right">项目名称：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xmInfo.name" id="xmInfo.name" value="${xmInfo.name }">
        			<input type="hidden" name="xmInfo.budgetType" id="xmInfo.budgetType" value="${thisBudgetType }">
        			<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">
        		</td>
        		<td align="right">建设年份：</td>
        		<td>
        			<s:select name="xmInfo.xmYear" id="xmYear" cssClass="dfinput4" list="yearList" listKey="key" listValue="value" />
        		</td>
        		<td align="right">项目地址：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xmInfo.address" id="xmInfo.address" value="${xmInfo.address }">
        		</td>
      		</tr>
      		<tr>
      			<td align="right">是否已经开工：</td>
        		<td>
        			<input type="radio" name="xmInfo.isState" value="00" <s:if test="%{xmInfo.isState=='00'}">checked="checked"</s:if>/> 未开工 
					<input type="radio" name="xmInfo.isState" value="01" <s:if test="%{xmInfo.isState=='01'}">checked="checked"</s:if>/> 已开工
        		</td>
        		<td align="right">是否已经竣工：</td>
        		<td colspan="5">
        			<input type="radio" name="xmInfo.isCompleted" value="00" <s:if test="%{xmInfo.isCompleted=='00'}">checked="checked"</s:if>/> 未竣工
					<input type="radio" name="xmInfo.isCompleted" value="01" <s:if test="%{xmInfo.isCompleted=='01'}">checked="checked"</s:if>/> 已竣工
        		</td>
      		</tr>
    	</table>
  </form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="openAdd();"><span><img src="<%=path%>/images/t01.png" /></span>项目立项</li>
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmInfo_findViewXmInfo.action?resUri=findViewXmInfo&thisBudgetType=${thisBudgetType}""><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>项目信息统计</strong></font></a></li>
       </ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="5%">建设年份</th>
              <th width="20%">项目名称</th>
              <th width="7%">行业（所属）</th>
              <th width="7%">项目层次</th>
              <th width="6%">项目性质</th>
              <th width="5%">起止年限</th>
              <th width="10%">项目业主</th>
              <th width="12%">项目地址</th>
              <th width="5%">是否已经开工</th>
              <th width="5%">是否已经竣工</th>
              <th width="3%">报告周期</th>
              <th width="15%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmYear}</td>
                <td align="center" class="td-overflow" title="${name}" width="20%">${name}</td>
                <td align="center">${industry}</td>
                <td align="center">
					<c:if test='${fn:contains(budgetType,"01") }'>重大项目（五千万以上）；</c:if>
                	<c:if test='${fn:contains(budgetType,"02") }'>城镇建设年；</c:if>
                	<c:if test='${fn:contains(budgetType,"03") }'>工业振兴年；</c:if>
                	<c:if test='${fn:contains(budgetType,"04") }'>服务业提速年；</c:if>
                	<c:if test='${fn:contains(budgetType,"05") }'>其他；</c:if>
				</td>
                <td align="center">
                	<s:if test="%{projectType=='01'}">新开工项目</s:if>
                	<s:if test="%{projectType=='02'}">续建项目</s:if>
                	<s:if test="%{projectType=='03'}">重大前期工作</s:if>
                	<s:if test="%{projectType=='04'}">竣工或部分竣工</s:if>
                </td>
                <td align="center">${period}</td>
                <td align="center" class="td-overflow" title="${owner}" width="10%">${owner}</td>
                <td align="center" class="td-overflow" title="${address}" width="12%">${address}</td>
                <td align="center">
                	<s:if test="%{isState=='00'}">未开工</s:if>
                	<s:if test="%{isState=='01'}">已开工</s:if>
                </td>
                <td align="center">
                	<s:if test="%{isCompleted=='00'}">未竣工</s:if>
                	<s:if test="%{isCompleted=='01'}">已竣工</s:if>
                </td>
                <td align="center">${cycle}天</td>
                <td align="center">
                	<a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/XmDuty_findDutyByNum.action?resUri=findDutyByNum&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="red"><strong>项目实施</strong></font></a>|
                	<a href="javascript:dialog('90%','90%','${name}责任监管','<%=path%>/biz/XmDuty_openAdds.action?resUri=openAdds&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="blue"><strong>责任监管</strong></font></a>|
                	<a href="javascript:dialog('90%','90%','${name}征地进度','<%=path%>/biz/XmInfo_addXmLandprojPage.action?resUri=addXmLandprojPage&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="blue"><strong>征地进度</strong></font></a>|
                  	<a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${xmNumber}');"><strong>修改</strong></a>|
					<a  href="<%=path%>/biz/XmInfo_delete.action?resUri=XmInfoMap&xmInfoId=${id}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><strong>删除</strong></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>