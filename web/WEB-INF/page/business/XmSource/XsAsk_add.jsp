<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>项目库维护</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css" />
		<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
		<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>
		<script type="text/javascript">
 	$(document).ready(function(e) {
	 $("#thisUser").autocomplete("<%=path%>/sysJson/userJson_likeAll.action", { 
			dataType:"json",
			type: "post",
			max: 100, //列表里的条目数 
		 	minChars: 0, //自动完成激活之前填入的最小字符 
		 	width: 350, //提示的宽度，溢出隐藏 
		 	scrollHeight: 300, //提示的高度，溢出显示滚动条 
		 	matchContains: true, //包含匹配，就是data参数里的数据示 
		 	autoFill: false, //自动填充
			extraParams: {instru_thisUnit:"123"},
			parse: function(datas) 
		        { 
		            data = datas.obj;
		            var rows = [];
		            for(var i=0; i<data.length; i++)
		         {	
		              rows[rows.length] = 
		           {                  
		                data:data[i],
		                result:data[i].itemName,
		                value:data[i].itemId
		              };
		            }           
		            return rows;
		        }, 
		 	formatMatch: function(row, i, max) { 
		    	return row.itemId ; 
		 	},  
		    formatItem: function(row, i, max) { 
			   return  row.itemName; 
		 	}
		 	}).result(function(event, row, formatted) { 
		    	var str=row.itemName;
				$("#thisPeopleId").val(row.itemId);
				
				document.getElementById('thisPeopleId').value = row.itemId;
		 	});   
 		});
 		
</script>
	</head>
	<body>
		<div class="formbody">
			<form id="form1" action="<%=path%>/biz/XmSource_addAsk.action?resUri=openAddAsk" method="post">
				<span style="display: block; margin-bottom: 5px; margin-top: 5px;"><strong>+批示内容（工作要求）</strong>
				</span>
				<table width="100%" border="1" class="formTable">
					<tr>
  						<td align="right" width="20%">输入关键字（姓名）交办领导：</td>
    					<td>
    						<input class="dfinput4 required" type="text" name="thisUser" id="thisUser" />
        					<input type="hidden" id="thisPeopleId" name="thisPeopleId" />
    					</td>
  					</tr>
  					<tr>
						<td align="right" width="20%">交办时间：</td>
						<td>
							<input type="text" class="dfinput4 Wdate required" placeHolder="请选择交办时间" style="width: 180px;" name="xmInfo.startTime" id="xmInfo.startTime" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});" />
						</td>
					</tr>
					<tr>
						<td align="right" width="20%">批示内容（工作要求）：</td>
						<td>
							<input type="hidden" id="xmInfo.xmSourceNumber" name="xmInfo.xmSourceNumber" value="${thisXsNumber }"/>
							<input type="hidden" id="thisXsNumber" name="thisXsNumber" value="${thisXsNumber }"/>
							<textarea name="xmInfo.name"  class="textinput2" id="xmInfo.name" ></textarea>
						</td>
					</tr>
				</table>
				<div align="center" style="margin-top: 5px;"><input type="submit" name="add_btn" class="scbtn" value="保存记录" /></div>
			</form>
		<table class="tablelist">
    	<thead>
    	<tr>
        	<th width="4%">序号</th>
        	<th>年份</th>
            <th>交办领导</th>
            <th>交办时间</th>
            <th width="60%">批示内容（工作要求）</th>
            <th width="8%">操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator id="p" value="listViewAsk" status="pp">
        <tr id="tr_${id}">
        	<td height="21" align="center"><s:property value="#pp.count"/></td>
            <td align="center">${xmYear}</td>
            <td align="center">${people}</td>
            <td align="center"><s:date name="startTime" format="yyyy-MM-dd" /></td>
            <td>${name }</td>
            <td align="center">
            	<a  href="<%=path%>/biz/XmSource_delAsk.action?resUri=openAddAsk&thisXmNumber=${xmNumber }&thisXsNumber=${xmSourceNumber}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
            </td>
        </tr> 
        </s:iterator>
        </tbody>
</table>
		</div>
	</body>
</html>