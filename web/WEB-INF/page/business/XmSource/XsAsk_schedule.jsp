<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>情况上报</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer-v2.4/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
		<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
	</head>
	<body>
		<div class="formbody">
			<form id="form1" action="<%=path%>/biz/XmSource_addSchedule.action?resUri=openSchedule" method="post">
				<span style="display: block; margin-bottom: 5px; margin-top: 5px;"><strong>+落实情况上报</strong>
				</span>
				<table width="100%" border="1" class="formTable">
  					<tr>
						<td align="right" width="20%">上报类型：</td>
						<td>
							<input type="radio" name="xmSchedule.type" value="01" checked="checked"/> 落实进度
							<input type="radio" name="xmSchedule.type" value="02"/> 存在的问题和困难
							<input type="radio" name="xmSchedule.type" value="03"/> 下一步工作计划
							<input type="radio" name="xmSchedule.type" value="04"/> 督查情况
						</td>
					</tr>
					<tr>
						<td align="right" width="20%">上报内容：</td>
						<td>
							<input type="hidden" id="xmSchedule.xmNumber" name="xmSchedule.xmNumber" value="${thisXmNumber }"/>
							<input type="hidden" id="thisXmNumber" name="thisXmNumber" value="${thisXmNumber }"/>
							<textarea name="xmSchedule.content" id="xmSchedule.content" class="textinput2"></textarea>
						</td>
					</tr>
				</table>
				<div align="center" style="margin-top: 5px;"><input type="submit" name="add_btn" class="scbtn" value="保存记录" /></div>
			</form>
		<table class="tablelist">
    	<thead>
    	<tr>
        	<th width="4%">序号</th>
        	<th width="11%">上报类型</th>
            <th>上报内容</th>
            <th>证明材料</th>
            <th width="8%">上报时间</th>
            <th width="5%">上报人</th>
            <th width="5%">操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator id="p" value="listViewXmSchedule" status="pp">
        <tr id="tr_${id}">
        	<td height="21" align="center"><s:property value="#pp.count"/></td>
            <td align="center">
            	<s:if test="%{type=='01'}"><font color="black"><strong>落实进度</strong></font></s:if>
    			<s:if test="%{type=='02'}"><font color="black"><strong>存在的问题和困难</strong></font></s:if>
    			<s:if test="%{type=='03'}"><font color="black"><strong>下一步工作计划</strong></font></s:if>
    			<s:if test="%{type=='04'}"><font color="black"><strong>督查情况</strong></font></s:if>
			</td>
            <td>${content}</td>
            <td width="750px">
          		<div id="layer-photos-demo-<s:property value="#pp.count"/>" class="layer-photos-demo" onmousedown="openPIC(<s:property value="#pp.count"/>);">
    				<s:iterator id="p3" value="listXmSchedulepic" status="pp3">
    					<img layer-src="<%=path%>${picLink }" layer-pid="<%=path%>${picLink }" src="<%=path%>${picLink }" width="80px"  />
    				</s:iterator>
    			</div>
          	</td>
            <td align="center">${creatTime }</td>
            <td>${userName }</td>
            <td align="center">
            	<a  href="<%=path%>/biz/XmSource_delSchedule.action?resUri=openSchedule&thisXmScheduleId=${id }&thisXmNumber=${thisXmNumber}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
            </td>
        </tr> 
        </s:iterator>
        </tbody>
</table>
		</div>
<script type="text/javascript">
function openPIC(_pp){
	//调用示例
	layer.photos({
	photos: '#layer-photos-demo-'+_pp,
	anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
	}); 
}
</script>
	</body>
</html>