<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>项目库维护</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
		<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
	</head>
	<body>
		<div class="formbody">
			<form id="form1" action="<%=path%>/biz/XmSource_add.action?resUri=list" target="rightFrame" method="post">
				<span style="display: block; margin-bottom: 5px; margin-top: 5px;"><strong>+批示来源（工作事项）</strong>
				</span>
				<table width="100%" border="1" class="formTable">
				<!-- 批示来源 -->
					<tr>
						<td align="right" width="20%">批示来源（工作事项）：</td>
						<td>
							<textarea name="xmSource.xsName"  class="textinput2" id="xsName" ></textarea>
						</td>
					</tr>
					<!-- 响应等级 -->
					<tr>
						<td align="right" width="40%">响应等级：</td>
						<td>
							<input type="radio" name="xmSource.xsLevel" value="一级" checked="checked"/> 一级
							<input type="radio" name="xmSource.xsLevel" value="二级"/> 二级
							<input type="radio" name="xmSource.xsLevel" value="三级"/> 三级
						</td>
					</tr>
					<!-- 相关产业 -->
					<tr>
						<td align="right" width="40%">相关产业：</td>
						<td>
							<input type="radio" name="xmSource.xsIndustry" value="第一产业" checked="checked"/> 第一产业
							<input type="radio" name="xmSource.xsIndustry" value="第二产业"/> 第二产业
							<input type="radio" name="xmSource.xsIndustry" value="第三产业"/> 第三产业
							<input type="radio" name="xmSource.xsIndustry" value="其他"/> 其他
						</td>
					</tr>
					<!-- 所属区域 -->
					<tr>
						<td align="right" width="20%">所属区域：</td>
						<td>
							<select class="dfinput4"  name="xmSource.xsArea" >
							<option value="">请选择...</option>
							<option value="分全区">分全区</option>
							<option value="贵城">贵城</option>
							<option value="港城">港城</option>
							<option value="根竹">根竹</option>
							<option value="大圩">大圩</option>
							<option value="庆丰">庆丰</option>
							<option value="武乐">武乐</option>
							<option value="中里">中里</option>
							<option value="奇石">奇石</option>
							</select>
						</td>
					</tr>
				</table>
				<div align="center" style="margin-top: 5px;"><input type="submit" name="add_btn" class="scbtn" value="保存" /></div>
			</form>
		</div>
	</body>
</html>