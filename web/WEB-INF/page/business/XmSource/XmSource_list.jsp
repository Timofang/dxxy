<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>督查事项（来源）</li>
</ul>
</div>
	<script type="text/javascript">
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增批示来源（工作事项）',
    	content: '<%=path%>/biz/XmSource_openAdd.action?resUri=openAdd'
	});
}
	function openEdit(_thisXmSourceId){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'修改批示来源（工作事项）',
    	content: '<%=path%>/biz/XmSource_openEdit.action?resUri=openEdit&thisXmSourceId='+_thisXmSourceId
	});
}

function excute(){
	$("#formXmSource").submit();
}

function findPage(_url){
	_url += "&resUri=list";
	$("#formXmSource").attr("action",_url);
	$("#formXmSource").submit();
}
	</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmSource_list.action?resUri=list" target="rightFrame" class="selected">批示来源（工作事项）</a></li>
			<li><a href="<%=path %>/biz/XmSource_listAsk.action?resUri=listAsk" target="rightFrame">批示内容（工作要求）</a></li>
			<li><a href="<%=path %>/biz/XmSource_findTableAsk.action?resUri=findTableAsk" target="rightFrame">落实情况汇总表</a></li>
		</ul>
	</div>
	
	<form  id="formXmSource" action="<%=path %>/biz/XmSource_list.action?resUri=list" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right" >批示来源（工作事项）：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xmSource.xsName" id="xmSource.xsName" value="${xmSource.xsName }">
        		</td>
        		
        		<!-- 所属区域 -->
					
				<td align="right" >所属区域：</td>
				<td>
					<select class="dfinput4"  name="xmSource.xsArea" >
					<option value="">请选择...</option>
					<option value="分全区">分全区</option>
					<option value="贵城">贵城</option>
					<option value="港城">港城</option>
					<option value="根竹">根竹</option>
					<option value="大圩">大圩</option>
					<option value="庆丰">庆丰</option>
					<option value="武乐">武乐</option>
					<option value="中里">中里</option>
					<option value="奇石">奇石</option>
					</select>
				</td>
      		</tr>
      		<!-- 响应等级 -->
			<tr>
				<td align="right" >响应等级：</td>
				<td>
					<input type="radio" name="xmSource.xsLevel" value="一级" <s:if test="%{xmSource.xsLevel=='一级'}">checked="checked"</s:if>/> 一级
					<input type="radio" name="xmSource.xsLevel" value="二级" <s:if test="%{xmSource.xsLevel=='二级'}">checked="checked"</s:if>/> 二级
					<input type="radio" name="xmSource.xsLevel" value="三级" <s:if test="%{xmSource.xsLevel=='三级'}">checked="checked"</s:if>/> 三级
				</td>
			<!-- 相关产业 -->
				<td align="right" >相关产业：</td>
				<td>
					<input type="radio" name="xmSource.xsIndustry" value="第一产业" <s:if test="%{xmSource.xsIndustry=='第一产业'}">checked="checked"</s:if> /> 第一产业
					<input type="radio" name="xmSource.xsIndustry" value="第二产业" <s:if test="%{xmSource.xsIndustry=='第二产业'}">checked="checked"</s:if> /> 第二产业
					<input type="radio" name="xmSource.xsIndustry" value="第三产业" <s:if test="%{xmSource.xsIndustry=='第三产业'}">checked="checked"</s:if> /> 第三产业
					<input type="radio" name="xmSource.xsIndustry" value="其他"  <s:if test="%{xmSource.xsIndustry=='其他'}">checked="checked"</s:if> /> 其他
				</td>
			</tr>
    	</table>
  	</form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="openAdd();"><span><img src="<%=path%>/images/t01.png" /></span>新建批示来源（工作事项）</li>
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <th width="10%">交办单单号</th>
              <th width="66%">批示来源（工作事项）</th>
              <th width="10%">响应等级</th>
              <th width="10%">相关产业</th>
              <th width="10%">所属区域</th>
              <th width="20%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xsNumber}</td>
                <td align="center" class="td-overflow" title="${xsName}" width="76%">${xsName}</td>
                <td align="center">${xsLevel}</td>
                <td align="center">${xsIndustry}</td>
                <td align="center">${xsArea}</td>
                <td align="center">
                	<a href="javascript:dialog('90%','90%','【${xsName}】批示内容（工作要求）','<%=path%>/biz/XmSource_openAddAsk.action?resUri=openAddAsk&thisXsNumber=${xsNumber}', 'true', '5%', '5%');" ><font color="red"><strong>批示内容</strong></font></a>|
                  	<a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${id}');"><strong>修改</strong></a>|
					<a href="<%=path%>/biz/XmSource_del.action?resUri=list&thisXmSourceId=${id}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><strong>删除</strong></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>