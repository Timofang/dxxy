<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>责任监管</title>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
		<link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

		<script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

		<script type="text/javascript">
 	$(document).ready(function(e) {
	 $("#thisUser").autocomplete("<%=path%>/sysJson/userJson_likeAll.action", { 
			dataType:"json",
			type: "post",
			max: 100, //列表里的条目数 
		 	minChars: 0, //自动完成激活之前填入的最小字符 
		 	width: 350, //提示的宽度，溢出隐藏 
		 	scrollHeight: 300, //提示的高度，溢出显示滚动条 
		 	matchContains: true, //包含匹配，就是data参数里的数据示 
		 	autoFill: false, //自动填充
			extraParams: {instru_thisUnit:"123"},
			parse: function(datas) 
		        { 
		            data = datas.obj;
		            var rows = [];
		            for(var i=0; i<data.length; i++)
		         {	
		              rows[rows.length] = 
		           {                  
		                data:data[i],
		                result:data[i].itemName,
		                value:data[i].itemId
		              };
		            }           
		            return rows;
		        }, 
		 	formatMatch: function(row, i, max) { 
		    	return row.itemId ; 
		 	},  
		    formatItem: function(row, i, max) { 
			   return  row.itemName; 
		 	}
		 	}).result(function(event, row, formatted) { 
		    	var str=row.itemName;
				$("#xmDuty.peopleId").val(row.itemId);
				$("#xmDuty.people").val(row.itemValue);
				
				document.getElementById('xmDuty.peopleId').value = row.itemId;
				document.getElementById('xmDuty.people').value = row.itemValue;
		 	});   
 		});
 		
	function saveRec(){
	 	$("#formDuty").submit();
	}
</script>
	</head>

	<body>
		<div class="main">
			<form action="<%=path%>/biz/XmSource_addDuty.action?resUri=openDuty" method="post" name="formDuty" id="formDuty">
				<ul>
					<li>
						<table width="100%" border="10" cellspacing="0" cellpadding="0"
							class="formTable">
							<tr>
								<td align="right" width="40%">责任单位：</td>
    							<td>
    								<input type="hidden"" class="dfinput4 required" name="xmDuty.xmNumber" id="xmDuty.xmNumber" value="${thisXmNumber }"/>
    								<input type="hidden"" class="dfinput4 required" name="thisXmNumber" id="thisXmNumber" value="${thisXmNumber }"/>
    								<input type="text" class="dfinput4 required" name="xmDuty.unit" id="xmDuty.unit"/>
    							</td>
  							</tr>
  							<tr>
  								<td align="right" width="40%">输入关键字（姓名）责任人：</td>
    							<td>
    								<input class="dfinput4 required" type="text" name="thisUser" id="thisUser" />
        							<input type="hidden" id="xmDuty.peopleId" name="xmDuty.peopleId" />
									<input type="hidden" name="xmDuty.people" id="xmDuty.people" />
    							</td>
  							</tr>
  							<tr>
  								<td align="right" width="40%">责任人类型：</td>
  								<td>
  									<input type="radio" name="xmDuty.type" value="08" checked="checked"/> 责任领导
  									<input type="radio" name="xmDuty.type" value="09"/> 具体跟踪人
  								</td>
  							</tr>
  							<tr>
								<td align="right" width="40%">协办单位：</td>
    							<td>
    								<input type="text" class="dfinput4 required" name="xmDuty.xbUnit" id="xmDuty.xbUnit"/>
    							</td>
  							</tr>
						</table>
					</li>
					<li>
						<div style="margin: 10px 50%;">
							<input name="" id="submit1" type="button" class="scbtn" value="保存记录" onClick="saveRec();"/>
						</div>
					</li>
				</ul>
			</form>
		</div>
	</body>
	<table class="tablelist">
    	<thead>
    	<tr>
        	<th width="4%">序号</th>
            <th>责任单位</th>
            <th>责任人</th>
            <th>责任人类型</th>
            <th>协办单位</th>
            <th>责任人联系方式</th>
            <th width="8%">操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator id="p" value="listXmDuty" status="pp">
        <tr id="tr_${id}">
        	<td height="21" align="center"><s:property value="#pp.count"/></td>
            <td align="center">${unit}</td>
            <td align="center">${people}</td>
            <td align="center">
            	<c:if test="${type=='07' }">
            		<font color="red"><strong>交办领导</strong></font>
   				</c:if>
				<c:if test="${type=='08' }">
   		     	 	 责任领导
   				</c:if>
   				<c:if test="${type=='09' }">
   		     		具体跟踪人
   				</c:if>
			</td>
			<td align="center">${xbUnit}</td>
            <td align="center">${peopleTel }</td>
            <td align="center">
            	<c:if test="${type!='07' }">
            		<a  href="<%=path%>/biz/XmSource_delDuty.action?resUri=openDuty&thisXmDutyId=${id }&thisXmNumber=${xmNumber }" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
            	</c:if>
            </td>
        </tr> 
        </s:iterator>
        </tbody>
</table>
</html>