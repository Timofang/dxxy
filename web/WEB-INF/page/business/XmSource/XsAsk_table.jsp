<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>督查事项（来源）</li>
</ul>
</div>
	<script type="text/javascript">
	function excute(){
		$("#formXmSource").submit();
	}

	function findPage(_url){
		_url += "&resUri=findTableAsk";
		$("#formXmSource").attr("action",_url);
		$("#formXmSource").submit();
	}

 	$(document).ready(function(e) {
	 $("#thisUser").autocomplete("<%=path%>/sysJson/userJson_likeAll.action", { 
			dataType:"json",
			type: "post",
			max: 100, //列表里的条目数 
		 	minChars: 0, //自动完成激活之前填入的最小字符 
		 	width: 350, //提示的宽度，溢出隐藏 
		 	scrollHeight: 300, //提示的高度，溢出显示滚动条 
		 	matchContains: true, //包含匹配，就是data参数里的数据示 
		 	autoFill: false, //自动填充
			extraParams: {instru_thisUnit:"123"},
			parse: function(datas) 
		        { 
		            data = datas.obj;
		            var rows = [];
		            for(var i=0; i<data.length; i++)
		         {	
		              rows[rows.length] = 
		           {                  
		                data:data[i],
		                result:data[i].itemName,
		                value:data[i].itemId
		              };
		            }           
		            return rows;
		        }, 
		 	formatMatch: function(row, i, max) { 
		    	return row.itemId ; 
		 	},  
		    formatItem: function(row, i, max) { 
			   return  row.itemName; 
		 	}
		 	}).result(function(event, row, formatted) { 
		    	var str=row.itemName;
				$("#viewAsk.peopleId").val(row.itemId);
				
				document.getElementById('thisUser').value = row.itemName;
				document.getElementById('viewAsk.peopleId').value = row.itemId;
		 	});   
 		});
 		
</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmSource_list.action?resUri=list" target="rightFrame">批示来源（工作事项）</a></li>
			<li><a href="<%=path %>/biz/XmSource_listAsk.action?resUri=listAsk" target="rightFrame">批示内容（工作要求）</a></li>
			<li><a href="<%=path %>/biz/XmSource_findTableAsk.action?resUri=findTableAsk" target="rightFrame" class="selected">落实情况汇总表</a></li>
		</ul>
	</div>
	
	<form  id="formXmSource" action="<%=path %>/biz/XmSource_findTableAsk.action?resUri=findTableAsk" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right" width="10%">交办单单号：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="viewAsk.xmSourceNumber" id="viewAsk.xmSourceNumber" value="${viewAsk.xmSourceNumber }">
        		</td>
      			<td align="right" width="15%">批示来源（工作事项）：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="viewAsk.xsName" id="viewAsk.xsName" value="${viewAsk.xsName }">
        		</td>
        		<td align="right" width="20%">输入关键字（姓名）交办领导：</td>
    			<td>
    				<input class="dfinput4 required" type="text" name="thisUser" id="thisUser" value="${thisUser }"/>
        			<input type="hidden" id="viewAsk.peopleId" name="viewAsk.peopleId" value="${viewAsk.peopleId }"/>
    			</td>
      		</tr>
    	</table>
  	</form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<ul class="toolbar1">
			<li><a href="<%=path%>/biz/XmSource_downViewXmInfo.action?"><span><img src="<%=path%>/images/t06.png" /></span><strong>导出数据</strong></a></li>
		</ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="2%">序号</th>
              <th width="6%">交办单单号</th>
              <th width="12%">批示来源（工作事项）</th>
              <th width="4%">交办领导</th>
              <th width="5%">交办时间</th>
              <th width="15%">批示内容（工作要求）</th>
              <th width="12%">落实进度</th>
              <th width="12%">存在的问题和困难</th>
              <th width="12%">下一步工作计划</th>
              <th width="10%">责任领导</th>
              <th width="10%">具体跟踪人</th>
              <th width="5%">操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageViewAsk.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmSourceNumber}</td>
                <td class="td-overflow" title="${xsName}" width="10%">${xsName}</td>
                <td align="center">${people}</td>
                <td align="center"><s:date name="startTime" format="yyyy-MM-dd" /></td>
                <td class="td-overflow" title="${name}" width="10%">${name}</td>
                <td class="td-overflow" title="${scheduleContent01}" width="10%">${scheduleContent01}</td>
                <td class="td-overflow" title="${scheduleContent02}" width="10%">${scheduleContent02}</td>
                <td class="td-overflow" title="${scheduleContent03}" width="10%">${scheduleContent03}</td>
                <td>${dutyPeople08}</td>
                <td>${dutyPeople09}</td>
                <td align="center">
                	<s:if test="%{isClose=='01'}">
                		<font color="green"><strong>办结</strong></font>
                	</s:if>
                	<s:else>
                		<font color="red"><strong><a  href="<%=path%>/biz/XmSource_createClose.action?resUri=findTableAsk&thisXmNumber=${xmNumber}" onClick="javascript:if(confirm('确定要设置办结吗？')){alert('设置成功！');return true;}return false;"><strong><font color="red">设置办结</font></strong></a></strong></font>
                	</s:else>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>