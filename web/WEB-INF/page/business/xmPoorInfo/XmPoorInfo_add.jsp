<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>项目库维护</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>

<script type="text/javascript">
 		
	//单击获取点击的经纬度
	map.addEventListener("click",function(e){
		alert(e.point.lng + "," + e.point.lat);
	});
	
	
</script>

</head>
<body>
 <div class="formbody">
<form id="form1" action="<%=path%>/biz/XmInfo_poorXmAdd.action?resUri=${thisResUri}" target="rightFrame" method="post">
<span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+项目立项基本信息</strong></span>
<table width="100%" border="1" class="formTable">
  <tr>
    <td align="right" width="10%">项目名称:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.name" id="xmInfo.name"/>
    </td>
    <td align="right" width="10%">建设年份:</td>
    <td>
    	<s:select name="xmInfo.xmYear" id="xmYear" cssClass="dfinput4" list="yearList" listKey="key" listValue="value"/>
    </td>
  </tr>
  <tr>
    <td align="right">项目层次:</td>
    <td>
    	<span style="font-size: 20px;color: blue;"> 扶贫项目</span>
    	<input  type="hidden" name="strBudgetType" value="07"/>
    </td> 
    <td align="right">项目性质:</td>
    <td>
    	<input type="radio" name="xmInfo.projectType" value="01" checked="checked"/> 新开工项目 
		<input type="radio" name="xmInfo.projectType" value="02"/> 续建项目
		<input type="radio" name="xmInfo.projectType" value="04"/> 竣工或部分竣工
		<input type="radio" name="xmInfo.projectType" value="03"/> 重大前期工作
    </td>
  </tr>
  <tr>
  	<td align="right" width="10%">起止年限:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.period" id="xmInfo.period"/>
    </td>
  	<td align="right">报告周期（天）:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.cycle" id="xmInfo.cycle"/>
    </td>
  </tr>
  <tr>
  	<td align="right">行业（所属）:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.industry" id="xmInfo.industry"/>
    </td>
  	<td align="right">建设内容及规模：</td>
    <td>
    	<textarea name="xmInfo.scale"  class="textinput2" id="scale" ></textarea>
    </td>
  </tr>
  <tr>
    <td align="right">项目业主:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.owner" id="xmInfo.owner"/>
    </td>
    <td align="right">所在县市区:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmInfo.county" id="xmInfo.county"/>
    </td>
  </tr>
  <tr>
    <td align="right">是否已经开工:</td>
    <td>
    	<input type="radio" name="xmInfo.isState" value="00" checked="checked"/> 未开工 
		<input type="radio" name="xmInfo.isState" value="01"/> 已开工
    </td>
    <td align="right">是否已经竣工:</td>
    <td>
    	<input type="radio" name="xmInfo.isCompleted" value="00" checked="checked"/> 未竣工
		<input type="radio" name="xmInfo.isCompleted" value="01"/> 已竣工
    </td>
  </tr>
  <tr>
    <td align="right">计划开工时间:</td>
    <td>
    	<input type="text" class="dfinput4 Wdate required" name="xmInfo.startTime" id="startTime" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'completionTime\')||\'2020-10-01\'}'})"  />
    </td>
    <td align="right">计划竣工时间:</td>
    <td>
    	<input type="text" class="dfinput4 Wdate required" name="xmInfo.completionTime" id="completionTime" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')||\'2020-10-01\'}'})"  />
    </td>
   </tr>
  </table>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+资金投资情况</strong></span>
	<table width="100%" border="1" class="formTable">
		<tr>
    		<td align="right" width="10%">总投资:</td>
    		<td>
    			<input type="text" class="dfinput4 required" name="thisInvestTotal" id="thisInvestTotal"/><font color="red">（万元）</font>
    		</td>
    		<td align="right" width="10%">资金来源:</td>
   			<td>
    			<textarea name="thisInvestSource"  class="textinput2" id="scale" ></textarea>
    		</td>
  		</tr>
  		<tr>
    		<td align="right" width="10%">当年计划投资:</td>
    		<td>
    			<input type="text" class="dfinput4 required" name="thisInvestPlan" id="thisInvestPlan"/><font color="red">（万元）</font>
    		</td>
    		<td align="right" width="10%">去年已完成投资:</td>
   			<td>
    			<input type="text" class="dfinput4 required" name="thisInvestComplete" id="thisInvestComplete"/><font color="red">（万元）</font>
    		</td>
  		</tr>
	</table>
	<span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+项目建设地点</strong></span>
	<table width="100%" border="1" class="formTable">
		<tr>
    		<td align="right" width="10%">项目建设地址：</td>
    		<td>
    			<input type="text" name="xmInfo.address" id="address" class="dfinput8 required" onblur="createGeo();" />
    			<font color="red">*写入地址后，鼠标移开，系统自动生成坐标</font>
    		</td>
    		<td align="right">地图坐标：</td>
    		<td align="left">
				<input type="text"" name="xmInfo.coordinate" id="coordinate" readonly="readonly" class="dfinput4 required" />
				<input type="hidden" name="xmInfo.coordinatePly" id="coordinatePly" class="dfinput4 required" readonly="readonly"/>
			</td>
  		</tr>
  		<tr >
    		<td align="right" height="400px">地图定位：<font color="red">*编辑完成后双击（面）保存</font></td>
    		<td colspan="3" id="allmap" style="overflow:hidden;zoom:1;position:relative;" >
				<div id="map" style="height:50%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
			</td>
  		</tr>
	</table>
  <div align="center" style="margin-top:5px;"><input type="submit" name="add_btn" class="scbtn" value="保存"/></div>
</form>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map.js"></script>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map_navigationControl.js"></script>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map_PanoramaControl.js"></script>

<script type="text/javascript">

	//单击获取点击的经纬度
	/*var geoc = new BMap.Geocoder();    
	map.addEventListener("click",function(e){
		document.getElementById("coordinate").value=e.point.lng + "," + e.point.lat;
		
		var pt = e.point;
		geoc.getLocation(pt, function(rs){
			var addComp = rs.addressComponents;
			document.getElementById("proAddress").value=addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber;
		});        
		
		//map.clearOverlays();
		var point = new BMap.Point(e.point.lng, e.point.lat);
		map.centerAndZoom(point, 15);
		var marker = new BMap.Marker(point);  // 创建标注
		map.addOverlay(marker);               // 将标注添加到地图中
	});*/
	
	function createGeo(_address){
	
		map.disableDoubleClickZoom();
		map.clearOverlays();
		var _address = document.getElementById("address").value;
		// 创建地址解析器实例
		var myGeo = new BMap.Geocoder();
		// 将地址解析结果显示在地图上,并调整地图视野
		myGeo.getPoint(_address, function(point){
			if (point) {
				map.centerAndZoom(point, 16);
				map.addOverlay(new BMap.Marker(point));
				
				document.getElementById("coordinate").value=point.lng + "," + point.lat;
				
				var polygon = new BMap.Polygon([
				new BMap.Point(point.lng,point.lat),
				new BMap.Point(Number((point.lng+0.001).toFixed(6)),point.lat),
				new BMap.Point(point.lng,Number((point.lat+0.001).toFixed(6))),
		
				], {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});  //创建多边形
				map.addOverlay(polygon);   //增加多边形
	
				polygon.enableEditing();
				polygon.setFillOpacity(0.3);
				polygon.setFillColor('green');
				polygon.enableMassClear();
		
				var thisPoint = "";
				polygon.addEventListener("dblclick", function showOverlayInfo2(e){   //添加监听事件,单击时触发
					var pa = polygon.getPath();
					var strpa = "";
					var pt;
					for(var i=0;i<pa.length;i++){
				
						if(i==pa.length-1){
							strpa += pa[i].lng+","+pa[i].lat;
						}else{
							strpa += pa[i].lng+","+pa[i].lat+"_";
						}
				
					pt = pa[i];
					
					thisPoint = pa[i].lng+","+pa[i].lat;
				}
				document.getElementById("coordinatePly").value=strpa;
				
				document.getElementById("coordinate").value=thisPoint;
			
				polygon.disableEditing();
				});
			}else{
			alert("您选择地址没有解析到结果!");
			}
		}, "梧州市蒙山县");
	}
	
	
</script>
</div>
</body>
</html>