<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>责任监管</title>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
		<link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

		<script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

		<script type="text/javascript">
 	$(document).ready(function(e) {
	 
	 $("#thisLandproj").autocomplete("<%=path%>/bizJson/XmLandprojJson_likeAll.action", { 
			dataType:"json",
			type: "post",
			max: 100, //列表里的条目数 
		 	minChars: 0, //自动完成激活之前填入的最小字符 
		 	width: 350, //提示的宽度，溢出隐藏 
		 	scrollHeight: 300, //提示的高度，溢出显示滚动条 
		 	matchContains: true, //包含匹配，就是data参数里的数据示 
		 	autoFill: false, //自动填充
			extraParams: {instru_thisUnit:"123"},
			parse: function(datas) 
		        { 
		            data = datas.obj;
		            var rows = [];
		            for(var i=0; i<data.length; i++)
		         {	
		              rows[rows.length] = 
		           {                  
		                data:data[i],
		                result:data[i].itemName,
		                value:data[i].itemId
		              };
		            }           
		            return rows;
		        }, 
		 	formatMatch: function(row, i, max) { 
		    	return row.itemId ; 
		 	},  
		    formatItem: function(row, i, max) { 
			   return  row.itemName; 
		 	}
		 	}).result(function(event, row, formatted) { 
		    	var str=row.itemName;
				$("#thisLandNumber").val(row.itemValue);
				
				document.getElementById('thisLandNumber').value = row.itemValue;
		 	});   
 		});
 		
	function saveRec(){
	 	$("#formAddXmLandproj").submit();
	}
</script>
	</head>

	<body>
		<div class="main">
			<form action="<%=path%>/biz/XmInfo_addXmLandproj.action?resUri=addXmLandprojPage" method="post" name="formAddXmLandproj" id="formAddXmLandproj">
				<ul>
					<li>
						<table width="100%" border="10" cellspacing="0" cellpadding="0"
							class="formTable">
  							<tr>
  								<td align="right" width="40%">输入关键字（名称）征地进度：</td>
    							<td>
    								<input class="dfinput4 required" type="text" name="thisLandproj" id="thisLandproj" />
        							<input type="hidden" id="thisLandNumber" name="thisLandNumber" />
									<input type="hidden" name="thisXmNumber" id="thisXmNumber" value="${thisXmNumber }"/>
    							</td>
  							</tr>
						</table>
					</li>
					<li>
						<div style="margin: 10px 50%;">
							<input name="" id="submit1" type="button" class="scbtn" value="保存记录" onClick="saveRec();"/>
						</div>
					</li>
				</ul>
			</form>
		</div>
	</body>
	<table class="tablelist">
    	<thead>
          	<tr>
          	  <th width="4%" rowspan="2">序号</th>
              <th rowspan="2">建设年份</th>
              <th rowspan="2">项目名称</th>
              <th rowspan="2">批文情况</th>
              <th colspan="3">土地征收（亩）</th>
              <th colspan="3">房屋征收（m2）</th>
              <th colspan="3">坟山迁移（座）</th>
              <th rowspan="2">大额投资委通过情况</th>
              <th rowspan="2">项目业主</th>
              <th rowspan="2">项目地址</th>
              <th rowspan="2">报告周期</th>
              <th rowspan="2">操作</th>
            </tr>
            <tr>
              
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="listViewToLandproj" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${laYear}</td>
                <td align="center">${name}</td>
				<td align="center">${approval}</td>
                <td align="center">${landTotal}</td>
                <td align="center">${landSurplus}</td>
                <td align="center">${landCurrent}</td>
                <td align="center">${houseTotal}</td>
                <td align="center">${houseSurplus}</td>
                <td align="center">${houseCurrent}</td>
                <td align="center">${mountainTotal}</td>
                <td align="center">${mountainSurplus}</td>
                <td align="center">${mountainCurrent}</td>
                <td align="center">${largeInvest}</td>
                <td align="center">${owner}</td>
                <td align="center">${address}</td>
                <td align="center">${cycle}天</td>
                <td align="center">
            		<a  href="<%=path%>/biz/XmInfo_delXmLandproj.action?resUri=addXmLandprojPage&thisToLandprojid=${toLandprojid }&thisXmNumber=${xmNumber}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
            	</td>
              </tr>
            </s:iterator>
          </tbody>
</table>
</html>