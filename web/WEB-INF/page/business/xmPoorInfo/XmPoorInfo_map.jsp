<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	body, html,#allmap{width: 100%;height: 96%;margin:0;font-family:"微软雅黑";}
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
	<script src="<%=path %>/js/baiduMap_util/GeoUtils.js" type="text/javascript"></script>
	<script src="<%=path %>/js/baiduMap_util/AreaRestriction.js" type="text/javascript"></script>

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>项目管理（地理信息）</li>
</ul>
</div>
<br>
	<div class="itab">
		<ul class="toolbar">
			<li class="click" onclick="openAdd();">
				<span><img src="<%=path%>/images/t01.png" /></span>项目立项
			</li>
		</ul>
		<ul>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=02" target="rightFrame" <s:if test="%{thisBudgetType=='02'}">class="selected"</s:if>>自治区层面</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=03" target="rightFrame" <s:if test="%{thisBudgetType=='03'}">class="selected"</s:if>>市级层面</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=01" target="rightFrame" <s:if test="%{thisBudgetType=='01'}">class="selected"</s:if>>中央预算</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=04" target="rightFrame" <s:if test="%{thisBudgetType=='04'}">class="selected"</s:if>>项目建设丰收年</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=05" target="rightFrame" <s:if test="%{thisBudgetType=='05'}">class="selected"</s:if>>现代服务业重点项目</a></li>
			<li><a href="<%=path %>/biz/XmInfo_openMap.action?resUri=XmPoorInfoMap&thisBudgetType=06" target="rightFrame" <s:if test="%{thisBudgetType=='06'}">class="selected"</s:if>>其他</a></li>
		</ul>
		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmInfo_openPoorPage.action?resUri=listPoor&thisBudgetType=${thisBudgetType}" target="rightFrame"><span><img src="<%=path%>/images/ico04.png" /></span>切换（项目列表）</a></li>
       </ul>
	</div>
	<div id="allmap" style="overflow:hidden;zoom:1;position:relative;">	
		<div id="map" style="height:100%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
	</div>
	
	<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:550px;height:auto; display:none;"></div>
	
	<script type="text/javascript" src="<%=path %>/js/baiduMap/map.js"></script>
	
	<script type="text/javascript" src="<%=path %>/js/baiduMap/map_navigationControl.js"></script>
	
	<script type="text/javascript" src="<%=path %>/js/baiduMap/map_PanoramaControl.js"></script>
	
	<script type="text/javascript">
	$(function(){
	 $.ajax({
             type: "GET",
             url: "<%=path%>/bizJson/XmInfoJson_findAllPoorXmInfo.action",
             data:{"thisBudgetType":'${thisBudgetType }'},
             dataType: "json",
             contentType : "application/x-www-form-urlencoded; charset=utf-8",
             success: function(data){
             	$.each(data.obj,function(i,items){
			 		var pc = items.coordinate.split(",");
			 		var point = new BMap.Point(pc[0],pc[1]);
			 		
					var label = new BMap.Label(items.name+"（"+items.xmYear+"年）",{offset:new BMap.Size(20,-10)});//添加信息提示框
					
					//删除
					var removeMarker = function(e,ee,marker){
						if(confirm('确定要删除此信息吗？')){
							$.ajax({
             					type: "GET",
             					url: "<%=path%>/bizJson/XmInfoJson_deleteAjax.action",
             					data: {xmInfoId:items.id},
             					dataType: "json",
             					success: function(data){
             						alert(data.msg);
             						location.reload();
								}
         					});
							return true;
						}return false;
					};
					//修改
					var editMarker = function(e,ee,marker){
						dialog('90%','90%','立项信息修改','<%=path%>/biz/XmInfo_openPoorEdit.action?resUri=openPoorEdit&thisXmNumber='+items.xmNumber, 'true', '5%', '5%');
					};
					
					//显示多边形
					var reply = function(e,ee,marker){
						
						var p = [];//线的数组
						var ply1;
					
						var zb2=items.coordinatePly.split("_");
						//面的数组
						for(var j=0;j<zb2.length;j++){
							var zb3 = zb2[j].split(",");
		                	p.push(new BMap.Point(zb3[0], zb3[1]));
		            	}
						ply1 = new BMap.Polygon(p, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
					
						map.addOverlay(ply1);  //添加覆盖物
						
						ply1.setFillOpacity(0.3);
						ply1.setFillColor('green');
					};
					
					var addsDuty = function(e,ee,marker){
						dialog('90%','90%','责任监管','<%=path%>/biz/XmDuty_openAdds.action?resUri=openAdds&thisXmNumber='+items.xmNumber, 'true', '5%', '5%');
					};
					
					var addsLandproj = function(e,ee,marker){
						dialog('90%','90%','征地进度','<%=path%>/biz/XmInfo_addXmLandprojPage.action?resUri=addXmLandprojPage&thisXmNumber='+items.xmNumber, 'true', '5%', '5%');
					};
					
					//创建右键菜单
					var markerMenu=new BMap.ContextMenu();
					markerMenu.addItem(new BMap.MenuItem('显示区域',reply.bind(marker)));
					markerMenu.addItem(new BMap.MenuItem('责任监管',addsDuty.bind(marker)));
					markerMenu.addItem(new BMap.MenuItem('征地进度',addsLandproj.bind(marker)));
					markerMenu.addItem(new BMap.MenuItem('修改',editMarker.bind(marker)));
					markerMenu.addItem(new BMap.MenuItem('删除',removeMarker.bind(marker)));
					var marker = new BMap.Marker(point);
							
					map.addOverlay(marker);
					marker.setLabel(label);
					
					marker.addContextMenu(markerMenu);
					/*单击锚点弹窗时间*/
					marker.addEventListener("click",getAttr);
					function getAttr(){
						dialog('90%','90%',items.name+'实施','<%=path%>/biz/XmDuty_findDutyByNum.action?resUri=findDutyByNum&thisXmNumber='+items.xmNumber, 'true', '5%', '5%');
					}
			 	});
			 }
         });
	});
	
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增项目立项信息填写',
    	content: '<%=path%>/biz/XmInfo_openPoorAdd.action?resUri=openPoorAdd&thisResUri=XmPoorInfoMap'
	});
    }
	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'立项信息修改',
    	content: '<%=path%>/biz/XmInfo_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

function excute(_thisBudgetType){
	_url="<%=path%>/biz/XmInfo_openPage.action?resUri=list&thisBudgetType="+_thisBudgetType;
	$("#form1").attr("action",_url);
	$("#form1").submit();
}
	</script>
	
	
</body>
</html>


