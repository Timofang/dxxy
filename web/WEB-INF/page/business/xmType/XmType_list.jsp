<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>项目类型管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>

    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />


    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>

    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" /><script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript">
        function openAdd(_isSecretary){
            layer.open({
                type: 2,
                area: ['90%', '90%'],
                fix: false, //不固定
                title:'添加教研室',
                maxmin: true,
                content: '<%=path%>/biz/XmType_openAdd.action?view=add&resUri=openAdd&isSecretary='+_isSecretary
            });
        }

    </script>
</head>
<body>
<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="<%=path %>/sys/Home_home.action">首页</a></li>
        <li>教研室管理</li>
    </ul>
</div>
<div class="rightinfo">
    <div class="itab" style="margin-bottom:5px;">
        <ul>
            <li><a class="selected" href="<%=path %>/biz/XmType_list.action?view=list&resUri=list&isOneAction=1" target="rightFrame">系统教研室管理</a></li>
        </ul>
    </div>
    <%--<form action="<%=path%>/biz/Unit_findByExample.action?view=list&resUri=list&isSecretary=${isSecretary}" method="post" id="household">--%>
        <%--<ul>--%>
            <%--<li>--%>
                <%--<table border="1" cellspacing="0" cellpadding="0" >--%>
                    <%--<tr>--%>
                        <%--<td valign="middle">负责人工号：</td>--%>
                        <%--<td >--%>
                            <%--<input class="dfinput" type="text" id="model.unitPerson" name="model.unitPerson" value="${model.unitPerson }"/>&nbsp;&nbsp;&nbsp;&nbsp;--%>
                        <%--</td>--%>
                        <%--<td>--%>
                            <%--<input name="" id="queryBtn" type="submit" class="scbtn" value="查询"/>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr><td>&nbsp;</td></tr>--%>
                <%--</table>--%>
            <%--</li>--%>
        <%--</ul>--%>
    <%--</form>--%>
    <div class="tools">
        <ul class="toolbar">
            <li class="click"><span><img src="<%=path%>/images/t01.png" /></span>
                <a href="#" onclick="openAdd('${isSecretary}')">添加项目类型</a></li>
        </ul>
    </div>

    <table class="tablelist">
        <thead>
        <tr>
            <!-- <th>项目类型编号：</th> -->
            <th>项目类型名称：</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator id="p" value="pageResult.data" status="pp">
            <tr id="tr_${typeId}" align="center" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <%-- <td align="center">${typeId}</td> --%>
                <%--<td align="center"><s:property value="#pp.count"/></td>--%>
                <td align="center">${typeName}</td>
                <td align="center">
                    <a href="<%=path%>/biz/XmType_del.action?view=list&resUri=del&delTypeId=${typeId }" target="rightFrame" onclick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
                    <a href="javascript:dialog('90%','90%','修改用户信息','<%=path%>/biz/XmType_openEdit.action?view=edit&thisTypeId=${typeId }&resUri=openEdit', 'true', '5%', '5%');" ><font color="red">修改</font></a>
                </td>
            </tr>
        </s:iterator>
        </tbody>
    </table>
    <c:if test="${isExample==0 }">
        <%@ include file="/WEB-INF/common/pagination.jsp"%>
    </c:if>
</div>
<br/>
</body>
</html>
