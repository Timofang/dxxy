<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>添加项目类型</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

    <script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

    <script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

    <script type="text/javascript">


        /**
         * @author soldier（黄结）
         * layerweb弹层组件
         */
        $(function(){
            var flag="${isClose}";
            if(flag!='' && flag!=undefined){
                $('#submit').attr('disabled','disabled');
                layer.msg('修改成功', {//样式
                    icon:1
                });
                /* 此处用setTimeout演示ajax的回调*/
                setTimeout(function(){
                    parent.location.reload();/*刷新父页面*/
                    layer.close(index);/*关闭当前layer弹出层*/
                },1000);
            }
        });

    </script>

</head>
<body>
<div class="main">
    <form action="<%=path %>/biz/XmType_add.action?view=add&resUri=list" method="post"  name="form" id="typeFrom" enctype="multipart/form-data">
        <ul>
            <li><strong><font color="red">项目类型信息：</font></strong></li>
            <li>
                <table width="100%" border="10" cellspacing="0" cellpadding="0" class="formTable">
                    <tr>
                        <td>项目类型名称：</td>
                        <td >
                            <input class="dfinput" type="text" id="typeName" name="model.typeName">
                        </td>
                        <%--<td>工号：</td>--%>
                        <%--<td>--%>
                            <%--<input class="dfinput" type="text" name="model.jobNumber" id="model.jobNumber"/>--%>
                        <%--</td>--%>
                    </tr>
                </table>
            <li>
                <div style="margin:10px 50%;">
                    <input name="add_btn" id="save" type="submit" class="scbtn" value="保存记录"/>
                </div>
            </li>
        </ul>
    </form>

    <script>
        //表单校验
        var form = document.forms[0],
        submit = $("#save")
        form.onsubmit = function(){
            var _typeName = $("#typeName").val();
            if (_typeName == "") {
                alert("项目类型名称不能为空！！")
                return false;
            }else{
            	window.opener=null;
				window.open('','_self');
				window.close();
            }
        }
        
        
        
    </script>
</div>
</body>
</html>