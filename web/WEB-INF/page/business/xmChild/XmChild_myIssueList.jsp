<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>任务列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>我的任务列表</li>
</ul>
</div>
	<script type="text/javascript">
		
	// 任务下发
	function xmAdd(xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增任务信息填写',
    	content: '<%=path%>/biz/Xm_getXmByXmNumber.action?resUri=unitXmAdd&thisResUri=list&thisXmNumber='+xmNumber
	});
}
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增任务信息填写',
    	content: '<%=path%>/biz/User_findUserByUnit.action?resUri=unitXmAdd&thisResUri=list'
	});
}

	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'任务信息修改',
    	content: '<%=path%>/biz/Xm_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

function excute(){
	$("#formXm").submit();
}

function findPage(_url){
	_url += "&resUri=list"
	$("#formXm").attr("action",_url);
	$("#formXm").submit();
}



    function openUpload(id,fliename,xmname){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'文件上传',
    	content: '<%=path%>/biz/Xm_openUpload.action?resUri=openUpload&xmid='+id+'&fliename='+fliename+'&xmname='+xmname
	});
}
	</script>
	<br>

	
	<form id="formXm" action="<%=path %>/biz/Xm_findExample.action?resUri=unitXmList" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
      			<td align="right">任务类型：</td>
        		<td>
        			<select name="xm.type" class="dfinput4" >
        				<option value="">--请选择--</option>
        				<option value="01">类型一</option>
        				<option value="02">类型一</option>
        				<option value="04">类型一</option>
        				<option value="03">类型一</option>
        			</select>
        		</td>
        		<td align="right">任务名称：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="Xm.name" id="Xm.name" value="${Xm.name }">
        			<input type="hidden" name="Xm.budgetType" id="Xm.budgetType" value="${thisBudgetType }">
        			<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">
        		</td>
      		</tr>
      		<tr>
        		<td align="right">是否已经完成：</td>
        		<td colspan="5">
        			<input type="radio" name="xm.status" value="00" <s:if test="%{xm.status==0}">checked="checked"</s:if>/> 未完成
					<input type="radio" name="xm.status" value="01" <s:if test="%{xm.status==1}">checked="checked"</s:if>/> 已完成
        		</td>
      		</tr>
    	</table>
  </form>
    
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<%-- <ul class="toolbar1">
        	<li><a href="<%=path %>/biz/Xm_findViewXm.action?resUri=findViewXm&thisBudgetType=${thisBudgetType}"><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>任务信息统计</strong></font></a></li>
       </ul> --%>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
              <th width="4%">序号</th>
              <!-- <th width="10%">任务编号</th> -->
              <th width="15%">任务名称</th>
              <th width="8%">任务类型</th>
              <th width="8%">发布人</th>
              <th width="6%">发布时间</th>
              <th width="6%">截止时间</th>
              <th width="20%">任务要求</th>
              <!-- <th width="10%">教研室要求</th> -->
              <th width="20%">文件名</th>
              <th width="4%">文件数</th>
              <th width="10%">接收单位/个人</th>
              <th width="6%">完成状态</th>
              <th width="12%">操作</th>
            </tr>
          </thead>
          <tbody>
          	<!-- 院下发的任务 -->
          	<s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${name}</td>
                 <td align="center">
                 	${type}
				</td>
                <td align="center">
                	<c:forEach items="${requestScope.users}" var="users">
                		<c:if test="${users.jobNumber == username}">${users.helpName}</c:if>
                	</c:forEach>
                </td>
				<td align="center"><fmt:formatDate value="${publishTime}" pattern="yyyy-MM-dd"/></td>
				<td align="center"><fmt:formatDate value="${deadLine}" pattern="yyyy-MM-dd"/></td>
				<td align="center">${requires}</td>
                <td align="center">${fileName}</td>
                <td align="center">${fileCount}</td>
                <td align="center">
					<!-- 院发到个人或教研室发到个人 -->
					<s:if test="%{receiver != null}">
	                	<c:forEach items="${requestScope.users}" var="users">
	                		<c:if test="${fn:contains(receiver,users.jobNumber)}">${users.helpName};</c:if>
	                	</c:forEach>
					</s:if>
                	<!-- 院发到教研室 -->
					<s:if test="%{receiverUnit != null}">
						<c:forEach items="${requestScope.units}" var="units">
	                		<c:if test="${fn:contains(receiverUnit,units.unitId)}">${units.unitName};</c:if>
	                	</c:forEach>
					</s:if>
				</td>
                <td align="center">
                	<s:if test="%{status==0}">未完成</s:if>
                	<s:if test="%{status==1}">已完成</s:if>
                </td>
                <td align="center">
                	<a href="javascript:dialog('90%','90%','任务进度','<%=path%>/biz/Xm_openDetail.action?resUri=unitXmDetail&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="red"><strong>详情</strong></font></a>
                	<a href="javascript:dialog('90%','90%','任务批示','<%=path%>/biz/Xm_openDetail.action?resUri=xmInstructions&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="green"><strong>批示</strong></font></a>
					
                </td>
              </tr>
            </s:iterator>
          	
          	<!-- 教研室下发的任务 -->
            <s:iterator id="p2" value="childPageResult.data" status="pp">
              <tr id="${id}"  style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmChildList[0].xmChildName}</td>
                <td align="center">无类型</td>
                <td align="center">
                	<c:forEach items="${requestScope.users}" var="users">
                		<c:if test="${users.jobNumber == userName}">${users.helpName}</c:if>
                	</c:forEach>
                </td>
				<td align="center"><fmt:formatDate value="${publishTime}" pattern="yyyy-MM-dd"/></td>
				<td align="center"><fmt:formatDate value="${deadLine}" pattern="yyyy-MM-dd"/></td>
				<td align="center">${requires}</td>
                <td align="center">${fileName}</td>
                <td align="center">${fileCount}</td>
                <td align="center">
                	<s:iterator id="dd" value="%{xmChildList}" status="p">
	                	<c:forEach items="${requestScope.users}" var="users">
	                		<c:if test="${users.jobNumber == userName}">${users.helpName};</c:if>
	                	</c:forEach>
                	</s:iterator>
                </td>
                <td align="center">
                	<s:if test="%{status==0}">未完成</s:if>
                	<s:if test="%{status==1}">已完成</s:if>
                </td>
                <td align="center">
					<!-- 教研室下发的任务 -->
                		<a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/Xm_openUnitIssueDetailXX.action?resUri=unitXmChildDetailXX&thisUnitId=${unitId}&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="red"><strong>详情</strong></font></a>
               			<a href="javascript:dialog('90%','90%','任务批示','<%=path%>/biz/Xm_openUnitIssueDetailXX.action?resUri=xmChildInstructions&thisUnitId=${unitId}&thisXmNumber=${xmNumber}', 'true', '5%', '5%');" ><font color="green"><strong>批示</strong></font></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
        
</body>
</html>