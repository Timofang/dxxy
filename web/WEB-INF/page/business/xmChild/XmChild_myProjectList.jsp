<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@ include file="/WEB-INF/common/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>任务列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
        <li>我的任务列表</li>
    </ul>
</div>
<script type="text/javascript">
    // 打开任务下发页面
    function issue(xmNumber, unitId, issue) {
        //已经下发过
        if (issue == 1) {
            alert("已经下发，不能重复下发")
        } else {
            layer.open({
                type: 2,
                area: ['90%', '90%'],
                fix: false, //不固定
                maxmin: true,
                title: '新增任务信息填写',
                content: '<%=path%>/biz/Xm_openProjectIssue.action?resUri=openProjectIssue&thisResUri=list&thisXmNumber=' + xmNumber + '&unitId=' + unitId
            });
        }
    }

    function openAdd() {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '新增任务信息填写',
            content: '<%=path%>/biz/User_findUserByUnit.action?resUri=unitXmAdd&thisResUri=list'
        });
    }

    function openEdit(_xmNumber) {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '任务信息修改',
            content: '<%=path%>/biz/Xm_openEdit.action?resUri=openEdit&thisXmNumber=' + _xmNumber
        });
    }

    function excute() {
        $("#formXm").submit();
    }

    function findPage(_url) {
        _url += "&resUri=list"
        $("#formXm").attr("action", _url);
        $("#formXm").submit();
    }

    function openUpload(xmNumber, userName, xmChildNumber, acceptstatus, unitParent, status) {
        if (status == 1) {
            if (window.confirm("已经上传过了，确定要重新上传吗？"))
                layer.open({
                    type: 2,
                    area: ['90%', '90%'],
                    fix: false, //不固定
                    maxmin: true,
                    title: '文件上传',
                    content: '<%=path%>/biz/Xm_fileUpload.action?resUri=openUpload&thisResUri=list&thisXmNumber=' + xmNumber + '&thisXmChildNumber=' + xmChildNumber + '&thisUnitParent=' + unitParent + '&thisAcceptstatus=' + acceptstatus + '&thisUserName=' + userName
                });
        } else {
            layer.open({
                type: 2,
                area: ['90%', '90%'],
                fix: false, //不固定
                maxmin: true,
                title: '文件上传',
                content: '<%=path%>/biz/Xm_fileUpload.action?resUri=openUpload&thisResUri=list&thisXmNumber=' + xmNumber + '&thisXmChildNumber=' + xmChildNumber + '&thisUnitParent=' + unitParent + '&thisAcceptstatus=' + acceptstatus + '&thisUserName=' + userName
            });
        }
    }
</script>
<br>


<form id="formXm" name="formXm" action="<%=path %>/biz/Xm_findExample.action?resUri=projectList" method="post">
    <table width="100%" border="0" class="formTable">
        <tr>
            <td align="right">任务类型：</td>
            <td>
                <select id="xmType" name="xm.type" class="dfinput4" onchange="document.formXm.submit()">
                    <option value="">--请选择--</option>
                    <c:forEach items="${typeList}" var="xmType">
                        <option
                                <c:if test="${xmType.typeId == optionValue}">selected="selected" </c:if>
                                value="${xmType.typeId}"> ${xmType.typeName}
                        </option>
                    </c:forEach>
                </select>
            </td>
            <td align="right">任务名称：</td>
            <td>
                <input type="text" class="dfinput4 required" name="Xm.name" id="XMName" placeholder="请输入关键字查找"
                       value="${Xm.name }">
                &nbsp;&nbsp;&nbsp;<a href="javascript:formXmChange()"><font color="green"><strong>查找</strong></font></a>
                <%--<input type="hidden" name="Xm.budgetType" id="Xm.budgetType" value="${thisBudgetType }">--%>
                <%--<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">--%>
            </td>
        </tr>
        <tr>
            <td align="right">是否已经完成：</td>
            <td colspan="5">
                <input type="radio" onchange="document.formXm.submit()" name="xm.status" value="0"
                       <s:if test="%{xm.status==0}">checked="checked"</s:if>/> 未完成
                <input type="radio" onchange="document.formXm.submit()" name="xm.status" value="1"
                       <s:if test="%{xm.status==1}">checked="checked"</s:if>/> 已完成
            </td>
        </tr>
    </table>
</form>

<div class="tools" style="margin-top:5px;">
    <ul class="toolbar">
        <li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png"/></span>查询</li>
    </ul>
    <%-- <ul class="toolbar1">
      <li><a href="<%=path %>/biz/Xm_findViewXm.action?resUri=findViewXm&thisBudgetType=${thisBudgetType}"><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>任务信息统计</strong></font></a></li>
 </ul> --%>
</div>
<table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
    <thead>
    <tr>
        <th width="4%">序号</th>
        <th width="15%">任务名称</th>
        <th width="8%">任务类型</th>
        <th width="8%">发布人</th>
        <th width="6%">发布时间</th>
        <th width="6%">截止时间</th>
        <th width="20%">任务要求</th>
        <th width="20%">文件名</th>
        <th width="4%">文件数</th>
        <th width="10%">接收单位/个人</th>
        <th width="6%">完成状态</th>
        <th width="12%">操作</th>
    </tr>
    </thead>
    <tbody>
    <s:iterator id="p2" value="childPageResult.data" status="pp">
        <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
            <td height="21" align="center"><s:property value="#pp.count"/></td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">${xmName }</s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">${xmChildName }</s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">
                    <%--${xm.type}--%>
                    <c:forEach items="${typeList}" var="xmType">
                        <c:if test="${xmType.typeId == xm.type}">${xmType.typeName}</c:if>
                    </c:forEach>
                </s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">
                    无类型
                </s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">${xm.helpUsername }</s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">${xmChild.unitName }教研室/组</s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}"><fmt:formatDate value="${xm.publishTime}"
                                                                  pattern="yyyy-MM-dd"/></s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}"><fmt:formatDate value="${xmChild.publishTime}" pattern="yyyy-MM-dd"/></s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}"><fmt:formatDate value="${xm.deadLine}" pattern="yyyy-MM-dd"/></s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}"><fmt:formatDate value="${xmChild.deadLine}"
                                                                  pattern="yyyy-MM-dd"/></s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">${xm.requires}</s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">${xmChild.requires}</s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">${xm.fileName}</s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">${xmChild.fileName}</s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室或个人 -->
                <s:if test="%{acceptstatus == 1}">${xm.fileCount}</s:if>
                <!-- 教研室发到个人 -->
                <s:if test="%{acceptstatus == 0}">${xmChild.fileCount}</s:if>
            </td>
            <td align="center">
                <!-- 院发到教研室 -->
                <s:if test="%{unitId != null}">
                    <%--${unitId }--%>
                    <c:forEach items="${requestScope.units}" var="unit">
                        <c:if test="${unitId == unit.unitId}">${unit.unitName }</c:if>
                    </c:forEach>
                </s:if>
                <!-- 院发到个人或教研室发到个人 -->
                <s:if test="%{unitId == null}">
                    <%--${userName }--%>
                    <c:forEach items="${requestScope.users}" var="user">
                        <c:if test="${userName == user.helpUserName}">${user.helpName }</c:if>
                    </c:forEach>
                </s:if>
            </td>
            <td align="center">
                <s:if test="%{status==0}">未完成</s:if>
                <s:if test="%{status==1}">已完成</s:if>
            </td>
            <td align="center">

                <!-- 院下发的任务 -->
                <s:if test="%{acceptstatus == 1}">
                    <a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/Xm_openDetail.action?resUri=unitXmDetail&thisXmNumber=${xmNumber}', 'true', '5%', '5%');"><font
                            color="red"><strong>详情</strong></font></a>
                </s:if>
                <!-- 教研室下发的任务 -->
                <s:if test="%{acceptstatus == 0}">
                    <a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/Xm_openUnitIssueDetailX.action?resUri=unitXmChildDetailX&thisXmNumber=${xmChildNumber}&thisUnitId=${unitParent }', 'true', '5%', '5%');"><font
                            color="red"><strong>详情</strong></font></a>
                </s:if>
                <s:if test="%{unitId != null }">
                    | <a href="javascript:void(0)" id="${xmNumber}" style="font-weight:bold;"
                         onclick="issue('${xmNumber}', '${unitId }', '${issue }');"><span style="color: green"><strong>下发</strong></span></a>
                </s:if>
                    <%-- <c:if test="${status == 0}"> --%>
                | <a href="javascript:void(0)"
                     onclick="openUpload('${xmNumber}','${userName}','${xmChildNumber}','${acceptstatus}','${unitParent}','${status}');"
                     style="font-weight:bold;"><strong>上传</strong></a>
                    <%-- </c:if>  --%>
                <!-- 院领导发任务到教研室 -->
                    <%-- <c:if test="${unitId == userinfo.unitId}">
                        <c:if test="${userinfo.leaderTypeId==2}">
                              | <a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${xmNumber}');"><strong>修改</strong></a> |
                            <a  href="<%=path%>/biz/Xm_delete.action?resUri=unitXmDelete&xmNumber=${id}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><strong>删除</strong></a>
                        </c:if>
                    </c:if>  --%>

            </td>
        </tr>
    </s:iterator>
    </tbody>
</table>
${footer }

</body>
</html>