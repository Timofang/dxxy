<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>任务实施</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
</style>

<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" /> 
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
<script src="<%=path %>/js/baiduMap_util/GeoUtils.js" type="text/javascript"></script>
<script src="<%=path %>/js/baiduMap_util/AreaRestriction.js" type="text/javascript"></script>

<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
      
</head>
	<body>
		<div class="formBody">
        	<span style="font-weight:bold;margin-bottom:5px;display:block">+任务基本情况</span>
			<table width="100%"  cellspacing="0" cellpadding="0" class="formTable">
				<tr>
   					<td><strong>任务名称：${xmChild.xmChildList[0].xmChildName }</strong></td>
    				<td style="border-right:none;">任务类型： 
						无类型
    				</td>
    				<td style="border-left:none;border-right:none;"></td>
 					</tr>
 					<tr>
 						<td>发布时间：<fmt:formatDate value="${xmChild.publishTime}" pattern="yyyy-MM-dd"/></td>
 						<td>完成截止时间：<fmt:formatDate value="${xmChild.deadLine}" pattern="yyyy-MM-dd"/></td>
 						<td>发布人：${xmChild.helpName }</td>
 					</tr>
 					<tr>
   					<td style="border-right:none;">是否已经完成：
   						<s:if test="%{xmChild.status==0}"><font color="red">未完成</font></s:if>
   						<s:if test="%{xmChild.status==1}"><font color="green">已完成</font></s:if>
   					</td>
   					<td style="border-left:none;border-right:none;"></td>
					<td style="border-left:none;"></td>
 					</tr>
			</table><br />
			
			<!-- 教研室任务批示 -->
			<c:forEach items="${instructionsList}" var="instructions" varStatus="vs"> 
				<c:if test="${(fn:length(instructionsList)) > 1}">
					<span style="font-weight:bold;margin-bottom:5px;display:block">+任务批示${vs.count}</span>
				</c:if>
				<c:if test="${(fn:length(instructionsList)) == 1}">
					<span style="font-weight:bold;margin-bottom:5px;display:block">+任务批示</span>
				</c:if>
				<table width="100%" cellspacing="0" cellpadding="0" class="formTable">
					<tr>
	   					<td>
	   						<textarea id="instructions.instructionsContent" name="instructions.instructionsContent" readonly="readonly" cols="180" rows="7" >${instructions.instructionsContent}</textarea>
						</td>
	 				</tr>
				</table><br />
			</c:forEach>		
			
		<form id="form1" action="<%=path%>/biz/Xm_xmChildInstructions.action?resUri=myIssue" target="rightFrame" method="post" >
		<span style="font-weight:bold;margin-bottom:5px;display:block">+新增任务批示</span>
		<table width="100%" cellspacing="0" cellpadding="0" class="formTable">
		<c:forEach items="${xmChild.userList}" var="user">
			<input type="hidden" name="instructions.xmNumber" value="${user.xmChild.xmChildNumber}" />
		</c:forEach>
			<input type="hidden" name="instructions.instructionsPerson" value="${xmChild.helpName }" />
			<tr>
				<td>
  					<textarea id="instructions" name="instructions.instructionsContent" cols="50%"  rows="16" style="width:50%; font-size: 14px;"></textarea>
  				</td>
			</tr>
		</table><br />
		<div align="center" style="margin-top:5px;"><input type="submit" id="save" name="add_btn" class="scbtn" value="保存"/></div>
		</form>
		</div>
		
		
		
<script type="text/javascript">

//表单校验
var form = document.forms[0],
   submit = $("#save")
   form.onsubmit = function(){
   if($("#instructions").val() == ""){
       alert("批示内容不能为空！");
       return false;
   }
}
</script>

</body>
</html>