<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>教研室下发任务</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.js"></script>

</head>
<body>
 <div class="formbody">
<form id="form1" action="<%=path%>/biz/Xm_unitXmAdd.action?resUri=${thisResUri}&unitId=${unitId}&thisXmNumber=${thisXmNumber}" target="rightFrame" method="post">


  
  
  <span style="display:block; margin-bottom:5px; margin-top:5px; font-size: 18px;" ><strong>+教研室下发任务基本信息</strong></span>
<table width="100%" border="1" class="formTable">
  <tr>
    <td align="right" width="10%">教研室任务名称:</td>
    <td>
    	<input type="text" value="${xmChild.xm.name}" class="dfinput4 required" name="xmChild.xmChildName" id="xmChildName"/>
    	<input type="hidden" name="xmChild.xmNumber" value="${xmChild.xmNumber}"/>
    </td>
  </tr>
  
    <tr>
   	<td align="right">完成截止时间:</td>
    <td>
    	<input type="text" class="dfinput4 Wdate required" name="xmChild.deadLine" id="deadLine" value="" onFocus="WdatePicker()"  />
    </td>
   </tr>
   
   <tr>
    <td align="right">难度系数:</td>
    <td>
    	<select name="xmChild.degree">
    		<option value="0.1">0.1</option>
    		<option value="0.2">0.2</option>
    		<option value="0.3">0.3</option>
    		<option value="0.4">0.4</option>
    		<option value="0.5">0.5</option>
    		<option value="0.6">0.6</option>
    		<option value="0.7">0.7</option>
    		<option value="0.8">0.8</option>
    		<option value="0.9">0.9</option>
    		<option value="1.0">1.0</option>
    	</select>
    </td>
  </tr>
 <tr>
  	<td align="right">教研室任务要求说明:</td>
  	<td>
  		<textarea name="xmChild.requires" cols="100" id="requires" rows="7">${xmChild.xm.requires}</textarea>
  	</td>
  	
  </tr>
  
  </table>
  
	<span style="display:block; margin-bottom:5px; margin-top:20px;"><strong>+将任务指派给教职工</</strong></span>
	<input type="hidden" name="receiverStatus" value="1"/>
	<br />
      	<div style="margin: auto; margin-left: 50px;">
      	<s:iterator value="#session.userList" var="user" status="status">
	      		<li id="userList" style="display: inline;">
			    	<label for="${user.helpUserName}" style="width:150px;">
			    		<input name="userIdList" id="${user.helpUserName}" type="checkbox" class="dfinput1" value="${user.helpUserName}" />&nbsp;${user.helpName}
			    	</label>
		    	</li>&nbsp;&nbsp;&nbsp;&nbsp;
      	</s:iterator>	
 		</div><br /><br />
 		
 	<span style="display:block; margin-bottom:5px; margin-top:20px;"><strong>+任务需上传材料</</strong></span> 
    <span style=" margin-bottom:5px; margin-top:5px; margin-left:80px; font-size: 14px;">请输入该任务需要上传的文件个数：(上限为10个)
    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input class="dfinput4 required" size="5" maxlength="2" name="xmChild.fileCount" value="${xm.fileCount}" type="text" id="fileCount"/></span>
    <span style="display:inline; margin-bottom:5px; margin-top:5px; margin-left:10px;"><input class="scbtn" type="button" onclick="makeFile()" value="确定"/></span>
    </span>
    <div id="file">
    	<div id="fileC">
    	
    	</div>
    </div>
   <br />
   <br />
  <div align="center" style="margin-top:5px;"><input type="submit" id="save" name="add_btn" class="scbtn" value="发布" /></div>
</form>

<script>

// radio单选框取消选中和重新选中
  $(document).ready(function(){
        var old = null; //用来保存原来的对象
        $("#radioCheck").each(function(){//循环绑定事件
            if(this.checked){
                old = this; //如果当前对象选中，保存该对象
            }
            this.onclick = function(){
                if(this == old){//如果点击的对象原来是选中的，取消选中
                    this.checked = false;
                    old = null;
                } else{
                    old = this;
                }
            }
        });
    })
	
	//生成文件名输入框
	function makeFile(){
		//清除原有框
		$("#fileC").remove();
		//加入div
		$("#file").append("<div id='fileC'></div>");
		var fileCount = $("#fileCount").val();
		if(fileCount != null && !isNaN(fileCount) && fileCount <= 10 && fileCount != ""){
			$("#fileC").append('<label id="nameLabel" style="margin-top:5px; margin-left: 80px; font-size: 14px;">请分别输入文件名称：</label>');
            for(var i = 0; i < fileCount; i++){
            	var j = i+1;
            	$("#nameLabel").append('<div style="margin-left: 80px;"><label style="display:block; margin-top:5px; margin-left: 80px; font-size: 14px;"></label>'+j+'：  <input class="dfinput4 required" style=" margin-top:5px; " type="text" name="xmChild.fileName" id="file' + i + '" value=""/></div><br/>');
            }
		}else{
			alert("输入有误，请重新输入!")
		}
	}

	/* function add(){
		// 点击发布按钮
		if(confirm('确定要发布此任务吗？')){alert('发布成功！');return true;}return false;
	} */
	
	 //表单校验
	var form = document.forms[0],
    submit = $("#save")
    form.onsubmit = function(){
        if($("#name").val() == ""){
            alert("任务名不能为空！");
            return false;
        }else if($("#type").val() == ""){
            alert("请选择任务类型！");
            return false;
        }else if(inputBtn[2].value == ""){
            alert("请您再次输入密码");
            return false;
        }else if(inputBtn[1].value != inputBtn[2].value){
            alert("两次密码输入不匹配，请更正！");
            return false;
        }   
		         
	}
</script>

</div>
</body>
</html>