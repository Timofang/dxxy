<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@ include file="/WEB-INF/common/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>任务列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
    <script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
    <script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
    <script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
        <li>我的任务列表</li>
    </ul>
</div>
<script type="text/javascript">
    // 任务下发
    function xmAdd(xmNumber) {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '新增任务信息填写',
            content: '<%=path%>/biz/Xm_getXmByXmNumber.action?resUri=unitXmAdd&thisResUri=list&thisXmNumber=' + xmNumber
        });
    }

    function openAdd() {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '新增任务信息填写',
            content: '<%=path%>/biz/User_findUserByUnit.action?resUri=unitXmAdd&thisResUri=list'
        });
    }

    function openEdit(_xmNumber) {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '任务信息修改',
            content: '<%=path%>/biz/Xm_openEdit.action?resUri=openEdit&thisXmNumber=' + _xmNumber
        });
    }

    function excute() {
        $("#formXm").submit();
    }

    function findPage(_url) {
        _url += "&resUri=list"
        $("#formXm").attr("action", _url);
        $("#formXm").submit();
    }

    function openUpload(id, fliename, xmname) {
        layer.open({
            type: 2,
            area: ['90%', '90%'],
            fix: false, //不固定
            maxmin: true,
            title: '文件上传',
            content: '<%=path%>/biz/Xm_openUpload.action?resUri=openUpload&xmid=' + id + '&fliename=' + fliename + '&xmname=' + xmname
        });
    }

    function formXmChange() {
        var xmName = $('#XMName').val();

        if (xmName == "") {
            return ;
        }

        //取消radio选中
        // var checkedState = $(this).attr('checked');//记录当前选中状态
        var inputList = document.getElementsByName("xm.status");
        for (var x = 0; x < inputList.length; x++) {
            inputList[x].checked = false;  //取消选中
        }
        document.formXm.submit();
    }
</script>
<br>

<form id="formXm" name="formXm" action="<%=path %>/biz/Xm_findExample.action?resUri=projectList" method="post">
    <table width="100%" border="0" class="formTable">
        <tr>
            <td align="right">任务类型：</td>
            <td>
                <select id="xmType" name="xm.type" class="dfinput4" onchange="document.formXm.submit()">
                    <option value="">--请选择--</option>
                    <c:forEach items="${typeList}" var="xmType">
                        <option
                                <c:if test="${xmType.typeId == optionValue}">selected="selected" </c:if>
                                value="${xmType.typeId}"> ${xmType.typeName}
                        </option>
                    </c:forEach>
                </select>
            </td>
            <td align="right">任务名称：</td>
            <td>
                <input type="text" class="dfinput4 required" name="Xm.name" id="XMName" placeholder="请输入关键字查找" value="${Xm.name }">
                &nbsp;&nbsp;&nbsp;<a href="javascript:formXmChange()"><font color="green"><strong>查找</strong></font></a>
                <%--<input type="hidden" name="Xm.budgetType" id="Xm.budgetType" value="${thisBudgetType }">--%>
                <%--<input type="hidden" name="thisBudgetType" id="thisBudgetType" value="${thisBudgetType }">--%>
            </td>
        </tr>
        <tr>
            <td align="right">是否已经完成：</td>
            <td colspan="5">
                <input type="radio" onchange="document.formXm.submit()" name="xm.status" value="0"
                       <s:if test="%{xm.status==0}">checked="checked"</s:if>/> 未完成
                <input type="radio" onchange="document.formXm.submit()" name="xm.status" value="1"
                       <s:if test="%{xm.status==1}">checked="checked"</s:if>/> 已完成
            </td>
        </tr>
    </table>
</form>

<table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
    <thead>
    <tr>
        <th width="4%">序号</th>
        <!-- <th width="10%">任务编号</th> -->
        <th width="15%">任务名称</th>
        <th width="8%">任务类型</th>
        <th width="8%">发布人</th>
        <th width="6%">发布时间</th>
        <th width="6%">截止时间</th>
        <th width="20%">任务要求</th>
        <!-- <th width="10%">教研室要求</th> -->
        <th width="20%">文件名</th>
        <th width="4%">文件数</th>
        <th width="10%">接收单位/个人</th>
        <th width="6%">完成状态</th>
        <th width="12%">操作</th>
    </tr>
    </thead>
    <tbody>
    <s:iterator id="p2" value="childPageResult.data" status="pp">
        <tr id="${id}" style="border: solid thin; border-color: #c7c7c7; border-left: 0px; border-right: 0px;">
            <td height="21" align="center"><s:property value="#pp.count"/></td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    ${xm.name}
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    ${xmChildName}
                </c:if>
            </td>
            <td align="center">
                    <%--${xm.type}--%>
                    <c:forEach items="${typeList}" var="xmType">
                    <c:if test="${xmType.typeName == xm.type}">${xmType.typeName}</c:if>
                    </c:forEach>
            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    ${xm.helpUsername}
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    ${xmChild.unitName }
                </c:if>

            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    <fmt:formatDate value="${xm.publishTime}" pattern="yyyy-MM-dd"/>
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    <fmt:formatDate value="${xmChild.publishTime}" pattern="yyyy-MM-dd"/>
                </c:if>
            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    <fmt:formatDate value="${xm.deadLine}" pattern="yyyy-MM-dd"/>
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    <fmt:formatDate value="${xmChild.deadLine}" pattern="yyyy-MM-dd"/>
                </c:if>
            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    ${xm.requires}
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    ${xmChild.requires}
                </c:if>
            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    ${xm.fileName}
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    ${xmChild.fileName}
                </c:if>
            </td>
            <td align="center">
                <!-- 院下发的任务 -->
                <c:if test="${xmChildNumber == null}">
                    ${xm.fileCount}
                </c:if>
                <!-- 教研室下发的任务 -->
                <c:if test="${xmNumber == null}">
                    ${xmChild.fileCount}
                </c:if>
            </td>
            <td align="center">
                <!-- 院发到教研室 -->
                <s:if test="%{unitId != null}">${receiverUnitName}</s:if>
                <!-- 院发到个人或教研室发到个人 -->
                <s:if test="%{unitId == null}">${receiverName }</s:if>
            </td>
            <td align="center">
                <s:if test="%{status==0}">未完成</s:if>
                <s:if test="%{status==1}">已完成</s:if>
            </td>
            <td align="center">

                <!-- 院下发的任务 -->
                <s:if test="%{acceptstatus == 1}">
                    <a href="javascript:dialog('90%','90%','${name}任务进度','<%=path%>/biz/Xm_openDetail.action?resUri=unitXmDetail&thisXmNumber=${xmNumber}', 'true', '5%', '5%');"><font
                            color="red"><strong>详情</strong></font></a>
                </s:if>

                <!-- 教研室下发的任务 -->
                <s:if test="%{acceptstatus == 0}">
                    <a href="javascript:dialog('90%','90%','${name}任务进度','<%=path%>/biz/Xm_openUnitIssueDetails.action?resUri=unitXmChildDetail&thisXmNumber=${xmChildNumber}', 'true', '5%', '5%');"><font
                            color="red"><strong>详情</strong></font></a>
                </s:if>

            </td>
        </tr>
    </s:iterator>
    </tbody>
</table>
${footer }

</body>
</html>