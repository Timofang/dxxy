<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>责任监管</title>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
		<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css">
		<link href="<%=path%>/css/newStyle.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />

		<script type="text/javascript" src="<%=path%>/third/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
		<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/third/jquery-autocomplete/jquery.autocomplete.js"></script>

		<script type="text/javascript" src="<%=path%>/js/core/dialog/closeDialog.js"></script>

		<script type="text/javascript">
 	$(document).ready(function(e) {
	 
	 $("#thisInfo").autocomplete("<%=path%>/bizJson/XmInfoJson_likeAll.action", { 
			dataType:"json",
			type: "post",
			max: 100, //列表里的条目数 
		 	minChars: 0, //自动完成激活之前填入的最小字符 
		 	width: 350, //提示的宽度，溢出隐藏 
		 	scrollHeight: 300, //提示的高度，溢出显示滚动条 
		 	matchContains: true, //包含匹配，就是data参数里的数据示 
		 	autoFill: false, //自动填充
			extraParams: {instru_thisUnit:"123"},
			parse: function(datas) 
		        { 
		            data = datas.obj;
		            var rows = [];
		            for(var i=0; i<data.length; i++)
		         {	
		              rows[rows.length] = 
		           {                  
		                data:data[i],
		                result:data[i].itemName,
		                value:data[i].itemId
		              };
		            }           
		            return rows;
		        }, 
		 	formatMatch: function(row, i, max) { 
		    	return row.itemId ; 
		 	},  
		    formatItem: function(row, i, max) { 
			   return  row.itemName; 
		 	}
		 	}).result(function(event, row, formatted) { 
		    	var str=row.itemName;
				$("#thisXmNumber").val(row.itemValue);
				
				document.getElementById('thisXmNumber').value = row.itemValue;
		 	});   
 		});
 		
	function saveRec(){
	 	$("#formAddXmInfo").submit();
	}
</script>
	</head>

	<body>
		<div class="main">
			<form action="<%=path%>/biz/XmLandproj_addXmInfo.action?resUri=addXmInfoPage" method="post" name="formAddXmInfo" id="formAddXmInfo">
				<ul>
					<li>
						<table width="100%" border="10" cellspacing="0" cellpadding="0"
							class="formTable">
  							<tr>
  								<td align="right" width="40%">输入关键字（名称）项目：</td>
    							<td>
    								<input class="dfinput4 required" type="text" name="thisInfo" id="thisInfo" />
        							<input type="hidden" id="thisLandNumber" name="thisLandNumber" value="${thisLandNumber }"/>
									<input type="hidden" name="thisXmNumber" id="thisXmNumber" />
    							</td>
  							</tr>
						</table>
					</li>
					<li>
						<div style="margin: 10px 50%;">
							<input name="" id="submit1" type="button" class="scbtn" value="保存记录" onClick="saveRec();"/>
						</div>
					</li>
				</ul>
			</form>
		</div>
	</body>
	<table class="tablelist">
    	<thead>
          	<tr>
          	  <th width="4%" >序号</th>
          	  <th>建设年份</th>
              <th>项目名称</th>
              <th>行业（所属）</th>
              <th>项目层次</th>
              <th>项目性质</th>
              <th>起止年限</th>
              <th>项目业主</th>
              <th>项目地址</th>
              <th>是否已经开工</th>
              <th>是否已经竣工</th>
              <th>报告周期</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="listViewToXmInfo" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${xmYear}</td>
                <td align="center">${name}</td>
                <td align="center">${industry}</td>
                <td align="center">
					<c:if test='${fn:contains(budgetType,"01") }'>中央预算；</c:if>
                	<c:if test='${fn:contains(budgetType,"02") }'>自治区层面；</c:if>
                	<c:if test='${fn:contains(budgetType,"03") }'>市级层面；</c:if>
                	<c:if test='${fn:contains(budgetType,"04") }'>项目建设丰收年；</c:if>
                	<c:if test='${fn:contains(budgetType,"05") }'>现代服务业重点项目；</c:if>
				</td>
                <td align="center">
                	<s:if test="%{projectType=='01'}">新开工项目</s:if>
                	<s:if test="%{projectType=='02'}">续建项目</s:if>
                	<s:if test="%{projectType=='03'}">重大前期工作</s:if>
                	<s:if test="%{projectType=='04'}">竣工或部分竣工</s:if>
                </td>
                <td align="center">${period}</td>
                <td align="center">${owner}</td>
                <td align="center">${address}</td>
                <td align="center">
                	<s:if test="%{isState=='00'}">未开工</s:if>
                	<s:if test="%{isState=='01'}">已开工</s:if>
                </td>
                <td align="center">
                	<s:if test="%{isCompleted=='00'}">未竣工</s:if>
                	<s:if test="%{isCompleted=='01'}">已竣工</s:if>
                </td>
                <td align="center">${cycle}天</td>
                <td align="center">
                	<a  href="<%=path%>/biz/XmLandproj_delXmInfo.action?resUri=addXmInfoPage&thisToLandprojid=${toLandprojid }&thisLandNumber=${landNumber}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><font color="red">删除</font></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
</table>
</html>