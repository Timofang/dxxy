<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>项目库维护</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body, html,#allmap{width: 100%;height: 85%;margin:0;font-family:"微软雅黑";}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=DOvuUGioLLij5CNXM6cRivob"></script>
	
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.css"/>

<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
<script type="text/javascript" src="<%=path%>/third/select-ui.min.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/third/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/third/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<%=path%>/third/datePicker/WdatePicker.js"></script>

<script type="text/javascript">
 		
	//单击获取点击的经纬度
	map.addEventListener("click",function(e){
		alert(e.point.lng + "," + e.point.lat);
	});
	
	
</script>

</head>
<body>
 <div class="formbody">
<form id="form1" action="<%=path%>/biz/XmLandproj_add.action?resUri=${thisResUri }" target="rightFrame" method="post">
<span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+征地进度基本信息</strong></span>
<table width="100%" border="1" class="formTable">
  <tr>
    <td align="right" width="10%">项目名称:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmLandproj.name" id="xmLandproj.name"/>
    </td>
    <td align="right" width="10%">报告周期（天）:</td>
  	<td>
    	<input type="text" class="dfinput4 required" name="xmLandproj.cycle" id="xmLandproj.cycle"/>
    </td>
    <td align="right" width="15%">建设年份:</td>
    <td>
    	<s:select name="xmLandproj.laYear" id="laYear" cssClass="dfinput4" list="yearList" listKey="key" listValue="value"/>
    </td>
  </tr>
  <tr>
  	<td align="right" width="10%">批文情况:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmLandproj.approval" id="xmLandproj.approval"/>
    </td>
    <td align="right" width="15%">大额投资委通过情况:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmLandproj.largeInvest" id="xmLandproj.largeInvest"/>
    </td>
    <td align="right" width="10%">项目业主:</td>
    <td>
    	<input type="text" class="dfinput4 required" name="xmLandproj.owner" id="xmLandproj.owner"/>
    </td>
  </tr>
  <tr>
  	<td align="right">征地类型：</td>
    <td colspan="5">
    	<input type="checkbox" name="strLandType" id="strLandType" value="01"/>分片区
		<input type="checkbox" name="strLandType" id="strLandType" value="02"/>片区外重点
		<input type="checkbox" name="strLandType" id="strLandType" value="03"/>片区外统筹
		<input type="checkbox" name="strLandType" id="strLandType" value="04"/>征拆大会战
    </td>
  </tr>
  </table>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+土地征收（亩）</strong></span>
  <table width="100%" border="1" class="formTable">
  	 <tr>
  		<td align="right" width="10%">项目总任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.landTotal" id="xmLandproj.landTotal"/>
    	</td>
  		<td align="right" width="15%">剩余待完成任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.landSurplus" id="xmLandproj.landSurplus"/>
    	</td>
  		<td align="right" width="10%">本年任务:</td>
   	 	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.landCurrent" id="xmLandproj.landCurrent"/>
    	</td>
  	</tr>
  </table>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+房屋征收（m2）</strong></span>
  <table width="100%" border="1" class="formTable">
  	 <tr>
  		<td align="right" width="10%">项目总任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.houseTotal" id="xmLandproj.landTotal"/>
    	</td>
  		<td align="right" width="15%">剩余待完成任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.houseSurplus" id="xmLandproj.landSurplus"/>
    	</td>
  		<td align="right" width="10%">本年任务:</td>
   	 	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.houseCurrent" id="xmLandproj.landCurrent"/>
    	</td>
  	</tr>
  </table>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+坟山迁移（座）</strong></span>
  <table width="100%" border="1" class="formTable">
  	 <tr>
  		<td align="right" width="10%">项目总任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.mountainTotal" id="xmLandproj.landTotal"/>
    	</td>
  		<td align="right" width="15%">剩余待完成任务:</td>
    	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.mountainSurplus" id="xmLandproj.landSurplus"/>
    	</td>
  		<td align="right" width="10%">本年任务:</td>
   	 	<td>
    		<input type="text" class="dfinput4 required" name="xmLandproj.mountainCurrent" id="xmLandproj.landCurrent"/>
    	</td>
  	</tr>
  </table>
  <span style="display:block; margin-bottom:5px; margin-top:5px;"><strong>+地理信息</strong></span>
  <table width="100%" border="1" class="formTable">
  	 <tr>
    <td align="right" width="10%">项目建设地址：</td>
    <td>
    	<input type="text" name="xmLandproj.address" id="address" class="dfinput8 required" onblur="createGeo();"/>
    	<br/><font color="red">*写入地址后，鼠标移开，系统自动生成坐标</font>
    </td>
    <td align="right" width="10%">地图坐标：</td>
    <td align="left" width="32%">
		<input type="text"" name="xmLandproj.coordinate" id="coordinate" readonly="readonly" class="dfinput4 required" />
		<input type="hidden" name="xmLandproj.coordinatePly" id="coordinatePly" class="dfinput4 required" readonly="readonly"/>
	</td>
  </tr>
  	<tr >
    	<td align="right" height="400px">地图定位：<font color="red">*编辑完成后双击（面）保存</font></td>
    	<td colspan="3" id="allmap" style="overflow:hidden;zoom:1;position:relative;" >
			<div id="map" style="height:50%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
		</td>
  	</tr>
  </table>
  <div align="center" style="margin-top:5px;"><input type="submit" name="add_btn" class="scbtn" value="保存"/></div>
</form>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map.js"></script>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map_navigationControl.js"></script>
<script type="text/javascript" src="<%=path %>/js/baiduMap/map_PanoramaControl.js"></script>

<script type="text/javascript">

	function createGeo(_address){
	
		map.disableDoubleClickZoom();
		map.clearOverlays();
		var _address = document.getElementById("address").value;
		// 创建地址解析器实例
		var myGeo = new BMap.Geocoder();
		// 将地址解析结果显示在地图上,并调整地图视野
		myGeo.getPoint(_address, function(point){
			if (point) {
				map.centerAndZoom(point, 16);
				map.addOverlay(new BMap.Marker(point));
				
				document.getElementById("coordinate").value=point.lng + "," + point.lat;
				
				var polygon = new BMap.Polygon([
				new BMap.Point(point.lng,point.lat),
				new BMap.Point(Number((point.lng+0.001).toFixed(6)),point.lat),
				new BMap.Point(point.lng,Number((point.lat+0.001).toFixed(6))),
		
				], {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});  //创建多边形
				map.addOverlay(polygon);   //增加多边形
	
				polygon.enableEditing();
				polygon.setFillOpacity(0.3);
				polygon.setFillColor('green');
				polygon.enableMassClear();
		
				var thisPoint = "";
				polygon.addEventListener("dblclick", function showOverlayInfo2(e){   //添加监听事件,单击时触发
					var pa = polygon.getPath();
					var strpa = "";
					var pt;
					for(var i=0;i<pa.length;i++){
				
						if(i==pa.length-1){
							strpa += pa[i].lng+","+pa[i].lat;
						}else{
							strpa += pa[i].lng+","+pa[i].lat+"_";
						}
				
					pt = pa[i];
					
					thisPoint = pa[i].lng+","+pa[i].lat;
				}
				document.getElementById("coordinatePly").value=strpa;
				
				document.getElementById("coordinate").value=thisPoint;
			
				polygon.disableEditing();
				});
			}else{
			alert("您选择地址没有解析到结果!");
			}
		}, "贵港市港北区");
	}
	
	
</script>
</div>
</body>
</html>