<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>征地进度（列表）</li>
</ul>
</div>
<br>
	<script type="text/javascript">
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增征地信息',
    	content: '<%=path%>/biz/XmLandproj_openAdd.action?resUri=openAdd&thisResUri=list'
	});
}
	function openEdit(_id){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'立项信息修改',
    	content: '<%=path%>/biz/XmLandproj_openEdit.action?resUri=openEdit&thisXmLandprojId='+_id
	});
}

function excute(){
	$("#formXmLandproj").submit();
}

function findPage(_url){
	_url += "&resUri=list"
	$("#formXmLandproj").attr("action",_url);
	$("#formXmLandproj").submit();
}
	</script>
	
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmLandproj_openPage.action?resUri=list&thisLandType=01" target="rightFrame" <s:if test="%{thisLandType=='01'}">class="selected"</s:if>>分片区</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_openPage.action?resUri=list&thisLandType=02" target="rightFrame" <s:if test="%{thisLandType=='02'}">class="selected"</s:if>>片区外重点</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_openPage.action?resUri=list&thisLandType=03" target="rightFrame" <s:if test="%{thisLandType=='03'}">class="selected"</s:if>>片区外统筹</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_openPage.action?resUri=list&thisLandType=04" target="rightFrame" <s:if test="%{thisLandType=='04'}">class="selected"</s:if>>征拆大会战</a></li>
		</ul>
		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmLandproj_openMap.action?resUri=XmLandprojMap"><span><img height="30px" src="<%=path%>/images/du002.jpg" /></span>切换（地理信息）</a></li>
       </ul>
	</div>
	
	<form  id="formXmLandproj" action="<%=path %>/biz/XmLandproj_findExample.action?resUri=list" method="post">
    	<table width="100%" border="0" class="formTable">
      		<tr>
        		<td align="right">项目名称：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xmLandproj.name" id="xmLandproj.name" value="${xmLandproj.name }">
        		</td>
        		<td align="right">建设年份：</td>
        		<td>
        			<s:select name="xmLandproj.laYear" id="laYear" cssClass="dfinput4" list="yearList" listKey="key" listValue="value" />
        		</td>
        		<td align="right">项目地址：</td>
        		<td>
        			<input type="text" class="dfinput4 required" name="xmLandproj.address" id="xmLandproj.address" value="${xmLandproj.address }">
        		</td>
      		</tr>
    	</table>
  </form>
  
	<div class="tools" style="margin-top:5px;">
    	<ul class="toolbar">
  			<li class="click" onclick="openAdd();"><span><img src="<%=path%>/images/t01.png" /></span>新建征地</li>
  			<li class="click" onclick="excute();"><span><img src="<%=path%>/images/t02.png" /></span>查询</li>
  		</ul>
  		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmLandproj_findXmLandproj.action?resUri=findXmLandproj&thisLandType=${thisLandType}""><span><img src="<%=path%>/images/ico03.png" /></span><font color="blue"><strong>征地信息统计</strong></font></a></li>
       </ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px;table-layout:fixed;">
          <thead>
          	<tr>
          	  <th width="4%" rowspan="2">序号</th>
              <th rowspan="2" width="5%">建设年份</th>
              <th rowspan="2" width="10%">项目名称</th>
              <th rowspan="2" width="8%">批文情况</th>
              <th rowspan="2" width="8%">征地类型</th>
              <th colspan="3">土地征收（亩）</th>
              <th colspan="3">房屋征收（m2）</th>
              <th colspan="3">坟山迁移（座）</th>
              <th rowspan="2" width="8%">大额投资委通过情况</th>
              <th rowspan="2" width="6%">项目业主</th>
              <th rowspan="2" width="8%">项目地址</th>
              <th rowspan="2" width="3%">报告周期</th>
              <th rowspan="2" width="12%">操作</th>
            </tr>
            <tr>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="pageResult.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${laYear}</td>
                <td align="center" class="td-overflow" title="${name}" width="10%">${name}</td>
				<td align="center">${approval}</td>
				<td align="center">
					<c:if test='${fn:contains(landType,"01") }'>分片区；</c:if>
                	<c:if test='${fn:contains(landType,"02") }'>片区外重点；</c:if>
                	<c:if test='${fn:contains(landType,"03") }'>片区外统筹；</c:if>
                	<c:if test='${fn:contains(landType,"04") }'>征拆大会战；</c:if>
				</td>
                <td align="center"><font color="bule">${landTotal}</font></td>
                <td align="center"><font color="bule">${landSurplus}</font></td>
                <td align="center"><font color="bule">${landCurrent}</font></td>
                <td align="center"><font color="bule">${houseTotal}</font></td>
                <td align="center"><font color="bule">${houseSurplus}</font></td>
                <td align="center"><font color="bule">${houseCurrent}</font></td>
                <td align="center"><font color="bule">${mountainTotal}</font></td>
                <td align="center"><font color="bule">${mountainSurplus}</font></td>
                <td align="center"><font color="bule">${mountainCurrent}</font></td>
                <td align="center" class="td-overflow" title="${largeInvest}" width="8%">${largeInvest}</td>
                <td align="center" class="td-overflow" title="${owner}" width="6%">${owner}</td>
                <td align="center" class="td-overflow" title="${address}" width="8%">${address}</td>
                <td align="center">${cycle}天</td>
                <td align="center">
                	<a href="javascript:dialog('90%','90%','${name}实施进度','<%=path%>/biz/XmSchedule_findScheduleByNumInLandproj.action?resUri=findScheduleByNumInLandproj&thisLandNumber=${landNumber}', 'true', '5%', '5%');" ><font color="red"><strong>项目实施</strong></font></a>|
					<a href="javascript:dialog('90%','90%','${name}责任监管','<%=path%>/biz/XmDuty_openAdds.action?resUri=openAdds&thisXmNumber=${landNumber}', 'true', '5%', '5%');" ><font color="blue"><strong>责任监管</strong></font></a>|
					<a href="javascript:dialog('90%','90%','${name}指定项目','<%=path%>/biz/XmLandproj_addXmInfoPage.action?resUri=addXmInfoPage&thisLandNumber=${landNumber}', 'true', '5%', '5%');" ><font color="blue"><strong>指定项目</strong></font></a>|
                	<a href="javascript:void(0)" style="font-weight:bold;" onclick="openEdit('${id}');"><strong>修改</strong></a>|
					<a  href="<%=path%>/biz/XmLandproj_delete.action?resUri=XmLandprojMap&thisXmLandprojId=${id}" onClick="javascript:if(confirm('确定要删除此信息吗？')){alert('删除成功！');return true;}return false;"><strong>删除</strong></a>
                </td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>


