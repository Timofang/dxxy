<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/common/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>项目列表</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<%=path%>/third/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/third/jquery.idTabs.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third/layer/extend/layer.ext.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/dialog.js"></script>
	<script type="text/javascript" src="<%=path%>/js/core/dialog/enable.js"></script>
</head>
<body>
<div class="place">
<span>位置：</span>
<ul class="placeul">
<li><a href="<%=path %>/sys/login_view.action?view=index">首页</a></li>
<li>项目管理（列表）</li>
</ul>
</div>
	<script type="text/javascript">
    function openAdd(){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'新增项目立项信息填写',
    	content: '<%=path%>/biz/XmInfo_openAdd.action?resUri=openAdd&thisResUri=list'
	});
}
	function openEdit(_xmNumber){
	layer.open({
    	type: 2,
    	area: ['90%', '90%'],
    	fix: false, //不固定
    	maxmin: true,
		title:'立项信息修改',
    	content: '<%=path%>/biz/XmInfo_openEdit.action?resUri=openEdit&thisXmNumber='+_xmNumber
	});
}

	function excute(){
		var _url;
		_url="<%=path%>/biz/HelpRecord_downDetail.action";
		$("#helpRecForm").attr("action", _url);
		$("#helpRecForm").submit();
	}
	</script>
	<br>
	<div class="itab">
		<ul>
			<li><a href="<%=path %>/biz/XmLandproj_findXmLandproj.action?resUri=findXmLandproj&thisLandType=01" target="rightFrame" <s:if test="%{thisLandType=='01'}">class="selected"</s:if>>分片区</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_findXmLandproj.action?resUri=findXmLandproj&thisLandType=02" target="rightFrame" <s:if test="%{thisLandType=='02'}">class="selected"</s:if>>片区外重点</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_findXmLandproj.action?resUri=findXmLandproj&thisLandType=03" target="rightFrame" <s:if test="%{thisLandType=='03'}">class="selected"</s:if>>片区外统筹</a></li>
			<li><a href="<%=path %>/biz/XmLandproj_findXmLandproj.action?resUri=findXmLandproj&thisLandType=04" target="rightFrame" <s:if test="%{thisLandType=='04'}">class="selected"</s:if>>征拆大会战</a></li>
		</ul>
	</div>
	<div class="tools" style="margin-top:5px;">
  		<ul class="toolbar">
			<li><a href="<%=path%>/biz/XmLandproj_downViewXmInfo.action?thisLandType=${thisLandType}"><span><img src="<%=path%>/images/t06.png" /></span><strong>导出数据</strong></a></li>
		</ul>
  		<ul class="toolbar1">
        	<li><a href="<%=path %>/biz/XmLandproj_openPage.action?resUri=list&thisLandType=${thisLandType}"><span><img src="<%=path%>/images/ico04.png" /></span><font color="blue"><strong>项目管理列表</strong></font></a></li>
       </ul>
  	</div>
	 <table class="tablelist" id="editTable" width="100%" style="margin-top:2px; table-layout:fixed;">
          <thead>
            <tr>
          	  <th width="4%" rowspan="2">序号</th>
              <th rowspan="2" width="5%">建设年份</th>
              <th rowspan="2" width="10%">项目名称</th>
              <th rowspan="2" width="8%">批文情况</th>
              <th rowspan="2" width="8%">征地类型</th>
              <th colspan="3">土地征收（亩）</th>
              <th colspan="3">房屋征收（m2）</th>
              <th colspan="3">坟山迁移（座）</th>
              <th rowspan="2">土地征收总任务数（亩）</th>
              <th rowspan="2">房屋征收总任务数（m2）</th>
              <th rowspan="2">坟山迁移总任务数（座）</th>
              <th rowspan="2">土地征收完成率（%）</th>
              <th rowspan="2" width="8%">大额投资委通过情况</th>
              <th rowspan="2" width="6%">项目业主</th>
              <th rowspan="2" width="8%">项目地址</th>
              <th rowspan="2" width="3%">报告周期（天）</th>
              <th rowspan="2">是否超期</th>
            </tr>
            <tr>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
              <th>项目总任务</th>
              <th>剩余待完成任务</th>
              <th>本年任务</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator id="p2" value="resultViewXmLandproj.data" status="pp">
              <tr id="${id}">
                <td height="21" align="center"><s:property value="#pp.count"/></td>
                <td align="center">${laYear}</td>
                <td align="center" class="td-overflow" title="${name}" width="10%">${name}</td>
				<td align="center">${approval}</td>
				<td align="center">
					<c:if test='${fn:contains(landType,"01") }'>分片区；</c:if>
                	<c:if test='${fn:contains(landType,"02") }'>片区外重点；</c:if>
                	<c:if test='${fn:contains(landType,"03") }'>片区外统筹；</c:if>
                	<c:if test='${fn:contains(landType,"04") }'>征拆大会战；</c:if>
				</td>
                <td align="center"><font color="bule">${landTotal}</font></td>
                <td align="center"><font color="bule">${landSurplus}</font></td>
                <td align="center"><font color="bule">${landCurrent}</font></td>
                <td align="center"><font color="bule">${houseTotal}</font></td>
                <td align="center"><font color="bule">${houseSurplus}</font></td>
                <td align="center"><font color="bule">${houseCurrent}</font></td>
                <td align="center"><font color="bule">${mountainTotal}</font></td>
                <td align="center"><font color="bule">${mountainSurplus}</font></td>
                <td align="center"><font color="bule">${mountainCurrent}</font></td>
                <td align="center"><font color="blue">${landXmTask}</font></td>
                <td align="center"><font color="blue">${houseXmTask}</font></td>
                <td align="center"><font color="blue">${mountainXmTask}</font></td>
                <td align="center"><font color="blue">${landCRate}</font></td>
                <td align="center" class="td-overflow" title="${largeInvest}" width="8%">${largeInvest}</td>
                <td align="center" class="td-overflow" title="${owner}" width="6%">${owner}</td>
                <td align="center" class="td-overflow" title="${address}" width="8%">${address}</td>
                <td align="center">${cycle}</td>
                <td align="center">
					<s:if test="%{isOverdue==0}">未超期</s:if>
                	<s:if test="%{isOverdue==1}">超期</s:if>
				</td>
              </tr>
            </s:iterator>
          </tbody>
        </table>
        ${footer }
</body>
</html>


