package com.gxwzu.business.action.project;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.business.service.project.XmScheduleService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewXmInfo;
import com.gxwzu.sysVO.ViewXmSchedule;
import com.opensymphony.xwork2.Preparable;

/**
 * 项目进度Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoAction
 * <br>Date: 2017-8-20下午02:01:58
 * <br>log:
 */
public class XmScheduleAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = -1609343528003709003L;

	private static final Log log=LogFactory.getLog(XmScheduleAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmSchedule xmSchedule = new XmSchedule();
	
	private ViewXmInfo viewXmInfo;
	private XmLandproj xmLandproj = new XmLandproj();
	
	private ResponeJson dataJson;
	private List<XmSchedule> listXmSchedule;
	private Result<XmSchedule> pageResult;
	
	private List<ViewXmSchedule> listViewXmSchedule;
	private Result<ViewXmSchedule> resultViewXmSchedule;
	
	private String thisXmNumber;
	private String thisLandNumber;
	
	/*********************注入Service*********************/
	private XmInfoService xmInfoService;
	private XmLandprojService xmLandprojService;
	private XmScheduleService xmScheduleService;
	
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}
	public void setXmScheduleService(XmScheduleService xmScheduleService) {
		this.xmScheduleService = xmScheduleService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String findScheduleByNum() throws Exception{
		try {
			viewXmInfo = xmInfoService.viewInfo(thisXmNumber);
			
			resultViewXmSchedule = xmScheduleService.findXmScheduleByNum(thisXmNumber, null, null, getPage(), 10);
			
			footer=PageUtil.pageFooter(resultViewXmSchedule, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findScheduleByNumInLandproj() throws Exception{
		try {
			if(thisLandNumber != null && !"".equals(thisLandNumber)){
				xmLandproj.setLandNumber(thisLandNumber);
				List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(xmLandproj);
				if(listXmLandproj.size()>0){
					xmLandproj = listXmLandproj.get(0);
				}
				
				resultViewXmSchedule = xmScheduleService.findXmScheduleByNum(thisLandNumber,null,null, getPage(), 10);
				
				footer=PageUtil.pageFooter(resultViewXmSchedule, getRequest());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public ViewXmInfo getViewXmInfo() {
		return viewXmInfo;
	}

	public void setViewXmInfo(ViewXmInfo viewXmInfo) {
		this.viewXmInfo = viewXmInfo;
	}
	public XmSchedule getXmSchedule() {
		return xmSchedule;
	}
	public void setXmSchedule(XmSchedule xmSchedule) {
		this.xmSchedule = xmSchedule;
	}
	public List<XmSchedule> getListXmSchedule() {
		return listXmSchedule;
	}
	public void setListXmSchedule(List<XmSchedule> listXmSchedule) {
		this.listXmSchedule = listXmSchedule;
	}
	public Result<XmSchedule> getPageResult() {
		return pageResult;
	}
	public void setPageResult(Result<XmSchedule> pageResult) {
		this.pageResult = pageResult;
	}
	public List<ViewXmSchedule> getListViewXmSchedule() {
		return listViewXmSchedule;
	}
	public void setListViewXmSchedule(List<ViewXmSchedule> listViewXmSchedule) {
		this.listViewXmSchedule = listViewXmSchedule;
	}
	public XmLandproj getXmLandproj() {
		return xmLandproj;
	}
	public void setXmLandproj(XmLandproj xmLandproj) {
		this.xmLandproj = xmLandproj;
	}
	public String getThisLandNumber() {
		return thisLandNumber;
	}
	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}
	public Result<ViewXmSchedule> getResultViewXmSchedule() {
		return resultViewXmSchedule;
	}
	public void setResultViewXmSchedule(Result<ViewXmSchedule> resultViewXmSchedule) {
		this.resultViewXmSchedule = resultViewXmSchedule;
	}

}
