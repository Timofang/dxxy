package com.gxwzu.business.action.project;

import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.business.service.project.XmCapitalService;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.business.service.project.XmtolandService;
import com.gxwzu.core.context.ConfigBeanTmpl;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.model.ViewbaseModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.util.poi.ExcelSheetModel;
import com.gxwzu.core.util.poi.ExcelUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.core.web.message.ErrorMessage;
import com.gxwzu.sysVO.ViewToLandproj;
import com.gxwzu.sysVO.ViewXmInfo;
import com.gxwzu.system.model.user.User;
import com.opensymphony.xwork2.Preparable;

/**
 * 项目基本信息Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoAction
 * <br>Date: 2017-8-20下午02:01:58
 * <br>log:
 */
public class XmInfoAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 4310806321555327182L;

	private static final Log log=LogFactory.getLog(XmInfoAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmInfo xmInfo = new XmInfo();
	private ViewXmInfo viewXmInfo;
	
	private ResponeJson dataJson;
	private List<XmInfo> listXmInfo;
	private List<ViewToLandproj> listViewToLandproj;
	private Result<XmInfo> pageResult;
	private List<ViewXmInfo> listViewXmInfo;
	private Result<ViewXmInfo> resultViewXmInfo;
	
	private String[] strBudgetType;
	private String thisXmNumber;
	private String thisLandNumber;
	private Integer xmInfoId;
	private String thisBudgetType;
	private String thisResUri;
	
	//投资计划
	private String thisInvestSource;//资金来源
	private String thisInvestTotal;//总投资
	private String thisInvestPlan;//当年计划投资
	private String thisInvestComplete;//去年已完成投资
	
	private String thisToLandprojid;
	private String thisYear;
	
	private ViewbaseModel dataModel;
	private InputStream is;//文件流
	private ConfigBeanTmpl configBeanTmpl;//模板配置模板
	
	/*********************注入Service*********************/
	private XmInfoService xmInfoService;
	private XmCapitalService xmCapitalService;
	private XmtolandService xmtolandService;
	private XmLandprojService xmLandprojService;
	
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}

	public void setXmtolandService(XmtolandService xmtolandService) {
		this.xmtolandService = xmtolandService;
	}

	public void setXmCapitalService(XmCapitalService xmCapitalService) {
		this.xmCapitalService = xmCapitalService;
	}

	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	
	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		yearList.put("", "--请选择--");
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String openPage() throws Exception{
		try {
			xmInfo.setBudgetType(thisBudgetType);
			pageResult=xmInfoService.find(xmInfo, page, 10);
			footer=PageUtil.pageFooter(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openPoorPage() throws Exception{
		log.info(new Gson().toJson(xmInfo));
		try {
			xmInfo.setBudgetType(thisBudgetType);
			pageResult=xmInfoService.findPoor(xmInfo, page, 10);
			footer=PageUtil.pageFooter(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openMap() throws Exception{
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openAdd() throws Exception{
		return resUri;
	}
	
	public String openPoorAdd() throws Exception{
		return resUri;
	}
	
	public String add() throws Exception{
		try {
			User user=(User) getSession().getAttribute("userinfo");
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			String thisBudgetType = "";
			if(strBudgetType.length>0){
				for (int i = 0; i < strBudgetType.length; i++) {
					thisBudgetType += strBudgetType[i]+"_";
				}
				xmInfo.setBudgetType(thisBudgetType);
				
				xmInfo.setUserId(user.getId()+"");
				xmInfo.setUserName(user.getHelpName());
				xmInfo.setPortrait(user.getPortrait());
				xmInfo.setCreateTime(Timestamp.valueOf(dateStr));
				
				//构建资金来源情况
				XmCapital xmCapital = new XmCapital();
				xmCapital.setInvestTotal(thisInvestTotal);
				xmCapital.setInvestSource(thisInvestSource);
				xmCapital.setInvestPlan(thisInvestPlan);
				xmCapital.setInvestComplete(thisInvestComplete);
				
				xmInfoService.add(xmInfo, xmCapital);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	
	public String poorXmAdd() throws Exception{
		try {
			User user=(User) getSession().getAttribute("userinfo");
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			String thisBudgetType = "";
			if(strBudgetType!=null && strBudgetType.length>0){
				for (int i = 0; i < strBudgetType.length; i++) {
					thisBudgetType += strBudgetType[i]+"_";
				}
				xmInfo.setBudgetType(thisBudgetType);
				
				xmInfo.setUserId(user.getId()+"");
				xmInfo.setUserName(user.getHelpName());
				xmInfo.setPortrait(user.getPortrait());
				xmInfo.setCreateTime(Timestamp.valueOf(dateStr));
				
				//构建资金来源情况
				XmCapital xmCapital = new XmCapital();
				xmCapital.setInvestTotal(thisInvestTotal);
				xmCapital.setInvestSource(thisInvestSource);
				xmCapital.setInvestPlan(thisInvestPlan);
				xmCapital.setInvestComplete(thisInvestComplete);
				
				xmInfoService.addPoorXm(xmInfo, xmCapital);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openPoorPage();
	}
	
	
	public String delete()throws Exception{
		
		log.info("##删除立项");
		try{
			xmInfoService.delete(xmInfoId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return openPage();
	}
	
	public String deletePoor()throws Exception{
		
		log.info("##删除立项");
		try{
			xmInfoService.delete(xmInfoId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return openPoorPage();
	}
	
	
	public String deleteAjax()throws Exception{
		
		try{
			xmInfoService.delete(xmInfoId);
			
			dataJson=new ResponeJson();
			dataJson.setSuccess(true);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
		}catch (Exception e) {
			e.printStackTrace();
			dataJson.setSuccess(false);
			dataJson.setMsg(ErrorMessage.WEB_ERR_002);
		}
		return SUCCESS;
	}
	
	public String openEdit() throws Exception{
		try {
			viewXmInfo = xmInfoService.viewInfo(thisXmNumber);
			if(null == viewXmInfo.getStartTime() || "null".equals(viewXmInfo.getStartTime())){
				viewXmInfo.setStartTime("");
			}
			if(null == viewXmInfo.getCompletionTime() || "null".equals(viewXmInfo.getCompletionTime())){
				viewXmInfo.setCompletionTime("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	
	public String openPoorEdit() throws Exception{
		try {
			
			viewXmInfo = xmInfoService.viewInfo(thisXmNumber);
			log.info(new Gson().toJson(viewXmInfo));
			if(null == viewXmInfo.getStartTime() || "null".equals(viewXmInfo.getStartTime())){
				viewXmInfo.setStartTime("");
			}
			if(null == viewXmInfo.getCompletionTime() || "null".equals(viewXmInfo.getCompletionTime())){
				viewXmInfo.setCompletionTime("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String edit()throws Exception{
		try{
			//1.修改项目基本信息
			String thisBudgetType = "";
			for (int i = 0; i < strBudgetType.length; i++) {
				thisBudgetType += strBudgetType[i]+"_";
			}
			xmInfo.setBudgetType(thisBudgetType);
			
			if(xmInfo.getStartTime() == null || "".equals(xmInfo.getStartTime())){
				xmInfo.setStartTime(null);
			}
			if(xmInfo.getCompletionTime() == null || "".equals(xmInfo.getCompletionTime())){
				xmInfo.setCompletionTime(null);
			}
			if(xmInfo.getCreateTime() == null || "".equals(xmInfo.getCreateTime())){
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = sdf.format(new Date());
				xmInfo.setCreateTime(Timestamp.valueOf(dateStr));
			}
			
			xmInfoService.update(xmInfo);
			
			//2.修改资金投资信息
			XmCapital findXmCapital = new XmCapital();
			findXmCapital.setXmNumber(xmInfo.getXmNumber());
			List<XmCapital> listXmCapital = xmCapitalService.findByExample(findXmCapital);
			if(listXmCapital.size() != 0){
				findXmCapital = listXmCapital.get(0);
				
				findXmCapital.setInvestTotal(thisInvestTotal);
				findXmCapital.setInvestSource(thisInvestSource);
				findXmCapital.setInvestPlan(thisInvestPlan);
				findXmCapital.setInvestComplete(thisInvestComplete);
				
				xmCapitalService.update(findXmCapital);
			}else{
				XmCapital newXmCapital = new XmCapital();
				
				newXmCapital.setXmNumber(xmInfo.getXmNumber());
				newXmCapital.setInvestTotal(thisInvestTotal);
				newXmCapital.setInvestSource(thisInvestSource);
				newXmCapital.setInvestPlan(thisInvestPlan);
				newXmCapital.setInvestComplete(thisInvestComplete);
				
				xmCapitalService.save(newXmCapital);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	public String poorXmEdit()throws Exception{
		try{
			//1.修改项目基本信息
			String thisBudgetType = "";
			for (int i = 0; i < strBudgetType.length; i++) {
				thisBudgetType += strBudgetType[i]+"_";
			}
			xmInfo.setBudgetType(thisBudgetType);
			
			if(xmInfo.getStartTime() == null || "".equals(xmInfo.getStartTime())){
				xmInfo.setStartTime(null);
			}
			if(xmInfo.getCompletionTime() == null || "".equals(xmInfo.getCompletionTime())){
				xmInfo.setCompletionTime(null);
			}
			if(xmInfo.getCreateTime() == null || "".equals(xmInfo.getCreateTime())){
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = sdf.format(new Date());
				xmInfo.setCreateTime(Timestamp.valueOf(dateStr));
			}
			
			xmInfoService.update(xmInfo);
			
			//2.修改资金投资信息
			XmCapital findXmCapital = new XmCapital();
			findXmCapital.setXmNumber(xmInfo.getXmNumber());
			List<XmCapital> listXmCapital = xmCapitalService.findByExample(findXmCapital);
			if(listXmCapital.size() != 0){
				findXmCapital = listXmCapital.get(0);
				
				findXmCapital.setInvestTotal(thisInvestTotal);
				findXmCapital.setInvestSource(thisInvestSource);
				findXmCapital.setInvestPlan(thisInvestPlan);
				findXmCapital.setInvestComplete(thisInvestComplete);
				
				xmCapitalService.update(findXmCapital);
			}else{
				XmCapital newXmCapital = new XmCapital();
				
				newXmCapital.setXmNumber(xmInfo.getXmNumber());
				newXmCapital.setInvestTotal(thisInvestTotal);
				newXmCapital.setInvestSource(thisInvestSource);
				newXmCapital.setInvestPlan(thisInvestPlan);
				newXmCapital.setInvestComplete(thisInvestComplete);
				
				xmCapitalService.save(newXmCapital);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	
	public String addXmLandprojPage() {
		try {
			listViewToLandproj = xmLandprojService.toLandproj(thisXmNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String addXmLandproj() {
		try {
			Xmtoland xmtoland = new Xmtoland();
			xmtoland.setLandNumber(thisLandNumber);
			xmtoland.setXmNumber(thisXmNumber);
			xmtolandService.save(xmtoland);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return addXmLandprojPage();
	}
	
	public String delXmLandproj() {
		try {
			if(thisToLandprojid != null && !"".equals(thisToLandprojid)){
				Xmtoland delXmtoland = xmtolandService.findById(Integer.parseInt(thisToLandprojid));
				
				xmtolandService.remove(delXmtoland);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return addXmLandprojPage();
	}
	
	public String findExample() {
		try {
			pageResult=xmInfoService.find(xmInfo, page, 10);
			footer=PageUtil.pageFooterForm(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findPoorExample() {
		try {
			pageResult=xmInfoService.findPoor(xmInfo, page, 10);
			footer=PageUtil.pageFooterForm(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 获取所有项目坐标，用于地图显示
	 * @return
	 * @throws Exception
	 */
	public String findAllXmInfo()throws Exception{
		try{
			
			XmInfo findXmInfo = new XmInfo();
			findXmInfo.setBudgetType(thisBudgetType);
			dataJson=new ResponeJson();
			dataJson.setSuccess(true);
			List<XmInfo> listXmInfo = xmInfoService.findCoordinate(findXmInfo);
			dataJson.setObj(listXmInfo);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
		}catch (Exception e) {
			e.printStackTrace();
			dataJson.setSuccess(false);
			dataJson.setMsg(ErrorMessage.WEB_ERR_002);
		}
		return SUCCESS;
	}
	
	public String findAllPoorXmInfo()throws Exception{
		try{
			
			XmInfo findXmInfo = new XmInfo();
			findXmInfo.setBudgetType(thisBudgetType);
			dataJson=new ResponeJson();
			dataJson.setSuccess(true);
			List<XmInfo> listXmInfo = xmInfoService.findPoorXmCoordinate(findXmInfo);
			dataJson.setObj(listXmInfo);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
		}catch (Exception e) {
			e.printStackTrace();
			dataJson.setSuccess(false);
			dataJson.setMsg(ErrorMessage.WEB_ERR_002);
		}
		return SUCCESS;
	}
	
	public String findViewXmInfo() {
		try {
			resultViewXmInfo = xmInfoService.resultViewXmInfoByYear(thisYear, null, thisBudgetType,getPage(), 10);
			footer=PageUtil.pageFooter(resultViewXmInfo, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findPoorViewXmInfo() {
		try {
			resultViewXmInfo = xmInfoService.resultViewPoorXmInfoByYear(thisYear, null, thisBudgetType,getPage(), 10);
			footer=PageUtil.pageFooter(resultViewXmInfo, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 导出项目信息统计
	 * @return
	 * @throws Exception
	 */
	public String downViewXmInfo() throws Exception{
		try {
			dataModel= xmInfoService.viewXmInfoDownload(thisYear, null, thisBudgetType);
			dataModel.setTplid("info1");//模板编号
			dataModel.setTplName("项目信息统计报表");
			
			//控制结果集输出的列，有可能与查询结果集不一致，已经在数据模型中重新排序，默认与查询结果一致
			dataModel.sort();
			//数据模型导出到Excel
			ExcelSheetModel model = new ExcelSheetModel(0, dataModel.getTplName(),dataModel.getSortdata(), 2);
			
			String filePath=getRootPath()+"templete\\"+configBeanTmpl.getTmplInfo().get(dataModel.getTplid());
			
			is = ExcelUtils.export(model,filePath);
		
			filename=model.getSheetTitle()+".xlsx";
			filename = (new String((filename).getBytes(),"ISO-8859-1"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "download";
	}
	
	public String likeAll() {
		log.info("likeAll11 user...");
		dataJson=new ResponeJson();
		try{
			//读取模板编号
			String q=getRequest().getParameter("q");
			log.info("#检索所有的模板:q:"+q);
			String keyword =URLDecoder.decode(q,"UTF-8");
			log.info("#检索所有的模板:转码:"+keyword);
			List<AutoCompleteHttpModel> rs =xmInfoService.likeAll(keyword);
			dataJson.setObj(rs);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
			dataJson.setSuccess(true);
			
		}catch(Exception e){
			dataJson.setMsg(ErrorMessage.WEB_ERR_014);
			dataJson.setSuccess(false);
		}
		return SUCCESS;
	}
	
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public XmInfo getXmInfo() {
		return xmInfo;
	}

	public void setXmInfo(XmInfo xmInfo) {
		this.xmInfo = xmInfo;
	}

	public List<XmInfo> getListXmInfo() {
		return listXmInfo;
	}

	public void setListXmInfo(List<XmInfo> listXmInfo) {
		this.listXmInfo = listXmInfo;
	}

	public Result<XmInfo> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmInfo> pageResult) {
		this.pageResult = pageResult;
	}

	public String[] getStrBudgetType() {
		return strBudgetType;
	}

	public void setStrBudgetType(String[] strBudgetType) {
		this.strBudgetType = strBudgetType;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public Integer getXmInfoId() {
		return xmInfoId;
	}

	public void setXmInfoId(Integer xmInfoId) {
		this.xmInfoId = xmInfoId;
	}

	public String getThisInvestSource() {
		return thisInvestSource;
	}

	public void setThisInvestSource(String thisInvestSource) {
		this.thisInvestSource = thisInvestSource;
	}

	public String getThisInvestTotal() {
		return thisInvestTotal;
	}

	public void setThisInvestTotal(String thisInvestTotal) {
		this.thisInvestTotal = thisInvestTotal;
	}

	public String getThisInvestPlan() {
		return thisInvestPlan;
	}

	public void setThisInvestPlan(String thisInvestPlan) {
		this.thisInvestPlan = thisInvestPlan;
	}

	public String getThisInvestComplete() {
		return thisInvestComplete;
	}

	public void setThisInvestComplete(String thisInvestComplete) {
		this.thisInvestComplete = thisInvestComplete;
	}

	public ViewXmInfo getViewXmInfo() {
		return viewXmInfo;
	}

	public void setViewXmInfo(ViewXmInfo viewXmInfo) {
		this.viewXmInfo = viewXmInfo;
	}

	public String getThisBudgetType() {
		return thisBudgetType;
	}

	public void setThisBudgetType(String thisBudgetType) {
		this.thisBudgetType = thisBudgetType;
	}

	public String getThisLandNumber() {
		return thisLandNumber;
	}

	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}

	public List<ViewToLandproj> getListViewToLandproj() {
		return listViewToLandproj;
	}

	public void setListViewToLandproj(List<ViewToLandproj> listViewToLandproj) {
		this.listViewToLandproj = listViewToLandproj;
	}

	public String getThisToLandprojid() {
		return thisToLandprojid;
	}

	public void setThisToLandprojid(String thisToLandprojid) {
		this.thisToLandprojid = thisToLandprojid;
	}

	public String getThisResUri() {
		return thisResUri;
	}

	public void setThisResUri(String thisResUri) {
		this.thisResUri = thisResUri;
	}

	public List<ViewXmInfo> getListViewXmInfo() {
		return listViewXmInfo;
	}

	public void setListViewXmInfo(List<ViewXmInfo> listViewXmInfo) {
		this.listViewXmInfo = listViewXmInfo;
	}

	public String getThisYear() {
		return thisYear;
	}

	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}

	public Result<ViewXmInfo> getResultViewXmInfo() {
		return resultViewXmInfo;
	}

	public void setResultViewXmInfo(Result<ViewXmInfo> resultViewXmInfo) {
		this.resultViewXmInfo = resultViewXmInfo;
	}

	public ViewbaseModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(ViewbaseModel dataModel) {
		this.dataModel = dataModel;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public ConfigBeanTmpl getConfigBeanTmpl() {
		return configBeanTmpl;
	}

	public void setConfigBeanTmpl(ConfigBeanTmpl configBeanTmpl) {
		this.configBeanTmpl = configBeanTmpl;
	}

}
