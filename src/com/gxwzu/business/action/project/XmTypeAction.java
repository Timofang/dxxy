package com.gxwzu.business.action.project;

import com.gxwzu.business.model.XmType;
import com.gxwzu.business.service.project.XmTypeService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
/**
 * @Author: soldier
 * @Date: 2019/3/5 9:24
 * @Desc:
 */
public class XmTypeAction extends BaseAction implements ModelDriven<XmType> {

    private static final long serialVersionUID = -9115801324542289155L;
    protected final Log logger = LogFactory.getLog(getClass());

    /***********************实例化XmType******************************/
    private XmType model=new XmType();

    @Override
    public XmType getModel() {
        return model;
    }
    public void setModel(XmType model) {
        this.model = model;
    }

    /***********************注入Service:记得写get方法******************************/
    private XmTypeService xmTypeService;

    public void setXmTypeService(XmTypeService xmTypeService) {
        this.xmTypeService = xmTypeService;
    }

    /***********************声明参数******************************/
    private String thisResUri;
    private ResponeJson dataJson;//JSON格式
    private Result<XmType> pageResult;//分页
    private List<XmType> xmTypeList;

    private String thisTypeId;
    private String delTypeId;
    private String isExample = "0";

    /**
     * @author soldier（黄结）
     * 用于关闭layerweb弹层组件thisState
     */
    private String isClose;


    /**
     * @Author: soldier
     * @Date: 2019/3/1 22:32
     * @Desc: 每次页面请求都重新获取数据库‘项目类型表’数据
     */
    private void RequestAddlistXmType() {

        xmTypeList = xmTypeService.findAll();
        getRequest().setAttribute("xmTypeList", xmTypeList);
        
    }
    /********************方法类*************************/
    public String list() throws Exception{
        try{
            logger.info("##项目类型管理列表");
            System.out.println(resUri);
            pageResult= xmTypeService.find(model, getPage(), getRow());
            System.out.println(pageResult.getData().get(0));
            footer= PageUtil.pageFooter(pageResult, getRequest());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return resUri;
    }

//    public String findByExample() throws Exception{
//        try{
//            logger.info("##项目类型管理列表");
//            RequestAddlistXmType();
//            if ((model.getHelpName() != null && !"".equals(model.getHelpName()))
//                    || (model.getHelpUnit() != null
//                    && !"".equals(model.getHelpUnit()))) {
//
//                if(thisUnit!=null && !"".equals(thisUnit)){
//                    model.setHelpUnit(thisUnit);
//                }
//                listUser= UserService.findByExample(model);
//                isExample = "1";
//            }else{
//
//                if(thisUnit!=null && !"".equals(thisUnit)){
//                    model.setHelpUnit(thisUnit);
//                }
//                pageResult= UserService.find(model, getPage(), getRow());
//                footer=PageUtil.pageFooter(pageResult, getRequest());
//                isExample = "0";
//            }
//
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//        return view;
//    }

    public String openAdd() throws Exception{
        try{
            RequestAddlistXmType();

            //查询省，用于界面显示
        }catch (Exception e){
            e.printStackTrace();
        }
        return resUri;
    }

    /**
     * 新增项目类型
     * @return
     * @throws Exception
     */
    public String add()throws Exception{
        try{
            logger.info("##添加项目类型");
            xmTypeService.add(model);
        }catch (Exception e) {
            e.printStackTrace();
        }
//        return list();
        isClose = "1";
        return view;
    }

    public String openEdit() throws Exception{
        try {
            logger.info("## 打开用户项目类型修改界面");
            model = xmTypeService.findById(Integer.parseInt(thisTypeId));
            RequestAddlistXmType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resUri;
    }

    public String edit() throws Exception{
        try{
            logger.info("##修改项目类型信息");
            RequestAddlistXmType();
            xmTypeService.update(model);
        }catch (Exception e) {
            e.printStackTrace();
        }
        isClose="1";
        return resUri;
    }

    public String del() throws Exception{
        logger.info("##删除项目类型信息");
        xmTypeService.del(Integer.parseInt(delTypeId));
        RequestAddlistXmType();
        return list();
    }


    /**
     * @author soldier（黄结）
     * 用于关闭layerweb弹层组件
     */
    public String getIsClose() {
        return isClose;
    }

    public void setIsClose(String isClose) {
        this.isClose = isClose;
    }

    /****************参数的getter和setter方法****************/

    public List<XmType> getXmTypeList() {
        return xmTypeList;
    }
    public void setXmTypeList(List<XmType> xmTypeList) {
        this.xmTypeList = xmTypeList;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Log getLogger() {
        return logger;
    }

    public XmTypeService getXmTypeService() {
        return xmTypeService;
    }

    public String getThisResUri() {
        return thisResUri;
    }

    public void setThisResUri(String thisResUri) {
        this.thisResUri = thisResUri;
    }

    public ResponeJson getDataJson() {
        return dataJson;
    }
    public void setDataJson(ResponeJson dataJson) {
        this.dataJson = dataJson;
    }
    public Result<XmType> getPageResult() {
        return pageResult;
    }
    public void setPageResult(Result<XmType> pageResult) {
        this.pageResult = pageResult;
    }
    public String getThisTypeId() {
        return thisTypeId;
    }
    public void setThisTypeId(String thisTypeId) {
        this.thisTypeId = thisTypeId;
    }
    public String getDelTypeId() {
        return delTypeId;
    }
    public void setDelTypeId(String delTypeId) {
        this.delTypeId = delTypeId;
    }
    public String getIsExample() {
        return isExample;
    }
    public void setIsExample(String isExample) {
        this.isExample = isExample;
    }
}
