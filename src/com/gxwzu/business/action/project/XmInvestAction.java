package com.gxwzu.business.action.project;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmInvest;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmInvestService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewXmInfo;
import com.opensymphony.xwork2.Preparable;

/**
 * 投资进度Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoAction
 * <br>Date: 2017-8-20下午02:01:58
 * <br>log:
 */
public class XmInvestAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = -616150508271211247L;

	private static final Log log=LogFactory.getLog(XmInvestAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmInvest xmInvest = new XmInvest();
	
	private ViewXmInfo viewXmInfo;
	private XmLandproj xmLandproj = new XmLandproj();
	
	private ResponeJson dataJson;
	private List<XmInvest> listXmInvest;
	private Result<XmInvest> pageResult;
	
	private String thisXmNumber;
	private String thisLandNumber;
	
	/*********************注入Service*********************/
	private XmInfoService xmInfoService;
	private XmLandprojService xmLandprojService;
	private XmInvestService xmInvestService;
	
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}

	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}

	public void setXmInvestService(XmInvestService xmInvestService) {
		this.xmInvestService = xmInvestService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String findInvestByNum() throws Exception{
		try {
			viewXmInfo = xmInfoService.viewInfo(thisXmNumber);
			
			XmInvest findXmInvest = new XmInvest();
			findXmInvest.setXmNumber(thisXmNumber);
			listXmInvest = xmInvestService.findByExample(findXmInvest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findInvestByNumInLandproj() throws Exception{
		try {
			if(thisLandNumber != null && !"".equals(thisLandNumber)){
				xmLandproj.setLandNumber(thisLandNumber);
				List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(xmLandproj);
				if(listXmLandproj.size()>0){
					xmLandproj = listXmLandproj.get(0);
				}
				
				XmInvest findXmInvest = new XmInvest();
				findXmInvest.setXmNumber(thisLandNumber);
				listXmInvest = xmInvestService.findByExample(findXmInvest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public ViewXmInfo getViewXmInfo() {
		return viewXmInfo;
	}

	public void setViewXmInfo(ViewXmInfo viewXmInfo) {
		this.viewXmInfo = viewXmInfo;
	}

	public XmInvest getXmInvest() {
		return xmInvest;
	}

	public void setXmInvest(XmInvest xmInvest) {
		this.xmInvest = xmInvest;
	}

	public List<XmInvest> getListXmInvest() {
		return listXmInvest;
	}

	public void setListXmInvest(List<XmInvest> listXmInvest) {
		this.listXmInvest = listXmInvest;
	}

	public Result<XmInvest> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmInvest> pageResult) {
		this.pageResult = pageResult;
	}

	public String getThisLandNumber() {
		return thisLandNumber;
	}

	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}

	public XmLandproj getXmLandproj() {
		return xmLandproj;
	}

	public void setXmLandproj(XmLandproj xmLandproj) {
		this.xmLandproj = xmLandproj;
	}

}
