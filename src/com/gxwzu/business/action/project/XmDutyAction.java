package com.gxwzu.business.action.project;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.service.project.XmDutyService;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewXmInfo;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.Preparable;

/**
 * 责任单位/责任人Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoAction
 * <br>Date: 2017-8-20下午02:01:58
 * <br>log:
 */
public class XmDutyAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 9104733691858858875L;

	private static final Log log=LogFactory.getLog(XmDutyAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmDuty xmDuty = new XmDuty();
	private XmLandproj xmLandproj = new XmLandproj();
	
	private ViewXmInfo viewXmInfo;
	
	private ResponeJson dataJson;
	private List<XmDuty> listXmDuty;
	private Result<XmDuty> pageResult;
	
	private String thisXmNumber;
	private String thisLandNumber;
	
	private String thisXmDutyId;
	
	/*********************注入Service*********************/
	private XmInfoService xmInfoService;
	private XmLandprojService xmLandprojService;
	private XmDutyService xmDutyService;
	private UserService userService;
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}
	public void setXmDutyService(XmDutyService xmDutyService) {
		this.xmDutyService = xmDutyService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String openAdds() {
		try {
			XmDuty findXmDuty = new XmDuty();
			findXmDuty.setXmNumber(thisXmNumber);
			listXmDuty = xmDutyService.findByExample(findXmDuty);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String add() throws Exception {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			xmDuty.setCreateTime(Timestamp.valueOf(dateStr));
			xmDuty.setUserId(user.getId());
			xmDuty.setUserName(user.getHelpName());
			
			//获取责任人电话
			User people = userService.findById(xmDuty.getPeopleId());
			xmDuty.setPeopleTel(people.getHelpTelephone());
			
			xmDutyService.save(xmDuty);
			
			
			//修改项目/征地的责任人/工作组组长
			if(xmDuty.getXmNumber().contains("zd")){
				if("01".equals(xmDuty.getType())){
					
					XmLandproj thisXmLandproj = new XmLandproj();
					thisXmLandproj.setLandNumber(thisXmNumber);
					List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(thisXmLandproj);
					
					if(listXmLandproj.size()>0){
						thisXmLandproj = listXmLandproj.get(0);
						
						thisXmLandproj.setUserId(people.getId()+"");
						thisXmLandproj.setUserName(people.getHelpName());
						thisXmLandproj.setPortrait(people.getPortrait());
						
						xmLandprojService.update(thisXmLandproj);
					}
				}
			}else{
				if("06".equals(xmDuty.getType())){
					XmInfo thisXmInfo = new XmInfo();
					thisXmInfo.setXmNumber(thisXmNumber);
					List<XmInfo> listXmInfo = xmInfoService.findByExample(thisXmInfo);
					if(listXmInfo.size()>0){
						thisXmInfo = listXmInfo.get(0);
						
						thisXmInfo.setUserId(people.getId()+"");
						thisXmInfo.setUserName(people.getHelpName());
						thisXmInfo.setPortrait(people.getPortrait());
						
						xmInfoService.update(thisXmInfo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openAdds();
	}
	
	public String findDutyByNum() throws Exception{
		try {
			viewXmInfo = xmInfoService.viewInfo(thisXmNumber);
			
			XmDuty findXmDuty = new XmDuty();
			findXmDuty.setXmNumber(thisXmNumber);
			listXmDuty = xmDutyService.findByExample(findXmDuty);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findDutyByNumInLandproj() throws Exception{
		try {
			if(thisLandNumber != null && !"".equals(thisLandNumber)){
				xmLandproj.setLandNumber(thisLandNumber);
				List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(xmLandproj);
				if(listXmLandproj.size()>0){
					xmLandproj = listXmLandproj.get(0);
				}
				
				XmDuty findXmDuty = new XmDuty();
				findXmDuty.setXmNumber(thisLandNumber);
				listXmDuty = xmDutyService.findByExample(findXmDuty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String del() throws Exception {
		try {
			if(thisXmDutyId != null && !"".equals(thisXmDutyId)){
				XmDuty delXmDuty = xmDutyService.findById(Integer.parseInt(thisXmDutyId));
				if(delXmDuty != null){
					//1.清除项目中的责任人
					XmInfo findXmInfo = new XmInfo();
					findXmInfo.setUserId(delXmDuty.getPeopleId()+"");
					findXmInfo.setXmNumber(findXmInfo.getXmNumber());
					List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
					for (int i = 0; i < listXmInfo.size(); i++) {
						findXmInfo = listXmInfo.get(i);
						findXmInfo.setUserId(null);
						findXmInfo.setUserName(null);
						findXmInfo.setPortrait(null);
						xmInfoService.update(findXmInfo);
					}
					//2.清除征地中的责任人
					XmLandproj findXmLandproj = new XmLandproj();
					findXmLandproj.setUserId(delXmDuty.getPeopleId()+"");
					findXmLandproj.setLandNumber(findXmInfo.getXmNumber());
					List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(findXmLandproj);
					for (int i = 0; i < listXmLandproj.size(); i++) {
						findXmLandproj = listXmLandproj.get(i);
						findXmLandproj.setUserId(null);
						findXmLandproj.setUserName(null);
						findXmLandproj.setPortrait(null);
						xmLandprojService.update(findXmLandproj);
					}
					//3.删除责任人
					xmDutyService.remove(delXmDuty);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openAdds();
	}
	
	public String delInLandproj() throws Exception {
		try {
			if(thisXmDutyId != null && !"".equals(thisXmDutyId)){
				XmDuty delXmDuty = xmDutyService.findById(Integer.parseInt(thisXmDutyId));
				if(delXmDuty != null){
					xmDutyService.remove(delXmDuty);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return findDutyByNumInLandproj();
	}
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public ViewXmInfo getViewXmInfo() {
		return viewXmInfo;
	}

	public void setViewXmInfo(ViewXmInfo viewXmInfo) {
		this.viewXmInfo = viewXmInfo;
	}

	public XmDuty getXmDuty() {
		return xmDuty;
	}

	public void setXmDuty(XmDuty xmDuty) {
		this.xmDuty = xmDuty;
	}

	public List<XmDuty> getListXmDuty() {
		return listXmDuty;
	}

	public void setListXmDuty(List<XmDuty> listXmDuty) {
		this.listXmDuty = listXmDuty;
	}

	public Result<XmDuty> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmDuty> pageResult) {
		this.pageResult = pageResult;
	}
	public String getThisXmDutyId() {
		return thisXmDutyId;
	}
	public void setThisXmDutyId(String thisXmDutyId) {
		this.thisXmDutyId = thisXmDutyId;
	}
	public String getThisLandNumber() {
		return thisLandNumber;
	}
	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}
	public XmLandproj getXmLandproj() {
		return xmLandproj;
	}
	public void setXmLandproj(XmLandproj xmLandproj) {
		this.xmLandproj = xmLandproj;
	}

}
