package com.gxwzu.business.action.project;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gxwzu.business.model.Attachment;
import com.gxwzu.business.model.Instructions;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.model.XmType;
import com.gxwzu.business.service.project.AttachmentService;
import com.gxwzu.business.service.project.InstructionsService;
import com.gxwzu.business.service.project.MaterialsService;
import com.gxwzu.business.service.project.UnitService;
import com.gxwzu.business.service.project.XmChildService;
import com.gxwzu.business.service.project.XmService;
import com.gxwzu.business.service.project.XmTypeService;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.util.UidUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;

public class XmAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 4310806321555327182L;

	private static final Log log=LogFactory.getLog(XmAction.class);
	
	/*********************参数列表*********************/

	private String thisResUri;
	private String unitId;
	private Xm xm = new Xm();
	private Instructions instructions = new Instructions();
	
	private User user = new User();
	private String thisUserHelpName;
	private String thisUnitId;
	//指派单位/个人状态符
	private int receiverStatus;
	// 项目集合
	private Result<Xm> pageResult;
	// 子项目集合
	private Result<XmChild> childPageResult;
	private Result result;
	// 用户集合
	private Result<User> userPageResult;
	private List<Xm> xmList;
	private List<XmChild> xcList;
	private List<XmType> typeList;
	// 附件集合
	private List<Attachment> attachmentList;
	// 任务批示集合
	private List<Instructions> instructionsList;
	//教研室集合
	private List<Unit> unitList;
	//教职工集合
	private List<User> userList;
	//操作项目编号
	private String thisXmNumber;
	private String thisReceiver;
	private String thisXmChildNumber;
	private String thisUserName;
	private Integer thisAcceptstatus;
	private String thisUnitParent;
	private String xmChildParentId;
	private XmChild xmChild = new XmChild();
	private File[] myfile;
	private String[] myfileFileName; 
	
	//教研室编号集合
	private String[] unitIdList;
	//教职工工号集合
	private String[] userIdList;

	private String optionValue;  //记录XmChild_myProjectList.jsp当前任务类型
	
	@Autowired
	private UnitService unitService;
	@Autowired
	private XmService xmService;
	@Autowired
	private UserService userService;
	@Autowired
	private XmTypeService xmTypeService;
	
	@Autowired
	private XmChildService xmChildService;
	@Autowired
	private MaterialsService materialsService;
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private InstructionsService instructionsService;
	
	private String xmid;
	private String fliename;
	private String xmname;
	
	//完成质量
	private String userName;
	private String xmNumber;
	private String degree;
	
	/*********************数据处理加工方法*********************/
	
	 /**
	  * 院任务批示
	  * @author SunYi
	  * @Date 2019年4月7日下午10:46:43
	  * @return String
	  */
	public String xmInstructions() throws Exception {
		try {
			// 接收页面传来的值
			String xmNumber = instructions.getXmNumber();
			String InstructionsPerson = instructions.getInstructionsPerson();
			String InstructionsContent = instructions.getInstructionsContent();
			// 获取系统当前时间
			Timestamp time = new Timestamp(new Date().getTime());
			// 实例化实体类对象
			instructions = new Instructions(xmNumber, InstructionsPerson, InstructionsContent, time);
			// 调用业务层方法
			instructionsService.xmInstructionsAdd(instructions);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openMyIssueList();
	}
	
	 /**
	  * 教研室任务批示
	  * @author SunYi
	  * @Date 2019年4月7日下午10:46:43
	  * @return String
	  */
	public String xmChildInstructions() throws Exception {
		try {
			// 接收页面传来的值
			String InstructionsPerson = instructions.getInstructionsPerson();
			String InstructionsContent = instructions.getInstructionsContent();
			// 获取系统当前时间
			Timestamp time = new Timestamp(new Date().getTime());
			// 用逗号分割页面传过来的子任务编号(逗号后面需加一个空格)
			String[] xmNumber = instructions.getXmNumber().split(", ");
			for(int i=0; i<xmNumber.length; i++) {
				thisXmNumber = xmNumber[i];
				// 实例化实体类对象
				instructions = new Instructions(thisXmNumber, InstructionsPerson, InstructionsContent, time);
				// 调用业务层方法
				instructionsService.xmInstructionsAdd(instructions);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openMyIssueList();
	}
	
	
	/**
	 * @Description 添加院下发任务完成质量
	 * @author 莫东林
	 * @date 2019年3月19日下午12:48:17
	 * void
	 */
	public void degree(){
		xmChildService.saveQuality(userName, xmNumber, degree);
	}
	
	/**
	 * @Description 添加教研室下发任务完成质量
	 * @author 莫东林
	 * @date 2019年3月19日下午12:48:17
	 * void
	 */
	public void degrees(){
		xmChildService.saveQualitys(userName, xmNumber, degree);
	}
	
	/**
	 * 统计所有任务的完成情况
	 * @author SunYi
	 * @Date 2019年3月18日下午9:40:31
	 * @return String
	 */
	public String xmStatistics() throws Exception{
		try {
			// 查出个人的所有任务信息
			userList=xmChildService.getXmStatistics();
			// 把数据放进Result集合，到前台进行分页显示
			userPageResult = new Result<User>(userList, page, 10);
			footer=PageUtil.pageFooter(userPageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 获取项目需要上传的文件信息
	 * @author SunYi
	 * @Date 2019年3月11日下午11:06:37
	 * @return String
	 */
	public String fileUpload() throws Exception{
		try{
			if(thisAcceptstatus == 1) {
				// 主项目表文件信息
				xm = xmService.getByXmNumber(thisXmNumber);
				Map request=(Map)ActionContext.getContext().get("request");
				String fileName = xm.getFileName();
				String[] fileNames = fileName.split(",");
				request.put("fileName",fileNames);
				
			}else if(thisAcceptstatus == 0) {
				// 子项目表文件信息 
				xmChild = xmChildService.getFileByXmNumber(thisXmChildNumber);
				Map request=(Map)ActionContext.getContext().get("request");
				String fileName = xmChild.getXmChild().getFileName();
				String[] fileNames = fileName.split(",");
				request.put("fileName",fileNames);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 通过任务编号获取任务信息
	 * @author SunYi
	 * @Date 2019年3月7日下午9:49:49
	 * @return String
	 */
	/*public String getXmByXmNumber() throws Exception{
		try {
			xm = xmService.getByXmNumber(thisXmNumber);
			getSession().setAttribute("xm", xm);
			String unitId = (String) getSession().getAttribute("unitId");
			String username = (String) getSession().getAttribute("username");
			userList = userService.findUserByUnitId(unitId, username);
			getSession().setAttribute("userList", userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}*/
	
	/**
	* @Description: 打开任务下发页面
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
	public String openProjectIssue(){
		try {
			xmChild = xmChildService.getChildByXmUser(thisXmNumber, unitId);
			getSession().setAttribute("xmChild", xmChild);
//			String unitId = (String) getSession().getAttribute("unitId");
			this.unitId = unitId;
			String username = (String) getSession().getAttribute("username");
			userList = userService.findUserByUnitId(unitId, username);
			getSession().setAttribute("userList", userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	

	/**
	 * 删除院任务以及任务对应的所有关联信息
	 * @author SunYi
	 * @Date 2019年4月19日下午9:31:33
	 * @return String
	 */
	public String delete() throws Exception{
		try {
			// 执行删除方法
			xmService.delete(xm.getXmNumber());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	
	/**
	* @Description: 院任务详情
	* @Author 莫东林
	* @date 2019年3月5日下午8:05:18
	* @return String
	* @throws
	 */
	public String openDetail() throws Exception{
		try {
			// 获取任务信息
			xm = xmService.getByXmNumber(thisXmNumber);
			// 获取附件信息
			attachmentList = attachmentService.getAttachmentByXmNuber(thisXmNumber);
			// 获取批示信息
			instructionsList = instructionsService.getInstructionsByXmNumber(thisXmNumber);
			// 获取当前登录用户信息
			thisUserHelpName = ((String) getSession().getAttribute("GG_USERID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	

	/**
	* @Description: 教研室下发任务详情
	* @Author 莫东林
	* @date 2019年3月5日下午8:05:18
	* @return String
	* @throws
	 */
	public String openUnitIssueDetail() throws Exception{
		try {
			// 获取子任务信息
			xmChild = xmChildService.getByXmUnitChildNumber(thisXmNumber, thisUnitId);
			// 获取附件信息
			attachmentList = attachmentService.getAttachmentByXmNuber(thisXmNumber);
			// 获取批示信息
			instructionsList = instructionsService.getInstructionsByXmNumber(thisXmNumber);
			// 获取当前登录用户信息
			thisUserHelpName = ((String) getSession().getAttribute("GG_USERID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	* @Description: 教研室下发任务详情X
	* @Author 莫东林
	* @date 2019年3月5日下午8:05:18
	* @return String
	* @throws
	 */
	public String openUnitIssueDetailX() throws Exception{
		try {
			// 获取子任务信息
			xmChild = xmChildService.getByXmUnitChildNumberX(thisXmNumber, thisUnitId);
			// 获取附件信息
			attachmentList = attachmentService.getAttachmentByXmNuber(thisXmNumber);
			// 获取批示信息
			instructionsList = instructionsService.getInstructionsByXmNumber(thisXmNumber);
			// 获取当前登录用户信息
			thisUserHelpName = ((String) getSession().getAttribute("GG_USERID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	* @Description: 教研室我的下发任务详情XX
	* @Author 莫东林
	* @date 2019年3月5日下午8:05:18
	* @return String
	* @throws
	 */
	public String openUnitIssueDetailXX() throws Exception{
		try {
			// 获取子任务信息
			xmChild = xmChildService.getByXmUnitChildNumberXX(thisXmNumber, thisUnitId);
			// 获取附件信息
			attachmentList = attachmentService.getAttachmentByXmNuber(thisXmNumber);
			// 遍历得到子任务编号
			//for(int i=0; i<xmChild.getXmChildList().size(); i++) {
			// 教研室发批示给一条任务下的多个人，每个人收到的批示内容一致，所以只需查询第一个人的批示内容即可
			thisXmChildNumber = xmChild.getXmChildList().get(0).getXmChildNumber();
			//}
			// 根据子任务编号获取教研室批示信息
			instructionsList = instructionsService.getInstructionsByXmNumber(thisXmChildNumber);
			// 获取当前登录用户信息
			thisUserHelpName = ((String) getSession().getAttribute("GG_USERID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	* @Description: 教研室下发的任务详情
	* @Author 莫东林
	* @date 2019年3月5日下午8:05:18
	* @return String
	* @throws
	 */
	public String openUnitIssueDetails() throws Exception{
		try {
			// 获取子任务信息
			xmChild = xmChildService.getByXmChildNumber(thisXmNumber);
			// 获取附件信息
			attachmentList = attachmentService.getAttachmentByXmNuber(thisXmNumber);
			// 获取批示信息
			instructionsList = instructionsService.getInstructionsByXmNumber(thisXmNumber);
			// 获取当前登录用户信息
			thisUserHelpName = ((String) getSession().getAttribute("GG_USERID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	
	
	/**
	 * @Description 用逗号组装数组中的字符后返回
	 * @author 莫东林
	 * @param arrayList
	 * @date 2019年3月3日上午1:12:31
	 * @return
	 * String
	 */
	public String getStrByArray(String[] arrayList){
		String s = "";
		for(int i = 0; i < arrayList.length; i++){
			s += arrayList[i] + ",";
		}
		return s;
	}
	
	
	/*********************Action方法体*********************/
	
	/**
	 * 员工任务-查看具体一个子项目任务信息
	 * @author 叶城廷
	 * @version2019.03.03
	 * @return
	 */
	public String openViewUserTask() {
		//从session中获取该员工信息
		User currentUser = (User) getSession().getAttribute("userinfo");
		//根据项目编号查询该员工在子项目的具体详情信息
		return resUri;
	}
	
	/**
	 * 员工任务-查询员工个人的所有任务
	 * @author 叶城廷
	 * @version2019.03.03
	 * @return
	 */
	public String openUserTask() {
		//从session中获取该员工信息
		User currentUser = (User) getSession().getAttribute("userinfo");
		//查询该员工所有子项目任务
		childPageResult=xmChildService.selUserTask(currentUser, page, 10);
		return resUri;
	}

	/**
	 * 根据教研室获取对应的项目信息
	 * @author SunYi
	 * @Date 2019年3月2日上午11:00:37
	 * @return String
	 */
	public String selXmByUnit() throws Exception{
		try {
			pageResult=xmService.selXmByUnit(xm, page, 10);
			footer=PageUtil.pageFooter(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 
	 * @Description 打开任务列表页面
	 * @author 莫东林
	 * @date 2019年3月12日上午1:03:41
	 * String
	 */
	public String openProjectList(){
		try {
			childPageResult=xmChildService.find(xmChild, page, 10);
			footer=PageUtil.pageFooter(childPageResult, getRequest());
			// 获取所有项目类型
			typeList = xmTypeService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	/**
	 * @Description 条件查询
	 * @author soldier
	 */
	public String findExample() {
		try {
			//用户选择任务类型
			if (xm.getType() == null || "".equals(xm.getType())) {
				xm.setType(null);
				optionValue = null;  //修改当前任务类型
			} else if (xm.getType() != null) {
				String xmType = xm.getType();
				optionValue = xmType;  //记录当前任务类型
			}

			if (xm.getName() != null || !"".equals(xm.getName())) {
				xm.setType(null);
				optionValue = null;  //修改当前任务类型
			}
			xmChild.setXm(xm);  //接受页面的xm实体类数据
			openProjectList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 
	 * @Description 打开我的任务列表
	 * @author 莫东林
	 * @date 2019年3月12日上午1:03:41
	 * String
	 */
	public String openMyProjectList(){
		try {
			childPageResult=xmChildService.findByUser(xmChild, page, 10);
			footer=PageUtil.pageFooter(childPageResult, getRequest());

			/**
			 * @Description 用于页面遍历
			 * @author 黄结
			 */
			List<User> users = userService.findAll();
			getRequest().setAttribute("users", users);
			List<Unit> units= unitService.findAll();
			getRequest().setAttribute("units", units);

			// 获取所有项目类型
			typeList = xmTypeService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 
	 * @Description 打开我的下发列表
	 * @author 莫东林
	 * @date 2019年3月12日上午1:03:41
	 * String
	 */
	public String openMyIssueList(){
		try {
			int userType = (Integer) getSession().getAttribute("leaderTypeId");
			//若果登陆用户为院领导
			if(userType == 1){
				pageResult=xmService.findIssueByUser(xm, page, 10);
				footer=PageUtil.pageFooter(pageResult, getRequest());
			}
			//如果登陆用户为教研室负责人
			else if(userType == 2){
				childPageResult=xmChildService.findIssueByUser(xmChild, page, 10);
				footer=PageUtil.pageFooter(childPageResult, getRequest());
			}

			/**
			 * @Description 用于页面遍历
			 * @author 黄结
			 */
			List<User> users = userService.findAll();
	        getRequest().setAttribute("users", users);
	        List<Unit> units= unitService.findAll();
            getRequest().setAttribute("units", units);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 
	* @Description: 打开教研室任务下发页面
	* @Author 莫东林
	* @date 2019年3月20日下午5:26:50
	* @return String
	* @throws
	 */
	public String openUnitProjectIssue(){
		try {
			String username = (String) getSession().getAttribute("username");
			Unit unit = unitService.getByUserId(username);
			
			userList = userService.findUserByUnitId(unit.getUnitId(), username);
			getSession().setAttribute("userList", userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
		
	}
	
	
	/**
	 * 教研室发布任务
	 * @author SunYi
	 * @Date 2019年3月2日上午11:00:49
	 * @return String
	 * @throws Exception 
	 */
	public String unitXmAdd() throws Exception {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			Timestamp ts = new Timestamp(new Date().getTime());
			//分割各文件名
			String[] fileNameList = xmChild.getFileName().split(",");
			//指派任务给教职工
			if(receiverStatus == 1){
					//新增子项目信息
					for(int i = 0; i < userIdList.length; i++){
						XmChild child = new XmChild();
						//随机生成材料编号
						child.setMaterialsId(UidUtils.UID());
						child.setXmNumber(xm.getXmNumber());
						child.setUnitParent(unitId);
						child.setUserName(userIdList[i]);
						child.setXmChildNumber(xmChild.getXmNumber() + userIdList[i]);
						child.setXmChildName(xmChild.getXmChildName());
						child.setStatus(0);
						child.setIssue(0);
						child.setAcceptstatus(0);
						child.setDegree(xmChild.getDegree());
						child.setPublishTime(ts);
						xmChildService.add(child);
						
						// 附件上传
						boolean flag=attachmentService.saveAttachment(myfile,myfileFileName,child.getXmChildNumber());
						//业务流程完毕
						if(flag) {
							
						//业务流程出错
						}else {
							
						}
						//根据所需提交文件信息构建材料表
						for(int y = 0; y < fileNameList.length; y++){
							Materials materials = new Materials();
							materials.setXmId(child.getXmChildNumber());
							materials.setMaterialsId(child.getMaterialsId());
							materials.setFilename(fileNameList[y]);
							materials.setUserId(userIdList[i]);
							materialsService.save(materials);
						}
					}
					//更变上级项目信息
					XmChild c = xmChildService.getChildByXmUser(thisXmNumber, unitId);
					c.setIssue(1);
					c.setRequires(xmChild.getRequires());
					c.setFileCount(xmChild.getFileCount());
					c.setFileName(xmChild.getFileName());
					c.setPublishTime(ts);
					c.setDeadLine(xmChild.getDeadLine());
					xmChildService.updateXmChild(c);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return selXmByUnit();
	}
	
	
	
	public String unitXmAdd2() throws Exception {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			Timestamp ts = new Timestamp(new Date().getTime());
			//分割各文件名
			String[] fileNameList = xmChild.getFileName().split(",");
			//指派任务给教职工
		
					//新增子项目信息
					for(int i = 0; i < userIdList.length; i++){
						XmChild child = new XmChild();
						//随机生成材料编号
						child.setMaterialsId(UidUtils.UID());
						child.setXmChildNumber(xmService.getMaxXmNumber2() + user.getJobNumber() + user.getJobNumber());
						//child.setXmNumber(xm.getXmNumber());
						child.setUnitParent(unitId);
						child.setUserName(userIdList[i]);
						//child.setXmChildNumber(xmChild.getXmNumber() + userIdList[i]);
						child.setXmChildName(xmChild.getXmChildName());
						child.setStatus(0);
						child.setIssue(0);
						child.setAcceptstatus(0);
						child.setDegree(xmChild.getDegree());
						child.setPublishTime(ts);
						xmChildService.add(child);
						
						//根据所需提交文件信息构建材料表
						for(int y = 0; y < fileNameList.length; y++){
							Materials materials = new Materials();
							materials.setXmId(child.getXmChildNumber());
							materials.setMaterialsId(child.getMaterialsId());
							materials.setFilename(fileNameList[y]);
							materials.setUserId(userIdList[i]);
							materialsService.save(materials);
						}
					}
					//更变上级项目信息
					XmChild c = xmChildService.getChildByXmUser(thisXmNumber, unitId);
					c.setIssue(1);
					c.setRequires(xmChild.getRequires());
					c.setFileCount(xmChild.getFileCount());
					c.setFileName(xmChild.getFileName());
					c.setPublishTime(ts);
					c.setDeadLine(xmChild.getDeadLine());
					xmChildService.updateXmChild(c);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return selXmByUnit();
	}
	
	
	/**
	 * 
	 * @Description 院发布项目
	 * @author 莫东林
	 * @return
	 * @throws Exception
	 * String
	 */
	public String add() throws Exception{
		try {
			User user=(User) getSession().getAttribute("userinfo");
			xm.setXmNumber(xmService.getMaxXmNumber() + user.getHelpUserName());
			// 保存附件
			boolean flag=attachmentService.saveAttachment(myfile,myfileFileName,xm.getXmNumber());
			//业务流程完毕
			if(flag) {
				
			//业务流程出错
			}else {
				
			}
			Timestamp ts = new Timestamp(new Date().getTime());
			xm.setPublishTime(ts);
			xm.setUsername(user.getHelpUserName());
			xm.setStatus(0);
			//分割各文件名
			String[] fileNameList = xm.getFileName().split(",");
			//指派给教研室
			if(receiverStatus == 0){
				xm.setReceiverUnit(getStrByArray(unitIdList));
				for(int i = 0; i < unitIdList.length; i++){
				//构建项目副表
					XmChild xmChild = new XmChild();
					List<Unit> unitList = unitService.findAllById(unitIdList[i]);
					//随机生成材料编号
					xmChild.setMaterialsId(UidUtils.UID());
					xmChild.setUserName(unitList.get(0).getUnitPerson());
					xmChild.setXmNumber(xm.getXmNumber());
					xmChild.setUnitId(unitIdList[i]);
					xmChild.setStatus(0);
					xmChild.setIssue(0);
					xmChild.setAcceptstatus(1);
					xmChild.setXmName(xm.getName());
					xmChild.setDegree(xm.getDegree());
					xmChild.setPublishTime(ts);
					xmChildService.add(xmChild);
					
					//根据所需提交文件信息构建材料表
					for(int y = 0; y < fileNameList.length; y++){
						Materials materials = new Materials();
						materials.setXmId(xm.getXmNumber());
						materials.setMaterialsId(xmChild.getMaterialsId());
						materials.setFilename(fileNameList[y]);
						materials.setUnitId(unitIdList[i]);
						materialsService.save(materials);
					}
					
				}
			}
			//指派个人
			else if(receiverStatus == 1){
				xm.setReceiver(getStrByArray(userIdList));
				for(int i = 0; i < userIdList.length; i++){
					XmChild xmChild = new XmChild();
					//随机生成材料编号
					xmChild.setMaterialsId(UidUtils.UID());
					xmChild.setXmNumber(xm.getXmNumber());
					xmChild.setUserName(userIdList[i]);
					xmChild.setStatus(0);
					xmChild.setIssue(0);
					xmChild.setAcceptstatus(1);
					xmChild.setXmName(xm.getName());
					xmChild.setPublishTime(ts);
					xmChildService.add(xmChild);
					
					//根据所需提交文件信息构建材料表
					for(int y = 0; y < fileNameList.length; y++){
						Materials materials = new Materials();
						materials.setXmId(xm.getXmNumber());
						materials.setMaterialsId(xmChild.getMaterialsId());
						materials.setFilename(fileNameList[y]);
						materials.setUserId(userIdList[i]);
						materialsService.save(materials);
					}
				}
			}
			xmService.add(xm);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	public String openAdd() throws Exception{
		// 查询所有教研室
		unitList=xmService.selUnit();
		// 查询所有教职工
		userList = userService.findAll();
		// 查询所有项目类型
		typeList = xmTypeService.findAll();
		return resUri;
	}

	/**
	 * 获取所有项目信息
	 * @author SunYi
	 * @Date 2019年4月19日下午4:21:40
	 * @return String
	 */
	public String openPage() throws Exception{
		try {
			pageResult=xmService.find(xm, page, 10);
			footer=PageUtil.pageFooter(pageResult, getRequest());

			List<User> users = userService.findAll();
	        getRequest().setAttribute("users", users);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	/**
	 * 打开学院项目编辑界面
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return String
	 */
	public String openEdit(){
		Map request=(Map)ActionContext.getContext().get("request");
		String unitId = (String) getSession().getAttribute("unitId");
		String username = (String) getSession().getAttribute("username");
		userList = userService.findUserByUnitId(unitId, username);
		getSession().setAttribute("userlist", userList);
		try {
			xm = xmService.getByXmNumber(thisXmNumber);
			request.put("xm",xm);
			//查询所有教研室
			unitList=xmService.selUnit();
			//查询所有教职工
			userList = userService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	/**
	 * 修改学院项目
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return String
	 * @throws Exception 
	 */
	public String Edit() throws Exception{
		xmService.delete(thisXmNumber);
		add();
		getSession().setAttribute("msg", "修改成功");	
		return openPage();
	}
	/**
	 * 查询个人任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return String
	 * @throws Exception 
	 */
	public String statisticsDetails(){
		/*院项目统计列表*/
		List yxm=xmService.xmstatisticsDetails(thisUserName);
		/*教研室项目统计列表*/
		List unxm=xmChildService.unitstatisticsDetails(thisUserName);
		yxm.addAll(unxm);
		userList=yxm;
		userPageResult = new Result<User>(userList, page, 10);
		footer=PageUtil.pageFooter(userPageResult, getRequest());
		return resUri;
	}
	
	@Override
	public void prepare() throws Exception {

	}
	
	
	
	
	
	public Xm getXm() {
		return xm;
	}
	public void setXm(Xm xm) {
		this.xm = xm;
	}
	public Result<Xm> getPageResult() {
		return pageResult;
	}
	public void setPageResult(Result<Xm> pageResult) {
		this.pageResult = pageResult;
	}
	public List<Unit> getUnitList() {
		return unitList;
	}
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	public void setUnitList(List<Unit> unitList) {
		this.unitList = unitList;
	}
	public String[] getUnitIdList() {
		return unitIdList;
	}
	public void setUnitIdList(String[] unitIdList) {
		this.unitIdList = unitIdList;
	}
	public String[] getUserIdList() {
		return userIdList;
	}
	public void setUserIdList(String[] userIdList) {
		this.userIdList = userIdList;
	}
	public int getReceiverStatus() {
		return receiverStatus;
	}
	public void setReceiverStatus(int receiverStatus) {
		this.receiverStatus = receiverStatus;
	}
	public String getThisResUri() {
		return thisResUri;
	}
	public void setThisResUri(String thisResUri) {
		this.thisResUri = thisResUri;
	}
	public Result<XmChild> getChildPageResult() {
		return childPageResult;
	}
	public void setChildPageResult(Result<XmChild> childPageResult) {
		this.childPageResult = childPageResult;
	}
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public String getXmid() {
		return xmid;
	}
	public XmChild getXmChild() {
		return xmChild;
	}
	public void setXmid(String xmid) {
		this.xmid = xmid;
	}
	public String getFliename() {
		return fliename;
	}
	public void setFliename(String fliename) {
		this.fliename = fliename;
	}
	public String getXmname() {
		return xmname;
	}
	public void setXmname(String xmname) {
		this.xmname = xmname;
	}
	public void setXmChild(XmChild xmChild) {
		this.xmChild = xmChild;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getThisUserName() {
		return thisUserName;
	}
	public void setThisUserName(String thisUserName) {
		this.thisUserName = thisUserName;
	}
	public String getThisReceiver() {
		return thisReceiver;
	}
	public void setThisReceiver(String thisReceiver) {
		this.thisReceiver = thisReceiver;
	}
	public String getThisXmChildNumber() {
		return thisXmChildNumber;
	}
	public void setThisXmChildNumber(String thisXmChildNumber) {
		this.thisXmChildNumber = thisXmChildNumber;
	}
	public Integer getThisAcceptstatus() {
		return thisAcceptstatus;
	}
	public void setThisAcceptstatus(Integer thisAcceptstatus) {
		this.thisAcceptstatus = thisAcceptstatus;
	}
	public String getThisUnitParent() {
		return thisUnitParent;
	}
	public void setThisUnitParent(String thisUnitParent) {
		this.thisUnitParent = thisUnitParent;
	}
	public List<Xm> getXmList() {
		return xmList;
	}
	public void setXmList(List<Xm> xmList) {
		this.xmList = xmList;
	}
	public List<XmChild> getXcList() {
		return xcList;
	}
	public void setXcList(List<XmChild> xcList) {
		this.xcList = xcList;
	}
	public String getXmChildParentId() {
		return xmChildParentId;
	}
	public void setXmChildParentId(String xmChildParentId) {
		this.xmChildParentId = xmChildParentId;
	}
	public List<XmType> getTypeList() {
		return typeList;
	}
	public void setTypeList(List<XmType> typeList) {
		this.typeList = typeList;
	}
	public String getThisUnitId() {
		return thisUnitId;
	}
	public void setThisUnitId(String thisUnitId) {
		this.thisUnitId = thisUnitId;
	}
	public Result<User> getUserPageResult() {
		return userPageResult;
	}
	public void setUserPageResult(Result<User> userPageResult) {
		this.userPageResult = userPageResult;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getXmNumber() {
		return xmNumber;
	}
	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public File[] getMyfile() {
		return myfile;
	}
	public void setMyfile(File[] myfile) {
		this.myfile = myfile;
	}
	public String[] getMyfileFileName() {
		return myfileFileName;
	}
	public void setMyfileFileName(String[] myfileFileName) {
		this.myfileFileName = myfileFileName;
	}
	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public Instructions getInstructions() {
		return instructions;
	}
	public void setInstructions(Instructions instructions) {
		this.instructions = instructions;
	}
	public List<Instructions> getInstructionsList() {
		return instructionsList;
	}
	public void setInstructionsList(List<Instructions> instructionsList) {
		this.instructionsList = instructionsList;
	}
	public String getThisUserHelpName() {
		return thisUserHelpName;
	}
	public void setThisUserHelpName(String thisUserHelpName) {
		this.thisUserHelpName = thisUserHelpName;
	}
	public String getOptionValue() {
		return optionValue;
	}
	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}
}
