package com.gxwzu.business.action.project;

import com.gxwzu.business.model.Unit;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.system.model.user.User;
import com.gxwzu.business.service.project.UnitService;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import java.io.*;
import java.util.List;

/**
 * @Author: soldier
 * @Date: 2019/3/1 22:32
 * @Desc: 定义jsp页面元素属性name值，然后再action层定义该属性，
 *         同时定义该属性的get、set方法，这样就能将页面表单元素值获得，还能将action层处理后的值传递到页面。
 *         如果是对象的这种注入、获取，action层需要实现ModelDriven<Ojbect>接口，这样就能避免jsp中元素属性用对象.属性的写法。
 */
public class UnitAction extends BaseAction implements ModelDriven<Unit> {

    private static final long serialVersionUID = -2151851388077210187L;

    protected final Log logger = LogFactory.getLog(getClass());

    /***********************实例化Unit******************************/
    private Unit model=new Unit();
    private String jobNumber;  //保存负责人工号及Unit_add.jsp所有被选择员工工号
    private String jobNumber_1;  //获取Unit_add.jsp所有被选择员工工号||User_edit.jsp所有被选择员工工号

    @Override
    public Unit getModel() {
        return model;
    }
    public void setModel(Unit model) {
        this.model = model;
    }
    public String getJobNumber_1() {
        return jobNumber_1;
    }
    public void setJobNumber_1(String jobNumber_1) {
        this.jobNumber_1 = jobNumber_1;
    }

    /***********************注入Service******************************/
    private UnitService UnitService;
    private UserService UserService;
    private XmInfoService xmInfoService;

    public void setXmInfoService(XmInfoService xmInfoService) {
        this.xmInfoService = xmInfoService;
    }
    public void setUnitService(UnitService UnitService) {
        this.UnitService = UnitService;
    }

    /***********************声明参数******************************/
    private String thisResUri;
    private ResponeJson dataJson;//JSON格式
    private List<Unit> unitsList;
    private List<User> usersList;
    private Result<Unit> pageResult;//分页

    private String thisState;
    private String thisUnitId;
    private String delUnitId;

    private InputStream is;
    private String isExample = "0";
    private String isOneAction;

    /**************************文件上传参数Start**************************/
    private String title;//封装文件标题请求参数的属性
    private File upload;//封装上传文件域的属性
    private String uploadContentType;//封装上传文件类型属性
    private String uploadFileName;//封装上传文件名属性
    private String savePath;//直接在struts.xml文件中配置的属性
    /**************************文件上传参数End**************************/

    /**
     * @Author: soldier
     * @Date: 2019/3/1 22:32
     * @Desc: 每次页面请求都重新获取数据库‘用户’表中有工号的用户数据
     */
    private void RequestAddlistUser() {


        unitsList= UnitService.findAll();
        getRequest().setAttribute("unitsList", unitsList);



        usersList = UserService.findHaveJobNumberUser();
        getRequest().setAttribute("usersList", usersList);
    }
    /********************方法类*************************/
    public String list() throws Exception{

        try {
            logger.info("##教研室列表--------");
            RequestAddlistUser();
            pageResult=UnitService.find(model, page, 10);
            footer=PageUtil.pageFooter(pageResult, getRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resUri;

    }

    /**
     * @author soldier
     * @date 19-3-16下午10:53
     * 检验教研室信息
     * @throws Exception
     */
    public String checkUnitId() throws Exception{
        logger.info("## 校验教研室编号及教研室名称    ");
        PrintWriter out=getResponse().getWriter();

        unitsList=UnitService.findByExample(model);
        /**
         * 教研室编号和名称不能重复
         */
        if(null!=unitsList && 0<unitsList.size()){//不可用
            out.print("false");
        }else{//可用
            out.print("true");
        }
        out.flush();
        out.close();
        return null;
    }

    /**
     * 根据信息查找 --不可用
     * @return
     * @throws Exception
     */
    public String findByExample() throws Exception{
        try{
            logger.info("##查询教研室");

            RequestAddlistUser();
            if (model.getUnitName() != null && !"".equals(model.getUnitName())) {


                unitsList= UnitService.findByExample(model);
                isExample = "1";
            }else{


                pageResult= UnitService.find(model, getPage(), getRow());
                footer=PageUtil.pageFooter(pageResult, getRequest());
                isExample = "0";
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public String openAdd() throws Exception{
        try{
            unitsList= UnitService.findAll();
            getRequest().setAttribute("unitsList", unitsList);

            List<User> usersLists = UserService.findJobNumberUser();
            getRequest().setAttribute("usersLists", usersLists);

            usersList = UserService.findHaveJobNumberUser();
            getRequest().setAttribute("usersList", usersList);
        }catch (Exception e){
            e.printStackTrace();
        }
        return resUri;
    }

    /**
     * 新增教研室
     * @return
     * @throws Exception
     */
    public String add()throws Exception{
        try{
            UnitService.add(model);
            String unitId = model.getUnitId();

            User user = UserService.findByHelpUserName(model.getUnitPerson());
            System.out.println(model.getUnitPerson());
            //把教研室负责人加入人员列表
            jobNumber = user.getJobNumber()+","+jobNumber_1;
            System.out.println(user.getJobNumber());
            String[] jobNumbers = jobNumber.split(",");  //获取所有员工工号
            for (int i=0; i<jobNumbers.length; i++) {

                User thisUser = UserService.findByJobNumber(jobNumbers[i]);
                //如果为负责人
                System.out.println(jobNumbers[i]);
                if(jobNumbers[i].equals(user.getJobNumber())){
                    thisUser.setUserType(thisUser.getUserType() + "_4");
                    thisUser.setLeaderTypeId(2);
                    System.out.println(thisUser);
                }
                thisUser.setUnitId(thisUser.getUnitId()+","+unitId);  //把新增的这个教研室的id拼接进用户的所属教研室
                UserService.update(thisUser);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return list() ;
    }

    public String openEdit() throws Exception{
        try {
            logger.info("## 打开教研室修改界面");
            model = UnitService.findById(thisUnitId);
            RequestAddlistUser();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resUri;
    }

    public String edit() throws Exception{
        try{
            logger.info("##修改教研室信息");

            RequestAddlistUser();
            /**
             * 修改教研室信息
             */
            UnitService.update(model);

            /**
             * 修改教研室成员信息
             */
            if (jobNumber_1 != null) {
                String[] allJobNumber = jobNumber_1.split(",");
                User editUser = new User();
                for (int i=0; i<allJobNumber.length; i++) {
                    editUser = UserService.findByJobNumber(allJobNumber[i]);

                    /**
                     * 为教研室添加新成员
                     */
                    if (!editUser.getUnitId().contains(model.getUnitId())) {

                        logger.info(editUser.getHelpName()+"原先并不属于教研室："+model.getUnitId());
                        editUser.setUnitId(editUser.getUnitId()+","+model.getUnitId());
                        UserService.update(editUser);
                    }
                }
            }


            /**
             * 删除教研室被取消勾选的成员
             */
            usersList = UserService.findHaveJobNumberUser();
            //找出被取消勾选的成员
            for (int i = 0; i < usersList.size(); i++) {
                //1、找出被勾选之外的成员及所属教研室不为空
                if(usersList.get(i).getUnitId() != null &&
                        !jobNumber_1.contains(usersList.get(i).getJobNumber())) {
                    //2、找出属于当前教研室的成员并剔除
                    if (usersList.get(i).getUnitId().contains(model.getUnitId())) {
                        usersList.get(i).setUnitId(StringUtils.remove(usersList.get(i).getUnitId(), model.getUnitId()));
                        UserService.update(usersList.get(i));
                    }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        thisState="1";
        return resUri;
    }

    public String del() throws Exception{
        logger.info("##删除教研室");
        UnitService.del(delUnitId);

        /**
         * 该教研室的成员删除这个教研室掉教研室id
         */
        usersList = UserService.findHaveJobNumberUser();
        //找出这个教研室的成员
        for (int i = 0; i < usersList.size(); i++) {
            //1、找出这个教研室的成员且所属教研室不为空
            if(usersList.get(i).getUnitId() != null &&
                    usersList.get(i).getUnitId().contains(delUnitId)) {
                logger.info("删除教研室"+delUnitId+"并去掉了成员:"+usersList.get(i).getHelpName());
                usersList.get(i).setUnitId(StringUtils.remove(usersList.get(i).getUnitId(), delUnitId));
                UserService.update(usersList.get(i));
            }
        }

        RequestAddlistUser();
        return list();
    }

    /****************参数的getter和setter方法****************/
    public ResponeJson getDataJson() {
        return dataJson;
    }
    public void setDataJson(ResponeJson dataJson) {
        this.dataJson = dataJson;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Log getLogger() {
        return logger;
    }

    public UnitService getUnitService() {
        return UnitService;
    }

    public XmInfoService getXmInfoService() {
        return xmInfoService;
    }

    public UserService getUserService() {
        return UserService;
    }

    public void setUserService(com.gxwzu.system.service.user.UserService userService) {
        UserService = userService;
    }

    public List<Unit> getUnitsList() {
        return unitsList;
    }

    public void setUnitsList(List<Unit> unitsList) {
        this.unitsList = unitsList;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public Result<Unit> getPageResult() {
        return pageResult;
    }

    public void setPageResult(Result<Unit> pageResult) {
        this.pageResult = pageResult;
    }

    public String getThisState() {
        return thisState;
    }

    public void setThisState(String thisState) {
        this.thisState = thisState;
    }

    public String getThisUnitId() {
        return thisUnitId;
    }

    public void setThisUnitId(String thisUnitId) {
        this.thisUnitId = thisUnitId;
    }

    public String getDelUnitId() {
        return delUnitId;
    }

    public void setDelUnitId(String delUnitId) {
        this.delUnitId = delUnitId;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public String getIsExample() {
        return isExample;
    }

    public void setIsExample(String isExample) {
        this.isExample = isExample;
    }

    public String getIsOneAction() {
        return isOneAction;
    }

    public void setIsOneAction(String isOneAction) {
        this.isOneAction = isOneAction;
    }

    public String getThisResUri() {
        return thisResUri;
    }

    public void setThisResUri(String thisResUri) {
        this.thisResUri = thisResUri;
    }

    /****************上传的getter和setter方法Start****************/
    //接受struts.xml文件配置的方法
    public void setSavePath(String value) {
        this.savePath = value;
    }

    //返回上传文件保存位置
    public String getSavePath() throws Exception{
        return ServletActionContext.getServletContext().getRealPath(savePath);
    }

    public String getTitle() {
        return (this.title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public File getUpload() {
        return (this.upload);
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    public String getUploadContentType() {
        return (this.uploadContentType);
    }

    public void setUploadContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public String getUploadFileName() {
        return (this.uploadFileName);
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }


}

