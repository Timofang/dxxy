package com.gxwzu.business.action.project;

import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.business.service.project.XmCapitalService;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.business.service.project.XmtolandService;
import com.gxwzu.core.context.ConfigBeanTmpl;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.model.ViewbaseModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.util.poi.ExcelSheetModel;
import com.gxwzu.core.util.poi.ExcelUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.core.web.message.ErrorMessage;
import com.gxwzu.sysVO.ViewToXmInfo;
import com.gxwzu.sysVO.ViewXmInfo;
import com.gxwzu.sysVO.ViewXmLandproj;
import com.gxwzu.system.model.user.User;
import com.opensymphony.xwork2.Preparable;

/**
 * 征地项目Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoAction
 * <br>Date: 2017-8-20下午02:01:58
 * <br>log:
 */
public class XmLandprojAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 4310806321555327182L;

	private static final Log log=LogFactory.getLog(XmLandprojAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmLandproj xmLandproj = new XmLandproj();
	private ResponeJson dataJson;
	private List<XmLandproj> listXmLandproj;
	private Result<XmLandproj> pageResult;
	
	private String thisXmLandprojId;
	
	private List<ViewToXmInfo> listViewToXmInfo;
	
	private Result<ViewXmLandproj> resultViewXmLandproj;
	
	private String thisLandNumber;
	private String thisXmNumber;
	private String[] strLandType;
	
	private String thisLandType;
	
	private String thisToLandprojid;
	private String thisResUri;
	
	private String thisYear;
	
	private ViewbaseModel dataModel;
	private InputStream is;//文件流
	private ConfigBeanTmpl configBeanTmpl;//模板配置模板
	
	/*********************注入Service*********************/
	private XmLandprojService xmLandprojService;
	private XmInfoService xmInfoService;
	private XmtolandService xmtolandService;
	
	public void setXmtolandService(XmtolandService xmtolandService) {
		this.xmtolandService = xmtolandService;
	}

	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}

	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		yearList.put("", "--请选择--");
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String openPage() throws Exception{
		try {
			xmLandproj.setLandType(thisLandType);
			pageResult=xmLandprojService.find(xmLandproj, page, 10);
			footer=PageUtil.pageFooter(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resUri;
	}
	
	public String openMap() throws Exception{
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openAdd() throws Exception{
		return resUri;
	}
	
	public String add() throws Exception{
		try {
			User user=(User) getSession().getAttribute("userinfo");
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			String thisLandType = "";
			if(strLandType.length>0){
				for (int i = 0; i < strLandType.length; i++) {
					thisLandType += strLandType[i]+"_";
				}
				xmLandproj.setLandType(thisLandType);
				
				xmLandproj.setUserId(user.getId()+"");
				xmLandproj.setUserName(user.getHelpName());
				xmLandproj.setPortrait(user.getPortrait());
				xmLandproj.setCreateTime(Timestamp.valueOf(dateStr));
					
				xmLandprojService.add(xmLandproj);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}
	
	public String delete()throws Exception{
		
		log.info("##删除立项");
		try{
			if(thisXmLandprojId != null && !"".equals(thisXmLandprojId)){
				xmLandprojService.delete(Integer.parseInt(thisXmLandprojId));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return openPage();
	}
	
	public String deleteAjax()throws Exception{
		
		try{
			if(thisXmLandprojId != null && !"".equals(thisXmLandprojId)){
				xmLandprojService.delete(Integer.parseInt(thisXmLandprojId));
				
				dataJson=new ResponeJson();
				dataJson.setSuccess(true);
				dataJson.setMsg(ErrorMessage.WEB_SUC_000);
			}
		}catch (Exception e) {
			e.printStackTrace();
			dataJson.setSuccess(false);
			dataJson.setMsg(ErrorMessage.WEB_ERR_002);
		}
		return SUCCESS;
	}
	
	public String openEdit() throws Exception{
		try {
			if(thisXmLandprojId != null && !"".equals(thisXmLandprojId)){
				xmLandproj=xmLandprojService.get(XmLandproj.class, Integer.parseInt(thisXmLandprojId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String edit() throws Exception{
		try{
			String thisLandType = "";
			for (int i = 0; i < strLandType.length; i++) {
				thisLandType += strLandType[i]+"_";
			}
			xmLandproj.setLandType(thisLandType);
			
			xmLandprojService.update(xmLandproj);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openPage();
	}

	/**
	 * 获取所有项目坐标，用于地图显示
	 * @return
	 * @throws Exception
	 */
	public String findAllXmLandproj()throws Exception{
		try{
			
			XmLandproj findXmLandproj = new XmLandproj();
			dataJson=new ResponeJson();
			dataJson.setSuccess(true);
			List<XmLandproj> listXmLandproj = xmLandprojService.findCoordinate(findXmLandproj);
			dataJson.setObj(listXmLandproj);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
		}catch (Exception e) {
			e.printStackTrace();
			dataJson.setSuccess(false);
			dataJson.setMsg(ErrorMessage.WEB_ERR_002);
		}
		return SUCCESS;
	}
	
	public String addXmInfoPage() {
		try {
			listViewToXmInfo = xmInfoService.toXmInfo(thisLandNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String addXmInfo() {
		try {
			Xmtoland xmtoland = new Xmtoland();
			xmtoland.setLandNumber(thisLandNumber);
			xmtoland.setXmNumber(thisXmNumber);
			xmtolandService.save(xmtoland);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return addXmInfoPage();
	}
	
	public String delXmInfo() {
		try {
			if(thisToLandprojid != null && !"".equals(thisToLandprojid)){
				Xmtoland delXmtoland = xmtolandService.findById(Integer.parseInt(thisToLandprojid));
				
				xmtolandService.remove(delXmtoland);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return addXmInfoPage();
	}
	
	public String findExample() {
		try {
			pageResult=xmLandprojService.find(xmLandproj, page, 10);
			footer=PageUtil.pageFooterForm(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String findXmLandproj() {
		try {
			if(thisYear == null || "".equals(thisYear)){
				DateFormat sdf = new SimpleDateFormat("yyyy");
				thisYear = sdf.format(new Date());
			}
			resultViewXmLandproj = xmLandprojService.resultXmLandproj(thisYear, null, thisLandType, page, 10);
			footer=PageUtil.pageFooter(resultViewXmLandproj, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 导出土地信息统计
	 * @return
	 * @throws Exception
	 */
	public String downViewXmInfo() throws Exception{
		try {
			dataModel= xmLandprojService.xmLandprojDownload(thisYear, null, thisLandType);
			dataModel.setTplid("landproj1");//模板编号
			dataModel.setTplName("土地征收信息统计报表");
			
			//控制结果集输出的列，有可能与查询结果集不一致，已经在数据模型中重新排序，默认与查询结果一致
			dataModel.sort();
			//数据模型导出到Excel
			ExcelSheetModel model = new ExcelSheetModel(0, dataModel.getTplName(),dataModel.getSortdata(), 3);
			
			String filePath=getRootPath()+"templete\\"+configBeanTmpl.getTmplInfo().get(dataModel.getTplid());
			
			is = ExcelUtils.export(model,filePath);
		
			filename=model.getSheetTitle()+".xlsx";
			filename = (new String((filename).getBytes(),"ISO-8859-1"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "download";
	}
	
	public String likeAll() {
		log.info("likeAll11 user...");
		dataJson=new ResponeJson();
		try{
			//读取模板编号
			String q=getRequest().getParameter("q");
			log.info("#检索所有的模板:q:"+q);
			String keyword =URLDecoder.decode(q,"UTF-8");
			log.info("#检索所有的模板:转码:"+keyword);
			List<AutoCompleteHttpModel> rs =xmLandprojService.likeAll(keyword);
			dataJson.setObj(rs);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
			dataJson.setSuccess(true);
			
		}catch(Exception e){
			dataJson.setMsg(ErrorMessage.WEB_ERR_014);
			dataJson.setSuccess(false);
		}
		return SUCCESS;
	}
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public XmLandproj getXmLandproj() {
		return xmLandproj;
	}

	public void setXmLandproj(XmLandproj xmLandproj) {
		this.xmLandproj = xmLandproj;
	}

	public List<XmLandproj> getListXmLandproj() {
		return listXmLandproj;
	}

	public void setListXmLandproj(List<XmLandproj> listXmLandproj) {
		this.listXmLandproj = listXmLandproj;
	}

	public Result<XmLandproj> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmLandproj> pageResult) {
		this.pageResult = pageResult;
	}

	public String getThisXmLandprojId() {
		return thisXmLandprojId;
	}

	public void setThisXmLandprojId(String thisXmLandprojId) {
		this.thisXmLandprojId = thisXmLandprojId;
	}

	public List<ViewToXmInfo> getListViewToXmInfo() {
		return listViewToXmInfo;
	}

	public void setListViewToXmInfo(List<ViewToXmInfo> listViewToXmInfo) {
		this.listViewToXmInfo = listViewToXmInfo;
	}

	public String getThisLandNumber() {
		return thisLandNumber;
	}

	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public String getThisToLandprojid() {
		return thisToLandprojid;
	}

	public void setThisToLandprojid(String thisToLandprojid) {
		this.thisToLandprojid = thisToLandprojid;
	}

	public String getThisResUri() {
		return thisResUri;
	}

	public void setThisResUri(String thisResUri) {
		this.thisResUri = thisResUri;
	}

	public String[] getStrLandType() {
		return strLandType;
	}

	public void setStrLandType(String[] strLandType) {
		this.strLandType = strLandType;
	}

	public String getThisLandType() {
		return thisLandType;
	}

	public void setThisLandType(String thisLandType) {
		this.thisLandType = thisLandType;
	}

	public Result<ViewXmLandproj> getResultViewXmLandproj() {
		return resultViewXmLandproj;
	}

	public void setResultViewXmLandproj(Result<ViewXmLandproj> resultViewXmLandproj) {
		this.resultViewXmLandproj = resultViewXmLandproj;
	}

	public String getThisYear() {
		return thisYear;
	}

	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}

	public ViewbaseModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(ViewbaseModel dataModel) {
		this.dataModel = dataModel;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public ConfigBeanTmpl getConfigBeanTmpl() {
		return configBeanTmpl;
	}

	public void setConfigBeanTmpl(ConfigBeanTmpl configBeanTmpl) {
		this.configBeanTmpl = configBeanTmpl;
	}

}
