package com.gxwzu.business.action.project;

import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.business.model.XmSource;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.business.service.project.XmDutyService;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmScheduleService;
import com.gxwzu.business.service.project.XmSchedulepicService;
import com.gxwzu.business.service.project.XmSourceService;
import com.gxwzu.core.context.ConfigBeanTmpl;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.model.ViewbaseModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.util.poi.ExcelSheetModel;
import com.gxwzu.core.util.poi.ExcelUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewAsk;
import com.gxwzu.sysVO.ViewXmSchedule;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.Preparable;

/**
 * 督查事项批示来源Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmSourceAction
 * <br>Date: 2017-11-28上午11:32:42
 * <br>log:
 */
public class XmSourceAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 9104733691858858875L;

	private static final Log log=LogFactory.getLog(XmSourceAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmSource xmSource = new XmSource();
	private XmInfo xmInfo = new XmInfo();
	private ViewAsk viewAsk = new ViewAsk();
	private XmSchedule xmSchedule = new XmSchedule();
	private XmDuty xmDuty = new XmDuty();
	
	private ResponeJson dataJson;
	private List<XmSource> listXmSource;
	private List<ViewAsk> listViewAsk;
	private Result<XmSource> pageResult;
	private Result<ViewAsk> pageViewAsk;
	private List<XmSchedule> listXmSchedule;
	private List<XmDuty> listXmDuty;
	private List<ViewXmSchedule> listViewXmSchedule = new ArrayList<ViewXmSchedule>();
	
	private String thisXmNumber;
	private String thisXsNumber;
	private String thisXmSourceId;
	private String thisPeopleId;
	private String thisUser;
	private String thisXmScheduleId;
	private String thisXmDutyId;
	
	private ViewbaseModel dataModel;
	private InputStream is;//文件流
	private ConfigBeanTmpl configBeanTmpl;//模板配置模板
	
	/*********************注入Service*********************/
	private XmSourceService xmSourceService;
	private XmScheduleService xmScheduleService;
	private XmSchedulepicService xmSchedulepicService;
	private XmDutyService xmDutyService;
	private UserService userService;
	private XmInfoService xmInfoService;
	
	
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}

	public void setXmSchedulepicService(XmSchedulepicService xmSchedulepicService) {
		this.xmSchedulepicService = xmSchedulepicService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setXmDutyService(XmDutyService xmDutyService) {
		this.xmDutyService = xmDutyService;
	}

	public void setXmScheduleService(XmScheduleService xmScheduleService) {
		this.xmScheduleService = xmScheduleService;
	}

	public void setXmSourceService(XmSourceService xmSourceService) {
		this.xmSourceService = xmSourceService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String list() {
		try {
			pageResult=xmSourceService.find(xmSource, page, 20);
			footer=PageUtil.pageFooterForm(pageResult, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openAdd() throws Exception{
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String add() throws Exception{
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			xmSource.setCreateTime(Timestamp.valueOf(dateStr));
			
			xmSourceService.add(xmSource);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list();
	}
	
	public String openEdit() {
		try {
			if(thisXmSourceId != null && !"".equals(thisXmSourceId)){
				xmSource = xmSourceService.findById(Integer.parseInt(thisXmSourceId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String edit() {
		try {
			if(xmSource.getId() != null && !"".equals(xmSource.getId())){
				XmSource thisXmSource = xmSourceService.findById(xmSource.getId());
				thisXmSource.setXsName(xmSource.getXsName());
				thisXmSource.setXsLevel(xmSource.getXsLevel());
				thisXmSource.setXsIndustry(xmSource.getXsIndustry());
				thisXmSource.setXsArea(xmSource.getXsArea());
				xmSourceService.update(thisXmSource);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list();
	}
	
	public String del() {
		try {
			if(thisXmSourceId != null && !"".equals(thisXmSourceId)){
				xmSourceService.del(thisXmSourceId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list();
	}
	
	/**
	 * 打开新建批示内容（工作要求）页面
	 * 数据存储至项目表
	 * @return
	 */
	public String openAddAsk() {
		try {
			listViewAsk = xmSourceService.findAsk(thisXsNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 保存批示内容（工作要求）
	 * 批示内容（工作要求）存储至项目表
	 * 		1.当前年份——>建设年份（督查事项年份）
	 * 		2.交办时间——>创建时间
	 * 交办领导存储至责任监管表，类型为07
	 * @return
	 */
	public String addAsk() {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			xmInfo.setUserId(user.getId()+"");
			xmInfo.setUserName(user.getHelpName());
			xmInfo.setPortrait(user.getPortrait());
			xmInfo.setCreateTime(Timestamp.valueOf(dateStr));
			//07督查事项
			xmInfo.setBudgetType("07");
			//报告周期设置为365天
			xmInfo.setCycle("365");
			//是否办结：01已办结，00未办结
			xmInfo.setIsClose("00");
			
			xmSourceService.addAsk(xmInfo, thisPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openAddAsk();
	}
	
	public String delAsk() {
		try {
			xmSourceService.delAsk(thisXmNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openAddAsk();
	}
	
	public String listAsk() {
		try {
			pageViewAsk=xmSourceService.findListAsk(viewAsk, page, 20);
			footer=PageUtil.pageFooterForm(pageViewAsk, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String openSchedule() {
		try {
			if(thisXmNumber != null && !"".equals(thisXmNumber)){
				XmSchedule findXmSchedule = new XmSchedule();
				findXmSchedule.setXmNumber(thisXmNumber);
				listXmSchedule = xmScheduleService.findByExample(findXmSchedule);
				
				for (int i = 0; i < listXmSchedule.size(); i++) {
					findXmSchedule = listXmSchedule.get(i);
					
					ViewXmSchedule viewXmSchedule = new ViewXmSchedule();
					viewXmSchedule.setId(findXmSchedule.getId());
					viewXmSchedule.setXmInfoName(findXmSchedule.getXmInfoName());
					viewXmSchedule.setContent(findXmSchedule.getContent());
					viewXmSchedule.setType(findXmSchedule.getType());
					viewXmSchedule.setUserId(findXmSchedule.getUserId());
					viewXmSchedule.setUserName(findXmSchedule.getUserName());
					viewXmSchedule.setCreatTime(findXmSchedule.getCreatTime()+"");
					viewXmSchedule.setXmNumber(findXmSchedule.getXmNumber());
					
					if(findXmSchedule.getUserId() != null && !"".equals(findXmSchedule.getUserId())){
						User thisUser = userService.findById(findXmSchedule.getUserId());
						if(thisUser != null){
							viewXmSchedule.setPortrait(thisUser.getPortrait());
						}
					}
					
					if(findXmSchedule.getId() != null && !"".equals(findXmSchedule.getId())){
						XmSchedulepic thisXmSchedulepic = new XmSchedulepic();
						thisXmSchedulepic.setScheduleId(findXmSchedule.getId());
						List<XmSchedulepic> listXmSchedulepic = xmSchedulepicService.findByExample(thisXmSchedulepic);
						viewXmSchedule.setListXmSchedulepic(listXmSchedulepic);
					}
					listViewXmSchedule.add(viewXmSchedule);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String addSchedule() {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			xmSchedule.setUserId(user.getId());
			xmSchedule.setUserName(user.getHelpName());
			xmSchedule.setCreatTime(Timestamp.valueOf(dateStr));
			
			xmScheduleService.save(xmSchedule);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openSchedule();
	}
	
	public String delSchedule() {
		try {
			if(thisXmScheduleId != null && !"".equals(thisXmScheduleId)){
				XmSchedule delXmSchedule = xmScheduleService.findById(Integer.parseInt(thisXmScheduleId));
				xmScheduleService.remove(delXmSchedule);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openSchedule();
	}
	
	public String openDuty() {
		try {
			if(thisXmNumber != null && !"".equals(thisXmNumber)){
				XmDuty findXmDuty = new XmDuty();
				findXmDuty.setXmNumber(thisXmNumber);
				listXmDuty = xmDutyService.findByExample(findXmDuty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String addDuty() throws Exception {
		try {
			User user=(User) getSession().getAttribute("userinfo");
			
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			xmDuty.setCreateTime(Timestamp.valueOf(dateStr));
			xmDuty.setUserId(user.getId());
			xmDuty.setUserName(user.getHelpName());
			
			//获取责任人电话
			User people = userService.findById(xmDuty.getPeopleId());
			xmDuty.setPeopleTel(people.getHelpTelephone());
			
			xmDutyService.save(xmDuty);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openDuty();
	}
	
	public String delDuty() throws Exception {
		try {
			if(thisXmDutyId != null && !"".equals(thisXmDutyId)){
				XmDuty delXmDuty = xmDutyService.findById(Integer.parseInt(thisXmDutyId));
				if(delXmDuty != null){
					xmDutyService.remove(delXmDuty);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openDuty();
	}
	
	public String findTableAsk() {
		try {
			pageViewAsk=xmSourceService.findTableAsk(viewAsk, page, 20);
			footer=PageUtil.pageFooterForm(pageViewAsk, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	/**
	 * 督查事项落实情况汇总表导出
	 * @return
	 * @throws Exception
	 */
	public String downViewXmInfo() throws Exception{
		try {
			dataModel= xmSourceService.tableAskDownload(viewAsk);
			dataModel.setTplid("source1");//模板编号
			dataModel.setTplName("督查事项落实情况汇总表");
			
			//控制结果集输出的列，有可能与查询结果集不一致，已经在数据模型中重新排序，默认与查询结果一致
			dataModel.sort();
			//数据模型导出到Excel
			ExcelSheetModel model = new ExcelSheetModel(0, dataModel.getTplName(),dataModel.getSortdata(), 2);
			
			String filePath=getRootPath()+"templete\\"+configBeanTmpl.getTmplInfo().get(dataModel.getTplid());
			
			is = ExcelUtils.export(model,filePath);
		
			filename=model.getSheetTitle()+".xlsx";
			filename = (new String((filename).getBytes(),"ISO-8859-1"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "download";
	}
	
	public String createClose() {
		try {
			if(thisXmNumber != null && !"".equals(thisXmNumber)){
				XmInfo findXmInfo = new XmInfo();
				findXmInfo.setXmNumber(thisXmNumber);
				List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
				for (int i = 0; i < listXmInfo.size(); i++) {
					findXmInfo = listXmInfo.get(i);
					findXmInfo.setIsClose("01");
					xmInfoService.update(findXmInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return findTableAsk();
	}
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public XmSource getXmSource() {
		return xmSource;
	}

	public void setXmSource(XmSource xmSource) {
		this.xmSource = xmSource;
	}

	public List<XmSource> getListXmSource() {
		return listXmSource;
	}

	public void setListXmSource(List<XmSource> listXmSource) {
		this.listXmSource = listXmSource;
	}

	public Result<XmSource> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmSource> pageResult) {
		this.pageResult = pageResult;
	}

	public String getThisXmSourceId() {
		return thisXmSourceId;
	}

	public void setThisXmSourceId(String thisXmSourceId) {
		this.thisXmSourceId = thisXmSourceId;
	}

	public String getThisXsNumber() {
		return thisXsNumber;
	}

	public void setThisXsNumber(String thisXsNumber) {
		this.thisXsNumber = thisXsNumber;
	}

	public String getThisPeopleId() {
		return thisPeopleId;
	}

	public void setThisPeopleId(String thisPeopleId) {
		this.thisPeopleId = thisPeopleId;
	}

	public XmInfo getXmInfo() {
		return xmInfo;
	}

	public void setXmInfo(XmInfo xmInfo) {
		this.xmInfo = xmInfo;
	}

	public List<ViewAsk> getListViewAsk() {
		return listViewAsk;
	}

	public void setListViewAsk(List<ViewAsk> listViewAsk) {
		this.listViewAsk = listViewAsk;
	}

	public String getThisXmNumber() {
		return thisXmNumber;
	}

	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public Result<ViewAsk> getPageViewAsk() {
		return pageViewAsk;
	}

	public void setPageViewAsk(Result<ViewAsk> pageViewAsk) {
		this.pageViewAsk = pageViewAsk;
	}

	public ViewAsk getViewAsk() {
		return viewAsk;
	}

	public void setViewAsk(ViewAsk viewAsk) {
		this.viewAsk = viewAsk;
	}

	public String getThisUser() {
		return thisUser;
	}

	public void setThisUser(String thisUser) {
		this.thisUser = thisUser;
	}

	public XmSchedule getXmSchedule() {
		return xmSchedule;
	}

	public void setXmSchedule(XmSchedule xmSchedule) {
		this.xmSchedule = xmSchedule;
	}

	public List<XmSchedule> getListXmSchedule() {
		return listXmSchedule;
	}

	public void setListXmSchedule(List<XmSchedule> listXmSchedule) {
		this.listXmSchedule = listXmSchedule;
	}

	public String getThisXmScheduleId() {
		return thisXmScheduleId;
	}

	public void setThisXmScheduleId(String thisXmScheduleId) {
		this.thisXmScheduleId = thisXmScheduleId;
	}

	public List<XmDuty> getListXmDuty() {
		return listXmDuty;
	}

	public void setListXmDuty(List<XmDuty> listXmDuty) {
		this.listXmDuty = listXmDuty;
	}

	public XmDuty getXmDuty() {
		return xmDuty;
	}

	public void setXmDuty(XmDuty xmDuty) {
		this.xmDuty = xmDuty;
	}

	public String getThisXmDutyId() {
		return thisXmDutyId;
	}

	public void setThisXmDutyId(String thisXmDutyId) {
		this.thisXmDutyId = thisXmDutyId;
	}

	public ViewbaseModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(ViewbaseModel dataModel) {
		this.dataModel = dataModel;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public ConfigBeanTmpl getConfigBeanTmpl() {
		return configBeanTmpl;
	}

	public void setConfigBeanTmpl(ConfigBeanTmpl configBeanTmpl) {
		this.configBeanTmpl = configBeanTmpl;
	}

	public List<ViewXmSchedule> getListViewXmSchedule() {
		return listViewXmSchedule;
	}

	public void setListViewXmSchedule(List<ViewXmSchedule> listViewXmSchedule) {
		this.listViewXmSchedule = listViewXmSchedule;
	}

}
