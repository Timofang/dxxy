package com.gxwzu.business.action.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.gxwzu.business.model.Attachment;
import com.gxwzu.business.service.project.AttachmentService;
import com.gxwzu.core.web.action.BaseAction;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 附件Action层
 * @ClassName: AttachmentAction
 * @author SunYi
 * @Date 2019年3月29日上午10:37:16
 */
public class AttachmentAction extends BaseAction implements ModelDriven<Attachment>{

	private static final long serialVersionUID = 6951270557216425617L;
	
	
	// 上传的附件
	private File[] myfile;
	// 上传附件的附件名
	private String[] myfileFileName;
	// 上传的附件对应的项目编号(主项目编号或子项目编号)
	private String thisXmNumber;
	// 下载的附件路径
	private String downloadFilePath;
	// 下载的附件名
	private String downloadFileName;
	// 文件流
	private InputStream fileStream;
	
	
	@Autowired
	private AttachmentService attachmentService;
	
	private Attachment attachment;
	
	
	@Override
	public Attachment getModel() {
		return attachment;
	}
	
	
	/**
	 * 附件上传
	 * @author SunYi
	 * @Date 2019年3月29日上午11:06:36
	 * @return String
	 */
	public String attachmentUpload() {
		//进入保存文件以及持久化数据业务
		boolean flag=attachmentService.saveAttachment(myfile,myfileFileName,thisXmNumber);
		//业务流程完毕
		if(flag) {
		//业务流程出错
		}else {

		}
		return resUri;
	}
	
	
	/**
	 * 附件下载
	 * @author SunYi
	 * @Date 2019年3月29日上午11:06:43
	 * @return String
	 */
    public String attachmentDownload() throws Exception{
    	try {
    		//this.downloadFileName=this.downloadFileName+downloadFilePath.substring(downloadFilePath.lastIndexOf("."),downloadFilePath.length());
			fileStream = new FileInputStream(new File(downloadFilePath));
			this.downloadFileName = new String(downloadFileName.getBytes("utf-8"),"iso-8859-1");
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	
	public File[] getMyfile() {
		return myfile;
	}
	
	public void setMyfile(File[] myfile) {
		this.myfile = myfile;
	}
	
	public String[] getMyfileFileName() {
		return myfileFileName;
	}
	
	public void setMyfileFileName(String[] myfileFileName) {
		this.myfileFileName = myfileFileName;
	}
	
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}

	public String getDownloadFilePath() {
		return downloadFilePath;
	}

	public void setDownloadFilePath(String downloadFilePath) {
		this.downloadFilePath = downloadFilePath;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
	}

	public InputStream getFileStream() {
		return fileStream;
	}

	public void setFileStream(InputStream fileStream) {
		this.fileStream = fileStream;
	}

	public AttachmentService getAttachmentService() {
		return attachmentService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}
	
}
