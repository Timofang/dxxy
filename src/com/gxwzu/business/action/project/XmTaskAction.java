package com.gxwzu.business.action.project;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.model.XmTask;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.business.service.project.XmTaskService;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.DateUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.opensymphony.xwork2.Preparable;

/**
 * 征地完成进度Action
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmTaskAction
 * <br>Date: 2017-9-6下午09:14:55
 * <br>log:
 */
public class XmTaskAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 7632550551016866463L;

	private static final Log log=LogFactory.getLog(XmTaskAction.class);
	
	/*********************参数列表*********************/
	private Map<String,String> yearList=new LinkedHashMap<String,String>();
	
	private XmTask xmTask = new XmTask();
	private XmLandproj xmLandproj = new XmLandproj();
	
	private ResponeJson dataJson;
	private Result<XmTask> pageResult;
	private List<XmTask> listXmTask;
	
	private String thisLandNumber;
	
	/*********************注入Service*********************/
	private XmLandprojService xmLandprojService;
	private XmTaskService xmTaskService;
	
	public void setXmTaskService(XmTaskService xmTaskService) {
		this.xmTaskService = xmTaskService;
	}

	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}

	/*********************Action方法体*********************/
	@Override
	public void prepare() throws Exception {
		int year=DateUtils.getYear();
		for(int i=year+3;i>(year-5);i--){
			yearList.put(i+"", i+"年");
		}
	}
	
	public String findTaskByNum() throws Exception{
		try {
			if(thisLandNumber != null && !"".equals(thisLandNumber)){
				xmLandproj.setLandNumber(thisLandNumber);
				List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(xmLandproj);
				if(listXmLandproj.size()>0){
					xmLandproj = listXmLandproj.get(0);
				}
				
				XmTask findXmTask = new XmTask();
				findXmTask.setLandNumber(thisLandNumber);
				listXmTask = xmTaskService.findByExample(findXmTask);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	
	/*********************getter/setter方法*********************/
	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public ResponeJson getDataJson() {
		return dataJson;
	}

	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}

	public String getThisLandNumber() {
		return thisLandNumber;
	}

	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}

	public XmLandproj getXmLandproj() {
		return xmLandproj;
	}

	public void setXmLandproj(XmLandproj xmLandproj) {
		this.xmLandproj = xmLandproj;
	}

	public XmTask getXmTask() {
		return xmTask;
	}

	public void setXmTask(XmTask xmTask) {
		this.xmTask = xmTask;
	}

	public Result<XmTask> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Result<XmTask> pageResult) {
		this.pageResult = pageResult;
	}

	public List<XmTask> getListXmTask() {
		return listXmTask;
	}

	public void setListXmTask(List<XmTask> listXmTask) {
		this.listXmTask = listXmTask;
	}

}
