package com.gxwzu.business.action.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.service.project.MaterialsService;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.system.model.user.User;
import com.opensymphony.xwork2.ModelDriven;

public class MaterialsAction extends BaseAction implements ModelDriven<Materials>{

	private static final long serialVersionUID = 1L;

	/**
	 * @author soldier（黄结）
	 * 用于关闭layerweb弹层组件
	 */
	private String isClose;

    //表单上提供的字段
	private File[] myfile;
	//struts2在文件上传时提供的属性
	private String[] myfileFileName;//上传的文件名。上传字段名称+FileName  注意大小写
	private String[] myfileContentType;//上传文件的MIME类型。上传字段名称+ContentType 注意大小写
	private String materialsId;
	//正在处理的文件所在项目编号
	private String xmId;
	private String userName;
	private String xcId;

	private String downloadFilePath;

	private String downloadFileName;

	private InputStream fileStream;

	@Autowired
    private MaterialsService materialsService;
	
	private Materials materials;
	
	private User user;
	
	@Override
	public Materials getModel() {
		return materials;
	}

	/**
	 * 文件上传
	 * 需要值：文件myfile，文件名myfileFileName，父项目编号xmId，子项目编号xcId，接收人userName,当前用户的用户名helpUserName
	 * @author SunYi
	 * @Date 2019年3月14日下午2:39:47
	 * @return String
	 */
	public String submitMaterials() {
		//查询当前用户存在session的信息
		user = (User) getSession().getAttribute("userinfo");
		//进入保存文件以及持久化数据业务
		boolean flag=materialsService.saveFileList(myfile,myfileFileName,userName,xcId,xmId,user.getHelpUserName());
		//业务流程完毕
		if(flag) {

		//业务流程出错
		}else {

		}

		/**
		 * @author soldier（黄结）
		 * 用于关闭layerweb弹层组件
		 */
		isClose = "1";
		return resUri;
	}

	/**
	 * 配置Tomcat：
	 * 在conf/catalina.properties中最后添加2行：
	 *
	 * tomcat.util.http.parser.HttpParser.requestTargetAllow=|{}
	 * org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true
	 *
	 *
	 * c在conf/server.xml中的<Connector>节点中，添加3个属性：
	 * relaxedPathChars="|{}[],"
	 * relaxedQueryChars="|{}[],"
	 * URIEncoding="UTF-8"
	 * 文件的下载
	 * @return
	 */
	public String downloadMaterials() {

		try {
			//拼接后缀
			this.downloadFileName=this.downloadFileName+downloadFilePath.substring(downloadFilePath.lastIndexOf("."),downloadFilePath.length());
			fileStream = new FileInputStream(new File(downloadFilePath));
			this.downloadFileName = new String(downloadFileName.getBytes("utf-8"),"iso-8859-1");

		} catch (Exception e) {

			e.printStackTrace();

		}

		return resUri;
	}

	/**
	 * @author soldier（黄结）
	 * 用于关闭layerweb弹层组件
	 */
	public String getIsClose() {
		return isClose;
	}
	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}
	
	public Materials getMaterials() {
		return materials;
	}

	public void setMaterials(Materials materials) {
		this.materials = materials;
	}

	public File[] getMyfile() {
		return myfile;
	}

	public void setMyfile(File[] myfile) {
		this.myfile = myfile;
	}

	public String[] getMyfileFileName() {
		return myfileFileName;
	}

	public void setMyfileFileName(String[] myfileFileName) {
		this.myfileFileName = myfileFileName;
	}

	public String[] getMyfileContentType() {
		return myfileContentType;
	}

	public void setMyfileContentType(String[] myfileContentType) {
		this.myfileContentType = myfileContentType;
	}

	public String getXmId() {
		return xmId;
	}

	public void setXmId(String xmId) {
		this.xmId = xmId;
	}

	public MaterialsService getMaterialsService() {
		return materialsService;
	}

	public void setMaterialsService(MaterialsService materialsService) {
		this.materialsService = materialsService;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMaterialsId() {
		return materialsId;
	}

	public void setMaterialsId(String materialsId) {
		this.materialsId = materialsId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getXcId() {
		return xcId;
	}

	public void setXcId(String xcId) {
		this.xcId = xcId;
	}

	public InputStream getFileStream() {
		return fileStream;
	}

	public void setFileStream(InputStream fileStream) {
		this.fileStream = fileStream;
	}

	public String getDownloadFilePath() {
		return downloadFilePath;
	}

	public void setDownloadFilePath(String downloadFilePath) {
		this.downloadFilePath = downloadFilePath;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
	}
}
