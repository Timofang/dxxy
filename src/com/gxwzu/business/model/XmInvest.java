package com.gxwzu.business.model;

import java.sql.Timestamp;

/**
 * XmInvest entity. @author MyEclipse Persistence Tools
 */

public class XmInvest implements java.io.Serializable {

	// Fields

	private Integer id;
	private String xmNumber;
	private String investment;
	private String payment;
	private String investYear;
	private String investMonth;
	private Integer userId;
	private String userName;
	private Timestamp createTime;

	// Constructors

	/** default constructor */
	public XmInvest() {
	}

	/** full constructor */
	public XmInvest(String investment, String payment, String investYear, String investMonth,
			Integer userId, String userName, Timestamp createTime, String xmNumber) {
		this.investment = investment;
		this.payment = payment;
		this.investYear = investYear;
		this.investMonth = investMonth;
		this.xmNumber = xmNumber;
		this.userId = userId;
		this.userName = userName;
		this.createTime = createTime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvestment() {
		return this.investment;
	}

	public void setInvestment(String investment) {
		this.investment = investment;
	}

	public String getPayment() {
		return this.payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getInvestYear() {
		return investYear;
	}

	public void setInvestYear(String investYear) {
		this.investYear = investYear;
	}

	public String getInvestMonth() {
		return investMonth;
	}

	public void setInvestMonth(String investMonth) {
		this.investMonth = investMonth;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

}