package com.gxwzu.business.model;

import java.sql.Timestamp;

public class XmSchedulepic implements java.io.Serializable {

	private Integer id;
	private Integer scheduleId;
	private String picLink;
	private Timestamp createTime;

	public XmSchedulepic() {
	}
	public XmSchedulepic(Integer scheduleId, String picLink,
			Timestamp createTime) {
		this.scheduleId = scheduleId;
		this.picLink = picLink;
		this.createTime = createTime;
	}

	public Integer getId() {
		return this.id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getScheduleId() {
		return this.scheduleId;
	}
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getPicLink() {
		return this.picLink;
	}
	public void setPicLink(String picLink) {
		this.picLink = picLink;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
}