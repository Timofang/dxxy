package com.gxwzu.business.model;

import java.sql.Timestamp;
import java.util.Date;

/**
 * XmSchedule entity. @author MyEclipse Persistence Tools
 */

public class XmSchedule implements java.io.Serializable {

	// Fields

	private Integer id;
	private String xmInfoName;
	private String content;
	private String type;
	private Integer userId;
	private String userName;
	private Date creatTime;
	private String xmNumber;

	// Constructors

	/** default constructor */
	public XmSchedule() {
	}

	/** full constructor */
	public XmSchedule(String xmInfoName, String content, String type, Integer userId,
			String userName, Date creatTime, String xmNumber) {
		this.xmInfoName = xmInfoName;
		this.content = content;
		this.type = type;
		this.userId = userId;
		this.userName = userName;
		this.creatTime = creatTime;
		this.xmNumber = xmNumber;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmInfoName() {
		return xmInfoName;
	}

	public void setXmInfoName(String xmInfoName) {
		this.xmInfoName = xmInfoName;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreatTime() {
		return this.creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

}