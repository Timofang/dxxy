package com.gxwzu.business.model;

import java.sql.Timestamp;
import java.util.List;

import com.gxwzu.system.model.user.User;

/**
 * Xm entity. @author MyEclipse Persistence Tools
 */

public class Xm implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String username;
	private String xmNumber;
	private Timestamp publishTime;
	private Timestamp deadLine;
	private String receiver;
	private String receiverUnit;
	private String type;
	private Integer fileCount;
	private String fileName;
	private Integer status;
	private String requires;
	private Float degree;
	private Timestamp finishTime;
	private String helpUsername;
	
	// 发布人姓名
	private String helpName;
	private List<User> userList;
	private List<Unit> unitList;
	private List<Instructions> psList;
	// Constructors

	/** default constructor */
	public Xm() {
	}

	/** full constructor */
	public Xm(String name, String username, String xmNumber, Timestamp publishTime, Timestamp deadLine, String receiver,
			String receiverUnit, String type, Integer fileCount, String fileName, Integer status, String requires,
			Float degree, Timestamp finishTime) {
		this.name = name;
		this.username = username;
		this.xmNumber = xmNumber;
		this.publishTime = publishTime;
		this.deadLine = deadLine;
		this.receiver = receiver;
		this.receiverUnit = receiverUnit;
		this.type = type;
		this.fileCount = fileCount;
		this.fileName = fileName;
		this.status = status;
		this.requires = requires;
		this.degree = degree;
		this.finishTime = finishTime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public Timestamp getPublishTime() {
		return this.publishTime;
	}

	public void setPublishTime(Timestamp publishTime) {
		this.publishTime = publishTime;
	}

	public Timestamp getDeadLine() {
		return this.deadLine;
	}

	public void setDeadLine(Timestamp deadLine) {
		this.deadLine = deadLine;
	}

	public String getReceiver() {
		return this.receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverUnit() {
		return this.receiverUnit;
	}

	public void setReceiverUnit(String receiverUnit) {
		this.receiverUnit = receiverUnit;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFileCount() {
		return this.fileCount;
	}

	public void setFileCount(Integer fileCount) {
		this.fileCount = fileCount;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRequires() {
		return this.requires;
	}

	public void setRequires(String requires) {
		this.requires = requires;
	}

	public Float getDegree() {
		return this.degree;
	}

	public void setDegree(Float degree) {
		this.degree = degree;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Unit> getUnitList() {
		return unitList;
	}

	public void setUnitList(List<Unit> unitList) {
		this.unitList = unitList;
	}

	public String getHelpUsername() {
		return helpUsername;
	}

	public void setHelpUsername(String helpUsername) {
		this.helpUsername = helpUsername;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public Timestamp getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Timestamp finishTime) {
		this.finishTime = finishTime;
	}

	public List<Instructions> getPsList() {
		return psList;
	}

	public void setPsList(List<Instructions> psList) {
		this.psList = psList;
	}
	

}