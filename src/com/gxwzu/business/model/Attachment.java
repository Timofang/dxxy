package com.gxwzu.business.model;

/**
 * Attachment entity. @author MyEclipse Persistence Tools
 */

public class Attachment implements java.io.Serializable {

	// Fields

	private Integer id;
	private String xmNumber;
	private String fileName;
	private String path;

	// Constructors

	/** default constructor */
	public Attachment() {
	}

	/** full constructor */
	public Attachment(String xmNumber, String fileName, String path) {
		this.xmNumber = xmNumber;
		this.fileName = fileName;
		this.path = path;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "Attachment [id=" + id + ", xmNumber=" + xmNumber + ", fileName=" + fileName + ", path=" + path + "]";
	}
	

}