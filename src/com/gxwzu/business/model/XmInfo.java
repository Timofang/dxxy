package com.gxwzu.business.model;

import java.sql.Timestamp;

public class XmInfo implements java.io.Serializable {

	private Integer id;
	private String xmNumber;
	private String xmYear;
	private String name;
	private String scale;
	private String period;
	private String cycle;
	private String owner;
	private String county;
	private Timestamp startTime;
	private Timestamp completionTime;
	private String allPay;
	private String address;
	private String industry;
	private String isState;
	private String isCompleted;
	private String budgetType;
	private String projectType;
	private String userId;
	private String userName;
	private String portrait;
	private Timestamp createTime;
	
	private String coordinate;
	private String coordinatePly;
	
	private String xmSourceNumber;
	
	private String isClose;
	
	

	public XmInfo() {
	}

	public XmInfo(Integer id, String xmNumber, String xmYear, String name,
			String scale, String period, String owner, String county,
			Timestamp startTime, Timestamp completionTime, String allPay, String address,
			String industry, String isState, String isCompleted,
			String budgetType, String projectType, String userId,
			String userName, String portrait, Timestamp createTime,
			String coordinate, String coordinatePly, String cycle, String xmSourceNumber,
			String isClose) {
		super();
		this.id = id;
		this.xmNumber = xmNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.scale = scale;
		this.period = period;
		this.cycle = cycle;
		this.owner = owner;
		this.county = county;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.allPay = allPay;
		this.address = address;
		this.industry = industry;
		this.isState = isState;
		this.isCompleted = isCompleted;
		this.budgetType = budgetType;
		this.projectType = projectType;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
		this.xmSourceNumber = xmSourceNumber;
		this.isClose = isClose;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScale() {
		return this.scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getCompletionTime() {
		return this.completionTime;
	}

	public void setCompletionTime(Timestamp completionTime) {
		this.completionTime = completionTime;
	}

	public String getAllPay() {
		return allPay;
	}

	public void setAllPay(String allPay) {
		this.allPay = allPay;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return this.industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIsState() {
		return this.isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}

	public String getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getBudgetType() {
		return this.budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getProjectType() {
		return this.projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getXmSourceNumber() {
		return xmSourceNumber;
	}

	public void setXmSourceNumber(String xmSourceNumber) {
		this.xmSourceNumber = xmSourceNumber;
	}

	public String getIsClose() {
		return isClose;
	}

	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}

}