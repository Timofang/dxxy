package com.gxwzu.business.model;

import java.sql.Timestamp;

/**
 * 任务批示实体类
 * @ClassName: Instructions
 * @author SunYi
 * @Date 2019年4月7日下午10:09:28
 */
public class Instructions implements java.io.Serializable{
	
	private static final long serialVersionUID = 1809375190678746121L;
	
	private Integer id;
	// 任务编号
	private String xmNumber;
	// 批示人
	private String instructionsPerson;
	// 批示内容
	private String instructionsContent;
	// 批示时间
	private Timestamp instructionsTime;
	
	
	
	
	public Instructions() {
		super();
	}
	
	public Instructions(String xmNumber, String instructionsPerson, String instructionsContent, Timestamp instructionsTime) {
		super();
		this.xmNumber = xmNumber;
		this.instructionsPerson = instructionsPerson;
		this.instructionsContent = instructionsContent;
		this.instructionsTime = instructionsTime;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getXmNumber() {
		return xmNumber;
	}
	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getInstructionsPerson() {
		return instructionsPerson;
	}
	public void setInstructionsPerson(String instructionsPerson) {
		this.instructionsPerson = instructionsPerson;
	}
	public String getInstructionsContent() {
		return instructionsContent;
	}
	public void setInstructionsContent(String instructionsContent) {
		this.instructionsContent = instructionsContent;
	}
	public Timestamp getInstructionsTime() {
		return instructionsTime;
	}
	public void setInstructionsTime(Timestamp instructionsTime) {
		this.instructionsTime = instructionsTime;
	}
	@Override
	public String toString() {
		return "Instructions [id=" + id + ", xmNumber=" + xmNumber + ", instructionsPerson=" + instructionsPerson
				+ ", instructionsContent=" + instructionsContent + ", instructionsTime=" + instructionsTime + "]";
	}
	
	
}
