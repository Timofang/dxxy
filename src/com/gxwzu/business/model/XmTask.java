package com.gxwzu.business.model;

import java.sql.Timestamp;

/**
 * XmTask entity. @author MyEclipse Persistence Tools
 */

public class XmTask implements java.io.Serializable {

	// Fields

	private Integer id;
	private String task;
	private Integer userId;
	private String userName;
	private Timestamp createTime;
	private String landNumber;
	private String taskYear;
	private String taskMonth;
	private String taskType;

	// Constructors

	/** default constructor */
	public XmTask() {
	}

	/** full constructor */
	public XmTask(String task, Integer userId, String userName,
			Timestamp createTime, String landNumber, String taskYear,
			String taskMonth, String taskType) {
		this.task = task;
		this.userId = userId;
		this.userName = userName;
		this.createTime = createTime;
		this.landNumber = landNumber;
		this.taskYear = taskYear;
		this.taskMonth = taskMonth;
		this.taskType = taskType;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTask() {
		return this.task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getLandNumber() {
		return this.landNumber;
	}

	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}

	public String getTaskYear() {
		return this.taskYear;
	}

	public void setTaskYear(String taskYear) {
		this.taskYear = taskYear;
	}

	public String getTaskMonth() {
		return this.taskMonth;
	}

	public void setTaskMonth(String taskMonth) {
		this.taskMonth = taskMonth;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

}