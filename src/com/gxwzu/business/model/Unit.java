package com.gxwzu.business.model;

import java.util.List;

/**
 * 教研室表
 */

public class Unit implements java.io.Serializable {

	// Fields

	private String unitId;
	private String unitName;
	private String unitPerson;  //负责人
	
	private XmChild xmChild;
	
	private List<Materials> materialsList;

	/** default constructor */
	public Unit() {
	}

	/** full constructor */
	public Unit(String unitName) {
		this.unitName = unitName;
	}

	public Unit(String unitId, String unitName, String unitPerson) {
		this.unitId = unitId;
		this.unitName = unitName;
		this.unitPerson = unitPerson;
	}

	public Unit(String unitName, String unitPerson) {
		this.unitName = unitName;
		this.unitPerson = unitPerson;
	}

	// Property accessors

	public String getUnitId() {
		return this.unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getUnitName() {
		return this.unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	public String getUnitPerson() {
		return unitPerson;
	}

	public void setUnitPerson(String unitPerson) {
		this.unitPerson = unitPerson;
	}

	
	public List<Materials> getMaterialsList() {
		return materialsList;
	}

	public void setMaterialsList(List<Materials> materialsList) {
		this.materialsList = materialsList;
	}

	public XmChild getXmChild() {
		return xmChild;
	}

	public void setXmChild(XmChild xmChild) {
		this.xmChild = xmChild;
	}

	@Override
	public String toString() {
		return "Unit [unitId=" + unitId + ", unitName=" + unitName + ", unitPerson=" + unitPerson + ", materialsList="
				+ materialsList + "]";
	}

	

}