package com.gxwzu.business.model;

/**
 * @Author: soldier
 * @Date: 2019/3/5 8:45
 * @Desc: 项目类型
 */
public class XmType {
    private Integer typeId;
    private String typeName;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public XmType() {
    }

    public XmType(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

	@Override
	public String toString() {
		return "XmType [typeId=" + typeId + ", typeName=" + typeName + "]";
	}
    
    
}
