package com.gxwzu.business.model;

import java.sql.Timestamp;

public class XmLandproj implements java.io.Serializable {

	private Integer id;
	private String landNumber;
	private String landType;
	private String name;
	private String laYear;
	private String cycle;
	private String approval;
	private String landTotal;
	private String landSurplus;
	private String landCurrent;
	private String houseTotal;
	private String houseSurplus;
	private String houseCurrent;
	private String mountainTotal;
	private String mountainSurplus;
	private String mountainCurrent;
	private String largeInvest;
	private String owner;
	private String address;
	private String coordinate;
	private String coordinatePly;
	private String userId;
	private String userName;
	private String portrait;
	private Timestamp createTime;

	public XmLandproj() {
	}

	public XmLandproj(Integer id, String landNumber, String landType, String name,
			String laYear, String cycle, String approval, String landTotal,
			String landSurplus, String landCurrent, String houseTotal,
			String houseSurplus, String houseCurrent, String mountainTotal,
			String mountainSurplus, String mountainCurrent, String largeInvest,
			String owner, String address, String coordinate,
			String coordinatePly, String userId, String userName,
			String portrait, Timestamp createTime) {
		super();
		this.id = id;
		this.landNumber = landNumber;
		this.landType = landType;
		this.name = name;
		this.laYear = laYear;
		this.cycle = cycle;
		this.approval = approval;
		this.landTotal = landTotal;
		this.landSurplus = landSurplus;
		this.landCurrent = landCurrent;
		this.houseTotal = houseTotal;
		this.houseSurplus = houseSurplus;
		this.houseCurrent = houseCurrent;
		this.mountainTotal = mountainTotal;
		this.mountainSurplus = mountainSurplus;
		this.mountainCurrent = mountainCurrent;
		this.largeInvest = largeInvest;
		this.owner = owner;
		this.address = address;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLandNumber() {
		return landNumber;
	}

	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}

	public String getLandType() {
		return landType;
	}

	public void setLandType(String landType) {
		this.landType = landType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLaYear() {
		return laYear;
	}

	public void setLaYear(String laYear) {
		this.laYear = laYear;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getLandTotal() {
		return landTotal;
	}

	public void setLandTotal(String landTotal) {
		this.landTotal = landTotal;
	}

	public String getLandSurplus() {
		return landSurplus;
	}

	public void setLandSurplus(String landSurplus) {
		this.landSurplus = landSurplus;
	}

	public String getLandCurrent() {
		return landCurrent;
	}

	public void setLandCurrent(String landCurrent) {
		this.landCurrent = landCurrent;
	}

	public String getHouseTotal() {
		return houseTotal;
	}

	public void setHouseTotal(String houseTotal) {
		this.houseTotal = houseTotal;
	}

	public String getHouseSurplus() {
		return houseSurplus;
	}

	public void setHouseSurplus(String houseSurplus) {
		this.houseSurplus = houseSurplus;
	}

	public String getHouseCurrent() {
		return houseCurrent;
	}

	public void setHouseCurrent(String houseCurrent) {
		this.houseCurrent = houseCurrent;
	}

	public String getMountainTotal() {
		return mountainTotal;
	}

	public void setMountainTotal(String mountainTotal) {
		this.mountainTotal = mountainTotal;
	}

	public String getMountainSurplus() {
		return mountainSurplus;
	}

	public void setMountainSurplus(String mountainSurplus) {
		this.mountainSurplus = mountainSurplus;
	}

	public String getMountainCurrent() {
		return mountainCurrent;
	}

	public void setMountainCurrent(String mountainCurrent) {
		this.mountainCurrent = mountainCurrent;
	}

	public String getLargeInvest() {
		return largeInvest;
	}

	public void setLargeInvest(String largeInvest) {
		this.largeInvest = largeInvest;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


}