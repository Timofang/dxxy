package com.gxwzu.business.model;

import java.sql.Timestamp;

public class XmSource implements java.io.Serializable {

	private Integer id;
	private String xsNumber;
	private String xsName;
	private String xsLevel;
	private String xsIndustry;
	private String xsArea;
	private Timestamp createTime;

	public XmSource() {
	}

	public XmSource(Integer id, String xsNumber, String xsName, String xsLevel, String xsIndustry, String xsArea,
			Timestamp createTime) {
		super();
		this.id = id;
		this.xsNumber = xsNumber;
		this.xsName = xsName;
		this.xsLevel = xsLevel;
		this.xsIndustry = xsIndustry;
		this.xsArea = xsArea;
		this.createTime = createTime;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXsNumber() {
		return this.xsNumber;
	}

	public void setXsNumber(String xsNumber) {
		this.xsNumber = xsNumber;
	}

	public String getXsName() {
		return this.xsName;
	}

	public void setXsName(String xsName) {
		this.xsName = xsName;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getXsLevel() {
		return xsLevel;
	}

	public void setXsLevel(String xsLevel) {
		this.xsLevel = xsLevel;
	}

	public String getXsIndustry() {
		return xsIndustry;
	}

	public void setXsIndustry(String xsIndustry) {
		this.xsIndustry = xsIndustry;
	}

	public String getXsArea() {
		return xsArea;
	}

	public void setXsArea(String xsArea) {
		this.xsArea = xsArea;
	}
	

}