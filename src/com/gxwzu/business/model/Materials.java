package com.gxwzu.business.model;

import java.sql.Timestamp;

/**
 * Materials entity. @author MyEclipse Persistence Tools
 */

public class Materials implements java.io.Serializable {

	// Fields

	private Integer id;
	private String xmId;
	private String materialsId;
	private String filename;
	private String path;
	private String unitId;
	private String userId;
	private Timestamp finishTime;

	// Constructors

	/** default constructor */
	public Materials() {
	}

	/** full constructor */
	public Materials(String xmId, String materialsId, String filename, String path, String unitId, String userId,
			Timestamp finishTime) {
		this.xmId = xmId;
		this.materialsId = materialsId;
		this.filename = filename;
		this.path = path;
		this.unitId = unitId;
		this.userId = userId;
		this.finishTime = finishTime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmId() {
		return this.xmId;
	}

	public void setXmId(String xmId) {
		this.xmId = xmId;
	}

	public String getMaterialsId() {
		return this.materialsId;
	}

	public void setMaterialsId(String materialsId) {
		this.materialsId = materialsId;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUnitId() {
		return this.unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Timestamp finishTime) {
		this.finishTime = finishTime;
	}

}