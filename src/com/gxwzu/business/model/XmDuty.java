package com.gxwzu.business.model;

import java.sql.Timestamp;

/**
 * XmDuty entity. @author MyEclipse Persistence Tools
 */

public class XmDuty implements java.io.Serializable {

	// Fields

	private Integer id;
	private String unit;
	private String xbUnit;
	private Timestamp createTime;
	private String xmNumber;
	private Integer userId;
	private String userName;
	private Integer peopleId;
	private String people;
	private String peopleTel;
	private String type;

	// Constructors

	/** default constructor */
	public XmDuty() {
	}

	/** full constructor */
	public XmDuty(String unit,String xbUnit, Timestamp createTime, String xmNumber,
			Integer userId, String userName, String people, Integer peopleId,
			String type, String peopleTel) {
		this.unit = unit;
		this.xbUnit = xbUnit;
		this.createTime = createTime;
		this.xmNumber = xmNumber;
		this.userId = userId;
		this.userName = userName;
		this.people = people;
		this.peopleId = peopleId;
		this.peopleTel = peopleTel;
		this.type = type;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public String getPeopleTel() {
		return peopleTel;
	}

	public void setPeopleTel(String peopleTel) {
		this.peopleTel = peopleTel;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getXbUnit() {
		return xbUnit;
	}

	public void setXbUnit(String xbUnit) {
		this.xbUnit = xbUnit;
	}

}