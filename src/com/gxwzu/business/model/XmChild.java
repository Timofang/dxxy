package com.gxwzu.business.model;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import com.gxwzu.system.model.user.User;

/**
 * XmChild entity. @author MyEclipse Persistence Tools
 */

public class XmChild implements java.io.Serializable {

	// Fields

	private Integer id;
	private String xmNumber;
	private String xmName;
	private String xmChildNumber;
	private String xmChildName;
	private String unitId;
	private String userName;
	private String unitParent;
	private Integer issue;
	private String requires;
	private Integer fileCount;
	private String fileName;
	private String materialsId;
	private Timestamp publishTime;
	private Timestamp deadLine;
	private Integer status;
	private Integer acceptstatus;
	private Float degree;
	private String quality;
	private Timestamp finishTime;
	private User user;
	private Xm xm;
	private Unit unit;
	private XmChild xmChild;
	private Integer finishCount;
	private Integer unfinishedCount;
	private Double proportion;
	
	private List<XmChild> xmChildList;
	
	private List<User> userList;
	
	private String helpUserName;
	private String helpName;
	
	private String unitName;
	
	private String receiverName;
	
	private String receiverUnitName;
	
	private String[] helpUserNameList;
	
	private String[] unitNameList;
	
	private String[] userNameList;
	
	private List<Instructions> psList;
	// Constructors

	/** default constructor */
	public XmChild() {
	}

	/** full constructor */
	public XmChild(String xmNumber, String xmName, String xmChildNumber, String xmChildName, String unitId,
			String userName, String unitParent, Integer issue, String requires, Integer fileCount, String fileName,
			String materialsId, Timestamp publishTime, Timestamp deadLine, Integer status, Integer acceptstatus,
			Float degree, String quality, Timestamp finishTime) {
		this.xmNumber = xmNumber;
		this.xmName = xmName;
		this.xmChildNumber = xmChildNumber;
		this.xmChildName = xmChildName;
		this.unitId = unitId;
		this.userName = userName;
		this.unitParent = unitParent;
		this.issue = issue;
		this.requires = requires;
		this.fileCount = fileCount;
		this.fileName = fileName;
		this.materialsId = materialsId;
		this.publishTime = publishTime;
		this.deadLine = deadLine;
		this.status = status;
		this.acceptstatus = acceptstatus;
		this.degree = degree;
		this.quality = quality;
		this.finishTime = finishTime;
	}
	
	

	public XmChild(String userName, Integer finishCount, Integer unfinishedCount, Double proportion) {
		super();
		this.userName = userName;
		this.finishCount = finishCount;
		this.unfinishedCount = unfinishedCount;
		this.proportion = proportion;
	}
	
	// Property accessors

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}


	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmName() {
		return this.xmName;
	}

	public void setXmName(String xmName) {
		this.xmName = xmName;
	}

	public String getXmChildNumber() {
		return this.xmChildNumber;
	}

	public void setXmChildNumber(String xmChildNumber) {
		this.xmChildNumber = xmChildNumber;
	}

	public String getXmChildName() {
		return this.xmChildName;
	}

	public void setXmChildName(String xmChildName) {
		this.xmChildName = xmChildName;
	}

	public String getUnitId() {
		return this.unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUnitParent() {
		return this.unitParent;
	}

	public void setUnitParent(String unitParent) {
		this.unitParent = unitParent;
	}

	public Integer getIssue() {
		return this.issue;
	}

	public void setIssue(Integer issue) {
		this.issue = issue;
	}

	public String getRequires() {
		return this.requires;
	}

	public void setRequires(String requires) {
		this.requires = requires;
	}

	public Integer getFileCount() {
		return this.fileCount;
	}

	public void setFileCount(Integer fileCount) {
		this.fileCount = fileCount;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMaterialsId() {
		return this.materialsId;
	}

	public void setMaterialsId(String materialsId) {
		this.materialsId = materialsId;
	}

	public Timestamp getPublishTime() {
		return this.publishTime;
	}

	public void setPublishTime(Timestamp publishTime) {
		this.publishTime = publishTime;
	}

	public Timestamp getDeadLine() {
		return this.deadLine;
	}

	public void setDeadLine(Timestamp deadLine) {
		this.deadLine = deadLine;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getAcceptstatus() {
		return this.acceptstatus;
	}

	public void setAcceptstatus(Integer acceptstatus) {
		this.acceptstatus = acceptstatus;
	}

	public Float getDegree() {
		return this.degree;
	}

	public void setDegree(Float degree) {
		this.degree = degree;
	}

	public String getQuality() {
		return this.quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Timestamp getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Timestamp finishTime) {
		this.finishTime = finishTime;
	}

	public Xm getXm() {
		return xm;
	}

	public void setXm(Xm xm) {
		this.xm = xm;
	}

	public XmChild getXmChild() {
		return xmChild;
	}

	public void setXmChild(XmChild xmChild) {
		this.xmChild = xmChild;
	}

	public List<XmChild> getXmChildList() {
		return xmChildList;
	}

	public void setXmChildList(List<XmChild> xmChildList) {
		this.xmChildList = xmChildList;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getHelpUserName() {
		return helpUserName;
	}

	public void setHelpUserName(String helpUserName) {
		this.helpUserName = helpUserName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	
	
	public String[] getHelpUserNameList() {
		return helpUserNameList;
	}

	public void setHelpUserNameList(String[] helpUserNameList) {
		this.helpUserNameList = helpUserNameList;
	}

	public String[] getUnitNameList() {
		return unitNameList;
	}

	public void setUnitNameList(String[] unitNameList) {
		this.unitNameList = unitNameList;
	}

	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverUnitName() {
		return receiverUnitName;
	}

	public void setReceiverUnitName(String receiverUnitName) {
		this.receiverUnitName = receiverUnitName;
	}

	public String[] getUserNameList() {
		return userNameList;
	}

	public void setUserNameList(String[] userNameList) {
		this.userNameList = userNameList;
	}
	
	public Integer getFinishCount() {
		return finishCount;
	}

	public void setFinishCount(Integer finishCount) {
		this.finishCount = finishCount;
	}

	public Integer getUnfinishedCount() {
		return unfinishedCount;
	}

	public void setUnfinishedCount(Integer unfinishedCount) {
		this.unfinishedCount = unfinishedCount;
	}

	public Double getProportion() {
		return proportion;
	}

	public void setProportion(Double proportion) {
		this.proportion = proportion;
	}
	
	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	@Override
	public String toString() {
		return "XmChild [id=" + id + ", xmNumber=" + xmNumber + ", xmName=" + xmName + ", xmChildNumber="
				+ xmChildNumber + ", xmChildName=" + xmChildName + ", unitId=" + unitId + ", userName=" + userName
				+ ", unitParent=" + unitParent + ", issue=" + issue + ", requires=" + requires + ", fileCount="
				+ fileCount + ", fileName=" + fileName + ", materialsId=" + materialsId + ", publishTime=" + publishTime
				+ ", deadLine=" + deadLine + ", status=" + status + ", acceptstatus=" + acceptstatus + ", degree="
				+ degree + ", quality=" + quality + ", finishTime=" + finishTime + ", xm=" + xm + ", xmChild=" + xmChild
				+ ", xmChildList=" + xmChildList + ", userList=" + userList + ", helpUserName=" + helpUserName
				+ ", unitName=" + unitName + ", receiverName=" + receiverName + ", receiverUnitName=" + receiverUnitName
				+ ", helpUserNameList=" + Arrays.toString(helpUserNameList) + ", unitNameList="
				+ Arrays.toString(unitNameList) + ", userNameList=" + Arrays.toString(userNameList) + "]";
	}

	public List<Instructions> getPsList() {
		return psList;
	}

	public void setPsList(List<Instructions> psList) {
		this.psList = psList;
	}
	
	
}