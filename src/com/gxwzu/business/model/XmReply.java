package com.gxwzu.business.model;

import java.sql.Timestamp;

public class XmReply  implements java.io.Serializable {

    private Integer id;
    private String content;
    private Integer reUserId;
    private String reUserName;
    private String xmNumber;
    private Integer scheduleId;
    private Timestamp createTime;
     
	public XmReply() {
		super();
	}

	public XmReply(Integer id, String content, Integer reUserId,
			String reUserName, 
			String xmNumber, Integer scheduleId, Timestamp createTime) {
		super();
		this.id = id;
		this.content = content;
		this.reUserId = reUserId;
		this.reUserName = reUserName;
		this.xmNumber = xmNumber;
		this.scheduleId = scheduleId;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getReUserId() {
		return reUserId;
	}

	public void setReUserId(Integer reUserId) {
		this.reUserId = reUserId;
	}

	public String getReUserName() {
		return reUserName;
	}

	public void setReUserName(String reUserName) {
		this.reUserName = reUserName;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


}