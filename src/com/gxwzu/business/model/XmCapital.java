package com.gxwzu.business.model;

/**
 * XmCapital entity. @author MyEclipse Persistence Tools
 */

public class XmCapital implements java.io.Serializable {

	// Fields

	private Integer id;
	private String investTotal;
	private String investSource;
	private String investPlan;
	private String investComplete;
	private String xmNumber;

	// Constructors

	/** default constructor */
	public XmCapital() {
	}

	/** full constructor */
	public XmCapital(String investTotal, String investSource,
			String investPlan, String investComplete, String xmNumber) {
		this.investTotal = investTotal;
		this.investSource = investSource;
		this.investPlan = investPlan;
		this.investComplete = investComplete;
		this.xmNumber = xmNumber;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvestTotal() {
		return this.investTotal;
	}

	public void setInvestTotal(String investTotal) {
		this.investTotal = investTotal;
	}

	public String getInvestSource() {
		return this.investSource;
	}

	public void setInvestSource(String investSource) {
		this.investSource = investSource;
	}

	public String getInvestPlan() {
		return this.investPlan;
	}

	public void setInvestPlan(String investPlan) {
		this.investPlan = investPlan;
	}

	public String getInvestComplete() {
		return this.investComplete;
	}

	public void setInvestComplete(String investComplete) {
		this.investComplete = investComplete;
	}

	public String getXmNumber() {
		return this.xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

}