package com.gxwzu.business.service.project;

import java.io.File;
import java.util.List;

import com.gxwzu.business.model.Materials;
import com.gxwzu.core.service.BaseService;

public interface MaterialsService extends BaseService<Materials>{

	/**
	 * 保存用户提交的文件
	 * @author SunYi
	 * @Date 2019年3月14日下午9:54:43
	 * @return boolean
	 */
	boolean saveFileList(File[] myfile, String[] myfileFileName, String userName, String xcId,String xmId, String helpUserName);

	Materials save(Materials materials);
	
	/**
	 * 
	 * @Description 根据教研室和任务编号获取所有材料信息
	 * @author 莫东林
	 * @date 2019年3月7日下午3:42:48
	 * List<Materials>
	 */
	List<Materials> getByUnit(String unitId, String xmNumber);
	
	/**
	 * 
	 * @Description 根据工号和任务编号获取所有材料信息
	 * @author 莫东林
	 * @date 2019年3月7日下午3:42:48
	 * List<Materials>
	 */
	List<Materials> getByUser(String userId, String xmNumber);
	
	
	/**
	 * 附件上传
	 * 需要值：文件myfile，文件名myfileFileName，项目编号xmNumber
	 * @author SunYi
	 * @Date 2019年3月14日下午2:39:47
	 * @return String
	 */
	boolean saveAttachment(File[] myfile, String[] myfileFileName, String xmNumber);

}
