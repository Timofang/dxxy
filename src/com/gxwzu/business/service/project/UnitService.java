package com.gxwzu.business.service.project;

import com.gxwzu.business.model.Unit;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

import java.util.List;

public interface UnitService extends BaseService<Unit>{

	public  List<Unit> findAll();

	public  Result<Unit> find(Unit model, int page, int size);

	public  List<Unit> findByExample(Unit model);
	
	public Unit  add(Unit Unit);

	public  List<Unit> findAllById(String findId);
	public  Unit findById(String unitId);

	public void del(String delId);

	public  List updateUnitInfo(Unit model);
	
	public Unit getByUserId(String userName);
	
}