package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmInvest;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmInvestService extends BaseService<XmInvest> {

	public abstract List<XmInvest> findAll();

	public abstract Result<XmInvest> find(XmInvest model, int page, int size);

	public abstract List<XmInvest> findByExample(XmInvest model);

	public abstract XmInvest findById(Integer findId);

	public abstract void add(XmInvest xmInvest);

	public abstract void del(Integer id);

}