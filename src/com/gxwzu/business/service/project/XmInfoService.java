package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.sysVO.*;

public interface XmInfoService extends BaseService<XmInfo> {

	public abstract List<XmInfo> findAll();

	public abstract Result<XmInfo> find(XmInfo model, int page, int size);
	
	public abstract Result<XmInfo> findPoor(XmInfo model, int page, int size);

	public abstract List<XmInfo> findByExample(XmInfo model);

	public abstract XmInfo findById(Integer findId);

	public abstract void add(XmInfo xmInfo, XmCapital xmCapital);
	
	public abstract void addPoorXm(XmInfo xmInfo, XmCapital xmcapital);

	public abstract ViewXmInfo viewInfo(String xmNumber);

	public abstract List<ViewXmInfo> allViewXmInfoByYear(String thisYear, String thisUserId, String thisBudgetType, String thisInfoName, String thisProjectType);

	public abstract List<ViewXmInfo> allViewXmPoorInfoByYear(String thisYear, String thisUserId, String thisBudgetType, String thisInfoName, String thisProjectType);
	
	public abstract List<ViewXmInfo> userViewXmInfoByYear(String thisYear, String thisBudgetType, String thisProjectType);

	public abstract List<XmInfo> findCoordinate(XmInfo model);
	
	public abstract List<XmInfo> findPoorXmCoordinate(XmInfo model);

	public abstract List<ViewToXmInfo> toXmInfo(String thisLandNumber);

	public abstract List<AutoCompleteHttpModel> likeAll(String keyword);

	public abstract Result<ViewXmInfo> resultViewXmInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, int page, int size);
	
	public abstract Result<ViewXmInfo> resultViewPoorXmInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, int page, int size);

	public abstract downViewXmInfo viewXmInfoDownload(String thisYear, String thisPeopleId, String thisBudgetType);

	public abstract void delete(Integer xmInfoId);

	public abstract String createNO(String thisNO);
	
	public abstract String createPoorXmNo(String thisNo);

	public abstract List<ViewXm> allViewXm(String thisBudgetType);

	public abstract String allViewXmType();

	public abstract List<ViewXm> allViewXmByUser(String thisUserId);

	public abstract List<ViewXm> findXmByNum(String xmNumber);

	public abstract List<ViewXmChild> findAllXmChild();

	public abstract List<ViewXmChild> findXmChildByNum(String thisUserId, String thisInfoName);

	public abstract List<ViewXmChild> allViewXmChildByUser(String thisUserId);

	public abstract List<ViewXmChild> findMyIssueByUnit(String thisUserId);

	public abstract List<ViewXm> findMyIssueByCollager(String thisUserId);

	public abstract List<ViewXm> findMyIssueByCollagerByNum(String thisUserId);

	public abstract List<ViewXmChild> findMyIssueByUnitByNum(String thisUserId, String thisUserName, String thisPsNumber);
}