package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmtolandService extends BaseService<Xmtoland>{

	public abstract List<Xmtoland> findAll();

	public abstract Result<Xmtoland> find(Xmtoland model, int page, int size);

	public abstract List<Xmtoland> findByExample(Xmtoland model);

	public abstract Xmtoland findById(Integer findId);

}