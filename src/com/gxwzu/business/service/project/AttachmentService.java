package com.gxwzu.business.service.project;

import java.io.File;
import java.util.List;

import com.gxwzu.business.model.Attachment;
import com.gxwzu.core.service.BaseService;

/**
 * 附件业务层接口
 * @Author SunYi
 * @date 2019年3月22日下午10:07:04
 */
public interface AttachmentService extends BaseService<Attachment>{

	/**
	 * 附件上传
	 * 需要值：文件myfile，文件名myfileFileName，项目编号xmNumber
	 * @author SunYi
	 * @Date 2019年3月14日下午2:39:47
	 * @return String
	 */
	public abstract boolean saveAttachment(File[] myfile, String[] myfileFileName, String xmNumber);
	
	/**
	 * 查询指定项目编号的附件信息
	 * @author SunYi
	 * @Date 2019年3月28日下午5:51:32
	 * @return List<Attachment>
	 */
	public abstract List<Attachment> getAttachmentByXmNuber(String xmNumber);

}
