package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmTask;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmTaskService extends BaseService<XmTask>{

	public abstract List<XmTask> findAll();

	public abstract Result<XmTask> find(XmTask model, int page, int size);

	public abstract List<XmTask> findByExample(XmTask model);

	public abstract XmTask findById(Integer findId);

}