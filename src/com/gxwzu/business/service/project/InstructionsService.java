package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.Instructions;

/**
 * 任务批示业务层接口
 * @ClassName: InstructionsService
 * @author SunYi
 * @Date 2019年4月8日上午9:50:12
 */
public interface InstructionsService {
	
	 /**
	  * 添加任务批示
	  * @author SunYi
	  * @Date 2019年4月8日上午9:52:44
	  * @return void
	  */
	public abstract void xmInstructionsAdd(Instructions instructions);

	/**
	 * 获取指定任务编号的任务批示
	 * @author SunYi
	 * @Date 2019年4月8日上午11:20:43
	 * @return List<Instructions>
	 */
	public abstract List<Instructions> getInstructionsByXmNumber(String thisXmNumber);
}
