package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmCapital;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.sysVO.ViewXmCapital;

public interface XmCapitalService extends BaseService<XmCapital> {

	public abstract List<XmCapital> findAll();

	public abstract Result<XmCapital> find(XmCapital model, int page, int size);

	public abstract List<XmCapital> findByExample(XmCapital model);

	public abstract XmCapital findById(Integer findId);

	public abstract List<ViewXmCapital> findCapitalInRate(String thisXmNumber, String thisYear);

}