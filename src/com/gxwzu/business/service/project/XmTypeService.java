package com.gxwzu.business.service.project;

import com.gxwzu.business.model.XmType;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

import java.util.List;

/**
 * @Author: soldier
 * @Date: 2019/3/5 9:06
 * @Desc:
 */
public interface XmTypeService extends BaseService<XmType> {

    public abstract  List<XmType> findAll();

    public abstract Result<XmType> find(XmType model, int page, int size);

    public abstract List<XmType> findByExample(XmType model);

    public abstract XmType  add(XmType model);

    public abstract XmType findById(Integer typeId);

    public abstract void del(Integer delId);
}
