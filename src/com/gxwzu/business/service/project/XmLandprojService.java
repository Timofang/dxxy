package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.sysVO.ViewToLandproj;
import com.gxwzu.sysVO.ViewXmLandproj;
import com.gxwzu.sysVO.downViewXmLandproj;

public interface XmLandprojService extends BaseService<XmLandproj>{

	public abstract List<XmLandproj> findAll();

	public abstract Result<XmLandproj> find(XmLandproj model, int page, int size);

	public abstract List<XmLandproj> findByExample(XmLandproj model);

	public abstract XmLandproj findById(Integer findId);

	public abstract List<XmLandproj> findCoordinate(XmLandproj model);

	public abstract void add(XmLandproj xmLandproj);

	public abstract List<ViewXmLandproj> findXmLandproj(String thisYear, String thisUserId, String thisLandType, String thisLandName);

	public abstract List<AutoCompleteHttpModel> likeAll(String keyword);

	public abstract List<ViewToLandproj> toLandproj(String thisXmNumber);

	public abstract Result<ViewXmLandproj> resultXmLandproj(String thisYear, String thisPeopleId, String thisLandType, int page, int size);

	public abstract downViewXmLandproj xmLandprojDownload(String thisYear, String thisPeopleId, String thisLandType);

	public abstract void delete(Integer thisXmLandprojId);

}