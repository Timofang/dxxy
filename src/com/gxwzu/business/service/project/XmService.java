package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmService extends BaseService<Xm> {
	
	public abstract Result<Xm> find(Xm model, int page, int size);

	/*
	 * 获取全部教研室信息到新建项目处勾选
	 * @auther 叶城廷
	 * @version	2019.2.28
	 */
	public abstract List<Unit> selUnit();
	
	/**
	 * 获取指定教研室所对应的项目信息
	 * @author SunYi
	 * @Date 2019年3月1日下午2:50:14
	 * @return List<Xm>
	 */
	public abstract Result<Xm> selXmByUnit(Xm model, int page, int size);
	
	/**
	 * 
	 * @Description 获取最大项目编号
	 * @author 莫东林
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */
	public String getMaxXmNumber();

	/**
	 * 
	* @Description: 任务发布
	* @Author 莫东林
	* @date 2019年3月5日下午8:13:57
	* @return void
	* @throws
	 */
	public abstract void add(Xm xm);
	
	/**
	 * 
	* @Description: 根据任务编号获取任务
	* @Author 莫东林
	* @date 2019年3月5日下午8:14:50
	* @return Xm
	* @throws
	 */
	public Xm getByXmNumber(String xmNumber);
	
	/**
	 * 
	 * @Description 删除院项目表
	 * @author 杨长官
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */	
	public abstract void delete(String xmNumber);

	/**
	 * 
	* @Description: 根据用户查找下发的任务信息
	* @Author 莫东林
	* @date 2019年3月13日下午5:36:47
	* @return Result<Xm>
	* @throws
	 */
	public abstract Result<Xm> findIssueByUser(Xm xm, Integer page, int i);

	/**
	 * 
	* @Description: 根据子项目编号查询其子父项目信息
	* @Author 莫东林
	* @date 2019年3月14日下午9:47:41
	* @return Xm
	* @throws
	 */
	public abstract Xm getByXmChildNumber(String thisXmNumber);
	
	/**
	 * 查询个人院任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public abstract List xmstatisticsDetails(String username);

	String getMaxXmNumber2();
	
}


