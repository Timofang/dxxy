package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmDuty;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmDutyService extends BaseService<XmDuty>  {

	public abstract List<XmDuty> findAll();

	public abstract Result<XmDuty> find(XmDuty model, int page, int size);

	public abstract List<XmDuty> findByExample(XmDuty model);

	public abstract XmDuty findById(Integer findId);

	public abstract void add(XmDuty xmDuty);

	public abstract void del(Integer id);

}