package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.sysVO.ViewPoorXmSchedule;
import com.gxwzu.sysVO.ViewPsInfoModel;
import com.gxwzu.sysVO.ViewXmSchedule;
import com.gxwzu.sysVO.ViewXmScheduleInInfo;

public interface XmScheduleService extends BaseService<XmSchedule> {

	public abstract List<XmSchedule> findAll();

	public abstract Result<XmSchedule> find(XmSchedule model, int page, int size);

	public abstract List<XmSchedule> findByExample(XmSchedule model);

	public abstract XmSchedule findById(Integer findId);

	public abstract Result<ViewXmSchedule> findXmScheduleByNum(String thisXmNumber, String thisType, String thisLoginUserId, int page, int size);

	public abstract void del(String thisXmScheduleId);

	public abstract Result<ViewXmSchedule> viewXmScheduleInInfo(int page, int size);
	
	public abstract Result<ViewPsInfoModel> viewPoorXmScheduleInInfo(int page, int size);

	public abstract String addReply(XmReply xmReply);

}