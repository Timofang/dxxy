package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmSchedulepicService extends BaseService<XmSchedulepic> {

	public abstract List<XmSchedulepic> findAll();

	public abstract Result<XmSchedulepic> find(XmSchedulepic model, int page,
			int size);

	public abstract List<XmSchedulepic> findByExample(XmSchedulepic model);

	public abstract XmSchedulepic findById(Integer findId);

	public abstract void delPic(String thisPicId);

}