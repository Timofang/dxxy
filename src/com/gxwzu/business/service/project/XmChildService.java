package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.system.model.user.User;

public interface XmChildService extends BaseService<XmChild> {
	/**
	 * 查询个人教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public abstract List unitstatisticsDetails(String username);
	/**
	 * 统计所有任务的完成情况
	 * @author SunYi
	 * @Date 2019年3月18日下午9:47:13
	 * @return Result<Xm>
	 */
	public abstract List<User> getXmStatistics();
	
	
	/**
	 * 通过子项目编号和接收人获取项目文件信息
	 * @author SunYi
	 * @param userName 
	 * @Date 2019年3月11日下午11:04:37
	 * @return XmChild
	 */
	public abstract XmChild getFileByXmNumber(String xmChildNumber);
	
	/**
	 * 
	 * @Description 分页查询所有子项目及其父项目信息
	 * @author 莫东林
	 * @date 2019年3月12日上午1:06:57
	 * Result<XmChild>
	 */
	public abstract Result<XmChild> find(XmChild model, int page, int size);
	
	/**
	 * 
	* @Description: 添加副项目
	* @Author 莫东林
	* @date 2019年3月2日下午10:42:07
	* @return void
	* @throws
	 */
	public abstract void add(XmChild xmChild);

	/**
     * 查询员工个人所有任务
     * @author 叶城廷
     * @version 2019.03.03
	 * @param size 
	 * @param page 
     */
	public abstract Result<XmChild> selUserTask(User currentUser, Integer page, int size);

	/**
	 * 
	* @Description: 根据用户查找子项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午6:29:31
	* @return Result<XmChild>
	* @throws
	 */
	public abstract Result<XmChild> findByUser(XmChild xmChild, Integer page, int i);

	/**
	 * 
	* @Description: 获取用户下发的项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午7:00:20
	* @return Result<XmChild>
	* @throws
	 */
	public abstract Result<XmChild> findIssueByUser(XmChild xmChild, Integer page, int i);

	/*
	* @Description: 通过任务编号以及接收者获取任务信息
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
	public abstract XmChild getChildByXmUser(String thisXmNumber, String unitId);

	public abstract void updateXmChild(XmChild c);

	/**
	 * 
	* @Description: 通过子子项目项目编号获取其及其父项目信息
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	 */
	public abstract XmChild getByXmChildNumber(String thisXmNumber);
	
	/**
	 * 
	* @Description: 通过子项目编号获取其子项目信息
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	 */
	public XmChild getByXmUnitChildNumber(String thisXmNumber, String unitId);

	XmChild getByXmUnitChildNumberX(String thisXmNumber, String unitId);

	XmChild getByXmUnitChildNumberXX(String thisXmNumber, String unitId);

	List<XmChild> findXmChildByNum(String thisUserId, String thisInfoName);

	List<XmChild> allViewXmChildByUser(String thisUserId);

	public abstract List<XmChild> findAllXmChild();

	XmChild getByUserXmNumber(String userName, String unitId);

	public abstract void saveQuality(String userName, String xmNumber, String quality);

	public abstract XmChild getByUnitXmNumber(String string, String xmNumber);

	public abstract void saveQualitys(String userName, String xmNumber, String degree);
	
	public abstract List<XmChild> findByLikeXmNumber(String xmNumber, String unitId);
	
}





















