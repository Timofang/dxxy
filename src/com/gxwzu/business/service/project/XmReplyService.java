package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmReply;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;

public interface XmReplyService extends BaseService<XmReply>{

	public abstract List<XmReply> findAll();

	public abstract Result<XmReply> find(XmReply model, int page, int size);

	public abstract List<XmReply> findByExample(XmReply model);

	public abstract XmReply findById(Integer findId);

}