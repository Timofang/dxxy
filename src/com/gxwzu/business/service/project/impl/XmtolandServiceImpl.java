package com.gxwzu.business.service.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmCapitalDao;
import com.gxwzu.business.dao.project.XmtolandDao;
import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.business.service.project.XmCapitalService;
import com.gxwzu.business.service.project.XmtolandService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.sysVO.ViewXmCapital;

/**
 * 项目-征地项目关联ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmtolandServiceImpl
 * <br>Date: 2017-9-5上午11:28:53
 * <br>log:
 */
@Service("xmtolandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmtolandServiceImpl extends BaseServiceImpl<Xmtoland> implements XmtolandService {
	
	private XmtolandDao xmtolandDao;

	@Autowired
	public void setXmtolandDao(XmtolandDao xmtolandDao) {
		this.xmtolandDao = xmtolandDao;
	}

	@Override
	public BaseDao<Xmtoland> getDao() {
		return this.xmtolandDao;
	}
	
	@Override
	public List<Xmtoland> findAll() {
		return xmtolandDao.getAll(Xmtoland.class);
	}

	@Override
	public Result<Xmtoland> find(Xmtoland model, int page, int size) {
		return xmtolandDao.find(model, page, size);
	}

	@Override
	public List<Xmtoland> findByExample(Xmtoland model) {
		return xmtolandDao.findByExample(model);
	}
	
	@Override
	public Xmtoland findById(Integer findId) {
		return xmtolandDao.findById(findId);
	}

}
