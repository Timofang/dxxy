package com.gxwzu.business.service.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.UnitDao;
import com.gxwzu.business.dao.project.XmDao;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.service.project.MaterialsService;
import com.gxwzu.business.service.project.XmChildService;
import com.gxwzu.business.service.project.XmService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;


@Service("xmService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmServiceImpl extends BaseServiceImpl<Xm> implements XmService  {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private XmDao xmDao;
	@Autowired
	private UnitDao unitDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private MaterialsService materialsService;
	@Autowired
	private UserService userService;
	@Autowired
	private XmChildService xmChildService;
	
	@Override
	public Result<Xm> find(Xm model, int page, int size) {
		return xmDao.find(model, page, size);
	}

	@Override
	public BaseDao<Xm> getDao() {
		return this.xmDao;
	}
	
	
	/**
	 * 获取指定教研室所对应的项目信息
	 * @author SunYi
	 * @Date 2019年3月1日下午2:50:14
	 * @return List<Xm>
	 */
	@Override
	public Result<Xm> selXmByUnit(Xm model, int page, int size) {
		return xmDao.selXmByUnit(model, page, size);
	}
	
	
	/**
	 * 
	 * @Description 获取最大项目编号
	 * @author 莫东林
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */
	public String getMaxXmNumber(){
		Xm xmInfo = xmDao.getMaxXmNumber();
		int xmN;
		//任务表为空
		if(xmInfo == null){
			xmN = 1;
		}else{
			//截取项目编号并+1(项目编号：院编号+自增编号+领导人编号)
			xmN = Integer.parseInt(xmInfo.getXmNumber().substring(1, xmInfo.getXmNumber().length() - xmInfo.getUsername().length()))+1;
		}
		return "B" + xmN;
	}
		
	
	/**
	 * 
	 * @Description 获取最大项目编号
	 * @author 莫东林
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */
	@Override
	public String getMaxXmNumber2(){
		Xm xmInfo = xmDao.getMaxXmNumber();
		int xmN;
		//任务表为空
		if(xmInfo == null){
			xmN = 1;
		}else{
			//截取项目编号并+1(项目编号：院编号+自增编号+领导人编号)
			xmN = Integer.parseInt(xmInfo.getXmNumber().substring(1, xmInfo.getXmNumber().length() - xmInfo.getUsername().length()))+1;
		}
		return "B" + xmN;
	}

	/*
	 * 获取全部教研室信息到新建项目处勾选
	 * @auther 叶城廷
	 * @version	2019.2.28
	 */
	@Override
	public List<Unit> selUnit() {
		
		return unitDao.selAllUnit();
	}

	@Override
	public void add(Xm xm) {
		System.out.println(xm);
		xmDao.save(xm);
		
	}
	
	/*
	 * 删除院项目表
	 * @auther 杨长官
	 * @version	2019.2.28
	 */
	@Override
	public void delete(String xmNumber) {
		try {
			xmDao.delete(xmNumber);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	* @Description: 根据任务编号获取院任务详情信息
	* @Author 莫东林
	* @date 2019年3月5日下午8:15:30
	* @throws
	 */
	@Override
	public Xm getByXmNumber(String xmNumber) {
		// 获取指定编号的项目信息
		Xm xm = xmDao.getByXmNumber(xmNumber);
		// 获取项目发布人信息
		User xmUser = userDao.getUserById(xm.getUsername());
		//指派到教研室
		if(xm.getReceiverUnit() != null){
			//存放教研室对象集合
			List<Unit> unitList = new ArrayList<Unit>();
			String[] unitIdList = xm.getReceiverUnit().split(",");
			//检出每个教研室对象
			for(int i = 0; i < unitIdList.length; i++){
				Unit unit = unitDao.findById(unitIdList[i]);
				//检出每个教研室关于此项目的信息
				XmChild xmChild = xmChildService.getByUnitXmNumber(unitIdList[i], xmNumber);
				unit.setXmChild(xmChild);
				//存放教研室对应所有材料
				List<Materials> materialsList = materialsService.getByUnit(unitIdList[i], xm.getXmNumber());
				unit.setMaterialsList(materialsList);
				unitList.add(unit);
			}
			xm.setUnitList(unitList);
		}
		
		//指派到教职工
		if(xm.getReceiver() != null){
			//存放教师对象集合
			List<User> userList = new ArrayList<User>();
			String[] userIdList = xm.getReceiver().split(",");
			//检出每个教师对象
			for(int i = 0; i < userIdList.length; i++){
				User user = userService.getByUserName(userIdList[i]);
				//检出每个教职工关于此项目的信息
				XmChild xmChild = xmChildService.getByUserXmNumber(userIdList[i], xmNumber);
				user.setXmChild(xmChild);
				//存放教研室对应所有材料
				List<Materials> materialsList = materialsService.getByUser(userIdList[i], xm.getXmNumber());
				user.setMaterialsList(materialsList);
				userList.add(user);
			}
			xm.setUserList(userList);
		}
		// 保存项目发布人姓名
		xm.setHelpName(xmUser.getHelpName());
		return xm;
	}

	/**
	 * 
	* @Description: 根据用户查找下发的任务信息
	* @Author 莫东林
	* @date 2019年3月13日下午5:36:47
	* @return Result<Xm>
	* @throws
	 */
	@Override
	public Result<Xm> findIssueByUser(Xm xm, Integer page, int size) {
		Result<Xm> xmList = xmDao.findIssueByUser(xm, page, size);
		return xmList;
	}

	/**
	 * 
	* @Description: 根据子项目编号查询其子父项目信息
	* @Author 莫东林
	* @date 2019年3月14日下午9:47:41
	* @return Xm
	* @throws
	 */
	@Override
	public Xm getByXmChildNumber(String thisXmNumber) {
		Xm xm = xmDao.getByXmNumber(thisXmNumber);
		//存放教师对象集合
		List<User> userList = new ArrayList<User>();
		String[] userIdList = xm.getReceiver().split(",");
		//检出每个教师对象
		for(int i = 0; i < userIdList.length; i++){
			User user = userService.getByUserName(userIdList[i]);
			//存放教研室对应所有材料
			List<Materials> materialsList = materialsService.getByUser(userIdList[i], xm.getXmNumber());
			user.setMaterialsList(materialsList);
			userList.add(user);
		}
		xm.setUserList(userList);
	
		return xm;
	}
	/**
	 * 查询个人院任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public  List xmstatisticsDetails(String username){
	/*	1.查询院任务发到个人
		2.查询院任务发到教研室
		3.拼接List
		*/
		List ToP= xmDao.xmstatisticsDetails(username);
		List ToU=xmDao.xmToUnitStatisticsDetails(username);
		ToP.addAll(ToU); return ToP;
	
	}
	
}
