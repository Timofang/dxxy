package com.gxwzu.business.service.project.impl;

import com.gxwzu.business.dao.project.XmTypeDao;
import com.gxwzu.business.model.XmType;
import com.gxwzu.business.service.project.XmTypeService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: soldier
 * @Date: 2019/3/5 9:06
 * @Desc:
 */
@Service("xmTypeService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmTypeServiceImpl extends BaseServiceImpl<XmType> implements XmTypeService {

	private XmType XmType;
	private XmTypeDao XmTypeDao;

	public XmType getXmType() {
		return XmType;
	}
	public void setXmType(XmType xmType) {
		XmType = xmType;
	}

	@Autowired
	public void setXmTypeDao(XmTypeDao XmTypeDao) {
		this.XmTypeDao = XmTypeDao;
	}

	@Override
	public BaseDao<XmType> getDao() {
		return this.XmTypeDao;
	}

	@Override
	public List<XmType> findAll() {
		return XmTypeDao.getAll(XmType.class);
	}

	@Override
	public Result<XmType> find(XmType model, int page, int size) {
		return XmTypeDao.find(model,page,size);
	}

	//根据用户输入信息查询
	@Override
	public List<XmType> findByExample(XmType model) {
		List params = new ArrayList<Object>();
		String queryString = "from XmType model where 1=1";
		if(model.getTypeId()!=null){
			queryString =queryString+" and model.typeId=?";
			params.add(model.getTypeId());
		}
		if(model.getTypeName()!=null){
			queryString =queryString+" and model.typeName=?";
            params.add(model.getTypeName());
		}
		return XmTypeDao.findByHql(queryString, params.toArray());
	}

	@Override
	public XmType add(XmType XmType) {
		try {
			XmTypeDao.save(XmType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return XmType;
	}

	@Override
	public XmType findById(Integer typeId) {
		return XmTypeDao.findById(typeId);
	}

	@Override
	public void del(Integer delId) {
		XmTypeDao.remove(XmTypeDao.findById(delId));
	}

}
