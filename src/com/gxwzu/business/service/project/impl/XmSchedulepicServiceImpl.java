package com.gxwzu.business.service.project.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmSchedulepicDao;
import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.business.service.project.XmSchedulepicService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 项目进度图片ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmSchedulepicServiceImpl
 * <br>Date: 2017-8-17下午06:39:41
 * <br>log:
 */
@Service("xmSchedulepicService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmSchedulepicServiceImpl extends BaseServiceImpl<XmSchedulepic> implements XmSchedulepicService {
	
	
	private XmSchedulepicDao xmSchedulepicDao;

	@Autowired
	public void setXmSchedulepicDao(XmSchedulepicDao xmSchedulepicDao) {
		this.xmSchedulepicDao = xmSchedulepicDao;
	}

	@Override
	public BaseDao<XmSchedulepic> getDao() {
		return this.xmSchedulepicDao;
	}
	
	@Override
	public List<XmSchedulepic> findAll() {
		return xmSchedulepicDao.getAll(XmSchedulepic.class);
	}

	@Override
	public Result<XmSchedulepic> find(XmSchedulepic model, int page, int size) {
		return xmSchedulepicDao.find(model, page, size);
	}

	@Override
	public List<XmSchedulepic> findByExample(XmSchedulepic model) {
		return xmSchedulepicDao.findByExample(model);
	}
	
	@Override
	public XmSchedulepic findById(Integer findId) {
		return xmSchedulepicDao.findById(findId);
	}

	@Override
	public void delPic(String thisPicId) {
		try {
			XmSchedulepic delXmSchedulepic = xmSchedulepicDao.findById(Integer.parseInt(thisPicId));
			if(delXmSchedulepic != null){
				xmSchedulepicDao.remove(delXmSchedulepic);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
