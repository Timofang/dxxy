package com.gxwzu.business.service.project.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmCapitalDao;
import com.gxwzu.business.dao.project.XmDutyDao;
import com.gxwzu.business.dao.project.XmInvestDao;
import com.gxwzu.business.dao.project.XmLandprojDao;
import com.gxwzu.business.dao.project.XmReplyDao;
import com.gxwzu.business.dao.project.XmScheduleDao;
import com.gxwzu.business.dao.project.XmSchedulepicDao;
import com.gxwzu.business.dao.project.XmTaskDao;
import com.gxwzu.business.dao.project.XmtolandDao;
import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInvest;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.business.model.XmTask;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.sysVO.ViewToLandproj;
import com.gxwzu.sysVO.ViewXmLandproj;
import com.gxwzu.sysVO.downViewXmLandproj;

/**
 * 征地项目ServiceImpl实现类
 * 
 * @author liqing
 * @version 1.0 <br>
 *          Copyright (C), 2015, 梧州学院 软件研发中心 <br>
 *          This program is protected by copyright laws. <br>
 *          Program Name: XmLandprojServiceImpl <br>
 *          Date: 2017-9-5上午11:14:22 <br>
 *          log:
 */
@Service("xmLandprojService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = RuntimeException.class)
public class XmLandprojServiceImpl extends BaseServiceImpl<XmLandproj>
		implements XmLandprojService {

	private XmLandprojDao xmLandprojDao;
	private XmCapitalDao xmCapitalDao;
	private XmDutyDao xmDutyDao;
	private XmInvestDao xmInvestDao;
	private XmReplyDao xmReplyDao;
	private XmScheduleDao xmScheduleDao;
	private XmSchedulepicDao xmSchedulepicDao;
	private XmTaskDao xmTaskDao;
	private XmtolandDao xmtolandDao;

	@Autowired
	public void setXmtolandDao(XmtolandDao xmtolandDao) {
		this.xmtolandDao = xmtolandDao;
	}

	@Autowired
	public void setXmTaskDao(XmTaskDao xmTaskDao) {
		this.xmTaskDao = xmTaskDao;
	}

	@Autowired
	public void setXmSchedulepicDao(XmSchedulepicDao xmSchedulepicDao) {
		this.xmSchedulepicDao = xmSchedulepicDao;
	}

	@Autowired
	public void setXmScheduleDao(XmScheduleDao xmScheduleDao) {
		this.xmScheduleDao = xmScheduleDao;
	}

	@Autowired
	public void setXmReplyDao(XmReplyDao xmReplyDao) {
		this.xmReplyDao = xmReplyDao;
	}

	@Autowired
	public void setXmInvestDao(XmInvestDao xmInvestDao) {
		this.xmInvestDao = xmInvestDao;
	}

	@Autowired
	public void setXmDutyDao(XmDutyDao xmDutyDao) {
		this.xmDutyDao = xmDutyDao;
	}

	@Autowired
	public void setXmCapitalDao(XmCapitalDao xmCapitalDao) {
		this.xmCapitalDao = xmCapitalDao;
	}
	
	@Autowired
	public void setXmLandprojDao(XmLandprojDao xmLandprojDao) {
		this.xmLandprojDao = xmLandprojDao;
	}

	@Override
	public BaseDao<XmLandproj> getDao() {
		return this.xmLandprojDao;
	}

	@Override
	public List<XmLandproj> findAll() {
		return xmLandprojDao.getAll(XmLandproj.class);
	}

	@Override
	public Result<XmLandproj> find(XmLandproj model, int page, int size) {
		return xmLandprojDao.find(model, page, size);
	}

	@Override
	public List<XmLandproj> findByExample(XmLandproj model) {
		return xmLandprojDao.findByExample(model);
	}

	@Override
	public XmLandproj findById(Integer findId) {
		return xmLandprojDao.findById(findId);
	}

	@Override
	public List<XmLandproj> findCoordinate(XmLandproj model) {
		return xmLandprojDao.findCoordinate(model);
	}

	@Override
	public void add(XmLandproj model) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateStr = sdf.format(new Date());
			String NewNO = createNO("zd" + dateStr);

			// 1.保存项目基本情况
			model.setLandNumber(NewNO);
			xmLandprojDao.save(model);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void delete(Integer thisXmLandprojId) {
		try {
			if(thisXmLandprojId != null && !"".equals(thisXmLandprojId)){
				XmLandproj delXmLandproj=xmLandprojDao.get(XmLandproj.class, thisXmLandprojId);
				//1.删除资金信息xm_capital
				XmCapital delXmCapital = new XmCapital();
				delXmCapital.setXmNumber(delXmLandproj.getLandNumber());
				List<XmCapital> listXmCapital = xmCapitalDao.findByExample(delXmCapital);
				for (int i = 0; i < listXmCapital.size(); i++) {
					delXmCapital = listXmCapital.get(i);
					xmCapitalDao.remove(delXmCapital);
				}
				//2.删除责任人xm_duty
				XmDuty delXmDuty = new XmDuty();
				delXmDuty.setXmNumber(delXmLandproj.getLandNumber());
				List<XmDuty> listXmDuty = xmDutyDao.findByExample(delXmDuty);
				for (int i = 0; i < listXmDuty.size(); i++) {
					delXmDuty = listXmDuty.get(i);
					xmDutyDao.remove(delXmDuty);
				}
				//3.资金投资进度xm_invest
				XmInvest delXmInvest = new XmInvest();
				delXmInvest.setXmNumber(delXmLandproj.getLandNumber());
				List<XmInvest> listXmInvest = xmInvestDao.findByExample(delXmInvest);
				for (int i = 0; i < listXmInvest.size(); i++) {
					delXmInvest = listXmInvest.get(i);
					xmInvestDao.remove(delXmInvest);
				}
				//5.工作进度xm_schedule
				XmSchedule delXmSchedule = new XmSchedule();
				delXmSchedule.setXmNumber(delXmLandproj.getLandNumber());
				List<XmSchedule> listXmSchedule = xmScheduleDao.findByExample(delXmSchedule);
				for (int i = 0; i < listXmSchedule.size(); i++) {
					delXmSchedule = listXmSchedule.get(i);
					
					//6.工作进度图片xm_schedulepic
					XmSchedulepic delXmSchedulepic = new XmSchedulepic();
					delXmSchedulepic.setScheduleId(delXmSchedule.getId());
					List<XmSchedulepic> listXmSchedulepic = xmSchedulepicDao.findByExample(delXmSchedulepic);
					for (int j = 0; j < listXmSchedulepic.size(); j++) {
						delXmSchedulepic = listXmSchedulepic.get(j);
						xmSchedulepicDao.remove(delXmSchedulepic);
					}
					
					xmScheduleDao.remove(delXmSchedule);
				}
				
				//4.回复xm_reply
				XmReply delXmReply = new XmReply();
				delXmReply.setXmNumber(delXmLandproj.getLandNumber());
				List<XmReply> listXmReply = xmReplyDao.findByExample(delXmReply);
				for (int i = 0; i < listXmReply.size(); i++) {
					delXmReply = listXmReply.get(i);
					xmReplyDao.remove(delXmReply);
				}
				//7.征地任务周报xm_task
				XmTask delXmTask = new XmTask();
				delXmTask.setLandNumber(delXmLandproj.getLandNumber());
				List<XmTask> listXmTask = xmTaskDao.findByExample(delXmTask);
				for (int i = 0; i < listXmTask.size(); i++) {
					delXmTask = listXmTask.get(i);
					xmTaskDao.remove(delXmTask);
				}
				//8.征地-项目关联xmtoland
				Xmtoland delXmtoland = new Xmtoland();
				delXmtoland.setLandNumber(delXmLandproj.getLandNumber());
				List<Xmtoland> listXmtoland = xmtolandDao.findByExample(delXmtoland);
				for (int i = 0; i < listXmtoland.size(); i++) {
					delXmtoland = listXmtoland.get(i);
					xmtolandDao.remove(delXmtoland);
				}
				//9.项目xm_landproj
				xmLandprojDao.remove(delXmLandproj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 征地信息列表
	 * 
	 * @return
	 */
	@Override
	public List<ViewXmLandproj> findXmLandproj(String thisYear, String thisPeopleId, String thisLandType, String thisLandName) {

		List<ViewXmLandproj> rsView = new ArrayList<ViewXmLandproj>();

		StringBuffer sqlBuff = new StringBuffer();

		sqlBuff.append(" SELECT ldas.id, ");
		sqlBuff.append(" ldas.landNumber, ");
		sqlBuff.append(" ldas.name, ");
		sqlBuff.append(" ldas.laYear, ");
		sqlBuff.append(" ldas.cycle, ");
		sqlBuff.append(" ldas.approval, ");
		sqlBuff.append(" ldas.largeInvest, ");
		sqlBuff.append(" ldas.owner, ");
		sqlBuff.append(" ldas.address, ");
		sqlBuff.append(" ldas.coordinate, ");
		sqlBuff.append(" ldas.coordinatePly, ");
		sqlBuff.append(" ldas.landTotal, ");
		sqlBuff.append(" ldas.landSurplus, ");
		sqlBuff.append(" ldas.landCurrent, ");
		sqlBuff.append(" ldas.houseTotal, ");
		sqlBuff.append(" ldas.houseSurplus, ");
		sqlBuff.append(" ldas.houseCurrent, ");
		sqlBuff.append(" ldas.mountainTotal, ");
		sqlBuff.append(" ldas.mountainSurplus, ");
		sqlBuff.append(" ldas.mountainCurrent, ");
		sqlBuff.append(" ldas.userId, ");
		sqlBuff.append(" ldas.userName, ");
		sqlBuff.append(" ldas.portrait, ");
		sqlBuff.append(" ldas.createTime, ");
		sqlBuff.append(" IFNULL(xt.st1, 0) AS sta1, ");
		sqlBuff.append(" IFNULL(xt.st2, 0) AS sta2, ");
		sqlBuff.append(" IFNULL(xt.st3, 0) AS sta3, ");
		sqlBuff.append(" FORMAT(IFNULL(xt.st1 / ldas.landCurrent * 100, 0), 2) AS f, ");
		sqlBuff.append(" IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt), 0) AS thisd, ");
		sqlBuff.append(" IF(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt) > ldas.cycle, 1, 0) AS ifd, ");
		sqlBuff.append(" ldas.landType ");
		sqlBuff.append(" FROM ");
		sqlBuff.append(" (SELECT lda.*,xs.nt FROM ");
		sqlBuff.append(" (SELECT * FROM ");
		sqlBuff.append(" (SELECT * FROM xm_landproj WHERE 1=1");
		if(thisLandType != null && !"".equals(thisLandType)){
			sqlBuff.append(" and landType like '%"+thisLandType+"%'");
		}
		if(thisLandName != null && !"".equals(thisLandName)){
			sqlBuff.append(" and name like '%"+thisLandName+"%'");
		}
		sqlBuff.append(" ) AS xl ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT unit,people,peopleId,type,xmNumber FROM xm_duty ");
		
		if (thisPeopleId == null || "".equals(thisPeopleId)) {
			sqlBuff.append(" GROUP BY xmNumber ");
		}
		
		sqlBuff.append(" ) AS xd ");
		sqlBuff.append(" ON xl.landNumber=xd.xmNumber) AS lda ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT MAX(creatTime) AS nt, xmNumber FROM xm_schedule WHERE type='01' GROUP BY xmNumber ) xs ");
		sqlBuff.append(" ON lda.landNumber=xs.xmNumber) AS ldas ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT SUM(CASE WHEN taskType = '01' THEN task END) AS st1, ");
		sqlBuff.append(" SUM(CASE WHEN taskType = '02' THEN task END) AS st2, ");
		sqlBuff.append(" SUM(CASE WHEN taskType = '03' THEN task END) AS st3, ");
		sqlBuff.append(" landNumber AS tn ");
		sqlBuff.append(" FROM xm_task ");
		sqlBuff.append(" WHERE taskYear = '"+thisYear+"' ");
		sqlBuff.append(" GROUP BY landNumber ) xt ");
		sqlBuff.append(" ON ldas.landNumber=xt.tn ");
		sqlBuff.append(" WHERE 1=1 ");
		
		if (thisPeopleId != null && !"".equals(thisPeopleId)) {
			sqlBuff.append(" AND ldas.peopleId='"+thisPeopleId+"' ");
		}
		

		System.out.println("征地信息列表=" + sqlBuff.toString());

		List list = xmLandprojDao.query(sqlBuff.toString());

		for (int j = 0; j < list.size(); j++) {
			Object[] oc = (Object[]) list.get(j);

			Integer id = (Integer) oc[0];
			String landNumber = (String) oc[1];
			String name = (String) oc[2];
			String laYear = (String) oc[3];
			String cycle = (String) oc[4];
			String approval = (String) oc[5];
			String largeInvest = (String) oc[6];
			String owner = (String) oc[7];
			String address = (String) oc[8];
			String coordinate = (String) oc[9];
			String coordinatePly = (String) oc[10];

			String landTotal = (String) oc[11];
			String landSurplus = (String) oc[12];
			String landCurrent = (String) oc[13];

			String houseTotal = (String) oc[14];
			String houseSurplus = (String) oc[15];
			String houseCurrent = (String) oc[16];

			String mountainTotal = (String) oc[17];
			String mountainSurplus = (String) oc[18];
			String mountainCurrent = (String) oc[19];

			String userId = (String) oc[20];
			String userName = (String) oc[21];
			String portrait = (String) oc[22];
			Timestamp createTime = (Timestamp) oc[23];

			String landXmTask = (Double) oc[24] + "";
			String houseXmTask = (Double) oc[25] + "";
			String mountainXmTask = (Double) oc[26] + "";
			String cRate = (String) oc[27];

			String nowCycle = (BigInteger) oc[28] + "";// 当前时间-最新工作进度（时间）
			String isOverdue = (BigInteger) oc[29] + "";// 是否超期：0不超期，1超期

			String landType = (String) oc[30];
			ViewXmLandproj viewXmLandproj = new ViewXmLandproj();

			viewXmLandproj.setId(id);
			viewXmLandproj.setLandNumber(landNumber);
			viewXmLandproj.setName(name);
			viewXmLandproj.setLaYear(laYear);
			viewXmLandproj.setCycle(cycle);
			viewXmLandproj.setApproval(approval);
			viewXmLandproj.setLandTotal(landTotal);
			viewXmLandproj.setLandSurplus(landSurplus);
			viewXmLandproj.setLandCurrent(landCurrent);
			viewXmLandproj.setHouseTotal(houseTotal);
			viewXmLandproj.setHouseSurplus(houseSurplus);
			viewXmLandproj.setHouseCurrent(houseCurrent);
			viewXmLandproj.setMountainTotal(mountainTotal);
			viewXmLandproj.setMountainSurplus(mountainSurplus);
			viewXmLandproj.setMountainCurrent(mountainCurrent);
			viewXmLandproj.setLargeInvest(largeInvest);
			viewXmLandproj.setOwner(owner);
			viewXmLandproj.setAddress(address);
			viewXmLandproj.setCoordinate(coordinate);
			viewXmLandproj.setCoordinatePly(coordinatePly);
			viewXmLandproj.setUserId(userId);
			viewXmLandproj.setUserName(userName);
			viewXmLandproj.setPortrait(portrait);
			viewXmLandproj.setCreateTime(createTime);
			viewXmLandproj.setLandXmTask(landXmTask);
			viewXmLandproj.setHouseXmTask(houseXmTask);
			viewXmLandproj.setMountainXmTask(mountainXmTask);
			viewXmLandproj.setLandCRate(cRate);
			viewXmLandproj.setNowCycle(nowCycle);
			viewXmLandproj.setIsOverdue(isOverdue);
			viewXmLandproj.setLandType(landType);

			rsView.add(viewXmLandproj);
		}
		return rsView;
	}

	/**
	 * 征地信息列表
	 * 
	 * @return
	 */
	@Override
	public Result<ViewXmLandproj> resultXmLandproj(String thisYear, String thisPeopleId, String thisLandType, int page, int size) {

		Result<ViewXmLandproj> rsView = new Result<ViewXmLandproj>();

		try {
			int start=(page-1)*size;
			int limit =size;
			StringBuffer sqlBuff = new StringBuffer();

			sqlBuff.append(" SELECT ldas.id, ");
			sqlBuff.append(" ldas.landNumber, ");
			sqlBuff.append(" ldas.name, ");
			sqlBuff.append(" ldas.laYear, ");
			sqlBuff.append(" ldas.cycle, ");
			sqlBuff.append(" ldas.approval, ");
			sqlBuff.append(" ldas.largeInvest, ");
			sqlBuff.append(" ldas.owner, ");
			sqlBuff.append(" ldas.address, ");
			sqlBuff.append(" ldas.coordinate, ");
			sqlBuff.append(" ldas.coordinatePly, ");
			sqlBuff.append(" ldas.landTotal, ");
			sqlBuff.append(" ldas.landSurplus, ");
			sqlBuff.append(" ldas.landCurrent, ");
			sqlBuff.append(" ldas.houseTotal, ");
			sqlBuff.append(" ldas.houseSurplus, ");
			sqlBuff.append(" ldas.houseCurrent, ");
			sqlBuff.append(" ldas.mountainTotal, ");
			sqlBuff.append(" ldas.mountainSurplus, ");
			sqlBuff.append(" ldas.mountainCurrent, ");
			sqlBuff.append(" ldas.userId, ");
			sqlBuff.append(" ldas.userName, ");
			sqlBuff.append(" ldas.portrait, ");
			sqlBuff.append(" ldas.createTime, ");
			sqlBuff.append(" IFNULL(xt.st1, 0) AS sta1, ");
			sqlBuff.append(" IFNULL(xt.st2, 0) AS sta2, ");
			sqlBuff.append(" IFNULL(xt.st3, 0) AS sta3, ");
			sqlBuff.append(" FORMAT(IFNULL(xt.st1 / ldas.landCurrent * 100, 0), 2) AS f, ");
			sqlBuff.append(" IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt), 0) AS thisd, ");
			sqlBuff.append(" IF(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt) > ldas.cycle, 1, 0) AS ifd, ");
			sqlBuff.append(" ldas.landType ");
			sqlBuff.append(" FROM ");
			sqlBuff.append(" (SELECT lda.*,xs.nt FROM ");
			sqlBuff.append(" (SELECT * FROM ");
			sqlBuff.append(" (SELECT * FROM xm_landproj WHERE 1=1");
			if(thisLandType != null && !"".equals(thisLandType)){
				sqlBuff.append(" and landType like '%"+thisLandType+"%'");
			}
			sqlBuff.append(" ) AS xl ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT unit,people,peopleId,type,xmNumber FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS xd ");
			sqlBuff.append(" ON xl.landNumber=xd.xmNumber) AS lda ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT MAX(creatTime) AS nt, xmNumber FROM xm_schedule WHERE type='01' GROUP BY xmNumber ) xs ");
			sqlBuff.append(" ON lda.landNumber=xs.xmNumber) AS ldas ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT SUM(CASE WHEN taskType = '01' THEN task END) AS st1, ");
			sqlBuff.append(" SUM(CASE WHEN taskType = '02' THEN task END) AS st2, ");
			sqlBuff.append(" SUM(CASE WHEN taskType = '03' THEN task END) AS st3, ");
			sqlBuff.append(" landNumber AS tn ");
			sqlBuff.append(" FROM xm_task ");
			sqlBuff.append(" WHERE taskYear = '"+thisYear+"' ");
			sqlBuff.append(" GROUP BY landNumber ) xt ");
			sqlBuff.append(" ON ldas.landNumber=xt.tn ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND ldas.peopleId='"+thisPeopleId+"' ");
			}

			System.out.println("征地信息列表=" + sqlBuff.toString());

			Result rs=xmLandprojDao.findBySQL(sqlBuff.toString(), null, start, limit);
			List list=rs.getData();

			for (int j = 0; j < list.size(); j++) {
				Object[] oc = (Object[]) list.get(j);

				Integer id = (Integer) oc[0];
				String landNumber = (String) oc[1];
				String name = (String) oc[2];
				String laYear = (String) oc[3];
				String cycle = (String) oc[4];
				String approval = (String) oc[5];
				String largeInvest = (String) oc[6];
				String owner = (String) oc[7];
				String address = (String) oc[8];
				String coordinate = (String) oc[9];
				String coordinatePly = (String) oc[10];

				String landTotal = (String) oc[11];
				String landSurplus = (String) oc[12];
				String landCurrent = (String) oc[13];

				String houseTotal = (String) oc[14];
				String houseSurplus = (String) oc[15];
				String houseCurrent = (String) oc[16];

				String mountainTotal = (String) oc[17];
				String mountainSurplus = (String) oc[18];
				String mountainCurrent = (String) oc[19];

				String userId = (String) oc[20];
				String userName = (String) oc[21];
				String portrait = (String) oc[22];
				Timestamp createTime = (Timestamp) oc[23];

				String landXmTask = (Double) oc[24] + "";
				String houseXmTask = (Double) oc[25] + "";
				String mountainXmTask = (Double) oc[26] + "";
				String cRate = (String) oc[27];

				String nowCycle = (BigInteger) oc[28] + "";// 当前时间-最新工作进度（时间）
				String isOverdue = (BigInteger) oc[29] + "";// 是否超期：0不超期，1超期

				String landType = (String) oc[30];
				ViewXmLandproj viewXmLandproj = new ViewXmLandproj();

				viewXmLandproj.setId(id);
				viewXmLandproj.setLandNumber(landNumber);
				viewXmLandproj.setName(name);
				viewXmLandproj.setLaYear(laYear);
				viewXmLandproj.setCycle(cycle);
				viewXmLandproj.setApproval(approval);
				viewXmLandproj.setLandTotal(landTotal);
				viewXmLandproj.setLandSurplus(landSurplus);
				viewXmLandproj.setLandCurrent(landCurrent);
				viewXmLandproj.setHouseTotal(houseTotal);
				viewXmLandproj.setHouseSurplus(houseSurplus);
				viewXmLandproj.setHouseCurrent(houseCurrent);
				viewXmLandproj.setMountainTotal(mountainTotal);
				viewXmLandproj.setMountainSurplus(mountainSurplus);
				viewXmLandproj.setMountainCurrent(mountainCurrent);
				viewXmLandproj.setLargeInvest(largeInvest);
				viewXmLandproj.setOwner(owner);
				viewXmLandproj.setAddress(address);
				viewXmLandproj.setCoordinate(coordinate);
				viewXmLandproj.setCoordinatePly(coordinatePly);
				viewXmLandproj.setUserId(userId);
				viewXmLandproj.setUserName(userName);
				viewXmLandproj.setPortrait(portrait);
				viewXmLandproj.setCreateTime(createTime);
				viewXmLandproj.setLandXmTask(landXmTask);
				viewXmLandproj.setHouseXmTask(houseXmTask);
				viewXmLandproj.setMountainXmTask(mountainXmTask);
				viewXmLandproj.setLandCRate(cRate);
				viewXmLandproj.setNowCycle(nowCycle);
				viewXmLandproj.setIsOverdue(isOverdue);
				viewXmLandproj.setLandType(landType);

				rsView.getData().add(viewXmLandproj);
			}
			rsView.setOffset(rs.getOffset());
			rsView.setPage(rs.getPage());
			rsView.setSize(rs.getSize());
			rsView.setTotal(rs.getTotal());
			rsView.setTotalPage(rs.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rsView;
	}
	
	/**
	 * 土地信息列表导出
	 */
	@Override
	public downViewXmLandproj xmLandprojDownload(String thisYear, String thisPeopleId, String thisLandType) {
		downViewXmLandproj rsView = new downViewXmLandproj();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT ldas.id, ");
			sqlBuff.append(" ldas.landNumber, ");
			sqlBuff.append(" ldas.name, ");
			sqlBuff.append(" ldas.laYear, ");
			sqlBuff.append(" ldas.cycle, ");
			sqlBuff.append(" ldas.approval, ");
			sqlBuff.append(" ldas.largeInvest, ");
			sqlBuff.append(" ldas.owner, ");
			sqlBuff.append(" ldas.address, ");
			sqlBuff.append(" ldas.coordinate, ");
			sqlBuff.append(" ldas.coordinatePly, ");
			sqlBuff.append(" ldas.landTotal, ");
			sqlBuff.append(" ldas.landSurplus, ");
			sqlBuff.append(" ldas.landCurrent, ");
			sqlBuff.append(" ldas.houseTotal, ");
			sqlBuff.append(" ldas.houseSurplus, ");
			sqlBuff.append(" ldas.houseCurrent, ");
			sqlBuff.append(" ldas.mountainTotal, ");
			sqlBuff.append(" ldas.mountainSurplus, ");
			sqlBuff.append(" ldas.mountainCurrent, ");
			sqlBuff.append(" ldas.userId, ");
			sqlBuff.append(" ldas.userName, ");
			sqlBuff.append(" ldas.portrait, ");
			sqlBuff.append(" ldas.createTime, ");
			sqlBuff.append(" IFNULL(xt.st1, 0) AS sta1, ");
			sqlBuff.append(" IFNULL(xt.st2, 0) AS sta2, ");
			sqlBuff.append(" IFNULL(xt.st3, 0) AS sta3, ");
			sqlBuff.append(" FORMAT(IFNULL(xt.st1 / ldas.landCurrent * 100, 0), 2) AS f, ");
			sqlBuff.append(" IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt), 0) AS thisd, ");
			sqlBuff.append(" IF(TO_DAYS(sysdate()) - TO_DAYS(ldas.nt) > ldas.cycle, 1, 0) AS ifd, ");
			sqlBuff.append(" ldas.landType ");
			sqlBuff.append(" FROM ");
			sqlBuff.append(" (SELECT lda.*,xs.nt FROM ");
			sqlBuff.append(" (SELECT * FROM ");
			sqlBuff.append(" (SELECT * FROM xm_landproj WHERE 1=1");
			if(thisLandType != null && !"".equals(thisLandType)){
				sqlBuff.append(" and landType like '%"+thisLandType+"%'");
			}
			sqlBuff.append(" ) AS xl ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT unit,people,peopleId,type,xmNumber FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS xd ");
			sqlBuff.append(" ON xl.landNumber=xd.xmNumber) AS lda ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT MAX(creatTime) AS nt, xmNumber FROM xm_schedule WHERE type='01' GROUP BY xmNumber ) xs ");
			sqlBuff.append(" ON lda.landNumber=xs.xmNumber) AS ldas ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT SUM(CASE WHEN taskType = '01' THEN task END) AS st1, ");
			sqlBuff.append(" SUM(CASE WHEN taskType = '02' THEN task END) AS st2, ");
			sqlBuff.append(" SUM(CASE WHEN taskType = '03' THEN task END) AS st3, ");
			sqlBuff.append(" landNumber AS tn ");
			sqlBuff.append(" FROM xm_task ");
			sqlBuff.append(" WHERE taskYear = '"+thisYear+"' ");
			sqlBuff.append(" GROUP BY landNumber ) xt ");
			sqlBuff.append(" ON ldas.landNumber=xt.tn ");
			sqlBuff.append(" WHERE 1=1 ");

			System.out.println("项目信息列表导出:"+sqlBuff.toString());
			
			List list=xmLandprojDao.query(sqlBuff.toString());
			
			rsView.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rsView;
	}
	
	@Override
	public List<AutoCompleteHttpModel> likeAll(String keyword) {
		List<AutoCompleteHttpModel> results = new ArrayList<AutoCompleteHttpModel>();
		try {

			String hql = "select distinct id,landNumber,name "
					+ "from xm_landproj where name like'%" + keyword + "%'";
			System.out.println(hql);
			List<Object[]> list = xmLandprojDao.query(hql);
			for (int i = 0; i < list.size(); i++) {
				AutoCompleteHttpModel model = new AutoCompleteHttpModel();

				Object[] obj = list.get(i);

				model.setItemId(((Integer) obj[0]).toString());
				model.setItemName((String) obj[2]);
				model.setItemValue(((String) obj[1]).toString());
				results.add(model);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * 项目所关联的征地信息
	 * 
	 * @return
	 */
	@Override
	public List<ViewToLandproj> toLandproj(String thisXmNumber) {

		List<ViewToLandproj> rsView = new ArrayList<ViewToLandproj>();

		StringBuffer sqlBuff = new StringBuffer();

		sqlBuff.append(" SELECT xt.tid,xt.xmNumber,xl.* FROM ");
		sqlBuff.append(" (SELECT id AS tid,xmNumber,landNumber FROM xmtoland) AS xt ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT id AS lid, ");
		sqlBuff.append(" landNumber, ");
		sqlBuff.append(" name, ");
		sqlBuff.append(" laYear, ");
		sqlBuff.append(" cycle, ");
		sqlBuff.append(" approval, ");
		sqlBuff.append(" largeInvest, ");
		sqlBuff.append(" owner, ");
		sqlBuff.append(" address, ");
		sqlBuff.append(" coordinate, ");
		sqlBuff.append(" coordinatePly, ");
		sqlBuff.append(" landTotal, ");
		sqlBuff.append(" landSurplus, ");
		sqlBuff.append(" landCurrent, ");
		sqlBuff.append(" houseTotal, ");
		sqlBuff.append(" houseSurplus, ");
		sqlBuff.append(" houseCurrent, ");
		sqlBuff.append(" mountainTotal, ");
		sqlBuff.append(" mountainSurplus, ");
		sqlBuff.append(" mountainCurrent, ");
		sqlBuff.append(" userId, ");
		sqlBuff.append(" userName, ");
		sqlBuff.append(" portrait, ");
		sqlBuff.append(" createTime FROM xm_landproj) AS xl ");
		sqlBuff.append(" ON xl.landNumber=xt.landNumber ");
		sqlBuff.append(" WHERE 1=1 ");
		if (thisXmNumber != null && !"".equals(thisXmNumber)) {
			sqlBuff.append(" and xt.xmNumber='" + thisXmNumber + "' ");
		}
		sqlBuff.append(" AND xl.landNumber IS NOT NULL ");
		sqlBuff.append(" GROUP BY xt.xmNumber,xt.landNumber ");
		
		System.out.println("项目所关联的征地信息=" + sqlBuff.toString());

		List list = xmLandprojDao.query(sqlBuff.toString());

		for (int j = 0; j < list.size(); j++) {
			Object[] oc = (Object[]) list.get(j);

			Integer toLandprojid = (Integer) oc[0];
			Integer landid = (Integer) oc[2];
			String landNumber = (String) oc[3];
			String xmNumber = (String) oc[1];
			String name = (String) oc[4];
			String laYear = (String) oc[5];
			String cycle = (String) oc[6];
			String approval = (String) oc[7];
			String largeInvest = (String) oc[8];
			String owner = (String) oc[9];
			String address = (String) oc[10];
			String coordinate = (String) oc[11];
			String coordinatePly = (String) oc[12];

			String landTotal = (String) oc[13];
			String landSurplus = (String) oc[14];
			String landCurrent = (String) oc[15];

			String houseTotal = (String) oc[16];
			String houseSurplus = (String) oc[17];
			String houseCurrent = (String) oc[18];

			String mountainTotal = (String) oc[19];
			String mountainSurplus = (String) oc[20];
			String mountainCurrent = (String) oc[21];

			String userId = (String) oc[22];
			String userName = (String) oc[23];
			String portrait = (String) oc[24];
			Timestamp createTime = (Timestamp) oc[25];

			ViewToLandproj viewToLandproj = new ViewToLandproj();

			viewToLandproj.setToLandprojid(toLandprojid);
			viewToLandproj.setLandid(landid);
			viewToLandproj.setXmNumber(xmNumber);
			viewToLandproj.setLandNumber(landNumber);
			viewToLandproj.setName(name);
			viewToLandproj.setLaYear(laYear);
			viewToLandproj.setCycle(cycle);
			viewToLandproj.setApproval(approval);
			viewToLandproj.setLandTotal(landTotal);
			viewToLandproj.setLandSurplus(landSurplus);
			viewToLandproj.setLandCurrent(landCurrent);
			viewToLandproj.setHouseTotal(houseTotal);
			viewToLandproj.setHouseSurplus(houseSurplus);
			viewToLandproj.setHouseCurrent(houseCurrent);
			viewToLandproj.setMountainTotal(mountainTotal);
			viewToLandproj.setMountainSurplus(mountainSurplus);
			viewToLandproj.setMountainCurrent(mountainCurrent);
			viewToLandproj.setLargeInvest(largeInvest);
			viewToLandproj.setOwner(owner);
			viewToLandproj.setAddress(address);
			viewToLandproj.setCoordinate(coordinate);
			viewToLandproj.setCoordinatePly(coordinatePly);
			viewToLandproj.setUserId(userId);
			viewToLandproj.setUserName(userName);
			viewToLandproj.setPortrait(portrait);
			viewToLandproj.setCreateTime(createTime);

			rsView.add(viewToLandproj);
		}
		return rsView;
	}

	/********************* 创建编号方法（内部调用） *********************/
	/**
	 * 创建新的编号
	 * 
	 * @param thisNO
	 * @return
	 */
	public String createNO(String thisNO) {

		String maxNO = maxNO(thisNO); // 查找最大号
		String autoNum = maxNO.substring(maxNO.length() - 2, maxNO.length()); // 截取查找结果的后2位
		int num = Integer.parseInt(autoNum); // 对截取结果+1
		num += 1;
		String NewNO = "";
		if (num < 99) {
			NewNO = thisNO + String.format("%1$,02d", num); // 组装成新的号
		} else {
			NewNO = thisNO + num; // 组装成新的号
		}
		return NewNO;
	}

	/**
	 * 查找最大编号
	 * 
	 * @param thisNO
	 * @return
	 */
	public String maxNO(String thisNO) {
		String queryString = "select max(landNumber) from XmLandproj where landNumber like'"
				+ thisNO + "__'";
		List list = xmLandprojDao.findByHql(queryString, null);
		String maxNO = thisNO + "00";
		if (list.size() != 0) {
			String oc = (String) list.get(0);
			if (oc != null) {
				maxNO = oc;
			}
		}
		return maxNO;
	}

}
