package com.gxwzu.business.service.project.impl;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.AttachmentDao;
import com.gxwzu.business.dao.project.MaterialsDao;
import com.gxwzu.business.dao.project.XmChildDao;
import com.gxwzu.business.dao.project.XmDao;
import com.gxwzu.business.model.Attachment;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.service.project.MaterialsService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;

@Service("materialsService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class MaterialsServiceImpl extends BaseServiceImpl<Materials> implements MaterialsService {
    @Autowired
	private MaterialsDao materialsDao;
    @Autowired
    private AttachmentDao attachmentDao;
	@Autowired
	private XmChildDao xmChildDao;
	@Autowired
	private XmDao xmDao;
	@Autowired
	private UserDao userDao;

	private Materials materials;
	private Attachment attachment;
    
	@Autowired
	public void setMaterialsDao(MaterialsDao materialsDao) {
		this.materialsDao = materialsDao;
	}

	@Override
	public BaseDao<Materials> getDao() {
		return this.materialsDao;
	
	}
	
	/**
	 * 保存用户上传的文件
	 * @author SunYi
	 * @Date 2019年3月14日下午10:25:34
	 */
	@Override
	public boolean saveFileList(File[] myfile,String[] myfileFileName, String userName,String xcId,String xmId, String helpUserName) {
		String fileName =null;
		 List<XmChild> xmChildList = null;
		 List<XmChild> xmChildFileList = null;
		 boolean flag=false;
		 
		 if(myfile != null && myfileFileName != null) {
			 
		 List<User> find = userDao.find("from User where helpUserName ='"+helpUserName+"'");
		 User user = find.get(0);
		//获取记录数据所需详细信息
        //调用xmChild层获取本次存储的文件名list和materialsId
		if(xmId !=null && xmId !="") {
			xmChildList = xmChildDao.find("from XmChild where xmNumber='"+xmId+"' and userName ='"+helpUserName+"'");
		}else if(xcId != null && xcId !=""){
			// 切割子项目名称得到父项目名称
			String xmNumber = xcId.substring(0, xcId.length() - userName.length());
			// 获取子项目的上级教研室
			xmChildList = xmChildDao.find("from XmChild where xmChildNumber='"+xcId+"'");
			String unitId = xmChildList.get(0).getUnitParent();
			// 根据子项目的上级教研室去查询父项目的文件信息
			xmChildFileList = xmChildDao.find("from XmChild where xmNumber = '"+xmNumber+"' and unitId = '"+unitId+"'");
			// 把查询出来的list再set进同一个list里
			for (XmChild xmChild : xmChildFileList) {
				xmChildList.add(xmChild);
			}
		}
		//取出实体
		XmChild xmChild = xmChildList.get(0);
		//因数据没取完整，二次取数据
		xmChild = xmChildDao.get(XmChild.class, xmChild.getId());
		if(xmChild.getAcceptstatus()==1){
			List<Xm> xmList = xmDao.find("from Xm where xmNumber ='"+xmChild.getXmNumber()+"'");
			Xm xm = xmList.get(0);
			fileName=xm.getFileName();
		}else{
			//教研室下发的任务
			//List<XmChild> parentXmChild = xmChildDao.find("from xmChild where unitParent='"+xmChild.getUnitParent()+"'");
			//fileName=parentXmChild.get(0).getFileName();
			fileName = xmChildFileList.get(0).getFileName();
		}
		//取出文件名列表
		 xmChild.getFileName();
		//拆分文件名列表
		String[] fileNameList = fileName.split(",");
		//取出材料编号
		String materialsId = xmChild.getMaterialsId();
		// 1.拿到ServletContext
	    ServletContext servletContext = ServletActionContext.getServletContext();
	    // 2.调用realPath方法，获取根据一个虚拟目录得到的真实目录
		String realPath = servletContext.getRealPath("/WEB-INF/materials");
		// 3.如果这个真实的目录不存在，需要创建
		File file = new File(realPath);
		    if (!file.exists()) {
				file.mkdirs();
			}
		 //遍历存储文件和记录数据
		 for (int i = 0; i < myfile.length; i++) {
			 //重置指标
			 flag=false;
			 //取出各文件后缀
			 String FileNameSplit = myfileFileName[i].substring(myfileFileName[i].lastIndexOf(".") + 1);
			 //保存文件并重命名
			myfile[i].renameTo(new File(file, fileNameList[i]+"-" + user.getHelpName()+"."+FileNameSplit));
			
			List<Materials> findMaterialsList = materialsDao.find("from Materials where materialsId='"+materialsId+"' and filename='"+fileNameList[i]+"'");
			Materials materials = findMaterialsList.get(0);
			
			materials.setPath(realPath+"/"+fileNameList[i]+ "-" + user.getHelpName()+"."+FileNameSplit);
			try {
				//持久化数据库materials表数据
				materialsDao.update(materials);
				flag=true;
			} catch (Exception e) {
				e.printStackTrace();
				flag=false;
			}
		}
		 //当所有文件保存完毕时
			if(flag) {
				//更改当前子项目数据完成状态
				  //重置标志
				  flag=false;
				  try {
					  Timestamp ts = new Timestamp(new Date().getTime());
					  // 重置任务完成状态
					  xmChild.setStatus(1);
					  // 设置子任务表任务完成时间
					  xmChild.setFinishTime(ts);
					  // 查询得到主任务
					  List<Xm> xmList = xmDao.find("from Xm where xmNumber ='"+xmChild.getXmNumber()+"'");
					  if(xmList != null && !xmList.equals("")) {
						  Xm xm = xmList.get(0);
						  // 设置主任务表任务完成时间
						  xm.setFinishTime(ts);
						  // 持久化到数据库
						  xmDao.update(xm);
					  }
					  // 持久化到数据库
					  xmChildDao.update(xmChild);
					  flag=true;
				} catch (Exception e) {
					e.printStackTrace();
					flag=false;
				}
			}
		 //当变更子任务完成状态后
		 if(flag) {
			 try {
				//重新评判该项目完成进度
				 flag=reJudgeStatus(xmId,xmChild.getAcceptstatus());
			} catch (Exception e) {
				 e.printStackTrace();
                 flag=false;
			}
		 }
		 
		 }
		return flag;
	}
	
	/**
	 * 保存用户上传的附件
	 * @author SunYi
	 * @Date 2019年3月14日下午10:25:34
	 */
	@Override
	public boolean saveAttachment(File[] myfile, String[] myfileFileName, String xmNumber) {
		
		String fileName =null;
		boolean flag=false;
		if(myfile != null && myfileFileName != null) {
		// 1.拿到ServletContext
	    ServletContext servletContext = ServletActionContext.getServletContext();
	    // 2.调用realPath方法，获取根据一个虚拟目录得到的真实目录
		String realPath = servletContext.getRealPath("/WEB-INF/Attachment/" + xmNumber);
		// 3.如果这个真实的目录不存在，需要创建
		File file = new File(realPath);
		    if (!file.exists()) {
				file.mkdirs();
			}
		 //遍历存储文件和记录数据
		 for (int i = 0; i < myfileFileName.length; i++) {
			 //重置指标
			 flag=false;
			 //取出各文件名
			 fileName = myfileFileName[i];
			 //保存文件并重命名
			System.out.println("文件名称：" + fileName);
			myfile[i].renameTo(new File(file, fileName));
			
			System.out.println("上传的文件个数" + myfile[i]);
			System.out.println("路径" + realPath);
			attachment = new Attachment();
			attachment.setPath(realPath+"/"+fileName);
			attachment.setFileName(fileName);
			attachment.setXmNumber(xmNumber);
			try {
				//持久化数据库attachment表数据
				attachmentDao.save(attachment);
				flag=true;
			} catch (Exception e) {
				e.printStackTrace();
				flag=false;
			}
		}
		 //当所有文件保存完毕时
			if(flag) {
				//更改当前子项目数据完成状态
				  //重置标志
				  flag=false;
				  try {
					  flag=true;
				} catch (Exception e) {
					e.printStackTrace();
					flag=false;
				}
			}
		}
		return flag;
	}

	
	

	/**
	 * 重新评判该项目完成进度
	 * @author 叶城廷
	 * @version 2019.03.05
	 * @param xmId 
	 * @param integer 
	 * @return
	 */
	private boolean reJudgeStatus(String xmId, Integer acceptStatus) {
		//定义指标
		boolean flag=true;
		//根据下发级别状态确认需要更改的数据
		 //上级对应院领导时
		if(acceptStatus==1) {
			//遍历该项目所有由院领导指派的子任务是否全部完成
			List<XmChild> XmChildList = xmChildDao.find("from XmChild where xmNumber='"+xmId+"' and acceptStatus=1");
			//开始遍历
			for(int i=0;i<XmChildList.size();i++) {
				//当有未完成的子项目任务时变更指标
				if(XmChildList.get(i).getStatus()==0) {
					flag=false;
				}
			}
			//判断指标.当指标完成时变更院级任务状态
			if(flag) {
				List<Xm> XmList = xmDao.find("from Xm where xmNumber='"+xmId+"'");
				//取出数据
				Xm xm = XmList.get(0);
				//重置完成进度标志
				xm.setStatus(1);
				Timestamp ts = new Timestamp(new Date().getTime());
				// 设置任务完成时间
				xm.setFinishTime(ts);
				//更新
				xmDao.update(xm);
			}
			
		  //上级对应教研室时
		}else if(acceptStatus==0) {
		  //无需任何处理
		}
		return flag;
	}

	/**
	 * @Description 根据教研室和任务编号获取所有材料信息
	 * @author 莫东林
	 * @date 2019年3月7日下午3:42:48
	 * List<Materials>
	 */
	public List<Materials> getByUnit(String unitId, String xmNumber){
		return materialsDao.find("from Materials where unitId = '"+unitId+"' and xmId = '"+xmNumber+"'");
	}
	
	/**
	 * 
	 * @Description 根据工号和任务编号获取所有材料信息
	 * @author 莫东林
	 * @date 2019年3月7日下午3:42:48
	 * List<Materials>
	 */
	public List<Materials> getByUser(String userId, String xmNumber){
		return materialsDao.find("from Materials where userId = '"+userId+"' and xmId = '"+xmNumber+"'");
	}

	public Materials getMaterials() {
		return materials;
	}

	public void setMaterials(Materials materials) {
		this.materials = materials;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}
	
	
}
