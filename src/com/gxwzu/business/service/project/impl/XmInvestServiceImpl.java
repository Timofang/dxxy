package com.gxwzu.business.service.project.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmInvestDao;
import com.gxwzu.business.model.XmInvest;
import com.gxwzu.business.service.project.XmInvestService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 资金投资进度ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInvestServiceImpl
 * <br>Date: 2017-8-17下午06:32:15
 * <br>log:
 */
@Service("xmInvestService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmInvestServiceImpl extends BaseServiceImpl<XmInvest> implements XmInvestService  {
	
	private XmInvestDao xmInvestDao;

	@Autowired
	public void setXmInvestDao(XmInvestDao xmInvestDao) {
		this.xmInvestDao = xmInvestDao;
	}

	@Override
	public BaseDao<XmInvest> getDao() {
		return this.xmInvestDao;
	}
	
	@Override
	public List<XmInvest> findAll() {
		return xmInvestDao.getAll(XmInvest.class);
	}

	@Override
	public Result<XmInvest> find(XmInvest model, int page, int size) {
		return xmInvestDao.find(model, page, size);
	}

	@Override
	public List<XmInvest> findByExample(XmInvest model) {
		return xmInvestDao.findByExample(model);
	}
	
	@Override
	public XmInvest findById(Integer findId) {
		return xmInvestDao.findById(findId);
	}
	
	@Override
	public void add(XmInvest xmInvest) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			xmInvest.setCreateTime(Timestamp.valueOf(dateStr));
			
			xmInvestDao.save(xmInvest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void del(Integer id) {
		try {
			XmInvest delXmInvest = xmInvestDao.findById(id);
			if(delXmInvest != null){
				xmInvestDao.remove(delXmInvest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
