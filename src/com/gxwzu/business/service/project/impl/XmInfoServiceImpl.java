package com.gxwzu.business.service.project.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gxwzu.business.dao.project.*;
import com.gxwzu.business.model.*;
import com.gxwzu.business.service.project.InstructionsService;
import com.gxwzu.business.service.project.UnitService;
import com.gxwzu.business.service.project.XmChildService;
import com.gxwzu.sysVO.*;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 项目基本信息ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoServiceImpl
 * <br>Date: 2017-8-17下午06:28:32
 * <br>log:
 */
@Service("xmInfoService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmInfoServiceImpl extends BaseServiceImpl<XmInfo> implements XmInfoService  {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	private XmInfoDao xmInfoDao;
	private XmCapitalDao xmCapitalDao;
	private XmDutyDao xmDutyDao;
	private XmInvestDao xmInvestDao;
	private XmReplyDao xmReplyDao;
	private XmScheduleDao xmScheduleDao;
	private XmSchedulepicDao xmSchedulepicDao;
	private XmTaskDao xmTaskDao;
	private XmtolandDao xmtolandDao;

	private XmChildDao xmChildDao;

	private XmChildService xmChildService;
	@Autowired
	private UserService userService;
	@Autowired
	private InstructionsService instructionsService;
	@Autowired
	private UnitService unitService;

	@Autowired
	public void setXmChildService(XmChildService xmChildService) {
		this.xmChildService = xmChildService;
	}

	@Autowired
	public void setXmChildDao(XmChildDao xmChildDao) {
		this.xmChildDao = xmChildDao;
	}

	@Autowired
	public void setXmtolandDao(XmtolandDao xmtolandDao) {
		this.xmtolandDao = xmtolandDao;
	}

	@Autowired
	public void setXmTaskDao(XmTaskDao xmTaskDao) {
		this.xmTaskDao = xmTaskDao;
	}

	@Autowired
	public void setXmSchedulepicDao(XmSchedulepicDao xmSchedulepicDao) {
		this.xmSchedulepicDao = xmSchedulepicDao;
	}

	@Autowired
	public void setXmScheduleDao(XmScheduleDao xmScheduleDao) {
		this.xmScheduleDao = xmScheduleDao;
	}

	@Autowired
	public void setXmReplyDao(XmReplyDao xmReplyDao) {
		this.xmReplyDao = xmReplyDao;
	}

	@Autowired
	public void setXmInvestDao(XmInvestDao xmInvestDao) {
		this.xmInvestDao = xmInvestDao;
	}

	@Autowired
	public void setXmDutyDao(XmDutyDao xmDutyDao) {
		this.xmDutyDao = xmDutyDao;
	}

	@Autowired
	public void setXmInfoDao(XmInfoDao xmInfoDao) {
		this.xmInfoDao = xmInfoDao;
	}

	@Autowired
	public void setXmCapitalDao(XmCapitalDao xmCapitalDao) {
		this.xmCapitalDao = xmCapitalDao;
	}

	@Override
	public BaseDao<XmInfo> getDao() {
		return this.xmInfoDao;
	}
	
	@Override
	public List<XmInfo> findAll() {
		return xmInfoDao.getAll(XmInfo.class);
	}

	@Override
	public Result<XmInfo> find(XmInfo model, int page, int size) {
		return xmInfoDao.find(model, page, size);
	}
	
	@Override
	public Result<XmInfo> findPoor(XmInfo model, int page, int size) {
		return xmInfoDao.findPoor(model, page, size);
	}

	@Override
	public List<XmInfo> findByExample(XmInfo model) {
		return xmInfoDao.findByExample(model);
	}
	
	@Override
	public List<XmInfo> findCoordinate(XmInfo model) {
		return xmInfoDao.findCoordinate(model);
	}
	
	@Override
	public XmInfo findById(Integer findId) {
		return xmInfoDao.findById(findId);
	}

	@Override
	public void add(XmInfo xmInfo, XmCapital xmCapital) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateStr = sdf.format(new Date());
			String NewNO = createNO(dateStr);
			
			//1.保存项目基本情况
			xmInfo.setXmNumber(NewNO);
			xmInfoDao.save(xmInfo);
			
			//2.保存资金来源情况
			xmCapital.setXmNumber(NewNO);
			xmCapitalDao.save(xmCapital);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void addPoorXm(XmInfo xmInfo, XmCapital xmCapital) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateStr = sdf.format(new Date());
			String NewNO = createPoorXmNo(dateStr);
			
			//1.保存项目基本情况
			xmInfo.setXmNumber(NewNO);
			xmInfoDao.save(xmInfo);
			
			//2.保存资金来源情况
			xmCapital.setXmNumber(NewNO);
			xmCapitalDao.save(xmCapital);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void delete(Integer xmInfoId) {
		try {
			if(xmInfoId != null && !"".equals(xmInfoId)){
				XmInfo delXmInfo=xmInfoDao.get(XmInfo.class, xmInfoId);
				//1.删除资金信息xm_capital
				XmCapital delXmCapital = new XmCapital();
				delXmCapital.setXmNumber(delXmInfo.getXmNumber());
				List<XmCapital> listXmCapital = xmCapitalDao.findByExample(delXmCapital);
				for (int i = 0; i < listXmCapital.size(); i++) {
					delXmCapital = listXmCapital.get(i);
					xmCapitalDao.remove(delXmCapital);
				}
				//2.删除责任人xm_duty
				XmDuty delXmDuty = new XmDuty();
				delXmDuty.setXmNumber(delXmInfo.getXmNumber());
				List<XmDuty> listXmDuty = xmDutyDao.findByExample(delXmDuty);
				for (int i = 0; i < listXmDuty.size(); i++) {
					delXmDuty = listXmDuty.get(i);
					xmDutyDao.remove(delXmDuty);
				}
				//3.资金投资进度xm_invest
				XmInvest delXmInvest = new XmInvest();
				delXmInvest.setXmNumber(delXmInfo.getXmNumber());
				List<XmInvest> listXmInvest = xmInvestDao.findByExample(delXmInvest);
				for (int i = 0; i < listXmInvest.size(); i++) {
					delXmInvest = listXmInvest.get(i);
					xmInvestDao.remove(delXmInvest);
				}
				//5.工作进度xm_schedule
				XmSchedule delXmSchedule = new XmSchedule();
				delXmSchedule.setXmNumber(delXmInfo.getXmNumber());
				List<XmSchedule> listXmSchedule = xmScheduleDao.findByExample(delXmSchedule);
				for (int i = 0; i < listXmSchedule.size(); i++) {
					delXmSchedule = listXmSchedule.get(i);
					
					//6.工作进度图片xm_schedulepic
					XmSchedulepic delXmSchedulepic = new XmSchedulepic();
					delXmSchedulepic.setScheduleId(delXmSchedule.getId());
					List<XmSchedulepic> listXmSchedulepic = xmSchedulepicDao.findByExample(delXmSchedulepic);
					for (int j = 0; j < listXmSchedulepic.size(); j++) {
						delXmSchedulepic = listXmSchedulepic.get(j);
						xmSchedulepicDao.remove(delXmSchedulepic);
					}
					
					xmScheduleDao.remove(delXmSchedule);
				}
				
				//4.回复xm_reply
				XmReply delXmReply = new XmReply();
				delXmReply.setXmNumber(delXmInfo.getXmNumber());
				List<XmReply> listXmReply = xmReplyDao.findByExample(delXmReply);
				for (int i = 0; i < listXmReply.size(); i++) {
					delXmReply = listXmReply.get(i);
					xmReplyDao.remove(delXmReply);
				}
				//7.征地任务周报xm_task
				XmTask delXmTask = new XmTask();
				delXmTask.setLandNumber(delXmInfo.getXmNumber());
				List<XmTask> listXmTask = xmTaskDao.findByExample(delXmTask);
				for (int i = 0; i < listXmTask.size(); i++) {
					delXmTask = listXmTask.get(i);
					xmTaskDao.remove(delXmTask);
				}
				//8.征地-项目关联xmtoland
				Xmtoland delXmtoland = new Xmtoland();
				delXmtoland.setXmNumber(delXmInfo.getXmNumber());
				List<Xmtoland> listXmtoland = xmtolandDao.findByExample(delXmtoland);
				for (int i = 0; i < listXmtoland.size(); i++) {
					delXmtoland = listXmtoland.get(i);
					xmtolandDao.remove(delXmtoland);
				}
				//9.项目xm_info
				xmInfoDao.remove(delXmInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public ViewXmInfo viewInfo(String xmNumber) {
		
		ViewXmInfo Vmodel = new ViewXmInfo();
		if(xmNumber != null && !"".equals(xmNumber)){
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT ");
			sqlBuff.append(" i.id, i.xmNumber,i.name,i.scale,i.period,i.owner,i.county, ");
			sqlBuff.append(" i.startTime,i.completionTime,i.address,i.industry,i.isState, ");
			sqlBuff.append(" i.isCompleted,i.budgetType,i.projectType,i.userId,i.userName, ");
			sqlBuff.append(" i.createTime,i.coordinate,i.coordinatePly,i.portrait,i.xmYear, ");
			sqlBuff.append(" i.cycle,c.id,c.investTotal,c.investSource,c.investPlan,c.investComplete, ");
			sqlBuff.append(" c.xmNumber,i.allPay ");
			sqlBuff.append(" FROM ");
			sqlBuff.append(" (SELECT * FROM xm_info) AS i ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_capital) AS c ");
			sqlBuff.append(" ON c.xmNumber=i.xmNumber ");
			sqlBuff.append(" WHERE i.xmNumber='"+xmNumber+"' ");
			
			//System.out.println("findByView2="+sqlBuff.toString());
			List list=xmInfoDao.query(sqlBuff.toString());;
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				Integer id = (Integer)oc[0];
				String thisXmNumber = (String)oc[1];
				String name = (String)oc[2];
				String scale = (String)oc[3];
				String period = (String)oc[4];
				String owner = (String)oc[5];
				String county = (String)oc[6];
				String startTime = (Timestamp)oc[7]+"";
				String completionTime = (Timestamp)oc[8]+"";
				String address = (String)oc[9];
				String industry = (String)oc[10];
				String isState = (String)oc[11];
				String isCompleted = (String)oc[12];
				String budgetType = (String)oc[13];
				String projectType = (String)oc[14];
				String userId = (String)oc[15];
				String userName = (String)oc[16];
				String createTime = (Timestamp)oc[17]+"";
				String coordinate = (String)oc[18];
				String coordinatePly = (String)oc[19];
				String portrait = (String)oc[20];
				String xmYear = (String)oc[21];
				String cycle = (String)oc[22];
				
				String investTotal = (String)oc[24];
				String investSource = (String)oc[25];
				String investPlan = (String)oc[26];
				String investComplete = (String)oc[27];
				String allPay = (String)oc[29];
				
				Vmodel.setId(id);
				Vmodel.setXmNumber(thisXmNumber);
				Vmodel.setXmYear(xmYear);
				Vmodel.setName(name);
				Vmodel.setScale(scale);
				Vmodel.setPeriod(period);
				Vmodel.setCycle(cycle);
				Vmodel.setOwner(owner);
				Vmodel.setCounty(county);
				Vmodel.setStartTime(startTime);
				Vmodel.setCompletionTime(completionTime);
				Vmodel.setAddress(address);
				Vmodel.setIndustry(industry);
				Vmodel.setIsState(isState);
				Vmodel.setIsCompleted(isCompleted);
				Vmodel.setBudgetType(budgetType);
				Vmodel.setProjectType(projectType);
				Vmodel.setUserId(userId);
				Vmodel.setUserName(userName);
				Vmodel.setPortrait(portrait);
				Vmodel.setCreateTime(createTime);
				Vmodel.setCoordinate(coordinate);
				Vmodel.setCoordinatePly(coordinatePly);
				Vmodel.setInvestTotal(investTotal);
				Vmodel.setInvestSource(investSource);
				Vmodel.setInvestPlan(investPlan);
				Vmodel.setInvestComplete(investComplete);
				Vmodel.setAllPay(allPay);
			}
		}
		
		return Vmodel;
	}
	
	
	/**
	 * 项目信息列表
	 * @return
	 */
	@Override
	public List<ViewXmInfo> allViewXmInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, String thisInfoName, String thisProjectType) {
		
		List<ViewXmInfo> rsView = new ArrayList<ViewXmInfo>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT dd.name, dd.scale, dd.period, dd.owner, dd.county ");
			sqlBuff.append(" , dd.startTime, dd.completionTime, dd.address, dd.industry, dd.isState ");
			sqlBuff.append(" , dd.isCompleted, dd.budgetType, dd.projectType, dd.userId, dd.userName ");
			sqlBuff.append(" , dd.portrait, dd.createTime, dd.coordinate, dd.coordinatePly, dd.investTotal ");
			sqlBuff.append(" , dd.investSource, dd.investPlan, dd.investComplete, dd.sInvestment1, dd.aps ");
			sqlBuff.append(" , dd.ix, dd.xmYear, dd.cycle, ss.nt, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > dd.cycle, 1, 0) AS ifd ");
			sqlBuff.append(" FROM (SELECT ic.name, ic.scale, ic.period, ic.owner, ic.county ");
			sqlBuff.append(" , ic.startTime, ic.completionTime, ic.address, ic.industry, ic.isState ");
			sqlBuff.append(" , ic.isCompleted, ic.budgetType, ic.projectType, ic.userId, ic.userName ");
			sqlBuff.append(" , ic.portrait, ic.createTime, ic.coordinate, ic.coordinatePly, ic.investTotal ");
			//sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(n.sInvestment, 0) AS sInvestment1, FORMAT(IFNULL(n.sInvestment / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(ic.allPay, 0) AS sInvestment1, FORMAT(IFNULL(ic.allPay / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.ix, ic.xmYear, ic.cycle,ic.people,ic.peopleId,ic.type ");
			sqlBuff.append(" FROM (SELECT i.*, c.* ");
			sqlBuff.append(" FROM (SELECT * FROM ");
			sqlBuff.append(" (SELECT name, xmYear, scale, period, owner ");
			sqlBuff.append(" , county, startTime, completionTime, address, industry ");
			sqlBuff.append(" , isState, isCompleted, budgetType, projectType, userId ");
			sqlBuff.append(" , userName, portrait, createTime, coordinate, coordinatePly ");
			sqlBuff.append(" , xmNumber AS ix, cycle ");
			//添加累计完成投资数
			sqlBuff.append(" ,allPay ");
			sqlBuff.append(" FROM xm_info ");
			sqlBuff.append(" WHERE 1 = 1 AND xmNumber NOT LIKE '%FP%'");
			if(thisBudgetType != null && !"".equals(thisBudgetType)){
				sqlBuff.append(" AND budgetType LIKE '%"+thisBudgetType+"%' ");
			}
			if(thisProjectType != null && !"".equals(thisProjectType)){
				sqlBuff.append(" AND projectType ='"+thisProjectType+"' ");
			}
			if(thisYear != null && !"".equals(thisYear)){
				sqlBuff.append(" AND xmYear ='"+thisYear+"' ");
			}
			if(thisInfoName != null && !"".equals(thisInfoName)){
				sqlBuff.append(" AND name LIKE '%"+thisInfoName+"%' ");
			}
			sqlBuff.append(" ORDER BY projectType DESC) AS d1 ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT people,peopleId,type,xmNumber AS dx FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS d2 ");
			sqlBuff.append(" ON d1.ix=d2.dx ");
			sqlBuff.append(" ) i ");
			sqlBuff.append(" LEFT JOIN (SELECT investTotal, investSource, investPlan, investComplete, xmNumber AS cx ");
			sqlBuff.append(" FROM xm_capital ");
			sqlBuff.append(" ) c ON c.cx = i.ix ");
			sqlBuff.append(" ) ic ");
			/*sqlBuff.append(" LEFT JOIN (SELECT IFNULL(SUM(Investment), 0) AS sInvestment, xmNumber AS nx ");
			sqlBuff.append(" FROM xm_invest ");
			sqlBuff.append(" WHERE 1 = 1 ");
			sqlBuff.append(" AND investYear = '"+thisYear+"' ");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) n ON ic.ix = n.nx ");*/
			sqlBuff.append(" ) dd ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01'");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON dd.ix = ss.xmNumber ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND dd.peopleId='"+thisPeopleId+"' ");
			}
			
			System.out.println("项目信息列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String thisXmNumber = (String)oc[25];
				String name = (String)oc[0];
				String xmYear = (String)oc[26];
				String scale = (String)oc[1];
				String period = (String)oc[2];
				String owner = (String)oc[3];
				String county = (String)oc[4];
				String startTime = (Timestamp)oc[5]+"";
				String completionTime = (Timestamp)oc[6]+"";
				String address = (String)oc[7];
				String industry = (String)oc[8];
				String isState = (String)oc[9];
				String isCompleted = (String)oc[10];
				String budgetType = (String)oc[11];
				String projectType = (String)oc[12];
				String userId = (String)oc[13];
				String userName = (String)oc[14];
				String portrait = (String)oc[15];
				String createTime = (Timestamp)oc[16]+"";
				
				String coordinate = (String)oc[17];
				String coordinatePly = (String)oc[18];
				
				String investTotal = (String)oc[19];
				String investSource = (String)oc[20];
				String investPlan = (String)oc[21];
				String investComplete = (String)oc[22];
				
				String sInvestment = (String)oc[23];
				String cRate = (String)oc[24]+"";
				
				String cycle = (String)oc[27];
				String maxScheduleTime = (Timestamp)oc[28]+"";//最新工作进度（时间）
				BigInteger nowCycle = (BigInteger)oc[29];//当前时间-最新工作进度（时间）
				BigInteger isOverdue = (BigInteger)oc[30];
				
				ViewXmInfo viewXmInfo = new ViewXmInfo();
				
				viewXmInfo.setXmNumber(thisXmNumber);
				viewXmInfo.setXmYear(xmYear);
				viewXmInfo.setName(name);
				viewXmInfo.setScale(scale);
				viewXmInfo.setPeriod(period);
				viewXmInfo.setOwner(owner);
				viewXmInfo.setCounty(county);
				viewXmInfo.setStartTime(startTime);
				viewXmInfo.setCompletionTime(completionTime);
				viewXmInfo.setAddress(address);
				viewXmInfo.setIndustry(industry);
				viewXmInfo.setIsState(isState);
				viewXmInfo.setIsCompleted(isCompleted);
				viewXmInfo.setBudgetType(budgetType);
				viewXmInfo.setProjectType(projectType);
				viewXmInfo.setUserId(userId);
				viewXmInfo.setUserName(userName);
				viewXmInfo.setPortrait(portrait);
				viewXmInfo.setCreateTime(createTime);
				viewXmInfo.setCoordinate(coordinate);
				viewXmInfo.setCoordinatePly(coordinatePly);
				viewXmInfo.setInvestTotal(investTotal);
				viewXmInfo.setInvestSource(investSource);
				viewXmInfo.setInvestPlan(investPlan);
				viewXmInfo.setInvestComplete(investComplete);
				
				viewXmInfo.setSInvestment(sInvestment);
				viewXmInfo.setCRate(cRate);
				
				viewXmInfo.setCycle(cycle);
				viewXmInfo.setMaxScheduleTime(maxScheduleTime);
				viewXmInfo.setNowCycle(nowCycle+"");
				viewXmInfo.setIsOverdue(isOverdue+"");
				
				rsView.add(viewXmInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	@Override
	public List<ViewXmInfo> allViewXmPoorInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, String thisInfoName, String thisProjectType) {
		
		List<ViewXmInfo> rsView = new ArrayList<ViewXmInfo>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT dd.name, dd.scale, dd.period, dd.owner, dd.county ");
			sqlBuff.append(" , dd.startTime, dd.completionTime, dd.address, dd.industry, dd.isState ");
			sqlBuff.append(" , dd.isCompleted, dd.budgetType, dd.projectType, dd.userId, dd.userName ");
			sqlBuff.append(" , dd.portrait, dd.createTime, dd.coordinate, dd.coordinatePly, dd.investTotal ");
			sqlBuff.append(" , dd.investSource, dd.investPlan, dd.investComplete, dd.sInvestment1, dd.aps ");
			sqlBuff.append(" , dd.ix, dd.xmYear, dd.cycle, ss.nt, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > dd.cycle, 1, 0) AS ifd ");
			sqlBuff.append(" FROM (SELECT ic.name, ic.scale, ic.period, ic.owner, ic.county ");
			sqlBuff.append(" , ic.startTime, ic.completionTime, ic.address, ic.industry, ic.isState ");
			sqlBuff.append(" , ic.isCompleted, ic.budgetType, ic.projectType, ic.userId, ic.userName ");
			sqlBuff.append(" , ic.portrait, ic.createTime, ic.coordinate, ic.coordinatePly, ic.investTotal ");
			//sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(n.sInvestment, 0) AS sInvestment1, FORMAT(IFNULL(n.sInvestment / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(ic.allPay, 0) AS sInvestment1, FORMAT(IFNULL(ic.allPay / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.ix, ic.xmYear, ic.cycle,ic.people,ic.peopleId,ic.type ");
			sqlBuff.append(" FROM (SELECT i.*, c.* ");
			sqlBuff.append(" FROM (SELECT * FROM ");
			sqlBuff.append(" (SELECT name, xmYear, scale, period, owner ");
			sqlBuff.append(" , county, startTime, completionTime, address, industry ");
			sqlBuff.append(" , isState, isCompleted, budgetType, projectType, userId ");
			sqlBuff.append(" , userName, portrait, createTime, coordinate, coordinatePly ");
			sqlBuff.append(" , xmNumber AS ix, cycle ");
			//添加累计完成投资数
			sqlBuff.append(" ,allPay ");
			sqlBuff.append(" FROM xm_info ");
			sqlBuff.append(" WHERE 1 = 1 AND xmNumber LIKE '%FP%'");
			if(thisBudgetType != null && !"".equals(thisBudgetType)){
				sqlBuff.append(" AND budgetType LIKE '%"+thisBudgetType+"%' ");
			}
			if(thisProjectType != null && !"".equals(thisProjectType)){
				sqlBuff.append(" AND projectType ='"+thisProjectType+"' ");
			}
			if(thisYear != null && !"".equals(thisYear)){
				sqlBuff.append(" AND xmYear ='"+thisYear+"' ");
			}
			if(thisInfoName != null && !"".equals(thisInfoName)){
				sqlBuff.append(" AND name LIKE '%"+thisInfoName+"%' ");
			}
			sqlBuff.append(" ORDER BY projectType DESC) AS d1 ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT people,peopleId,type,xmNumber AS dx FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS d2 ");
			sqlBuff.append(" ON d1.ix=d2.dx ");
			sqlBuff.append(" ) i ");
			sqlBuff.append(" LEFT JOIN (SELECT investTotal, investSource, investPlan, investComplete, xmNumber AS cx ");
			sqlBuff.append(" FROM xm_capital ");
			sqlBuff.append(" ) c ON c.cx = i.ix ");
			sqlBuff.append(" ) ic ");
			/*sqlBuff.append(" LEFT JOIN (SELECT IFNULL(SUM(Investment), 0) AS sInvestment, xmNumber AS nx ");
			sqlBuff.append(" FROM xm_invest ");
			sqlBuff.append(" WHERE 1 = 1 ");
			sqlBuff.append(" AND investYear = '"+thisYear+"' ");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) n ON ic.ix = n.nx ");*/
			sqlBuff.append(" ) dd ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01'");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON dd.ix = ss.xmNumber ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND dd.peopleId='"+thisPeopleId+"' ");
			}
			
			System.out.println("项目信息列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String thisXmNumber = (String)oc[25];
				String name = (String)oc[0];
				String xmYear = (String)oc[26];
				String scale = (String)oc[1];
				String period = (String)oc[2];
				String owner = (String)oc[3];
				String county = (String)oc[4];
				String startTime = (Timestamp)oc[5]+"";
				String completionTime = (Timestamp)oc[6]+"";
				String address = (String)oc[7];
				String industry = (String)oc[8];
				String isState = (String)oc[9];
				String isCompleted = (String)oc[10];
				String budgetType = (String)oc[11];
				String projectType = (String)oc[12];
				String userId = (String)oc[13];
				String userName = (String)oc[14];
				String portrait = (String)oc[15];
				String createTime = (Timestamp)oc[16]+"";
				
				String coordinate = (String)oc[17];
				String coordinatePly = (String)oc[18];
				
				String investTotal = (String)oc[19];
				String investSource = (String)oc[20];
				String investPlan = (String)oc[21];
				String investComplete = (String)oc[22];
				
				String sInvestment = (String)oc[23];
				String cRate = (String)oc[24]+"";
				
				String cycle = (String)oc[27];
				String maxScheduleTime = (Timestamp)oc[28]+"";//最新工作进度（时间）
				BigInteger nowCycle = (BigInteger)oc[29];//当前时间-最新工作进度（时间）
				BigInteger isOverdue = (BigInteger)oc[30];
				
				ViewXmInfo viewXmInfo = new ViewXmInfo();
				
				viewXmInfo.setXmNumber(thisXmNumber);
				viewXmInfo.setXmYear(xmYear);
				viewXmInfo.setName(name);
				viewXmInfo.setScale(scale);
				viewXmInfo.setPeriod(period);
				viewXmInfo.setOwner(owner);
				viewXmInfo.setCounty(county);
				viewXmInfo.setStartTime(startTime);
				viewXmInfo.setCompletionTime(completionTime);
				viewXmInfo.setAddress(address);
				viewXmInfo.setIndustry(industry);
				viewXmInfo.setIsState(isState);
				viewXmInfo.setIsCompleted(isCompleted);
				viewXmInfo.setBudgetType(budgetType);
				viewXmInfo.setProjectType(projectType);
				viewXmInfo.setUserId(userId);
				viewXmInfo.setUserName(userName);
				viewXmInfo.setPortrait(portrait);
				viewXmInfo.setCreateTime(createTime);
				viewXmInfo.setCoordinate(coordinate);
				viewXmInfo.setCoordinatePly(coordinatePly);
				viewXmInfo.setInvestTotal(investTotal);
				viewXmInfo.setInvestSource(investSource);
				viewXmInfo.setInvestPlan(investPlan);
				viewXmInfo.setInvestComplete(investComplete);
				
				viewXmInfo.setSInvestment(sInvestment);
				viewXmInfo.setCRate(cRate);
				
				viewXmInfo.setCycle(cycle);
				viewXmInfo.setMaxScheduleTime(maxScheduleTime);
				viewXmInfo.setNowCycle(nowCycle+"");
				viewXmInfo.setIsOverdue(isOverdue+"");
				
				rsView.add(viewXmInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 项目信息列表(分页)
	 * @return
	 */
	@Override
	public Result<ViewXmInfo> resultViewXmInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, int page, int size) {
		Result<ViewXmInfo> rsView = new Result<ViewXmInfo>();
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			int start=(page-1)*size;
			int limit =size;
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT dd.name, dd.scale, dd.period, dd.owner, dd.county ");
			sqlBuff.append(" , dd.startTime, dd.completionTime, dd.address, dd.industry, dd.isState ");
			sqlBuff.append(" , dd.isCompleted, dd.budgetType, dd.projectType, dd.userId, dd.userName ");
			sqlBuff.append(" , dd.portrait, dd.createTime, dd.coordinate, dd.coordinatePly, dd.investTotal ");
			sqlBuff.append(" , dd.investSource, dd.investPlan, dd.investComplete, dd.sInvestment1, dd.aps ");
			sqlBuff.append(" , dd.ix, dd.xmYear, dd.cycle, ss.nt, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > dd.cycle, 1, 0) AS ifd ");
			sqlBuff.append(" FROM (SELECT ic.name, ic.scale, ic.period, ic.owner, ic.county ");
			sqlBuff.append(" , ic.startTime, ic.completionTime, ic.address, ic.industry, ic.isState ");
			sqlBuff.append(" , ic.isCompleted, ic.budgetType, ic.projectType, ic.userId, ic.userName ");
			sqlBuff.append(" , ic.portrait, ic.createTime, ic.coordinate, ic.coordinatePly, ic.investTotal ");
			//sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(n.sInvestment, 0) AS sInvestment1, FORMAT(IFNULL(n.sInvestment / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(ic.allPay, 0) AS sInvestment1, FORMAT(IFNULL(ic.allPay / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.ix, ic.xmYear, ic.cycle,ic.people,ic.peopleId,ic.type ");
			sqlBuff.append(" FROM (SELECT i.*, c.* ");
			sqlBuff.append(" FROM (SELECT * FROM ");
			sqlBuff.append(" (SELECT name, xmYear, scale, period, owner ");
			sqlBuff.append(" , county, startTime, completionTime, address, industry ");
			sqlBuff.append(" , isState, isCompleted, budgetType, projectType, userId ");
			sqlBuff.append(" , userName, portrait, createTime, coordinate, coordinatePly ");
			sqlBuff.append(" , xmNumber AS ix, cycle ");
			//添加累计完成投资数
			sqlBuff.append(" ,allPay ");
			sqlBuff.append(" FROM xm_info ");
			sqlBuff.append(" WHERE 1 = 1");
			if(thisBudgetType != null && !"".equals(thisBudgetType)){
				sqlBuff.append(" AND budgetType LIKE '%"+thisBudgetType+"%' ");
			}
			if(thisYear != null && !"".equals(thisYear)){
				sqlBuff.append(" AND xmYear ='"+thisYear+"' ");
			}
			sqlBuff.append(" ORDER BY projectType DESC) AS d1 ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT people,peopleId,type,xmNumber AS dx FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS d2 ");
			sqlBuff.append(" ON d1.ix=d2.dx ");
			sqlBuff.append(" ) i ");
			sqlBuff.append(" LEFT JOIN (SELECT investTotal, investSource, investPlan, investComplete, xmNumber AS cx ");
			sqlBuff.append(" FROM xm_capital ");
			sqlBuff.append(" ) c ON c.cx = i.ix ");
			sqlBuff.append(" ) ic ");
			/*sqlBuff.append(" LEFT JOIN (SELECT IFNULL(SUM(Investment), 0) AS sInvestment, xmNumber AS nx ");
			sqlBuff.append(" FROM xm_invest ");
			sqlBuff.append(" WHERE 1 = 1 ");
			if(thisYear!=null && !"".equals(thisYear)){
				sqlBuff.append(" AND investYear = '"+thisYear+"' ");
			}
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) n ON ic.ix = n.nx ");*/
			sqlBuff.append(" ) dd ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01'");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON dd.ix = ss.xmNumber ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND dd.peopleId='"+thisPeopleId+"' ");
			}
			
			///System.out.println("项目信息列表="+sqlBuff.toString());
			
			Result rs=xmInfoDao.findBySQL(sqlBuff.toString(), null, start, limit);
			List list=rs.getData();
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String thisXmNumber = (String)oc[25];
				String name = (String)oc[0];
				String xmYear = (String)oc[26];
				String scale = (String)oc[1];
				String period = (String)oc[2];
				String owner = (String)oc[3];
				String county = (String)oc[4];
				
				String startTime = "";
				if((Timestamp)oc[5] != null && !"".equals((Timestamp)oc[5])){
					startTime = sdf.format((Timestamp)oc[5]);
				}
				
				String completionTime = "";
				if((Timestamp)oc[6] != null && !"".equals((Timestamp)oc[6])){
					completionTime = sdf.format((Timestamp)oc[6]);
				}
				
				String address = (String)oc[7];
				String industry = (String)oc[8];
				String isState = (String)oc[9];
				String isCompleted = (String)oc[10];
				String budgetType = (String)oc[11];
				String projectType = (String)oc[12];
				String userId = (String)oc[13];
				String userName = (String)oc[14];
				String portrait = (String)oc[15];
				String createTime = (Timestamp)oc[16]+"";
				String coordinate = (String)oc[17];
				String coordinatePly = (String)oc[18];
				String investTotal = (String)oc[19];
				String investSource = (String)oc[20];
				String investPlan = (String)oc[21];
				String investComplete = (String)oc[22];
				String sInvestment = (String)oc[23];
				String cRate = (String)oc[24]+"";
				String cycle = (String)oc[27];
				String maxScheduleTime = (Timestamp)oc[28]+"";//最新工作进度（时间）
				BigInteger nowCycle = (BigInteger)oc[29];//当前时间-最新工作进度（时间）
				BigInteger isOverdue = (BigInteger)oc[30];;
				
				ViewXmInfo viewXmInfo = new ViewXmInfo();
				
				viewXmInfo.setXmNumber(thisXmNumber);
				viewXmInfo.setXmYear(xmYear);
				viewXmInfo.setName(name);
				viewXmInfo.setScale(scale);
				viewXmInfo.setPeriod(period);
				viewXmInfo.setOwner(owner);
				viewXmInfo.setCounty(county);
				viewXmInfo.setStartTime(startTime);
				viewXmInfo.setCompletionTime(completionTime);
				viewXmInfo.setAddress(address);
				viewXmInfo.setIndustry(industry);
				viewXmInfo.setIsState(isState);
				viewXmInfo.setIsCompleted(isCompleted);
				viewXmInfo.setBudgetType(budgetType);
				viewXmInfo.setProjectType(projectType);
				viewXmInfo.setUserId(userId);
				viewXmInfo.setUserName(userName);
				viewXmInfo.setPortrait(portrait);
				viewXmInfo.setCreateTime(createTime);
				viewXmInfo.setCoordinate(coordinate);
				viewXmInfo.setCoordinatePly(coordinatePly);
				viewXmInfo.setInvestTotal(investTotal);
				viewXmInfo.setInvestSource(investSource);
				viewXmInfo.setInvestPlan(investPlan);
				viewXmInfo.setInvestComplete(investComplete);
				viewXmInfo.setSInvestment(sInvestment);
				viewXmInfo.setCRate(cRate);
				viewXmInfo.setCycle(cycle);
				viewXmInfo.setMaxScheduleTime(maxScheduleTime);
				viewXmInfo.setNowCycle(nowCycle+"");
				viewXmInfo.setIsOverdue(isOverdue+"");
				
				rsView.getData().add(viewXmInfo);
			}
			rsView.setOffset(rs.getOffset());
			rsView.setPage(rs.getPage());
			rsView.setSize(rs.getSize());
			rsView.setTotal(rs.getTotal());
			rsView.setTotalPage(rs.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	
	/**
	 * 扶贫项目信息列表(分页)
	 * @return
	 */
	@Override
	public Result<ViewXmInfo> resultViewPoorXmInfoByYear(String thisYear, String thisPeopleId, String thisBudgetType, int page, int size) {
		Result<ViewXmInfo> rsView = new Result<ViewXmInfo>();
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			int start=(page-1)*size;
			int limit =size;
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT dd.name, dd.scale, dd.period, dd.owner, dd.county ");
			sqlBuff.append(" , dd.startTime, dd.completionTime, dd.address, dd.industry, dd.isState ");
			sqlBuff.append(" , dd.isCompleted, dd.budgetType, dd.projectType, dd.userId, dd.userName ");
			sqlBuff.append(" , dd.portrait, dd.createTime, dd.coordinate, dd.coordinatePly, dd.investTotal ");
			sqlBuff.append(" , dd.investSource, dd.investPlan, dd.investComplete, dd.sInvestment1, dd.aps ");
			sqlBuff.append(" , dd.ix, dd.xmYear, dd.cycle, ss.nt, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > dd.cycle, 1, 0) AS ifd ");
			sqlBuff.append(" FROM (SELECT ic.name, ic.scale, ic.period, ic.owner, ic.county ");
			sqlBuff.append(" , ic.startTime, ic.completionTime, ic.address, ic.industry, ic.isState ");
			sqlBuff.append(" , ic.isCompleted, ic.budgetType, ic.projectType, ic.userId, ic.userName ");
			sqlBuff.append(" , ic.portrait, ic.createTime, ic.coordinate, ic.coordinatePly, ic.investTotal ");
			//sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(n.sInvestment, 0) AS sInvestment1, FORMAT(IFNULL(n.sInvestment / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(ic.allPay, 0) AS sInvestment1, FORMAT(IFNULL(ic.allPay / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.ix, ic.xmYear, ic.cycle,ic.people,ic.peopleId,ic.type ");
			sqlBuff.append(" FROM (SELECT i.*, c.* ");
			sqlBuff.append(" FROM (SELECT * FROM ");
			sqlBuff.append(" (SELECT name, xmYear, scale, period, owner ");
			sqlBuff.append(" , county, startTime, completionTime, address, industry ");
			sqlBuff.append(" , isState, isCompleted, budgetType, projectType, userId ");
			sqlBuff.append(" , userName, portrait, createTime, coordinate, coordinatePly ");
			sqlBuff.append(" , xmNumber AS ix, cycle ");
			//添加累计完成投资数
			sqlBuff.append(" ,allPay ");
			sqlBuff.append(" FROM xm_info ");
			sqlBuff.append(" WHERE 1 = 1 AND xmNumber like '%FP%'");
			if(thisBudgetType != null && !"".equals(thisBudgetType)){
				sqlBuff.append(" AND budgetType LIKE '%"+thisBudgetType+"%' ");
			}
			if(thisYear != null && !"".equals(thisYear)){
				sqlBuff.append(" AND xmYear ='"+thisYear+"' ");
			}
			sqlBuff.append(" ORDER BY projectType DESC) AS d1 ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT people,peopleId,type,xmNumber AS dx FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS d2 ");
			sqlBuff.append(" ON d1.ix=d2.dx ");
			sqlBuff.append(" ) i ");
			sqlBuff.append(" LEFT JOIN (SELECT investTotal, investSource, investPlan, investComplete, xmNumber AS cx ");
			sqlBuff.append(" FROM xm_capital ");
			sqlBuff.append(" ) c ON c.cx = i.ix ");
			sqlBuff.append(" ) ic ");
			/*sqlBuff.append(" LEFT JOIN (SELECT IFNULL(SUM(Investment), 0) AS sInvestment, xmNumber AS nx ");
			sqlBuff.append(" FROM xm_invest ");
			sqlBuff.append(" WHERE 1 = 1 ");
			if(thisYear!=null && !"".equals(thisYear)){
				sqlBuff.append(" AND investYear = '"+thisYear+"' ");
			}
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) n ON ic.ix = n.nx ");*/
			sqlBuff.append(" ) dd ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01'");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON dd.ix = ss.xmNumber ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND dd.peopleId='"+thisPeopleId+"' ");
			}
			
			//System.out.println("项目信息列表="+sqlBuff.toString());
			
			Result rs=xmInfoDao.findBySQL(sqlBuff.toString(), null, start, limit);
			List list=rs.getData();
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String thisXmNumber = (String)oc[25];
				String name = (String)oc[0];
				String xmYear = (String)oc[26];
				String scale = (String)oc[1];
				String period = (String)oc[2];
				String owner = (String)oc[3];
				String county = (String)oc[4];
				
				String startTime = "";
				if((Timestamp)oc[5] != null && !"".equals((Timestamp)oc[5])){
					startTime = sdf.format((Timestamp)oc[5]);
				}
				
				String completionTime = "";
				if((Timestamp)oc[6] != null && !"".equals((Timestamp)oc[6])){
					completionTime = sdf.format((Timestamp)oc[6]);
				}
				
				String address = (String)oc[7];
				String industry = (String)oc[8];
				String isState = (String)oc[9];
				String isCompleted = (String)oc[10];
				String budgetType = (String)oc[11];
				String projectType = (String)oc[12];
				String userId = (String)oc[13];
				String userName = (String)oc[14];
				String portrait = (String)oc[15];
				String createTime = (Timestamp)oc[16]+"";
				String coordinate = (String)oc[17];
				String coordinatePly = (String)oc[18];
				String investTotal = (String)oc[19];
				String investSource = (String)oc[20];
				String investPlan = (String)oc[21];
				String investComplete = (String)oc[22];
				String sInvestment = (String)oc[23];
				String cRate = (String)oc[24]+"";
				String cycle = (String)oc[27];
				String maxScheduleTime = (Timestamp)oc[28]+"";//最新工作进度（时间）
				BigInteger nowCycle = (BigInteger)oc[29];//当前时间-最新工作进度（时间）
				BigInteger isOverdue = (BigInteger)oc[30];;
				
				ViewXmInfo viewXmInfo = new ViewXmInfo();
				
				viewXmInfo.setXmNumber(thisXmNumber);
				viewXmInfo.setXmYear(xmYear);
				viewXmInfo.setName(name);
				viewXmInfo.setScale(scale);
				viewXmInfo.setPeriod(period);
				viewXmInfo.setOwner(owner);
				viewXmInfo.setCounty(county);
				viewXmInfo.setStartTime(startTime);
				viewXmInfo.setCompletionTime(completionTime);
				viewXmInfo.setAddress(address);
				viewXmInfo.setIndustry(industry);
				viewXmInfo.setIsState(isState);
				viewXmInfo.setIsCompleted(isCompleted);
				viewXmInfo.setBudgetType(budgetType);
				viewXmInfo.setProjectType(projectType);
				viewXmInfo.setUserId(userId);
				viewXmInfo.setUserName(userName);
				viewXmInfo.setPortrait(portrait);
				viewXmInfo.setCreateTime(createTime);
				viewXmInfo.setCoordinate(coordinate);
				viewXmInfo.setCoordinatePly(coordinatePly);
				viewXmInfo.setInvestTotal(investTotal);
				viewXmInfo.setInvestSource(investSource);
				viewXmInfo.setInvestPlan(investPlan);
				viewXmInfo.setInvestComplete(investComplete);
				viewXmInfo.setSInvestment(sInvestment);
				viewXmInfo.setCRate(cRate);
				viewXmInfo.setCycle(cycle);
				viewXmInfo.setMaxScheduleTime(maxScheduleTime);
				viewXmInfo.setNowCycle(nowCycle+"");
				viewXmInfo.setIsOverdue(isOverdue+"");
				
				rsView.getData().add(viewXmInfo);
			}
			rsView.setOffset(rs.getOffset());
			rsView.setPage(rs.getPage());
			rsView.setSize(rs.getSize());
			rsView.setTotal(rs.getTotal());
			rsView.setTotalPage(rs.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 项目信息列表导出
	 */
	@Override
	public downViewXmInfo viewXmInfoDownload(String thisYear, String thisPeopleId, String thisBudgetType) {
		downViewXmInfo rsView = new downViewXmInfo();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT dd.name, dd.scale, dd.period, dd.owner, dd.county ");
			sqlBuff.append(" , dd.startTime, dd.completionTime, dd.address, dd.industry, dd.isState ");
			sqlBuff.append(" , dd.isCompleted, dd.budgetType, dd.projectType, dd.userId, dd.userName ");
			sqlBuff.append(" , dd.portrait, dd.createTime, dd.coordinate, dd.coordinatePly, dd.investTotal ");
			sqlBuff.append(" , dd.investSource, dd.investPlan, dd.investComplete, dd.sInvestment1, dd.aps ");
			sqlBuff.append(" , dd.ix, dd.xmYear, dd.cycle, ss.nt, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > dd.cycle, 1, 0) AS ifd ");
			sqlBuff.append(" FROM (SELECT ic.name, ic.scale, ic.period, ic.owner, ic.county ");
			sqlBuff.append(" , ic.startTime, ic.completionTime, ic.address, ic.industry, ic.isState ");
			sqlBuff.append(" , ic.isCompleted, ic.budgetType, ic.projectType, ic.userId, ic.userName ");
			sqlBuff.append(" , ic.portrait, ic.createTime, ic.coordinate, ic.coordinatePly, ic.investTotal ");
			//sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(n.sInvestment, 0) AS sInvestment1, FORMAT(IFNULL(n.sInvestment / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.investSource, ic.investPlan, ic.investComplete, IFNULL(ic.allPay, 0) AS sInvestment1, FORMAT(IFNULL(ic.allPay / ic.investPlan * 100, 0), 2) AS aps ");
			sqlBuff.append(" , ic.ix, ic.xmYear, ic.cycle,ic.people,ic.peopleId,ic.type ");
			sqlBuff.append(" FROM (SELECT i.*, c.* ");
			sqlBuff.append(" FROM (SELECT * FROM ");
			sqlBuff.append(" (SELECT name, xmYear, scale, period, owner ");
			sqlBuff.append(" , county, startTime, completionTime, address, industry ");
			sqlBuff.append(" , isState, isCompleted, budgetType, projectType, userId ");
			sqlBuff.append(" , userName, portrait, createTime, coordinate, coordinatePly ");
			sqlBuff.append(" , xmNumber AS ix, cycle ");
			//添加累计完成投资数
			sqlBuff.append(" ,allPay ");
			sqlBuff.append(" FROM xm_info ");
			sqlBuff.append(" WHERE 1 = 1 ");
			if(thisBudgetType != null && !"".equals(thisBudgetType)){
				sqlBuff.append(" AND budgetType LIKE '%"+thisBudgetType+"%' ");
			}
			if(thisYear != null && !"".equals(thisYear)){
				sqlBuff.append(" AND xmYear ='"+thisYear+"' ");
			}
			sqlBuff.append(" ORDER BY projectType DESC) AS d1 ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT people,peopleId,type,xmNumber AS dx FROM xm_duty ");
			
			if (thisPeopleId == null || "".equals(thisPeopleId)) {
				sqlBuff.append(" GROUP BY xmNumber ");
			}
			
			sqlBuff.append(" ) AS d2 ");
			sqlBuff.append(" ON d1.ix=d2.dx ");
			sqlBuff.append(" ) i ");
			sqlBuff.append(" LEFT JOIN (SELECT investTotal, investSource, investPlan, investComplete, xmNumber AS cx ");
			sqlBuff.append(" FROM xm_capital ");
			sqlBuff.append(" ) c ON c.cx = i.ix ");
			sqlBuff.append(" ) ic ");
			/*sqlBuff.append(" LEFT JOIN (SELECT IFNULL(SUM(Investment), 0) AS sInvestment, xmNumber AS nx ");
			sqlBuff.append(" FROM xm_invest ");
			sqlBuff.append(" WHERE 1 = 1 ");
			if(thisYear!=null && !"".equals(thisYear)){
				sqlBuff.append(" AND investYear = '"+thisYear+"' ");
			}
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) n ON ic.ix = n.nx ");*/
			sqlBuff.append(" ) dd ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01'");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON dd.ix = ss.xmNumber ");
			sqlBuff.append(" WHERE 1=1 ");
			
			if (thisPeopleId != null && !"".equals(thisPeopleId)) {
				sqlBuff.append(" AND dd.peopleId='"+thisPeopleId+"' ");
			}

			//System.out.println("项目信息列表导出:"+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			rsView.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 用户-项目列表（取最新一条）
	 * @return
	 */
	@Override
	public List<ViewXmInfo> userViewXmInfoByYear(String thisYear, String thisBudgetType, String thisProjectType) {
		
		List<ViewXmInfo> rsView = new ArrayList<ViewXmInfo>();
		
		StringBuffer sqlBuff=new StringBuffer();
		
		sqlBuff.append(" SELECT us.id,us.helpName,us.portrait, ");
		sqlBuff.append(" IFNULL(xi.name,'##') AS name1, ");
		sqlBuff.append(" IFNULL(xi.scale,'##') AS scale1, ");
		sqlBuff.append(" IFNULL(xi.period,'##') AS period1, ");
		sqlBuff.append(" IFNULL(xi.owner,'##') AS owner1, ");
		sqlBuff.append(" IFNULL(xi.county,'##') AS county1, ");
		sqlBuff.append(" xi.startTime, ");
		sqlBuff.append(" xi.completionTime, ");
		sqlBuff.append(" IFNULL(xi.address,'##') AS address1, ");
		sqlBuff.append(" IFNULL(xi.industry,'##') AS industry1, ");
		sqlBuff.append(" IFNULL(xi.isState,'##') AS isState1, ");
		sqlBuff.append(" IFNULL(xi.isCompleted,'##') AS isCompleted1, ");
		sqlBuff.append(" IFNULL(xi.budgetType,'##') AS budgetType1, ");
		sqlBuff.append(" IFNULL(xi.projectType,'##') AS projectType1, ");
		sqlBuff.append(" xi.createTime, ");
		sqlBuff.append(" IFNULL(xi.coordinate,'##') AS coordinate1, ");
		sqlBuff.append(" IFNULL(xi.coordinatePly,'##') AS coordinatePly1, ");
		sqlBuff.append(" IFNULL(xi.investTotal,'##') AS investTotal1, ");
		sqlBuff.append(" IFNULL(xi.investSource,'##') AS investSource1, ");
		sqlBuff.append(" IFNULL(xi.investPlan,'##') AS investPlan1, ");
		sqlBuff.append(" IFNULL(xi.investComplete,'##') AS investComplete1, ");
		/*sqlBuff.append(" IFNULL(xi.sInvestment,0) AS sInvestment1, ");
		sqlBuff.append(" FORMAT(IFNULL((xi.sInvestment/xi.investPlan)*100,0),2) AS aps, ");*/
		sqlBuff.append(" IFNULL(xi.allPay,0) AS sInvestment1, ");
		sqlBuff.append(" FORMAT(IFNULL((xi.allPay/xi.investPlan)*100,0),2) AS aps, ");
		sqlBuff.append(" IFNULL(xi.ix,'##') AS ix1, ");
		sqlBuff.append(" IFNULL(xi.xmYear,'##') AS xmYear1 ");
		sqlBuff.append(" FROM ");
		sqlBuff.append(" (SELECT * FROM user) AS us ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT  ");
		sqlBuff.append(" ic.name,ic.scale,ic.period,ic.owner,ic.county,ic.startTime,ic.completionTime, ");
		sqlBuff.append(" ic.address,ic.industry,ic.isState,ic.isCompleted,ic.budgetType,ic.projectType, ");
		sqlBuff.append(" ic.userId,ic.userName,ic.portrait,ic.createTime,ic.coordinate,ic.coordinatePly, ");
		sqlBuff.append(" ic.investTotal,ic.investSource,ic.investPlan,ic.investComplete, ");
		//sqlBuff.append(" n.sInvestment,FORMAT((n.sInvestment/ic.investPlan)*100,2) AS aps,ic.ix,ic.xmYear ");
		sqlBuff.append(" ic.allPay,FORMAT((ic.allPay/ic.investPlan)*100,2) AS aps,ic.ix,ic.xmYear ");
		sqlBuff.append(" FROM ");
		sqlBuff.append(" (SELECT i.*,c.* FROM   ");
		sqlBuff.append(" (SELECT name,xmYear,scale,period,owner,county,startTime,completionTime, ");
		sqlBuff.append(" address,industry,isState,isCompleted,budgetType,projectType, ");
		sqlBuff.append(" userId,userName,portrait,createTime,coordinate,coordinatePly,xmNumber AS ix ");
		//添加累计完成投资数
		sqlBuff.append(" ,allPay ");
		sqlBuff.append(" FROM xm_info where 1=1 ");
		if(thisBudgetType != null && !"".equals(thisBudgetType)){
			sqlBuff.append(" and budgetType LIKE '%"+thisBudgetType+"%' ");
		}
		if(thisProjectType != null && !"".equals(thisProjectType)){
			sqlBuff.append(" and projectType ='"+thisBudgetType+"' ");
		}
		sqlBuff.append(" ORDER BY projectType DESC) AS i   ");
		sqlBuff.append(" LEFT JOIN   ");
		sqlBuff.append(" (SELECT investTotal,investSource,investPlan,investComplete,xmNumber AS cx FROM xm_capital ) AS c   ");
		sqlBuff.append(" ON c.cx=i.ix) AS ic ");
		/*sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT IFNULL(SUM(Investment),0) AS sInvestment,xmNumber AS nx FROM xm_invest WHERE investYear='"+thisYear+"'  ");
		sqlBuff.append(" GROUP BY xmNumber) AS n ");
		sqlBuff.append(" ON ic.ix=n.nx ");*/
		sqlBuff.append(" )  AS xi ON xi.userId=us.id ");
		sqlBuff.append(" GROUP BY us.id; ");

		//System.out.println("用户-项目列表（取最新一条）="+sqlBuff.toString());
		
		List list=xmInfoDao.query(sqlBuff.toString());
		
		for(int j=0;j<list.size();j++){
			Object[] oc = (Object[])list.get(j);
			
			Integer userId = (Integer)oc[0];
			String userName = (String)oc[1];
			String portrait = (String)oc[2];
			
			String thisXmNumber = oc[25]+"";
			String xmYear = oc[26]+"";
			String name = oc[3]+"";
			String scale = oc[4]+"";
			String period = oc[5]+"";
			String owner = oc[6]+"";
			String county = oc[7]+"";
			String startTime = (Timestamp)oc[8]+"";
			String completionTime = (Timestamp)oc[9]+"";
			String address = oc[10]+"";
			String industry = oc[11]+"";
			String isState = oc[12]+"";
			String isCompleted = oc[13]+"";
			String budgetType = oc[14]+"";
			String projectType = oc[15]+"";
			String createTime = (Timestamp)oc[16]+"";
			String coordinate = oc[17]+"";
			String coordinatePly = oc[18]+"";
			
			String investTotal = oc[19]+"";
			String investSource = oc[20]+"";
			String investPlan = oc[21]+"";
			String investComplete = (String)oc[22];
			
			String sInvestment = (String)oc[23];
			String cRate = (String)oc[24]+"";
			
			ViewXmInfo viewXmInfo = new ViewXmInfo();
			
			viewXmInfo.setXmNumber(thisXmNumber+"");
			viewXmInfo.setXmYear(xmYear+"");
			viewXmInfo.setName(name);
			viewXmInfo.setScale(scale);
			viewXmInfo.setPeriod(period);
			viewXmInfo.setOwner(owner);
			viewXmInfo.setCounty(county);
			viewXmInfo.setStartTime(startTime);
			
			viewXmInfo.setCompletionTime(completionTime);
			viewXmInfo.setAddress(address);
			viewXmInfo.setIndustry(industry);
			viewXmInfo.setIsState(isState);
			viewXmInfo.setIsCompleted(isCompleted);
			viewXmInfo.setBudgetType(budgetType);
			viewXmInfo.setProjectType(projectType);
			viewXmInfo.setUserId(userId+"");
			viewXmInfo.setUserName(userName);
			viewXmInfo.setPortrait(portrait);
			viewXmInfo.setCreateTime(createTime);
			viewXmInfo.setCoordinate(coordinate);
			viewXmInfo.setCoordinatePly(coordinatePly);
			viewXmInfo.setInvestTotal(investTotal);
			viewXmInfo.setInvestSource(investSource);
			viewXmInfo.setInvestPlan(investPlan);
			viewXmInfo.setInvestComplete(investComplete);
			
			viewXmInfo.setSInvestment(sInvestment);
			viewXmInfo.setCRate(cRate);
			
			rsView.add(viewXmInfo);
		}
		return rsView;
	}
	
	/**
	 * 征地所关联的项目信息
	 * @return
	 */
	@Override
	public List<ViewToXmInfo> toXmInfo(String thisLandNumber) {
		
		List<ViewToXmInfo> rsView = new ArrayList<ViewToXmInfo>();
		
		StringBuffer sqlBuff=new StringBuffer();
		
		sqlBuff.append(" SELECT xt.tid,xt.landNumber,xi.* FROM ");
		sqlBuff.append(" (SELECT id AS tid, xmNumber, landNumber FROM xmtoland) AS xt ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT id AS fid, ");
		sqlBuff.append(" xmNumber, ");
		sqlBuff.append(" name, ");
		sqlBuff.append(" scale, ");
		sqlBuff.append(" period, ");
		sqlBuff.append(" owner, ");
		sqlBuff.append(" county, ");
		sqlBuff.append(" startTime, ");
		sqlBuff.append(" completionTime, ");
		sqlBuff.append(" address, ");
		sqlBuff.append(" industry, ");
		sqlBuff.append(" isState, ");
		sqlBuff.append(" isCompleted, ");
		sqlBuff.append(" budgetType, ");
		sqlBuff.append(" projectType, ");
		sqlBuff.append(" userId, ");
		sqlBuff.append(" userName, ");
		sqlBuff.append(" createTime, ");
		sqlBuff.append(" coordinate, ");
		sqlBuff.append(" coordinatePly, ");
		sqlBuff.append(" portrait, ");
		sqlBuff.append(" xmYear, ");
		sqlBuff.append(" cycle ");
		sqlBuff.append(" FROM xm_info) AS xi ");
		sqlBuff.append(" ON xi.xmNumber=xt.xmNumber ");
		sqlBuff.append(" WHERE 1=1 ");
		if(thisLandNumber != null && !"".equals(thisLandNumber)){
			sqlBuff.append(" and xt.landNumber='"+thisLandNumber+"' ");
		}
		sqlBuff.append(" AND xi.xmNumber IS NOT NULL ");
		sqlBuff.append(" GROUP BY xt.xmNumber,xt.landNumber ");
		
		//System.out.println("征地所关联的项目信息="+sqlBuff.toString());
		
		List list=xmInfoDao.query(sqlBuff.toString());
		
		for(int j=0;j<list.size();j++){
			Object[] oc = (Object[])list.get(j);
			
			Integer toLandprojid = (Integer)oc[0];
			String landNumber = (String)oc[1];
			Integer infoid = (Integer)oc[2];
			String xmNumber = (String)oc[3];
			
			String xmYear = (String)oc[23];
			String name = (String)oc[4];
			String scale = (String)oc[5];
			String period = (String)oc[6];
			String cycle = (String)oc[24];
			String owner = (String)oc[7];
			String county = (String)oc[8];
			Timestamp startTime = (Timestamp)oc[9];
			Timestamp completionTime = (Timestamp)oc[10];
			String address = (String)oc[11];
			String industry = (String)oc[12];
			String isState = (String)oc[13];
			String isCompleted = (String)oc[14];
			String budgetType = (String)oc[15];
			String projectType = (String)oc[16];
			String userId = (String)oc[17];
			String userName = (String)oc[18];
			String portrait = (String)oc[22];
			Timestamp createTime = (Timestamp)oc[19];
			
			String coordinate = (String)oc[20];
			String coordinatePly = (String)oc[21];
			
			ViewToXmInfo viewToXmInfo = new ViewToXmInfo();
			
			viewToXmInfo.setToLandprojid(toLandprojid);
			viewToXmInfo.setInfoid(infoid);
			viewToXmInfo.setXmNumber(xmNumber);
			viewToXmInfo.setLandNumber(landNumber);
			viewToXmInfo.setName(name);
			viewToXmInfo.setXmYear(xmYear);
			viewToXmInfo.setScale(scale);
			viewToXmInfo.setPeriod(period);
			viewToXmInfo.setCycle(cycle);
			viewToXmInfo.setOwner(owner);
			viewToXmInfo.setCounty(county);
			viewToXmInfo.setStartTime(startTime);
			viewToXmInfo.setCompletionTime(completionTime);
			viewToXmInfo.setAddress(address);
			viewToXmInfo.setIndustry(industry);
			viewToXmInfo.setIsState(isState);
			viewToXmInfo.setIsCompleted(isCompleted);
			viewToXmInfo.setBudgetType(budgetType);
			viewToXmInfo.setProjectType(projectType);
			viewToXmInfo.setUserId(userId);
			viewToXmInfo.setUserName(userName);
			viewToXmInfo.setPortrait(portrait);
			viewToXmInfo.setCreateTime(createTime);
			viewToXmInfo.setCoordinate(coordinate);
			viewToXmInfo.setCoordinatePly(coordinatePly);
			
			rsView.add(viewToXmInfo);
		}
		return rsView;
	}
	
	@Override
	public List<AutoCompleteHttpModel> likeAll(String keyword) {
		List<AutoCompleteHttpModel> results=new ArrayList<AutoCompleteHttpModel> ();
		try {
			
			String hql="select distinct id,xmNumber,name " +
					"from xm_info where name like'%"+keyword+"%'";
			//System.out.println(hql);
			List<Object[]> list=  xmInfoDao.query(hql);
			for(int i=0;i<list.size();i++){
				AutoCompleteHttpModel model =new AutoCompleteHttpModel();
				
				Object[] obj = list.get(i);
			
				model.setItemId(((Integer)obj[0]).toString());
				model.setItemName((String)obj[2]);
				model.setItemValue(((String)obj[1]).toString());
				results.add(model);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return results;
	}
	/*********************创建编号方法（内部调用）*********************/
	/**
	 * 创建新的编号
	 * @param thisNO
	 * @return
	 */
	@Override
	public String createNO(String thisNO) {
		
    	String maxNO = maxNO(thisNO); //查找最大号
		String autoNum = maxNO.substring(maxNO.length() - 2,maxNO.length()); //截取查找结果的后2位
		int num = Integer.parseInt(autoNum); //对截取结果+1
		num += 1;
		String NewNO = "";
		if(num<99){
			NewNO = thisNO + String.format("%1$,02d", num); //组装成新的号
		}else{
			NewNO = thisNO + num; //组装成新的号
		}
		return NewNO;
	}
	
	/**
	 * 创建新扶贫项目的编号
	 * @param thisNO
	 * @return
	 */
	@Override
	public String createPoorXmNo(String thisNO) {
		
    	String maxNO = maxPoorNO(thisNO); //查找最大号
		String autoNum = maxNO.substring(maxNO.length() - 2,maxNO.length()); //截取查找结果的后2位
		int num = Integer.parseInt(autoNum); //对截取结果+1
		num += 1;
		String NewNO = "";
		
		if(num<99){
			NewNO = "FP"+thisNO + String.format("%1$,02d", num); //组装成新的号
		}else{
			NewNO = "FP"+thisNO + num; //组装成新的号
		}
		return NewNO;
	}
	
	/**
	 * 查找最大编号
	 * @param thisNO
	 * @return
	 */
	public String maxNO(String thisNO) {
		 
		 String queryString = "select max(xmNumber) from XmInfo where xmNumber like'"+thisNO+"__'";
		 List list = xmInfoDao.findByHql(queryString, null);
		 String maxNO = thisNO+"00";
		 //System.out.println("sql:"+ queryString);
		 //System.out.println(new Gson().toJson(list));
		 if(list.size()!=0){
			 String oc = (String)list.get(0); 
			 if(oc != null){
				 maxNO = oc;
			 }
		 }
		return maxNO;
	}
	
	/**
	 * 查找最大编号
	 * @param thisNO
	 * @return
	 */
	public String maxPoorNO(String thisNO) {
		 
		 String queryString = "select max(xmNumber) from XmInfo where xmNumber like'FP"+thisNO+"__'";
		 List list = xmInfoDao.findByHql(queryString, null);
		 String maxNO = thisNO+"00";
		 System.out.println("sql:"+ queryString);
		 System.out.println(new Gson().toJson(list));
		 if(list.size()!=0) {
			 String oc = (String)list.get(0); 
			 if(oc != null){
				 maxNO = oc;
			 }
		 }
		return maxNO;
	}

	@Override
	public List<XmInfo> findPoorXmCoordinate(XmInfo model) {
		
		return xmInfoDao.findPoorXmCoordinate(model);
	}

	/**
	 * 查询当前所有项目类型
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	@Override
	public String allViewXmType() {

		String str="";
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			
			sqlBuff.append("SELECT * FROM xm_type");
			
			
			System.out.println("项目类型列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				
				String typeName=(String)oc[1];
				if(j==0){

					str=str+"全部,"+typeName;
				}else{
					str=str+","+typeName;
				}
				System.out.println(str);
			/*	Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				String xm_number = (String)oc[3];
				String  publish_time = (Timestamp)oc[4] + "";
				String  dead_line = (Timestamp)oc[5]  + "";
				String receiver = (String)oc[6];
				String type = (String)oc[7];
				Integer file_count = (Integer)oc[8];
				String file_name = (String)oc[9];
				Integer status = (Integer)oc[10];
				String requires = (String)oc[11];*/
			
				/*ViewXm viewXm = new ViewXm();
				
				viewXm.setId(id);
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setReceiver(receiver);
				viewXm.setType(type);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);*/
				
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * 学院任务信息列表
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 * @param thisBudgetType
	 */
	@Override
	public List<ViewXm> allViewXm(String thisBudgetType) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<ViewXm> rsView = new ArrayList<ViewXm>();
		try {
			StringBuffer sqlBuff=new StringBuffer();

			if(thisBudgetType.equals("全部")){
				sqlBuff.append("SELECT * FROM xm  ORDER BY publish_time DESC");
			}else{
				sqlBuff.append("SELECT * FROM xm where type ='"+thisBudgetType+"'  ORDER BY publish_time DESC");
			}
			System.out.println("学院任务信息列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				User user2 = userService.getByUserName(username);
				String xm_number = (String)oc[3];
				String  publish_time =df.format( (Timestamp)oc[4]);
				String  dead_line =df.format( (Timestamp)oc[5]);
				String receiver = (String)oc[6];
				String receiver_unit = (String)oc[7];
				List<Unit> unitlist = new ArrayList();
				List<User> userlist = new ArrayList(); 
				System.out.println(receiver+"/");
				System.out.println(receiver_unit+"?");
				if(receiver != null){
					String [] userList =  receiver.split(",");
					
					System.out.println(userList.length+"//");
					for (int i = 0; i < userList.length; i++) {
						User user = userService.findByJobNumber(userList[i]);
						userlist.add(user);
					}
				}
				if(receiver_unit != null){
					String [] unitList =  receiver_unit.split(",");
					for (int i = 0; i < unitList.length; i++) {
						Unit user = unitService.findById(unitList[i]);
						unitlist.add(user);
					}
				}
				
				ViewXm viewXm = new ViewXm();
				String type = (String)oc[8];
				Integer file_count = (Integer)oc[9];
				String file_name = (String)oc[10];
				Integer status = (Integer)oc[11];
				String requires = (String)oc[12];
				if(oc[14] != null){
					String finishTime = df.format((Timestamp)oc[14]);
					viewXm.setFinish_time(finishTime);
				}
				viewXm.setUser(user2);
				viewXm.setListUser(userlist);
				viewXm.setListUnit(unitlist);
				viewXm.setId(id);
				
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setType(type);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);
				viewXm.setReceiver(receiver);
				viewXm.setReceiver_unit(receiver_unit);
				rsView.add(viewXm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rsView;
	}
	
	/**
	 * 根据项目ID查找该项目基本信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	@Override
	public List<ViewXm> findXmByNum(String xmNumber) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<ViewXm> rsView = new ArrayList<ViewXm>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append("SELECT * FROM xm where xm_number='"+xmNumber+"'  ORDER BY publish_time DESC");
			
			System.out.println("学院任务信息列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);

				Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				String xm_number = (String)oc[3];
				String  publish_time =df.format( (Timestamp)oc[4]);
				String  dead_line =df.format( (Timestamp)oc[5]);
				String receiver = (String)oc[6];
				String receiver_unit = (String)oc[7];
				List<Unit> unitlist = new ArrayList();
				List<User> userlist = new ArrayList();
				User user1 = userService.getByUserName(username);
				if(receiver != null){
					String [] userList =  receiver.split(",");
					for (int i = 0; i < userList.length; i++) {
						User user = userService.findByJobNumber(userList[i]);
						userlist.add(user);
					}
				}
				if(receiver_unit != null){
					String [] unitList =  receiver_unit.split(",");
					for (int i = 0; i < unitList.length; i++) {
						Unit user = unitService.findById(unitList[i]);
						unitlist.add(user);
					}
				}
					
				String type = (String)oc[8];
				Integer file_count = (Integer)oc[9];
				String file_name = (String)oc[10];
				Integer status = (Integer)oc[11];
				String requires = (String)oc[12];
				ViewXm viewXm = new ViewXm();
				viewXm.setListUser(userlist);
				viewXm.setListUnit(unitlist);
				viewXm.setUser(user1);
				viewXm.setId(id);
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setType(type);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);
				viewXm.setReceiver(receiver);
				viewXm.setReceiver_unit(receiver_unit);
				//查询所有批示
				List<Instructions> instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(xm_number);
			    List<ViewPsInfoModel> viePsInfoModelList=new ArrayList<ViewPsInfoModel>();
				for (int i = 0; i < instructionsByXmNumber.size(); i++) {
					Instructions instructions = instructionsByXmNumber.get(i);
					ViewPsInfoModel viewPsInfoModel=new ViewPsInfoModel();
					viewPsInfoModel.setInstructionsContent(instructions.getInstructionsContent());
					viewPsInfoModel.setInstructionsPerson(instructions.getInstructionsPerson());
					viewPsInfoModel.setInstructionsTime(instructions.getInstructionsTime().toString());
					viewPsInfoModel.setXmNumber(instructions.getXmNumber());
					viePsInfoModelList.add(viewPsInfoModel);
				}
				viewXm.setListPs(viePsInfoModelList);
				rsView.add(viewXm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}


	/**
	 * 学院任务-点击头像查询该用户名下的所有总项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<ViewXm> allViewXmByUser(String thisUserId) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<ViewXm> rsView = new ArrayList<ViewXm>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			
			sqlBuff.append("SELECT * FROM xm Where username = '"+thisUserId+"'  ORDER BY publish_time DESC");
			
			
			System.out.println("学院任务信息列表="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);

				Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				User user2 = userService.getByUserName(username);
				String xm_number = (String)oc[3];
				String  publish_time =df.format( (Timestamp)oc[4]);
				String  dead_line =df.format( (Timestamp)oc[5]);
				String receiver = (String)oc[6];
				String receiver_unit = (String)oc[7];
				List<Unit> unitlist = new ArrayList();
				List<User> userlist = new ArrayList();
				User user1 = userService.getByUserName(username);
				if(receiver != null){
					String [] userList =  receiver.split(",");
					for (int i = 0; i < userList.length; i++) {
						User user = userService.findByJobNumber(userList[i]);
						userlist.add(user);
					}
				}
				if(receiver_unit != null){
					String [] unitList =  receiver_unit.split(",");
					for (int i = 0; i < unitList.length; i++) {
						Unit user = unitService.findById(unitList[i]);
						unitlist.add(user);
					}
				}
				String type = (String)oc[8];
				Integer file_count = (Integer)oc[9];
				String file_name = (String)oc[10];
				Integer status = (Integer)oc[11];
				String requires = (String)oc[12];
				ViewXm viewXm = new ViewXm();
				viewXm.setUser(user2);
				viewXm.setListUser(userlist);
				viewXm.setListUnit(unitlist);
				viewXm.setUser(user1);
				viewXm.setId(id);
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setType(type);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);
				viewXm.setReceiver(receiver);
				viewXm.setReceiver_unit(receiver_unit);

				rsView.add(viewXm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}

	/**
	 * 安卓查询所有子项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	@Override
	public List<ViewXmChild> findAllXmChild() {

		List<XmChild> allXmChild = xmChildService.findAllXmChild();
		List<ViewXmChild> viewXmChildList=changeXmChild(allXmChild);
		return viewXmChildList;
	}

	/**
	 * 安卓根据项目编号查询详细信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<ViewXmChild> findXmChildByNum(String thisUserId,String thisInfoName) {
		List<XmChild> allXmChild = xmChildService.findXmChildByNum(thisUserId,thisInfoName);
		List<ViewXmChild> viewXmChildList=changeXmChild(allXmChild);
		ViewXmChild viewXmChild = viewXmChildList.get(0);
		//查询所有批示
		List<Instructions> instructionsByXmNumber =new ArrayList<Instructions>();
		if(thisUserId==null){
			 instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(thisInfoName);
		}else{
			 instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(thisUserId);
		}
		
	    List<ViewPsInfoModel> viePsInfoModelList=new ArrayList<ViewPsInfoModel>();
		for (int i = 0; i < instructionsByXmNumber.size(); i++) {
			Instructions instructions = instructionsByXmNumber.get(i);
			ViewPsInfoModel viewPsInfoModel=new ViewPsInfoModel();
			viewPsInfoModel.setInstructionsContent(instructions.getInstructionsContent());
			viewPsInfoModel.setInstructionsPerson(instructions.getInstructionsPerson());
			viewPsInfoModel.setInstructionsTime(instructions.getInstructionsTime().toString());
			viewPsInfoModel.setXmNumber(instructions.getXmNumber());
			viePsInfoModelList.add(viewPsInfoModel);
		}
		viewXmChild.setListPs(viePsInfoModelList);
		List<ViewXmChild> viewXmChildList2=new ArrayList<ViewXmChild>();
		viewXmChildList2.add(viewXmChild);
		return viewXmChildList2;
	}
	/**
	 * 安卓根据用户名查询该名下所有子项目任务
	 * @param thisUserId
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	@Override
		public List<ViewXmChild> allViewXmChildByUser(String thisUserId) {
		List<XmChild> allXmChild = xmChildService.allViewXmChildByUser(thisUserId);
		List<ViewXmChild> viewXmChildList=changeXmChild(allXmChild);
		return viewXmChildList;
	}

	@Override
	public List<ViewXmChild> findMyIssueByUnit(String thisUserId) {
		List<ViewXmChild> viewXmChildList=new ArrayList();
		try{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		//查询所有我的下发
		List<XmChild> xmChildResult = xmChildDao.findMyIssueByUnit(thisUserId);
		for(int i=0; i<xmChildResult.size();i++){
			XmChild xmChildResults=xmChildResult.get(i);
			ViewXmChild viewXmChild=new ViewXmChild();
			//查询相应相关人员信息
			List<XmChild> xmChildList = xmChildDao.findByLikeXmNumber(xmChildResult.get(i).getXmNumber(), xmChildResult.get(i).getUnitId());
			List<User> listUser=new ArrayList();
			for(int j=0;j<xmChildList.size();j++){
				XmChild xmChild = xmChildList.get(j);
				//查询该人信息
				User findByJobNumber = userService.findByJobNumber(	xmChild.getUserName());
				//放入list
				
				listUser.add(findByJobNumber);
				viewXmChild.setXm_name(xmChildResults.getXmName());
			}
			viewXmChild.setListUser(listUser);
			viewXmChild.setAcceptstatus(xmChildResults.getAcceptstatus());
			viewXmChild.setDead_line(df.format(xmChildResults.getDeadLine()));
			viewXmChild.setFile_count(xmChildResults.getFileCount());
			viewXmChild.setFile_name(xmChildResults.getFileName());
			viewXmChild.setId(xmChildResults.getId());
			viewXmChild.setIssue(xmChildResults.getIssue());
			viewXmChild.setPublish_time(df.format(xmChildResults.getPublishTime()));
			viewXmChild.setRequires(xmChildResults.getRequires());
			viewXmChild.setStatus(xmChildResults.getStatus());
			viewXmChild.setXm_child_number(xmChildResults.getXmNumber());
			//viewXmChild.setXm_number(xmChild.getXmNumber());
			//viewXmChild.setMaterials_id(xmChildIssue.getMaterialsId());
			viewXmChild.setUnit_id(xmChildResults.getUnitId());
			//viewXmChild.setUnit_parent(xmChildIssue.getUnitParent());
			User findByJobNumber = userService.findByJobNumber(thisUserId);
			viewXmChild.setUser(findByJobNumber);
			viewXmChildList.add(viewXmChild);
		}
		}catch (Exception e) {
            e.printStackTrace();		
       }
		return viewXmChildList;
		
		
		/*SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //根据条件筛选获取子项目列表
		List<XmChild> xmChildResult = xmChildDao.findMyIssueByUnit(thisUserId);
		List<XmChild> xmChildIssueResult=new ArrayList<XmChild>();
		List<ViewXmChild> viewXmChildList=new ArrayList<ViewXmChild>();
		//遍历子项目列表获取其子项目信息
		for(int i = 0; i < xmChildResult.size(); i++){
			List<XmChild> xmChildList = new ArrayList<XmChild>();
			//获取子项目下发的项目列表
			xmChildList = xmChildDao.getChildListByXmUser(xmChildResult.get(i).getXmNumber(), xmChildResult.get(i).getUnitId());
			for (int j = 0; j < xmChildList.size(); j++) {
				xmChildIssueResult.add(xmChildList.get(j));
				
					XmChild xmChild = xmChildResult.get(i);
					XmChild xmChildIssue = xmChildList.get(j);
					ViewXmChild view=new ViewXmChild();
					view.setXm_name(xmChildIssue.getXmChildName());
					view.setAcceptstatus(xmChild.getAcceptstatus());
					view.setDead_line(df.format(xmChild.getDeadLine()));
					view.setFile_count(xmChild.getFileCount());
					view.setFile_name(xmChild.getFileName());
					view.setId(xmChild.getId());
					view.setIssue(xmChild.getIssue());
					view.setPublish_time(df.format(xmChild.getPublishTime()));
					view.setRequires(xmChild.getRequires());
					view.setStatus(xmChild.getStatus());
					view.setXm_child_number(xmChild.getXmNumber());
					//view.setXm_number(xmChild.getXmNumber());
					//view.setMaterials_id(xmChildIssue.getMaterialsId());
					view.setUnit_id(xmChild.getUnitId());
					view.setUnit_parent(xmChildIssue.getUnitParent());
					//模糊查询与该项目相关的人员
					List<XmChild> findByLikeXmNumber = xmChildService.findByLikeXmNumber(xmChild.getXmNumber(),xmChild.getUnitId());
					List<User> userList=new ArrayList<User>();
					for (int k = 0; k < findByLikeXmNumber.size(); k++) {
						XmChild xmChild2 = findByLikeXmNumber.get(k);
						//查询改用户
						User findByJobNumber = userService.findByJobNumber(xmChild2.getUserName());
						userList.add(findByJobNumber);
					}
					//查询发布人信息
					User user = userService.findByJobNumber(xmChild.getUserName());
					view.setListUser(userList);
					view.setUser(user);
					view.setUser_name(xmChildIssue.getUserName());
					//view.setXm_child_name(xmChildIssue.getXmChildName());
					//view.setXm_child_number(xmChildIssue.getXmChildNumber());
					//List<XmChild> xmChildList = xmChild.getXmChildList();
					List<ViewXmChild> projectChildListModel=new ArrayList<ViewXmChild>();
					for (int z=0;z<xmChildList.size();z++){
						XmChild xmChild1 = xmChildList.get(z);
						ViewXmChild viewDemo=new ViewXmChild();
						viewDemo.setAcceptstatus(xmChild1.getAcceptstatus());
						//viewDemo.setDead_line(df.format(xmChild1.getDeadLine()));
						//viewDemo.setFile_count(xmChild1.getFileCount());
						//viewDemo.setFile_name(xmChild1.getFileName());
						viewDemo.setId(xmChild1.getId());
						viewDemo.setIssue(xmChild1.getIssue());
						//viewDemo.setPublish_time(df.format(xmChild1.getPublishTime()));
						//viewDemo.setRequires(xmChild1.getRequires());
						viewDemo.setStatus(xmChild1.getStatus());
						viewDemo.setXm_number(xmChild1.getXmNumber());
						viewDemo.setMaterials_id(xmChild.getMaterialsId());
						viewDemo.setUnit_id(xmChild1.getUnitId());
						viewDemo.setUnit_parent(xmChild1.getUnitParent());
						//User user = userService.getByUserName(xmChild1.getUserName());
						viewDemo.setUser_name(user.getHelpName());
						viewDemo.setXm_child_name(xmChild1.getXmChildName());
						viewDemo.setXm_child_number(xmChild1.getXmChildNumber());
						projectChildListModel.add(viewDemo);
					}
					//view.setProjectChildListModel(projectChildListModel);
					viewXmChildList.add(view);
				
			}
			
		}
	
		
		return viewXmChildList;
*/
	}

	@Override
	public List<ViewXm> findMyIssueByCollager(String thisUserId) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<ViewXm> rsView = new ArrayList<ViewXm>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			sqlBuff.append("SELECT * FROM xm Where username = '"+thisUserId+"'  ORDER BY publish_time DESC");
			System.out.println("学院任务信息列表="+sqlBuff.toString());
			List list=xmInfoDao.query(sqlBuff.toString());

			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				String xm_number = (String)oc[3];
				String   publish_time = df.format((Timestamp)oc[4]);
				String  dead_line = df.format((Timestamp)oc[5]);
				String receiver = (String)oc[6];
				String type = (String)oc[8];
				String receiver_unit=(String)oc[7];
				Integer file_count = (Integer)oc[9];
				String file_name = (String)oc[10];
				Integer status = (Integer)oc[11];
				String requires = (String)oc[12];
				List<Unit> unitlist = new ArrayList();
				List<User> userlist = new ArrayList(); 
				if(receiver != null){
					String [] userList =  receiver.split(",");
					
					
					for (int i = 0; i < userList.length; i++) {
						User user = userService.findByJobNumber(userList[i]);
						userlist.add(user);
					}
				}
				if(receiver_unit != null){
					String [] unitList =  receiver_unit.split(",");
					for (int i = 0; i < unitList.length; i++) {
						Unit user = unitService.findById(unitList[i]);
						unitlist.add(user);
					}
				}

				ViewXm viewXm = new ViewXm();
				viewXm.setId(id);
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setReceiver(receiver);
				viewXm.setType(type);
				viewXm.setReceiver_unit(receiver_unit);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);
				viewXm.setListUser(userlist);
				viewXm.setListUnit(unitlist);
				rsView.add(viewXm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}

	/**
	 * 我的下发-查看详细项目（院领导）
	 * @author 叶城廷
	 * @version 2019.03.15
	 * @return
	 */
	@Override
	public List<ViewXm> findMyIssueByCollagerByNum(String thisUserId) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<ViewXm> rsView = new ArrayList<ViewXm>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			sqlBuff.append("SELECT * FROM xm Where xmNumber = '"+thisUserId+"'  ORDER BY publish_time DESC");
			System.out.println("学院任务信息列表="+sqlBuff.toString());
			List list=xmInfoDao.query(sqlBuff.toString());

			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				Integer id = (Integer)oc[0];
				String name = (String)oc[1];
				String username = (String)oc[2];
				String xm_number = (String)oc[3];
				String   publish_time = df.format((Timestamp)oc[4]);
				String  dead_line = df.format((Timestamp)oc[5]);
				String receiver = (String)oc[6];
				String type = (String)oc[8];
				String receiver_unit=(String)oc[7];
				Integer file_count = (Integer)oc[9];
				String file_name = (String)oc[10];
				Integer status = (Integer)oc[11];
				String requires = (String)oc[12];
				ViewXm viewXm = new ViewXm();
				viewXm.setId(id);
				viewXm.setName(name);
				viewXm.setUsername(username);
				viewXm.setXm_number(xm_number);
				viewXm.setPublish_time(publish_time);
				viewXm.setDead_line(dead_line);
				viewXm.setReceiver(receiver);
				viewXm.setType(type);
				viewXm.setReceiver_unit(receiver_unit);
				viewXm.setFile_count(file_count);
				viewXm.setFile_name(file_name);
				viewXm.setStatus(status);
				viewXm.setRequires(requires);
				//查询所有批示
				List<Instructions> instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(thisUserId);
			    List<ViewPsInfoModel> viePsInfoModelList=new ArrayList<ViewPsInfoModel>();
				for (int k = 0; k < instructionsByXmNumber.size(); k++) {
					Instructions instructions = instructionsByXmNumber.get(k);
					ViewPsInfoModel viewPsInfoModel=new ViewPsInfoModel();
					viewPsInfoModel.setInstructionsContent(instructions.getInstructionsContent());
					viewPsInfoModel.setInstructionsPerson(instructions.getInstructionsPerson());
					viewPsInfoModel.setInstructionsTime(instructions.getInstructionsTime().toString());
					viewPsInfoModel.setXmNumber(instructions.getXmNumber());
					viePsInfoModelList.add(viewPsInfoModel);
				}
				viewXm.setListPs(viePsInfoModelList);
				rsView.add(viewXm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}

	/**
	 * 我的下发-我的下发首页查询所有我下发的项目（教研室领导）
	 * @author 叶城廷
	 * @version 2019.03.15
	 * @return
	 */
	@Override
	public List<ViewXmChild> findMyIssueByUnitByNum(String thisUserId,String thisUserName, String thisPsNumber) {
		List<ViewXmChild> viewXmChildList=new ArrayList();
		try{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		//查询所有我的下发
		List<XmChild> xmChildResult = xmChildDao.findMyIssueByXmNumber2(thisUserId,thisUserName);
		for(int i=0; i<xmChildResult.size();i++){
			XmChild xmChildResults=xmChildResult.get(i);
			ViewXmChild viewXmChild=new ViewXmChild();
			//查询相应相关人员信息
			List<XmChild> xmChildList = xmChildDao.findByLikeXmNumber(xmChildResult.get(i).getXmNumber(), xmChildResult.get(i).getUnitId());
			List<User> listUser=new ArrayList();
			for(int j=0;j<xmChildList.size();j++){
				XmChild xmChild = xmChildList.get(j);
				//查询该人信息
				User findByJobNumber = userService.findByJobNumber(	xmChild.getUserName());
				//放入list
				listUser.add(findByJobNumber);
				viewXmChild.setXm_name(xmChild.getXmChildName());
			}
			viewXmChild.setListUser(listUser);
			viewXmChild.setAcceptstatus(xmChildResults.getAcceptstatus());
			viewXmChild.setDead_line(df.format(xmChildResults.getDeadLine()));
			viewXmChild.setFile_count(xmChildResults.getFileCount());
			viewXmChild.setFile_name(xmChildResults.getFileName());
			viewXmChild.setId(xmChildResults.getId());
			viewXmChild.setIssue(xmChildResults.getIssue());
			viewXmChild.setPublish_time(df.format(xmChildResults.getPublishTime()));
			viewXmChild.setRequires(xmChildResults.getRequires());
			viewXmChild.setStatus(xmChildResults.getStatus());
			viewXmChild.setXm_child_number(xmChildResults.getXmNumber());
			//viewXmChild.setXm_number(xmChild.getXmNumber());
			//viewXmChild.setMaterials_id(xmChildIssue.getMaterialsId());
			viewXmChild.setUnit_id(xmChildResults.getUnitId());
			//viewXmChild.setUnit_parent(xmChildIssue.getUnitParent());
			User findByJobNumber = userService.findByJobNumber(thisUserName);
			viewXmChild.setUser(findByJobNumber);
			//查询所有批示
			List<Instructions> instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(thisPsNumber);
		    List<ViewPsInfoModel> viePsInfoModelList=new ArrayList<ViewPsInfoModel>();
			for (int k = 0; k < instructionsByXmNumber.size(); k++) {
				Instructions instructions = instructionsByXmNumber.get(k);
				ViewPsInfoModel viewPsInfoModel=new ViewPsInfoModel();
				viewPsInfoModel.setInstructionsContent(instructions.getInstructionsContent());
				viewPsInfoModel.setInstructionsPerson(instructions.getInstructionsPerson());
				viewPsInfoModel.setInstructionsTime(instructions.getInstructionsTime().toString());
				viewPsInfoModel.setXmNumber(instructions.getXmNumber());
				viePsInfoModelList.add(viewPsInfoModel);
			}
			viewXmChild.setListPs(viePsInfoModelList);
			viewXmChildList.add(viewXmChild);
		}
		}catch (Exception e) {
            e.printStackTrace();		
       }
		return viewXmChildList;
		
		/*SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		//根据条件筛选获取子项目列表
		List<XmChild> xmChildResult = xmChildDao.findMyIssueByXmNumber(thisUserId);

		//遍历子项目列表获取其子项目信息
		for(int i = 0; i < xmChildResult.size(); i++){
			List<XmChild> xmChildList = new ArrayList<XmChild>();
			//获取子项目下发的项目列表
			xmChildList = xmChildDao.getChildListByXmUser(xmChildResult.get(i).getXmNumber(), xmChildResult.get(i).getUnitId());
			xmChildResult.get(i).setXmChildList(xmChildList);
		}
		List<ViewXmChild> viewXmChildList=new ArrayList<ViewXmChild>();
		for (int j = 0; j < xmChildResult.size(); j++){
			XmChild xmChild = xmChildResult.get(j);
			ViewXmChild view=new ViewXmChild();
			view.setXm_name(xmChild.getXmName());
			view.setAcceptstatus(xmChild.getAcceptstatus());
			view.setDead_line(df.format(xmChild.getDeadLine()));
			view.setFile_count(xmChild.getFileCount());
			view.setFile_name(xmChild.getFileName());
			view.setId(xmChild.getId());
			view.setIssue(xmChild.getIssue());
			view.setPublish_time(df.format(xmChild.getPublishTime()));
			view.setRequires(xmChild.getRequires());
			view.setStatus(xmChild.getStatus());
			view.setXm_number(xmChild.getXmNumber());
			view.setMaterials_id(xmChild.getMaterialsId());
			view.setUnit_id(xmChild.getUnitId());
			view.setUnit_parent(xmChild.getUnitParent());
			view.setUser_name(xmChild.getUserName());
			view.setXm_child_name(xmChild.getXmChildName());
			view.setXm_child_number(xmChild.getXmChildNumber());
			List<XmChild> xmChildList = xmChild.getXmChildList();
			List<ViewXmChild> projectChildListModel=new ArrayList<ViewXmChild>();
			for (int z=0;z<xmChildList.size();z++){
				XmChild xmChild1 = xmChildList.get(z);
				ViewXmChild viewDemo=new ViewXmChild();
				viewDemo.setAcceptstatus(xmChild1.getAcceptstatus());
				viewDemo.setDead_line(df.format(xmChild1.getDeadLine()));
				viewDemo.setFile_count(xmChild1.getFileCount());
				viewDemo.setFile_name(xmChild1.getFileName());
				viewDemo.setId(xmChild1.getId());
				viewDemo.setIssue(xmChild1.getIssue());
				viewDemo.setPublish_time(df.format(xmChild1.getPublishTime()));
				viewDemo.setRequires(xmChild1.getRequires());
				viewDemo.setStatus(xmChild1.getStatus());
				viewDemo.setXm_number(xmChild1.getXmNumber());
				viewDemo.setMaterials_id(xmChild.getMaterialsId());
				viewDemo.setUnit_id(xmChild1.getUnitId());
				viewDemo.setUnit_parent(xmChild1.getUnitParent());
				viewDemo.setUser_name(xmChild1.getUserName());
				viewDemo.setXm_child_name(xmChild1.getXmChildName());
				viewDemo.setXm_child_number(xmChild1.getXmChildNumber());
				projectChildListModel.add(viewDemo);
			}
			view.setProjectChildListModel(projectChildListModel);
			viewXmChildList.add(view);
		}
		return viewXmChildList;
*/
	}

	/**
	 * 封装XmchildList转ViewXmChild
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param XmChildList
	 * @return
	 */
	public List<ViewXmChild> changeXmChild(List<XmChild> XmChildList){
	List<ViewXmChild> viewXmChildList = new ArrayList<ViewXmChild>();
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	for (int i=0;i<XmChildList.size();i++){
		XmChild xmChild = XmChildList.get(i);
		ViewXmChild view =new ViewXmChild();
		view.setAcceptstatus(xmChild.getAcceptstatus());
		view.setId(xmChild.getId());
		view.setIssue(xmChild.getIssue());
		view.setStatus(xmChild.getStatus());
		view.setXm_number(xmChild.getXmNumber());
		view.setMaterials_id(xmChild.getMaterialsId());
		view.setUnit_id(xmChild.getUnitId());
		view.setUnit_parent(xmChild.getUnitParent());
		view.setUser_name(xmChild.getUserName());
		if(view.getFinish_time() != null){
			view.setFinish_time(df.format(xmChild.getFinishTime()));
		}
		
		if(xmChild.getUnitId() != null){
			Unit unit = unitService.getByUserId(xmChild.getUserName());
			view.setUnit(unit);
		}
		
		User user = userService.getByUserName(xmChild.getUserName());
		view.setUser(user);
		view.setXm_child_name(xmChild.getXmChildName());
		view.setXm_child_number(xmChild.getXmChildNumber());
		//教研室下发的项目
		if(xmChild.getAcceptstatus()==0){
			view.setDead_line(df.format(xmChild.getXmChild().getDeadLine()));
			view.setPublish_time(df.format(xmChild.getXmChild().getPublishTime()));
			if(xmChild.getFinishTime() != null){
				view.setFinish_time(df.format(xmChild.getFinishTime()));
			}
			view.setFile_count(xmChild.getXmChild().getFileCount());
			view.setFile_name(xmChild.getXmChild().getFileName());
			view.setRequires(xmChild.getXmChild().getRequires());
			ViewXmChild viewXmChild=new ViewXmChild();
			//viewXmChild.setPublish_time(xmChild.getXmChild().getPublishTime());
			viewXmChild.setAcceptstatus(xmChild.getXmChild().getAcceptstatus());
			viewXmChild.setDead_line(df.format(xmChild.getXmChild().getDeadLine()));
			viewXmChild.setFile_count(xmChild.getXmChild().getFileCount());
			viewXmChild.setFile_name(xmChild.getXmChild().getFileName());
			viewXmChild.setId(xmChild.getXmChild().getId());
			viewXmChild.setIssue(xmChild.getXmChild().getIssue());
			viewXmChild.setPublish_time(df.format(xmChild.getXmChild().getPublishTime()));
			viewXmChild.setRequires(xmChild.getXmChild().getRequires());  
			viewXmChild.setStatus(xmChild.getXmChild().getStatus());
			viewXmChild.setXm_number(xmChild.getXmChild().getXmNumber());
			viewXmChild.setMaterials_id(xmChild.getXmChild().getMaterialsId());
			viewXmChild.setUnit_id(xmChild.getXmChild().getUnitId());
			viewXmChild.setUnit_parent(xmChild.getXmChild().getUnitParent());
			viewXmChild.setUser_name(xmChild.getXmChild().getUserName());
			viewXmChild.setXm_child_name(xmChild.getXmChild().getXmChildName());
			viewXmChild.setXm_child_number(xmChild.getXmChild().getXmChildNumber());
			
			
			view.setProjectChildModel(viewXmChild);
		}else{
			//院级下发的直属任务
			
			view.setDead_line(df.format(xmChild.getXm().getDeadLine()));
			view.setPublish_time(df.format(xmChild.getXm().getPublishTime()));
			if(xmChild.getFinishTime() != null){
				view.setFinish_time(df.format(xmChild.getFinishTime()));
			}
			view.setFile_count(xmChild.getXm().getFileCount());
			view.setFile_name(xmChild.getXm().getFileName());
			view.setRequires(xmChild.getXm().getRequires());
			ViewXm viewXm=new ViewXm();
			viewXm.setName(xmChild.getXm().getName());
			viewXm.setReceiver_unit(xmChild.getXm().getReceiverUnit());
			viewXm.setDead_line(df.format(xmChild.getXm().getDeadLine()));
			viewXm.setFile_count(xmChild.getXm().getFileCount());
			viewXm.setFile_name(xmChild.getXm().getFileName());
			viewXm.setId(xmChild.getXm().getId());
			viewXm.setRequires(xmChild.getXm().getRequires());
			viewXm.setType(xmChild.getXm().getType());
			viewXm.setStatus(xmChild.getXm().getStatus());
			viewXm.setName(xmChild.getXm().getName());
			viewXm.setPublish_time(df.format(xmChild.getXm().getPublishTime()));
			viewXm.setUsername(xmChild.getXm().getUsername());
			User user1 = userService.getByUserName(xmChild.getXm().getUsername());
			viewXm.setUser(user1);
			viewXm.setXm_number(xmChild.getXm().getXmNumber());
//			//查询所有批示
//			List<Instructions> instructionsByXmNumber = instructionsService.getInstructionsByXmNumber(xmChild.getXm().getXmNumber());
//		    List<ViewPsInfoModel> viePsInfoModelList=new ArrayList<ViewPsInfoModel>();
//			for (int i1 = 0; i1 < instructionsByXmNumber.size(); i1++) {
//				Instructions instructions = instructionsByXmNumber.get(i1);
//				ViewPsInfoModel viewPsInfoModel=new ViewPsInfoModel();
//				viewPsInfoModel.setInstructionsContent(instructions.getInstructionsContent());
//				viewPsInfoModel.setInstructionsPerson(instructions.getInstructionsPerson());
//				viewPsInfoModel.setInstructionsTime(instructions.getInstructionsTime().toString());
//				viewPsInfoModel.setXmNumber(instructions.getXmNumber());
//				viePsInfoModelList.add(viewPsInfoModel);
//			}
//			viewXm.setListPs(viePsInfoModelList);
			viewXm.setReceiver(xmChild.getXm().getReceiver());
			view.setProjectModel(viewXm);
		}
		viewXmChildList.add(view);
	}
	return viewXmChildList;
   }
}
