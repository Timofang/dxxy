package com.gxwzu.business.service.project.impl;

import com.gxwzu.business.dao.project.UnitDao;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.service.project.UnitService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("unitService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public  class UnitServiceImpl extends BaseServiceImpl<Unit> implements UnitService {

	private Unit Unit;
	private UnitDao UnitDao;

	public Unit getUnit() {
		return Unit;
	}
	public void setUnit(Unit Unit) {
		this.Unit = Unit;
	}

	@Autowired
	public void setUnitDao(UnitDao UnitDao) {
		this.UnitDao = UnitDao;
	}

	@Override
	public BaseDao<Unit> getDao() {
		return this.UnitDao;
	}

	@Override
	public List<Unit> findAll() {
		return UnitDao.getAll(Unit.class);
	}

	@Override
	public Result<Unit> find(Unit model, int page, int size) {
		return UnitDao.find(model,page,size);
	}

	/**
	 * @author soldier
	 * @date 19-3-16下午10:53
	 * @description: 1、用于用户输入信息查询
	 * 				  2、用于Unit_add.jsp页面【js检验教研室信息】
	 */
	@Override
	public List<Unit> findByExample(Unit model) {
		List params = new ArrayList<Object>();
		String queryString = "from Unit model where 1=1";
		if(model.getUnitId()!=null){
			queryString =queryString+" and model.unitId like'%"+model.getUnitId()+"%'";
			params.add(model.getUnitId());
		}
		if(model.getUnitName()!=null){
			queryString =queryString+" and model.unitName like'%"+model.getUnitName()+"%'";
            params.add(model.getUnitName());
		}
		if(model.getUnitPerson()!=null){
			queryString =queryString+" and model.unitPerson=?";
			params.add(model.getUnitPerson());
		}
		return UnitDao.findByHql(queryString, params.toArray());
	}

	@Override
	public Unit add(Unit Unit) {
		try {
			//获取当前最大教研室编号
			int[] unitIdList = UnitDao.getMaxUnitId();
			Arrays.sort(unitIdList);
			int maxId = unitIdList[unitIdList.length - 1];
			String maxUnitId = "";
			int a = maxId+1;
		    if(String.valueOf(maxId).length() == 1){
		    	maxUnitId = "00" + String.valueOf(a);
		    }else if(String.valueOf(maxId).length() == 2){
		    	if(a == 100){
		    		maxUnitId = String.valueOf(a);
		    	}else{
		    		maxUnitId = "0" + String.valueOf(a);
		    	}
		    }else if(String.valueOf(maxId).length() == 3){
		    	maxUnitId = String.valueOf(a);
		    }else if(String.valueOf(maxId).length() == 4){
		    	maxUnitId = String.valueOf(a);
		    }else if(String.valueOf(maxId).length() == 5){
		    	maxUnitId = String.valueOf(a);
		    }
		    Unit.setUnitId(maxUnitId);
			UnitDao.save(Unit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Unit;
	}

	/**
	 * 用于查用户的多个教研室，因为员工有多个教研室
	 * @param findId
	 * @return
	 */
	@Override
	public List<Unit> findAllById(String findId) {

		List<Unit> unitList = new ArrayList<Unit>();

		if (findId!=null || findId.isEmpty()) {
			//把用户所有教研室Id取出，数据库自动添加“,”
			String thisUnitId[] = findId.split(",");
			List<String> UnitIdList = new ArrayList();
			for (int i = 0; i < thisUnitId.length; i++) {
				UnitIdList.add(thisUnitId[i]);
			}

			try {
				for (int i = 0; i < UnitIdList.size(); i++) {
					String queryString = "from Unit model where unitId = " + findId;
					List<Unit> unitListss = UnitDao.find(queryString);
					unitList.add(unitListss.get(0));
				}
			} catch (RuntimeException re) {
				log.error("get failed", re);
				throw re;
			}
		}
		return unitList;
	}

	@Override
	public Unit findById(String unitId) {

		return UnitDao.findById(unitId);
	}

	@Override
	public void del(String delId) {
		UnitDao.remove(UnitDao.findById(delId));
	}

	@Override
	public List updateUnitInfo(Unit model) {
		List<Object> list = new ArrayList<Object>();
		Unit Unit = new Unit();
		try {
			//获取原Unit数据
			Unit thisUnit = UnitDao.findById(model.getUnitId());
			if(null != thisUnit){
				thisUnit.setUnitName(model.getUnitName());
				thisUnit.setUnitPerson(model.getUnitPerson());
				UnitDao.update(thisUnit);

				Unit = UnitDao.findById(model.getUnitId());
				list.add("1");
				list.add(Unit);
				return list;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public Unit getByUserId(String userName) {
		return UnitDao.getByUserId(userName);
	}

}
