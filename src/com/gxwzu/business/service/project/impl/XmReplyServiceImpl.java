package com.gxwzu.business.service.project.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmReplyDao;
import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.service.project.XmReplyService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 回复ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmDutyServiceImpl
 * <br>Date: 2017-9-9下午03:40:33
 * <br>log:
 */
@Service("xmReplyService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmReplyServiceImpl extends BaseServiceImpl<XmReply> implements XmReplyService{
	
	private XmReplyDao xmReplyDao;

	@Autowired
	public void setXmReplyDao(XmReplyDao xmReplyDao) {
		this.xmReplyDao = xmReplyDao;
	}

	@Override
	public BaseDao<XmReply> getDao() {
		return this.xmReplyDao;
	}
	
	@Override
	public List<XmReply> findAll() {
		return xmReplyDao.getAll(XmReply.class);
	}

	@Override
	public Result<XmReply> find(XmReply model, int page, int size) {
		return xmReplyDao.find(model, page, size);
	}

	@Override
	public List<XmReply> findByExample(XmReply model) {
		return xmReplyDao.findByExample(model);
	}
	
	@Override
	public XmReply findById(Integer findId) {
		return xmReplyDao.findById(findId);
	}

}
