package com.gxwzu.business.service.project.impl;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.AttachmentDao;
import com.gxwzu.business.model.Attachment;
import com.gxwzu.business.service.project.AttachmentService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 附件业务层实现类
 * @Author SunYi
 * @date 2019年3月22日下午10:07:04
 */
@Service("attachmentService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class AttachmentServiceImpl extends BaseServiceImpl<Attachment> implements AttachmentService{
	@Autowired
	private AttachmentDao attachmentDao;

	public void setAttachmentDao(AttachmentDao attachmentDao) {
		this.attachmentDao = attachmentDao;
	}

	@Override
	public BaseDao<Attachment> getDao() {
		return this.attachmentDao;
	}

	private Attachment attachment;
	
	
	
	/* **********************************************方法体**********************************************  */
	
	/**
	 * 保存用户上传的附件
	 * @author SunYi
	 * @Date 2019年3月14日下午10:25:34
	 */
	@Override
	public boolean saveAttachment(File[] myfile, String[] myfileFileName, String xmNumber) {
		boolean flag=false;
		if(myfile != null && myfileFileName != null) {
		String fileName =null;
		// 1.拿到ServletContext
	    ServletContext servletContext = ServletActionContext.getServletContext();
	    // 2.调用realPath方法，获取根据一个虚拟目录得到的真实目录
	    String realPath = servletContext.getRealPath("/Attachment/" + xmNumber);
		realPath = realPath.replace("\\", "/");
		// 3.如果这个真实的目录不存在，需要创建
		File file = new File(realPath);
		    if (!file.exists()) {
				file.mkdirs();
			}
		 //遍历存储文件和记录数据
		 for (int i = 0; i < myfileFileName.length; i++) {
			 //重置指标
			 flag=false;
			 //取出各文件名
			 fileName = myfileFileName[i];
			 //保存文件并重命名
			try {
				FileUtils.copyFile(myfile[i], new File(file, fileName));
//				myfile[i].renameTo(new File(file, fileName));
				attachment = new Attachment();
				attachment.setPath(realPath+"/"+fileName);
				attachment.setFileName(fileName);
				attachment.setXmNumber(xmNumber);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			try {
				//持久化数据库attachment表数据
				attachmentDao.save(attachment);
				flag=true;
			} catch (Exception e) {
				e.printStackTrace();
				flag=false;
			}
		}
		 //当所有文件保存完毕时
			if(flag) {
				//更改当前子项目数据完成状态
				  //重置标志
				  flag=false;
				  try {
					  flag=true;
				} catch (Exception e) {
					e.printStackTrace();
					flag=false;
				}
			}
			
		}
		return flag;
	}
	
	/**
	 * 查询指定项目编号的附件信息
	 * @author SunYi
	 * @Date 2019年3月28日下午5:54:08
	 */
	@Override
	public List<Attachment> getAttachmentByXmNuber(String xmNumber) {
		return attachmentDao.getAttachmentByXmNuber(xmNumber);
	}
	

	
	

	
	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	
}
