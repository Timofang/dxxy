package com.gxwzu.business.service.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmCapitalDao;
import com.gxwzu.business.model.XmCapital;
import com.gxwzu.business.service.project.XmCapitalService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.sysVO.ViewXmCapital;

/**
 * 资金信息ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmCapitalServiceImpl
 * <br>Date: 2017-8-17下午06:21:31
 * <br>log:
 */
@Service("xmCapitalService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmCapitalServiceImpl extends BaseServiceImpl<XmCapital> implements XmCapitalService  {
	
	
	private XmCapitalDao xmCapitalDao;

	@Autowired
	public void setXmCapitalDao(XmCapitalDao xmCapitalDao) {
		this.xmCapitalDao = xmCapitalDao;
	}

	@Override
	public BaseDao<XmCapital> getDao() {
		return this.xmCapitalDao;
	}
	
	@Override
	public List<XmCapital> findAll() {
		return xmCapitalDao.getAll(XmCapital.class);
	}

	@Override
	public Result<XmCapital> find(XmCapital model, int page, int size) {
		return xmCapitalDao.find(model, page, size);
	}

	@Override
	public List<XmCapital> findByExample(XmCapital model) {
		return xmCapitalDao.findByExample(model);
	}
	
	@Override
	public XmCapital findById(Integer findId) {
		return xmCapitalDao.findById(findId);
	}

	/**
	 * 资金信息，含资金进度汇总、完成率
	 * @return
	 */
	@Override
	public List<ViewXmCapital> findCapitalInRate(String thisXmNumber, String thisYear) {
		
		List<ViewXmCapital> rsView = new ArrayList<ViewXmCapital>();
		
		StringBuffer sqlBuff=new StringBuffer();
		
		sqlBuff.append(" SELECT *,FORMAT(IFNULL((xi.sInvestment/xc.investPlan)*100,'-'),2) AS sci FROM ");
		sqlBuff.append(" (SELECT * FROM xm_capital ");
		sqlBuff.append(" WHERE 1=1 ");
		if(thisXmNumber != null && !"".equals(thisXmNumber)){
			sqlBuff.append(" and xmNumber='"+thisXmNumber+"'");
		}
		sqlBuff.append(" ) AS xc ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT IFNULL(SUM(Investment),0) AS sInvestment,xmNumber AS nx FROM xm_invest ");
		sqlBuff.append(" WHERE 1=1 and investYear='"+thisYear+"'");
		sqlBuff.append(" GROUP BY xmNumber) AS xi ");
		sqlBuff.append(" ON xc.xmNumber=xi.nx ");
		
		System.out.println("资金信息，含资金进度汇总、完成率="+sqlBuff.toString());
		
		List list=xmCapitalDao.query(sqlBuff.toString());
		
		for(int j=0;j<list.size();j++){
			Object[] oc = (Object[])list.get(j);
			
			Integer id = (Integer)oc[0];
			String xmNumber = (String)oc[5];
			
			String investTotal = (String)oc[1];
			String investSource = (String)oc[2];
			String investPlan = (String)oc[3];
			String investComplete = (String)oc[4];
			
			String sInvestment = (Double)oc[6]+"";
			String cRate = (String)oc[8]+"";
			
			ViewXmCapital viewXmCapital = new ViewXmCapital();
			
			viewXmCapital.setId(id);
			viewXmCapital.setXmNumber(xmNumber);
			
			viewXmCapital.setInvestTotal(investTotal);
			viewXmCapital.setInvestSource(investSource);
			viewXmCapital.setInvestPlan(investPlan);
			viewXmCapital.setInvestComplete(investComplete);
			
			viewXmCapital.setsInvestment(sInvestment);
			viewXmCapital.setcRate(cRate);
			
			rsView.add(viewXmCapital);
		}
		return rsView;
	}
}
