package com.gxwzu.business.service.project.impl;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.gxwzu.business.dao.project.XmDutyDao;
import com.gxwzu.business.dao.project.XmReplyDao;
import com.gxwzu.business.dao.project.XmScheduleDao;
import com.gxwzu.business.dao.project.XmSchedulepicDao;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmScheduleService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.sysVO.ViewPoorXmSchedule;
import com.gxwzu.sysVO.ViewPsInfoModel;
import com.gxwzu.sysVO.ViewXm;
import com.gxwzu.sysVO.ViewXmInfo;
import com.gxwzu.sysVO.ViewXmSchedule;
import com.gxwzu.sysVO.ViewXmScheduleInInfo;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;
import com.gxwzu.system.service.user.impl.UserServiceImpl;

/**
 * 项目进度ServiceImpl实现类
 * 
 * @author liqing
 * @version 1.0 <br>
 *          Copyright (C), 2015, 梧州学院 软件研发中心 <br>
 *          This program is protected by copyright laws. <br>
 *          Program Name: XmScheduleServiceImpl <br>
 *          Date: 2017-8-17下午06:35:18 <br>
 *          log:
 */
@Service("xmScheduleService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = RuntimeException.class)
public class XmScheduleServiceImpl extends BaseServiceImpl<XmSchedule>
		implements XmScheduleService {

	private XmScheduleDao xmScheduleDao;
	private XmSchedulepicDao xmSchedulepicDao;
	private XmReplyDao xmReplyDao;
	private UserDao userDao;
	private XmDutyDao xmDutyDao;
	@Autowired
	private XmInfoService xmInfoService;
	
	@Autowired
	private UserService userService;

	@Autowired
	public void setXmDutyDao(XmDutyDao xmDutyDao) {
		this.xmDutyDao = xmDutyDao;
	}

	@Autowired
	public void setXmReplyDao(XmReplyDao xmReplyDao) {
		this.xmReplyDao = xmReplyDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Autowired
	public void setXmSchedulepicDao(XmSchedulepicDao xmSchedulepicDao) {
		this.xmSchedulepicDao = xmSchedulepicDao;
	}

	@Autowired
	public void setXmScheduleDao(XmScheduleDao xmScheduleDao) {
		this.xmScheduleDao = xmScheduleDao;
	}
	
	

	public XmInfoService getXmInfoService() {
		return xmInfoService;
	}

	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}

	@Override
	public BaseDao<XmSchedule> getDao() {
		return this.xmScheduleDao;
	}

	@Override
	public List<XmSchedule> findAll() {
		return xmScheduleDao.getAll(XmSchedule.class);
	}

	@Override
	public Result<XmSchedule> find(XmSchedule model, int page, int size) {
		return xmScheduleDao.find(model, page, size);
	}

	@Override
	public List<XmSchedule> findByExample(XmSchedule model) {
		return xmScheduleDao.findByExample(model);
	}

	@Override
	public XmSchedule findById(Integer findId) {
		return xmScheduleDao.findById(findId);
	}

	@Override
	public Result<ViewXmSchedule> findXmScheduleByNum(String thisXmNumber, String thisType, String thisLoginUserId, int page, int size) {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Result<ViewXmSchedule> resultViewXmSchedule = new Result<ViewXmSchedule>();
		try {
			XmSchedule findXmSchedule = new XmSchedule();
			findXmSchedule.setXmNumber(thisXmNumber);
			findXmSchedule.setType(thisType);
			Result<XmSchedule> resultXmSchedule = xmScheduleDao.find(findXmSchedule, page, size);
			for (int i = 0; i < resultXmSchedule.getData().size(); i++) {
				findXmSchedule = resultXmSchedule.getData().get(i);

				//查询工作进度/问题进度
				XmSchedulepic findXmSchedulepic = new XmSchedulepic();
				findXmSchedulepic.setScheduleId(findXmSchedule.getId());
				List<XmSchedulepic> listXmSchedulepic = xmSchedulepicDao.findByExample(findXmSchedulepic);
				
				//查询评论
				XmReply findXmReply = new XmReply();
				findXmReply.setScheduleId(findXmSchedule.getId());
				List<XmReply> listXmReply = xmReplyDao.findByExample(findXmReply);

				//工作进度/问题进度提交用户信息
				User thisUser = userDao.findById(findXmSchedule.getUserId());
				
				//当前登录用户，是否有评论权限
				String loginUserType = "";
				if(thisLoginUserId != null && !"".equals(thisLoginUserId)){
					User loginUser = userDao.findById(Integer.parseInt(thisLoginUserId));
					
					loginUserType = loginUser.getIsReply();
				}

				ViewXmSchedule thisViewXmSchedule = new ViewXmSchedule();
				thisViewXmSchedule.setId(findXmSchedule.getId());
				thisViewXmSchedule.setXmNumber(findXmSchedule.getXmNumber());
				thisViewXmSchedule.setXmInfoName(findXmSchedule.getXmInfoName());
				thisViewXmSchedule.setContent(findXmSchedule.getContent());
				thisViewXmSchedule.setType(findXmSchedule.getType());
				thisViewXmSchedule.setUserId(findXmSchedule.getUserId());
				thisViewXmSchedule.setUserName(findXmSchedule.getUserName());
				thisViewXmSchedule.setPortrait(thisUser.getPortrait());
				String dateStr = sdf.format(findXmSchedule.getCreatTime());
				thisViewXmSchedule.setCreatTime(dateStr);
				thisViewXmSchedule.setListXmSchedulepic(listXmSchedulepic);
				thisViewXmSchedule.setListXmReply(listXmReply);
				
				thisViewXmSchedule.setLoginUserType(loginUserType);

				resultViewXmSchedule.getData().add(thisViewXmSchedule);
			}
			resultViewXmSchedule.setOffset(resultXmSchedule.getOffset());
			resultViewXmSchedule.setPage(resultXmSchedule.getPage());
			resultViewXmSchedule.setSize(resultXmSchedule.getSize());
			resultViewXmSchedule.setTotal(resultXmSchedule.getTotal());
			resultViewXmSchedule.setTotalPage(resultXmSchedule.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultViewXmSchedule;
	}

	@Override
	public void del(String thisXmScheduleId) {
		try {
			XmSchedule delXmSchedule = xmScheduleDao.findById(Integer
					.parseInt(thisXmScheduleId));
			if (delXmSchedule != null) {

				XmSchedulepic findXmSchedulepic = new XmSchedulepic();
				findXmSchedulepic.setScheduleId(delXmSchedule.getId());
				List<XmSchedulepic> listXmSchedulepic = xmSchedulepicDao
						.findByExample(findXmSchedulepic);
				for (int i = 0; i < listXmSchedulepic.size(); i++) {
					findXmSchedulepic = listXmSchedulepic.get(i);

					xmSchedulepicDao.remove(findXmSchedulepic);
				}
				xmScheduleDao.remove(delXmSchedule);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Result<ViewXmSchedule> viewXmScheduleInInfo(int page, int size) {
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Result<ViewXmSchedule> rsView = new Result<ViewXmSchedule>();
		
		int start=(page-1)*size;
		int limit =size;
		
		StringBuffer sqlBuff=new StringBuffer();
		
		sqlBuff.append(" SELECT xs.*,(CASE when IFNULL(xr.scheduleId,0)=0 then '0' else '1' END) as ni,i.budgetType FROM ");
		sqlBuff.append(" (SELECT xs.*, us.portrait FROM  ");
		sqlBuff.append(" (SELECT * FROM xm_schedule ) xs ");
		sqlBuff.append(" LEFT JOIN  ");
		sqlBuff.append(" (SELECT * FROM user ) us  ");
		sqlBuff.append(" ON us.id = xs.userId) AS xs ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT scheduleId FROM xm_reply GROUP BY scheduleId) AS xr ");
		sqlBuff.append(" ON xr.scheduleId=xs.id ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT * FROM xm_info) AS i ");
		sqlBuff.append(" ON xs.xmNumber=i.xmNumber ");
		sqlBuff.append(" WHERE i.budgetType <> '07' ");
		sqlBuff.append(" ORDER BY creatTime DESC ");
		
		System.out.println("工作进度/问题列表（前十条），带项目基本信息="+sqlBuff.toString());
		
		Result rs=xmScheduleDao.findBySQL(sqlBuff.toString(), null, start, limit);
		List list=rs.getData();
		
		for(int j=0;j<list.size();j++){
			Object[] oc = (Object[])list.get(j);
			
			Integer id = (Integer)oc[0];
			String content = (String)oc[1];
			String type = (String)oc[2];
			Integer userId = (Integer)oc[3];
			String userName = (String)oc[4];
			Timestamp creatTime = (Timestamp)oc[5];
			String thisXmNumber = (String)oc[6];
			String xmInfoName = (String)oc[7];
			String portrait = (String)oc[8];
			String isReply = (String)oc[9];
			String budgetType = (String)oc[10];
			
			ViewXmSchedule viewXmSchedule = new ViewXmSchedule();
			
			viewXmSchedule.setId(id);
			viewXmSchedule.setContent(content);
			viewXmSchedule.setType(type);
			viewXmSchedule.setUserId(userId);
			viewXmSchedule.setUserName(userName);
			
			String dateStr = sdf.format(creatTime);
			viewXmSchedule.setCreatTime(dateStr);
			viewXmSchedule.setXmNumber(thisXmNumber);
			viewXmSchedule.setXmInfoName(xmInfoName);
			viewXmSchedule.setPortrait(portrait);
			viewXmSchedule.setIsReply(isReply);
			viewXmSchedule.setBudgetType(budgetType);
			
			rsView.getData().add(viewXmSchedule);
		}
		rsView.setOffset(rs.getOffset());
		rsView.setPage(rs.getPage());
		rsView.setSize(rs.getSize());
		rsView.setTotal(rs.getTotal());
		rsView.setTotalPage(rs.getTotalPage());
		
		return rsView;
	}
	
	@Override
	public Result<ViewPsInfoModel> viewPoorXmScheduleInInfo(int page, int size) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Result<ViewPsInfoModel> rsView = new Result<ViewPsInfoModel>();
		
		int start=(page-1)*size;
		int limit =size;
		
		StringBuffer sqlBuff=new StringBuffer();
		sqlBuff.append("SELECT * FROM instructions WHERE LENGTH(xmNumber)<12 ORDER BY instructions_time DESC");

		//sqlBuff.append(" SELECT xs.*,(CASE when IFNULL(xr.scheduleId,0)=0 then '0' else '1' END) as ni,i.budgetType,d.peopleId FROM ");
	/*	sqlBuff.append(" (SELECT xs.*, us.portrait FROM  ");
		sqlBuff.append(" (SELECT * FROM xm_schedule ) xs ");
		sqlBuff.append(" LEFT JOIN  ");
		sqlBuff.append(" (SELECT * FROM user ) us  ");
		sqlBuff.append(" ON us.id = xs.userId) AS xs ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT scheduleId FROM xm_reply GROUP BY scheduleId) AS xr ");
		sqlBuff.append(" ON xr.scheduleId=xs.id ");
		sqlBuff.append(" LEFT JOIN ");
		sqlBuff.append(" (SELECT * FROM xm_info) AS i ");
		sqlBuff.append(" ON xs.xmNumber=i.xmNumber ");
		sqlBuff.append(" JOIN (SELECT peopleId,xmNumber FROM xm_duty) AS d ON xs.xmNumber=d.xmNumber");
		sqlBuff.append(" WHERE i.budgetType <> '07' and peopleId=?");
		sqlBuff.append(" ORDER BY creatTime DESC ");*/
		
		System.out.println("工作进度/问题列表（前十条），带项目基本信息="+sqlBuff.toString());
		Result rs=xmScheduleDao.findBySQL(sqlBuff.toString());
		List list=rs.getData();
		System.out.println(list.size());
		String xmNumber=null;
		String instructionsPerson =null;
		String instructionsContent =null;
		String instructionsTime =null;
		for(int j = 0; j < list.size(); j++){
			Object[] oc = (Object[]) list.get(j);
			xmNumber = (String) oc[1];
			instructionsPerson = (String) oc[2];
			instructionsContent = (String) oc[3];
			instructionsTime = df.format( (Timestamp)oc[4]);
			
			ViewPsInfoModel viewPsInfoModel = new ViewPsInfoModel();
			User user = userService.getByUserName2(instructionsPerson);
			
			if(user.getLeaderTypeId() == 1){
				
				List<ViewXm> userViewXm = new ArrayList<ViewXm>();
				System.out.println(xmNumber);
				userViewXm = xmInfoService.findXmByNum(xmNumber);
				viewPsInfoModel.setListXm(userViewXm);
				
				viewPsInfoModel.setXmNumber(xmNumber);
				viewPsInfoModel.setInstructionsPerson(instructionsPerson);
				viewPsInfoModel.setInstructionsContent(instructionsContent);
				viewPsInfoModel.setInstructionsTime(instructionsTime);

				rsView.getData().add(viewPsInfoModel);
			}
		
		}
		Gson g = new Gson();
        String json = g.toJson(rsView);
        System.out.println(json);
		return rsView;
		
//		for(int j=0;j<list.size();j++){
//			Object[] oc = (Object[])list.get(j);
//			
//			Integer id = (Integer)oc[0];
//			String content = (String)oc[1];
//			String type = (String)oc[2];
//			Integer userId = (Integer)oc[3];
//			String userName = (String)oc[4];
//			Timestamp creatTime = (Timestamp)oc[5];
//			String thisXmNumber = (String)oc[6];
//			String xmInfoName = (String)oc[7];
//			String portrait = (String)oc[8];
//			String isReply = (String)oc[9];
//			String budgetType = (String)oc[10];
//			Integer people_Id = (Integer)oc[11];
//			
//			ViewPoorXmSchedule viewXmSchedule = new ViewPoorXmSchedule();
//			
//			viewXmSchedule.setId(id);
//			viewXmSchedule.setContent(content);
//			viewXmSchedule.setType(type);
//			viewXmSchedule.setUserId(userId);
//			viewXmSchedule.setUserName(userName);
//			
//			String dateStr = sdf.format(creatTime);
//			viewXmSchedule.setCreatTime(dateStr);
//			viewXmSchedule.setXmNumber(thisXmNumber);
//			viewXmSchedule.setXmInfoName(xmInfoName);
//			viewXmSchedule.setPortrait(portrait);
//			viewXmSchedule.setIsReply(isReply);
//			viewXmSchedule.setBudgetType(budgetType);
//			viewXmSchedule.setPeopleId(people_Id);
//			
//			rsView.getData().add(viewXmSchedule);
//		}
//		rsView.setOffset(rs.getOffset());
//		rsView.setPage(rs.getPage());
//		rsView.setSize(rs.getSize());
//		rsView.setTotal(rs.getTotal());
//		rsView.setTotalPage(rs.getTotalPage());
		
		
	}

	@Override
	public String addReply(XmReply xmReply) {
		String actionState = "";
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			xmReply.setCreateTime(Timestamp.valueOf(dateStr));
			
			if(xmReply.getReUserId() != null && !"".equals(xmReply.getReUserId())){
				
				XmReply thisXmReply = xmReplyDao.save(xmReply);
						
				actionState = thisXmReply.getId()+"";//批示成功,返回添加成功的对象Id
			}else{
				actionState = "0";//请登录，登录后方可批示
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return actionState;
	}
}
