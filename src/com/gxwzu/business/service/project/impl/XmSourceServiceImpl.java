package com.gxwzu.business.service.project.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmDutyDao;
import com.gxwzu.business.dao.project.XmInfoDao;
import com.gxwzu.business.dao.project.XmScheduleDao;
import com.gxwzu.business.dao.project.XmSourceDao;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.model.XmSource;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmSourceService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.sysVO.ViewAsk;
import com.gxwzu.sysVO.downViewAsk;
import com.gxwzu.sysVO.downViewXmInfo;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;

/**
 * 督查事项批示来源ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmSourceServiceImpl
 * <br>Date: 2017-11-28上午11:28:20
 * <br>log:
 */
@Service("xmSourceService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmSourceServiceImpl extends BaseServiceImpl<XmSource> implements XmSourceService{
	
	private XmSourceDao xmSourceDao;
	private XmInfoDao xmInfoDao;
	private XmInfoService xmInfoService;
	private UserDao userDao;
	private XmDutyDao xmDutyDao;
	private XmScheduleDao xmScheduleDao;

	@Autowired
	public void setXmScheduleDao(XmScheduleDao xmScheduleDao) {
		this.xmScheduleDao = xmScheduleDao;
	}

	@Autowired
	public void setXmDutyDao(XmDutyDao xmDutyDao) {
		this.xmDutyDao = xmDutyDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Autowired
	public void setXmInfoDao(XmInfoDao xmInfoDao) {
		this.xmInfoDao = xmInfoDao;
	}

	@Autowired
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}

	@Autowired
	public void setXmSourceDao(XmSourceDao xmSourceDao) {
		this.xmSourceDao = xmSourceDao;
	}

	@Override
	public BaseDao<XmSource> getDao() {
		return this.xmSourceDao;
	}
	
	@Override
	public List<XmSource> findAll() {
		return xmSourceDao.getAll(XmSource.class);
	}

	@Override
	public Result<XmSource> find(XmSource model, int page, int size) {
		return xmSourceDao.find(model, page, size);
	}

	@Override
	public List<XmSource> findByExample(XmSource model) {
		return xmSourceDao.findByExample(model);
	}
	
	@Override
	public XmSource findById(Integer findId) {
		return xmSourceDao.findById(findId);
	}
	
	@Override
	public void add(XmSource model) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateStr = sdf.format(new Date());
			String NewNO = createXs("xs" + dateStr);

			model.setXsNumber(NewNO);
			xmSourceDao.save(model);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void del(String thisXmSourceId) {
		try {
			if(thisXmSourceId != null && !"".equals(thisXmSourceId)){
				XmSource delXmSource = xmSourceDao.findById(Integer.parseInt(thisXmSourceId));
				
				XmInfo delXmInfo = new XmInfo();
				delXmInfo.setXmSourceNumber(delXmSource.getXsNumber());
				List<XmInfo> listXmInfo = xmInfoDao.findByExample(delXmInfo);
				for (int i = 0; i < listXmInfo.size(); i++) {
					delXmInfo = listXmInfo.get(i);
					
					//1.删除责任人
					XmDuty delXmDuty = new XmDuty();
					delXmDuty.setXmNumber(delXmInfo.getXmNumber());
					List<XmDuty> listXmDuty = xmDutyDao.findByExample(delXmDuty);
					for (int j = 0; j < listXmDuty.size(); j++) {
						delXmDuty = listXmDuty.get(j);
						xmDutyDao.remove(delXmDuty);
					}
					
					//2.删除工作进度
					XmSchedule delXmSchedule = new XmSchedule();
					delXmSchedule.setXmNumber(delXmInfo.getXmNumber());
					List<XmSchedule> listXmSchedule = xmScheduleDao.findByExample(delXmSchedule);
					for (int j = 0; j < listXmSchedule.size(); j++) {
						delXmSchedule = listXmSchedule.get(j);
						xmScheduleDao.remove(delXmSchedule);
					}
					
					//3.删除项目
					xmInfoDao.remove(delXmInfo);
					
				}
				//4.删除督查事项批示来源
				xmSourceDao.remove(delXmSource);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void addAsk(XmInfo xmInfo, String thisPeopleId) {
		try {
			//1.项目信息
			DateFormat sdf = new SimpleDateFormat("yyyy");
			String dateStr = sdf.format(new Date());
			xmInfo.setXmYear(dateStr);
			
			DateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
			String dateStr1 = sdf1.format(new Date());
			String NewNO = xmInfoService.createNO(dateStr1);
			xmInfo.setXmNumber(NewNO);
			xmInfo = xmInfoDao.save(xmInfo);
			
			//2.责任人
			if(thisPeopleId != null && !"".equals(thisPeopleId)){
				User thisUser = userDao.findById(Integer.parseInt(thisPeopleId));
				
				XmDuty newXmDuty = new XmDuty();
				newXmDuty.setXmNumber(xmInfo.getXmNumber());
				newXmDuty.setPeopleId(thisUser.getId());
				newXmDuty.setPeople(thisUser.getHelpName());
				newXmDuty.setPeopleTel(thisUser.getHelpTelephone());
				newXmDuty.setType("07");//07交办领导（用于督查事项）
				xmDutyDao.save(newXmDuty);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void delAsk(String thisXmNumber) {
		try {
			if(thisXmNumber != null && !"".equals(thisXmNumber)){
				XmInfo delXmInfo = new XmInfo();
				delXmInfo.setXmNumber(thisXmNumber);
				List<XmInfo> listXmInfo = xmInfoDao.findByExample(delXmInfo);
				for (int i = 0; i < listXmInfo.size(); i++) {
					delXmInfo = listXmInfo.get(i);
					
					//1.删除责任人
					XmDuty delXmDuty = new XmDuty();
					delXmDuty.setXmNumber(thisXmNumber);
					List<XmDuty> listXmDuty = xmDutyDao.findByExample(delXmDuty);
					for (int j = 0; j < listXmDuty.size(); j++) {
						delXmDuty = listXmDuty.get(j);
						xmDutyDao.remove(delXmDuty);
					}
					
					//2.删除工作进度
					XmSchedule delXmSchedule = new XmSchedule();
					delXmSchedule.setXmNumber(delXmInfo.getXmNumber());
					List<XmSchedule> listXmSchedule = xmScheduleDao.findByExample(delXmSchedule);
					for (int j = 0; j < listXmSchedule.size(); j++) {
						delXmSchedule = listXmSchedule.get(j);
						xmScheduleDao.remove(delXmSchedule);
					}
					
					//2.删除项目
					xmInfoDao.remove(delXmInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 批示内容（工作要求）,根据批示来源Number查询
	 * @return
	 */
	@Override
	public List<ViewAsk> findAsk(String xsNumber) {
		
		List<ViewAsk> rsView = new ArrayList<ViewAsk>();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT i.xmNumber,i.name,i.startTime,i.xmYear,i.budgetType,i.xmSourceNumber,d.peopleId,d.people,d.peopleTel FROM ");
			sqlBuff.append(" (SELECT * FROM xm_info WHERE 1=1 ");
			if(xsNumber != null && !"".equals(xsNumber)){
				sqlBuff.append(" and xmSourceNumber='"+xsNumber+"' ");
			}
			sqlBuff.append(" AND budgetType='07') AS i ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_duty WHERE type='07') AS d ");
			sqlBuff.append(" ON i.xmNumber=d.xmNumber ");
			sqlBuff.append(" ORDER BY i.startTime DESC ");
			
			System.out.println("批示内容（工作要求）,根据批示来源Number查询="+sqlBuff.toString());
			
			List list=xmInfoDao.query(sqlBuff.toString());
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String xmNumber = (String)oc[0];
				String name = (String)oc[1];
				Timestamp startTime = (Timestamp)oc[2];
				String xmYear = (String)oc[3];
				String budgetType = (String)oc[4];
				String xmSourceNumber = (String)oc[5];
				Integer peopleId = (Integer)oc[6];
				String people = (String)oc[7];
				String peopleTel = (String)oc[8];
				
				ViewAsk viewAsk = new ViewAsk();
				
				viewAsk.setXmNumber(xmNumber);
				viewAsk.setName(name);
				viewAsk.setStartTime(startTime);
				viewAsk.setXmYear(xmYear);
				viewAsk.setBudgetType(budgetType);
				viewAsk.setXmSourceNumber(xmSourceNumber);
				viewAsk.setPeopleId(peopleId);
				viewAsk.setPeople(people);
				viewAsk.setPeopleTel(peopleTel);
				
				rsView.add(viewAsk);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 批示内容（工作要求）列表
	 * @return
	 */
	@Override
	public Result<ViewAsk> findListAsk(ViewAsk thisViewAsk, int page, int size) {
		System.out.println("tz===="+thisViewAsk.getName());
		
		Result<ViewAsk> rsView = new Result<ViewAsk>();
		try {
			int start=(page-1)*size;
			int limit =size;
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT si.xmNumber,si.name,si.startTime,si.xmYear,si.budgetType,si.xmSourceNumber,si.xsName,si.xsLevel,si.xsIndustry,si.xsArea,d.peopleId,d.people,d.peopleTel, IFNULL(TO_DAYS(sysdate()) - TO_DAYS(ss.nt), 0) AS thisd, IF(TO_DAYS(sysdate()) - TO_DAYS(ss.nt) > si.cycle, 1, 0) AS ifd,si.isClose FROM ");
			sqlBuff.append(" (SELECT i.xmNumber,i.name,i.startTime,i.xmYear,i.budgetType,i.xmSourceNumber,s.xsName,s.xsLevel,s.xsIndustry,s.xsArea,i.cycle,i.isClose FROM ");
			sqlBuff.append(" (SELECT * FROM xm_source WHERE 1=1 ");
			if(thisViewAsk.getXmSourceNumber() != null && !"".equals(thisViewAsk.getXmSourceNumber())){
				sqlBuff.append(" AND xsNumber='"+thisViewAsk.getXmSourceNumber()+"' ");
			}
			if(thisViewAsk.getXsName() != null && !"".equals(thisViewAsk.getXsName())){
				sqlBuff.append(" AND xsName LIKE '%"+thisViewAsk.getXsName()+"%' ");
			}
			sqlBuff.append(" ) AS s ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_info WHERE budgetType='07' ");
			if(thisViewAsk.getName() != null && !"".equals(thisViewAsk.getName())){
				sqlBuff.append(" AND name LIKE '%"+thisViewAsk.getName()+"%' ");
			}
			sqlBuff.append(" )AS i ");
			sqlBuff.append(" ON s.xsNumber=i.xmSourceNumber) AS si ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_duty WHERE 1=1 ");
			//sqlBuff.append(" and type='07' ");
			sqlBuff.append(" GROUP BY xmNumber) AS d ");
			sqlBuff.append(" ON si.xmNumber=d.xmNumber ");
			sqlBuff.append(" LEFT JOIN (SELECT MAX(creatTime) AS nt, xmNumber ");
			sqlBuff.append(" FROM xm_schedule WHERE type='01' ");
			sqlBuff.append(" GROUP BY xmNumber ");
			sqlBuff.append(" ) ss ON si.xmNumber = ss.xmNumber ");
			sqlBuff.append(" WHERE si.xmNumber IS NOT NULL ");
			if(thisViewAsk.getPeopleId() != null && !"".equals(thisViewAsk.getPeopleId())){
				sqlBuff.append(" AND d.peopleId='"+thisViewAsk.getPeopleId()+"' ");
			}
			sqlBuff.append(" ORDER BY si.isClose,si.startTime DESC ");
			
			System.out.println("批示内容（工作要求）列表="+sqlBuff.toString());
			
			Result rs=xmSourceDao.findBySQL(sqlBuff.toString(), null, start, limit);
			List list=rs.getData();
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String xmNumber = (String)oc[0];
				String name = (String)oc[1];
				Timestamp startTime = (Timestamp)oc[2];
				String xmYear = (String)oc[3];
				String budgetType = (String)oc[4];
				String xmSourceNumber = (String)oc[5];
				String xsName = (String)oc[6];
				String xsLevel = (String)oc[7];
				String xsIndustry = (String)oc[8];
				String xsArea = (String)oc[9];
				Integer peopleId = (Integer)oc[10];
				String people = (String)oc[11];
				String peopleTel = (String)oc[12];
				Integer nowCycle = (Integer)oc[13];//当前时间-最新工作进度（时间）
				Integer isOverdue = (Integer)oc[14];
				String isClose = (String)oc[15];
				
				
				ViewAsk viewAsk = new ViewAsk();
				
				viewAsk.setXmNumber(xmNumber);
				viewAsk.setName(name);
				viewAsk.setStartTime(startTime);
				viewAsk.setXmYear(xmYear);
				viewAsk.setBudgetType(budgetType);
				viewAsk.setXmSourceNumber(xmSourceNumber);
				viewAsk.setXsName(xsName);
				viewAsk.setXsLevel(xsLevel);
				viewAsk.setXsIndustry(xsIndustry);
				viewAsk.setXsArea(xsArea);
				viewAsk.setPeopleId(peopleId);
				viewAsk.setPeople(people);
				viewAsk.setPeopleTel(peopleTel);
				viewAsk.setNowCycle(nowCycle+"");
				viewAsk.setIsOverdue(isOverdue+"");
				viewAsk.setIsClose(isClose);
				
				
				rsView.getData().add(viewAsk);
			}
			rsView.setOffset(rs.getOffset());
			rsView.setPage(rs.getPage());
			rsView.setSize(rs.getSize());
			rsView.setTotal(rs.getTotal());
			rsView.setTotalPage(rs.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 督查事项落实情况汇总表
	 * @return
	 */
	@Override
	public Result<ViewAsk> findTableAsk(ViewAsk thisViewAsk, int page, int size) {
		
		Result<ViewAsk> rsView = new Result<ViewAsk>();
		try {
			int start=(page-1)*size;
			int limit =size;
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT ");
			sqlBuff.append(" sid.xmNumber,sid.name,sid.startTime,sid.xmYear,sid.budgetType, ");
			sqlBuff.append(" sid.xmSourceNumber,sid.xsName,sid.xsLevel,sid.xsIndustry,sid.xsArea,sid.peopleId,sid.people,sid.peopleTel, ");
			sqlBuff.append(" xs01.c01, xs02.c02, xs03.c03, dut08.strDut08, dut09.strDut09, ");
			sqlBuff.append(" sid.isClose ");
			sqlBuff.append(" FROM ");
			sqlBuff.append(" (SELECT si.xmNumber, si.name, si.startTime, si.xmYear, si.budgetType, si.xmSourceNumber, si.xsName,si.xsLevel,si.xsIndustry,si.xsArea,d.peopleId, d.people, d.peopleTel,si.isClose ");
			sqlBuff.append(" FROM ( ");
			sqlBuff.append(" SELECT i.xmNumber, i.name, i.startTime, i.xmYear, i.budgetType, i.xmSourceNumber, s.xsName,s.xsLevel,s.xsIndustry,s.xsArea,i.isClose FROM ");
			sqlBuff.append(" (SELECT * FROM xm_source WHERE 1 = 1 ");
			if(thisViewAsk.getXmSourceNumber() != null && !"".equals(thisViewAsk.getXmSourceNumber())){
				sqlBuff.append(" AND xsNumber='"+thisViewAsk.getXmSourceNumber()+"' ");
			}
			if(thisViewAsk.getXsName() != null && !"".equals(thisViewAsk.getXsName())){
				sqlBuff.append(" AND xsName LIKE '%"+thisViewAsk.getXsName()+"%' ");
			}
			sqlBuff.append(" ) s ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_info WHERE budgetType = '07') i ");
			sqlBuff.append(" ON s.xsNumber = i.xmSourceNumber) si ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_duty WHERE type = '07' GROUP BY xmNumber) d ");
			sqlBuff.append(" ON si.xmNumber = d.xmNumber ");
			sqlBuff.append(" WHERE si.xmNumber IS NOT NULL ");
			if(thisViewAsk.getPeopleId() != null && !"".equals(thisViewAsk.getPeopleId())){
				sqlBuff.append(" AND d.peopleId='"+thisViewAsk.getPeopleId()+"' ");
			}
			sqlBuff.append(" ORDER BY si.startTime DESC ");
			sqlBuff.append(" ) AS sid ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c01,xmNumber FROM xm_schedule WHERE type='01' ORDER BY creatTime DESC ) AS xs01 ");
			sqlBuff.append(" ON sid.xmNumber=xs01.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c02,xmNumber FROM xm_schedule WHERE type='02' ORDER BY creatTime DESC) AS xs02 ");
			sqlBuff.append(" ON sid.xmNumber=xs02.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c03,xmNumber FROM xm_schedule WHERE type='03' ORDER BY creatTime DESC ) AS xs03 ");
			sqlBuff.append(" ON sid.xmNumber=xs03.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT GROUP_CONCAT(people,'(',unit,')-',peopleTel SEPARATOR ';<br>') AS strDut08,xmNumber FROM xm_duty WHERE type ='08' GROUP BY xmNumber) AS dut08 ");
			sqlBuff.append(" ON sid.xmNumber=dut08.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT GROUP_CONCAT(people,'(',unit,')-',peopleTel SEPARATOR ';<br>') AS strDut09,xmNumber FROM xm_duty WHERE type ='09' GROUP BY xmNumber) AS dut09 ");
			sqlBuff.append(" ON sid.xmNumber=dut09.xmNumber ");
			sqlBuff.append(" ORDER BY sid.isClose,sid.startTime DESC ");
			
			System.out.println("督查事项落实情况汇总表="+sqlBuff.toString());
			
			Result rs=xmSourceDao.findBySQL(sqlBuff.toString(), null, start, limit);
			List list=rs.getData();
			
			for(int j=0;j<list.size();j++){
				Object[] oc = (Object[])list.get(j);
				
				String xmNumber = (String)oc[0];
				String name = (String)oc[1];
				Timestamp startTime = (Timestamp)oc[2];
				String xmYear = (String)oc[3];
				String budgetType = (String)oc[4];
				String xmSourceNumber = (String)oc[5];
				String xsName = (String)oc[6];
				String xsLevel = (String)oc[7];
				String xsIndustry = (String)oc[8];
				String xsArea = (String)oc[9];
				Integer peopleId = (Integer)oc[10];
				String people = (String)oc[11];
				String peopleTel = (String)oc[12];
				String scheduleContent01 = (String)oc[13];//最新落实情况
				String scheduleContent02 = (String)oc[14];//存在的问题和困难
				String scheduleContent03 = (String)oc[15];//下一步工作计划
				
				String dutyPeople08 = (String)oc[16];//责任领导
				String dutyPeople09 = (String)oc[17];//具体跟踪人及联系方式
				
				String isClose = (String)oc[18];
				
				ViewAsk viewAsk = new ViewAsk();
				
				viewAsk.setXmNumber(xmNumber);
				viewAsk.setName(name);
				viewAsk.setStartTime(startTime);
				viewAsk.setXmYear(xmYear);
				viewAsk.setBudgetType(budgetType);
				viewAsk.setXmSourceNumber(xmSourceNumber);
				viewAsk.setXsName(xsName);
				viewAsk.setXsLevel(xsLevel);
				viewAsk.setXsIndustry(xsIndustry);
				viewAsk.setXsArea(xsArea);
				viewAsk.setPeopleId(peopleId);
				viewAsk.setPeople(people);
				viewAsk.setPeopleTel(peopleTel);
				viewAsk.setScheduleContent01(scheduleContent01);
				viewAsk.setScheduleContent02(scheduleContent02);
				viewAsk.setScheduleContent03(scheduleContent03);
				viewAsk.setDutyPeople08(dutyPeople08);
				viewAsk.setDutyPeople09(dutyPeople09);
				viewAsk.setIsClose(isClose);
				
				rsView.getData().add(viewAsk);
			}
			rsView.setOffset(rs.getOffset());
			rsView.setPage(rs.getPage());
			rsView.setSize(rs.getSize());
			rsView.setTotal(rs.getTotal());
			rsView.setTotalPage(rs.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	
	/**
	 * 督查事项落实情况汇总表导出
	 */
	@Override
	public downViewAsk tableAskDownload(ViewAsk thisViewAsk) {
		downViewAsk rsView = new downViewAsk();
		try {
			StringBuffer sqlBuff=new StringBuffer();
			
			sqlBuff.append(" SELECT sid.*,xs01.c01,xs02.c02,xs03.c03,dut08.strDut08,dut09.strDut09 FROM ");
			sqlBuff.append(" (SELECT si.xmNumber, si.name, si.startTime, si.xmYear, si.budgetType, si.xmSourceNumber, si.xsName,si.xsLevel,si.xsIndustry,si.xsArea, d.peopleId, d.people, d.peopleTel ");
			sqlBuff.append(" FROM ( ");
			sqlBuff.append(" SELECT i.xmNumber, i.name, i.startTime, i.xmYear, i.budgetType, i.xmSourceNumber, s.xsName, s.xsLevel, s.xsIndustry, s.xsArea FROM ");
			sqlBuff.append(" (SELECT * FROM xm_source WHERE 1 = 1 ");
			if(thisViewAsk.getXmSourceNumber() != null && !"".equals(thisViewAsk.getXmSourceNumber())){
				sqlBuff.append(" AND xsNumber='"+thisViewAsk.getXmSourceNumber()+"' ");
			}
			if(thisViewAsk.getXsName() != null && !"".equals(thisViewAsk.getXsName())){
				sqlBuff.append(" AND xsName LIKE '%"+thisViewAsk.getXsName()+"%' ");
			}
			sqlBuff.append(" ) s ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_info WHERE budgetType = '07') i ");
			sqlBuff.append(" ON s.xsNumber = i.xmSourceNumber) si ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT * FROM xm_duty WHERE type = '07' GROUP BY xmNumber) d ");
			sqlBuff.append(" ON si.xmNumber = d.xmNumber ");
			sqlBuff.append(" WHERE si.xmNumber IS NOT NULL ");
			if(thisViewAsk.getPeopleId() != null && !"".equals(thisViewAsk.getPeopleId())){
				sqlBuff.append(" AND d.peopleId='"+thisViewAsk.getPeopleId()+"' ");
			}
			sqlBuff.append(" ORDER BY si.startTime DESC ");
			sqlBuff.append(" ) AS sid ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c01,xmNumber FROM xm_schedule WHERE type='01' ORDER BY creatTime DESC ) AS xs01 ");
			sqlBuff.append(" ON sid.xmNumber=xs01.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c02,xmNumber FROM xm_schedule WHERE type='02' ORDER BY creatTime DESC ) AS xs02 ");
			sqlBuff.append(" ON sid.xmNumber=xs02.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT content AS c03,xmNumber FROM xm_schedule WHERE type='03' ORDER BY creatTime DESC ) AS xs03 ");
			sqlBuff.append(" ON sid.xmNumber=xs03.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT GROUP_CONCAT(people,'(',unit,')-',peopleTel SEPARATOR ';') AS strDut08,xmNumber FROM xm_duty WHERE type ='08' GROUP BY xmNumber) AS dut08 ");
			sqlBuff.append(" ON sid.xmNumber=dut08.xmNumber ");
			sqlBuff.append(" LEFT JOIN ");
			sqlBuff.append(" (SELECT GROUP_CONCAT(people,'(',unit,')-',peopleTel SEPARATOR ';') AS strDut09,xmNumber FROM xm_duty WHERE type ='09' GROUP BY xmNumber) AS dut09 ");
			sqlBuff.append(" ON sid.xmNumber=dut09.xmNumber ");

			System.out.println("督查事项落实情况汇总表导出:"+sqlBuff.toString());
			
			List list=xmSourceDao.query(sqlBuff.toString());
			
			rsView.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsView;
	}
	/********************* 创建编号方法（内部调用） *********************/
	/**
	 * 创建新的编号
	 * 
	 * @param thisNO
	 * @return
	 */
	public String createXs(String thisNO) {

		String maxNO = maxXs(thisNO); // 查找最大号
		String autoNum = maxNO.substring(maxNO.length() - 2, maxNO.length()); // 截取查找结果的后2位
		int num = Integer.parseInt(autoNum); // 对截取结果+1
		num += 1;
		String NewNO = "";
		if (num < 99) {
			NewNO = thisNO + String.format("%1$,02d", num); // 组装成新的号
		} else {
			NewNO = thisNO + num; // 组装成新的号
		}
		return NewNO;
	}

	/**
	 * 查找最大编号
	 * 
	 * @param thisNO
	 * @return
	 */
	public String maxXs(String thisNO) {
		String queryString = "select max(xsNumber) from XmSource where xsNumber like'"
				+ thisNO + "__'";
		List list = xmSourceDao.findByHql(queryString, null);
		String maxNO = thisNO + "00";
		if (list.size() != 0) {
			String oc = (String) list.get(0);
			if (oc != null) {
				maxNO = oc;
			}
		}
		return maxNO;
	}

}
