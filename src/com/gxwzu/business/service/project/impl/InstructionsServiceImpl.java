package com.gxwzu.business.service.project.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.InstructionsDao;
import com.gxwzu.business.model.Instructions;
import com.gxwzu.business.service.project.InstructionsService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 任务批示业务层实现类
 * @ClassName: InstructionsServiceImpl
 * @author SunYi
 * @Date 2019年4月8日上午9:53:20
 */
@Service("instructionsService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class InstructionsServiceImpl extends BaseServiceImpl<Instructions> implements InstructionsService {
	
	@Autowired
	private InstructionsDao instructionsDao;

	public void setInstructionsDao(InstructionsDao instructionsDao) {
		this.instructionsDao = instructionsDao;
	}
	
	@Override
	public BaseDao<Instructions> getDao() {
		return this.instructionsDao;
	}
	
	private Instructions instructions;
	
	
	
	
	/* **********************************************方法体**********************************************  */
	
	/**
	  * 添加任务批示
	  * @author SunYi
	  * @Date 2019年4月8日上午9:52:44
	  * @return void
	  */
	@Override
	public void xmInstructionsAdd(Instructions instructions) {
		instructionsDao.save(instructions);
	}

	 /**
	  * 获取指定任务编号的任务批示
	  * @author SunYi
	  * @Date 2019年4月8日上午11:21:29
	  */
	@Override
	public List<Instructions> getInstructionsByXmNumber(String thisXmNumber) {
		return instructionsDao.getInstructionsByXmNumber(thisXmNumber);
	}
	
	
	
	/* **********************************************实体类**********************************************  */
	
	public Instructions getInstructions() {
		return instructions;
	}

	public void setInstructions(Instructions instructions) {
		this.instructions = instructions;
	}


}
