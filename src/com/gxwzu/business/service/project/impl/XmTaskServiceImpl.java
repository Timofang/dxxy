package com.gxwzu.business.service.project.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmTaskDao;
import com.gxwzu.business.model.XmTask;
import com.gxwzu.business.service.project.XmTaskService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 征地任务周报ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmTaskServiceImpl
 * <br>Date: 2017-9-6下午08:52:45
 * <br>log:
 */
@Service("xmTaskService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmTaskServiceImpl extends BaseServiceImpl<XmTask> implements XmTaskService{
	
	
	private XmTaskDao xmTaskDao;

	@Autowired
	public void setXmTaskDao(XmTaskDao xmTaskDao) {
		this.xmTaskDao = xmTaskDao;
	}

	@Override
	public BaseDao<XmTask> getDao() {
		return this.xmTaskDao;
	}
	
	@Override
	public List<XmTask> findAll() {
		return xmTaskDao.getAll(XmTask.class);
	}

	@Override
	public Result<XmTask> find(XmTask model, int page, int size) {
		return xmTaskDao.find(model, page, size);
	}

	@Override
	public List<XmTask> findByExample(XmTask model) {
		return xmTaskDao.findByExample(model);
	}
	
	@Override
	public XmTask findById(Integer findId) {
		return xmTaskDao.findById(findId);
	}

}
