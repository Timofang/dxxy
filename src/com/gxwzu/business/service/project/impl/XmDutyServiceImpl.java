package com.gxwzu.business.service.project.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.XmDutyDao;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.service.project.XmDutyService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;

/**
 * 责任单位/责任人ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmDutyServiceImpl
 * <br>Date: 2017-8-17下午06:25:09
 * <br>log:
 */
@Service("xmDutyService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmDutyServiceImpl extends BaseServiceImpl<XmDuty> implements XmDutyService  {
	
	
	private XmDutyDao xmDutyDao;

	@Autowired
	public void setXmDutyDao(XmDutyDao xmDutyDao) {
		this.xmDutyDao = xmDutyDao;
	}

	@Override
	public BaseDao<XmDuty> getDao() {
		return this.xmDutyDao;
	}
	
	@Override
	public List<XmDuty> findAll() {
		return xmDutyDao.getAll(XmDuty.class);
	}

	@Override
	public Result<XmDuty> find(XmDuty model, int page, int size) {
		return xmDutyDao.find(model, page, size);
	}

	@Override
	public List<XmDuty> findByExample(XmDuty model) {
		return xmDutyDao.findByExample(model);
	}
	
	@Override
	public XmDuty findById(Integer findId) {
		return xmDutyDao.findById(findId);
	}

	@Override
	public void add(XmDuty xmDuty) {
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			xmDuty.setCreateTime(Timestamp.valueOf(dateStr));
			
			xmDutyDao.save(xmDuty);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void del(Integer id) {
		try {
			XmDuty delXmDuty = xmDutyDao.findById(id);
			if(delXmDuty != null){
				xmDutyDao.remove(delXmDuty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
