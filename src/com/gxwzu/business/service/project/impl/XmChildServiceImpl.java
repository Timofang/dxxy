package com.gxwzu.business.service.project.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.UnitDao;
import com.gxwzu.business.dao.project.XmChildDao;
import com.gxwzu.business.dao.project.XmDao;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.service.project.MaterialsService;
import com.gxwzu.business.service.project.XmChildService;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;

@Service("XmChildService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class XmChildServiceImpl extends BaseServiceImpl<XmChild> implements XmChildService  {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private XmChildDao xmChildDao;
	@Autowired
	private XmDao xmDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserService userService;
	@Autowired
	private MaterialsService materialsService;
	@Autowired
	private UnitDao unitDao;

	@Override
	public void add(XmChild XmChild) {
		xmChildDao.save(XmChild);
	}
	
	/**
	 * 查询个人教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public  List unitstatisticsDetails(String username){
		
		return xmChildDao.unitstatisticsDetails(username);
	}

	
	/**
	 * 统计所有任务的完成情况
	 * @author SunYi
	 * @Date 2019年3月18日下午9:47:58
	 */
	@Override
	public List<User> getXmStatistics() {
		// 查询所有任务信息
		List<XmChild> xcList = xmChildDao.getXmStatistics();
		List<User> userList = new ArrayList<User>();
		for(int i=0; i<xcList.size(); i++) {
			List<XmChild> userXmChildList = null;
			User user = new User();
			// 获取集合里的对象
			String userName = xcList.get(i).getUserName();
			// 创建计数器
			int fcount = 0;
			int ufcount = 0;
			// 根据用户名去查询对应的所有任务
			userXmChildList = xmChildDao.getUserXmChild(userName);
			for (int j = 0; j < userXmChildList.size(); j++) {
				// 取出每条任务的完成状态
				if(userXmChildList.get(j).getStatus() == 1){
					// 每查出一条任务状态为1，则将完成状态值自增后存进该任务对应的用户对象
					fcount++;
					user.setFinishCount(fcount);
				} else if(userXmChildList.get(j).getStatus() == 0) {
					// 每查出一条任务状态为0，则将未完成状态值自增后存进该任务对应的用户对象
					ufcount++;
					user.setUnfinishedCount(ufcount);
				}
			}
			// 通过完成任务数和未完成任务数计算任务完成比例
			 if((fcount+ufcount) != 0) {
				 // 将完成比例转换成百分比的形式
				 int result = (int) ((new BigDecimal((float) fcount / (fcount+ufcount)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())*100);
				 String proportion = result+"%"; 
				 user.setProportion(proportion);
				 user.setAllCount(fcount + ufcount);
			 }else if((fcount+ufcount) == 0){
				 user.setProportion("100%");
				 user.setAllCount(0);
			 }
			// 将任务对象存进用户对象中，每个用户对象中都有一个任务对象
			user.setXmChildList(userXmChildList);
			// 根据子任务表里的接收人工号查找用户
			User userInfo = userDao.getUserById(userName);
			String helpName = userInfo.getHelpName();
			user.setHelpName(helpName);
			user.setHelpUserName(userInfo.getHelpUserName());
			// 将用户对象存进List集合
			userList.add(user);
		}
		return userList;
	}
	
	
	/**
	 * 根据子项目编号和接收人获取项目文件信息
	 * @author SunYi
	 * @Date 2019年3月11日下午11:05:45
	 */
	@Override
	public XmChild getFileByXmNumber(String xmChildNumber) {
		// 查询子项目信息
		XmChild xc = xmChildDao.getFileByXmNumber(xmChildNumber);
		// 切割项目编号(父项目编号 = 子项目编号-接收人姓名)
		String xmNumber = xmChildNumber.substring(0, xc.getXmChildNumber().length() - xc.getUserName().length());
		// 查询子项目对应的父项目信息
		XmChild xcParent = xmChildDao.getChildByXmUser(xmNumber, xc.getUnitParent());
		xc.setXmChild(xcParent);
		return xc;
	}

	/**
	 * 
	 * @Description 分页查询所有子项目及其父项目信息
	 * @author 莫东林
	 * @date 2019年3月12日上午1:06:57
	 * Result<XmChild>
	 */
	@Override
	public Result<XmChild> find(XmChild model, int page, int size) {
		//根据条件筛选获取子项目列表
		Result<XmChild> xmChildResult = xmChildDao.find(model, page, size);
		//遍历子项目列表获取其父项目信息
		for(int i = 0; i < xmChildResult.getData().size(); i++){
			Xm xm = null;
			//如果此项目为教研室继续下发的子项目
			if(xmChildResult.getData().get(i).getXmNumber() == null){
				int userNameLength = xmChildResult.getData().get(i).getUserName().length();
				String xmChildNumber = xmChildResult.getData().get(i).getXmChildNumber().substring(
						0, xmChildResult.getData().get(i).getXmChildNumber().length() - userNameLength);
				xm = xmDao.getByXmNumber(xmChildNumber);
				
				String unitName = unitDao.getByUnitId(xmChildResult.getData().get(i).getUnitParent()).getUnitName();
				//获取教研室名称
				XmChild xc = xmChildDao.getChildByXmUser(xmChildNumber, xmChildResult.getData().get(i).getUnitParent());
				xc.setUnitName(unitName);
				
				//获取接收者姓名
				String receiverName = userService.findByJobNumber(xmChildResult.getData().get(i).getUserName()).getHelpName();
				xmChildResult.getData().get(i).setReceiverName(receiverName);
				
				xmChildResult.getData().get(i).setXmChild(xc);
			}
			//院下发的任务
			else{
				xm = xmDao.getByXmNumber(xmChildResult.getData().get(i).getXmNumber());
				String username = userService.getByUserName(xm.getUsername()).getHelpName();
				xm.setHelpUsername(username);
				
				//获取接收教研室名称
				if(xmChildResult.getData().get(i).getUnitId() != null){
					String receiverUnitName = unitDao.getByUnitId(xmChildResult.getData().get(i).getUnitId()).getUnitName();
					xmChildResult.getData().get(i).setReceiverUnitName(receiverUnitName);
				}else{
					String receiverName = userService.findByJobNumber(xmChildResult.getData().get(i).getUserName()).getHelpName();
					xmChildResult.getData().get(i).setReceiverName(receiverName);
				}
				
			}
			
			xmChildResult.getData().get(i).setXm(xm);
		}
		return xmChildResult;
	}


	@Override
	public BaseDao<XmChild> getDao() {
		// TODO Auto-generated method stub
		return null;
	}

    /**
     * 查询员工个人所有任务
     * @author 叶城廷
     * @version 2019.03.03
     */
	@Override
	public Result<XmChild> selUserTask(User currentUser, Integer page, int size) {
		
		return xmChildDao.selUserTask(currentUser,page,size);
	}


	/**
	 * 
	* @Description: 根据用户查找子项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午6:29:31
	* @return Result<XmChild>
	* @throws
	 */
	@Override
	public Result<XmChild> findByUser(XmChild xmChild, Integer page, int size) {
		//根据条件筛选获取子项目列表
		Result<XmChild> xmChildResult = xmChildDao.findByUser(xmChild, page, size);
		//遍历子项目列表获取其父项目信息
		for(int i = 0; i < xmChildResult.getData().size(); i++){
			//如果为院下发的任务
			if(xmChildResult.getData().get(i).getAcceptstatus() == 1){
				Xm xm = xmDao.getByXmNumber(xmChildResult.getData().get(i).getXmNumber());
				String username = userService.getByUserName(xm.getUsername()).getHelpName();
				xm.setHelpUsername(username);
				xmChildResult.getData().get(i).setXm(xm);
			}
			//如果为教研室下发的任务
			if(xmChildResult.getData().get(i).getAcceptstatus() == 0){
				XmChild c = xmChildResult.getData().get(i);
				XmChild cx = xmChildDao.getChildByXmUser(c.getXmChildNumber().substring(0, c.getXmChildNumber().length() - c.getUserName().length()),
						c.getUnitParent());
				String unitName = unitDao.getByUnitId(c.getUnitParent()).getUnitName();
				cx.setUnitName(unitName);
				xmChildResult.getData().get(i).setXmChild(cx);
			}
		}
		return xmChildResult;
	}

	/**
	 * 
	* @Description: 获取教研室用户下发的项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午7:00:20
	* @return Result<XmChild>
	* @throws
	 */
	@Override
	public Result<XmChild> findIssueByUser(XmChild xmChild, Integer page, int size) {
		//根据条件筛选获取子项目列表
		Result<XmChild> xmChildResult = xmChildDao.findIssueByUser(xmChild, page, size);
		//遍历子项目列表获取其子项目信息
		for(int i = 0; i < xmChildResult.getData().size(); i++){
			List<XmChild> xmChildList = new ArrayList<XmChild>();
			//获取子项目下发的项目列表
			xmChildList = xmChildDao.getChildListByXmUser(xmChildResult.getData().get(i).getXmNumber(), xmChildResult.getData().get(i).getUnitId());
			xmChildResult.getData().get(i).setXmChildList(xmChildList);
		}
		return xmChildResult;
	}

	/*
	* @Description: 通过任务编号以及接收者获取任务信息
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
	@Override
	public XmChild getChildByXmUser(String thisXmNumber, String unitId) {
		XmChild xmChild = xmChildDao.getChildByXmUser(thisXmNumber, unitId);
		Xm xm = xmDao.getByXmNumber(xmChild.getXmNumber());
		xmChild.setXm(xm);
		return xmChild;
	}


	@Override
	public void updateXmChild(XmChild c) {
		xmDao.updateXmChild(c);
	}

	/**
	 * 
	* @Description: 通过子子项目项目编号获取其及其父项目信息(Unit_xmChildDetail.jsp)
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	 */
	@Override
	public XmChild getByXmChildNumber(String thisXmNumber){
		XmChild xc = xmChildDao.getFileByXmNumber(thisXmNumber);
		//获取其父项目信息
		XmChild xcP = xmChildDao.getChildByXmUser(xc.getXmChildNumber().substring(0, xc.getXmChildNumber().length() - xc.getUserName().length()), xc.getUnitParent());
		User user = userService.getByUserName(xc.getUserName());
		//存放教研室对应所有材料
		List<Materials> materialsList = materialsService.getByUser(xc.getUserName(), xc.getXmChildNumber());
		user.setMaterialsList(materialsList);
		List<User> userList = new ArrayList<User>();
		userList.add(user);
		xcP.setUserList(userList);
		xc.setXmChild(xcP);
		// 获取发布人
		User xcUser = userDao.getUserById(xc.getXmChild().getUserName());
		// 保存发布人姓名
		xc.setHelpName(xcUser.getHelpName());
		return xc;
	}
	
	/**
	 * 
	* @Description: 通过子项目编号获取其子项目信息
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	 */
	@Override
	public XmChild getByXmUnitChildNumber(String thisXmNumber, String unitId) {
		XmChild xc = xmChildDao.getChildByXmUser(thisXmNumber, unitId);
		List<XmChild> xcList = xmChildDao.getChildListByXmUser(thisXmNumber, unitId);
		List<User> userList = new ArrayList<User>();
		for(int i = 0; i < xcList.size(); i++){
			User user = userService.getByUserName(xcList.get(i).getUserName());
			List<Materials> materialsList = materialsService.getByUser(user.getHelpUserName(), xcList.get(i).getUnitId());
			user.setMaterialsList(materialsList);
			userList.add(user);
		} 
		xc.setUserList(userList);
		return xc;
	}
	
	/**
	 * 
	* @Description: 根据用户工号和项目编号查询xmChild
	* @Author 莫东林
	* @date 2019年3月18日下午9:41:00
	* @return XmChild
	* @throws
	 */
	@Override
	public XmChild getByUserXmNumber(String userName, String unitId){
		return xmChildDao.getByUserXmNumber(userName, unitId);
	}
	
	@Override
	public XmChild getByUnitXmNumber(String userName, String unitId){
		return xmChildDao.getByUnitXmNumber(userName, unitId);
	}
	
	/**
	 * 
	* @Description: 通过子项目编号获取其父项目信息X
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	 */
	@Override
	public XmChild getByXmUnitChildNumberX(String thisXmNumber, String unitId) {
		XmChild xc = xmChildDao.getChildByXmUserX(thisXmNumber, unitId);
		// 分割子项目编号(xmChildNumber)，查询得到项目编号(xmNumber),再根据项目编号去查询改项目编号对应的任务信息
		XmChild xcP = xmChildDao.getChildByXmUser(xc.getXmChildNumber().substring(0, xc.getXmChildNumber().length() - xc.getUserName().length()), unitId);
		// 根据xcP中得到的父任务接收人,去查询该接收人的姓名(院发任务给教研室时,接收人是教研室主任,而教研室往下发任务时,发布人是教研室主任,所以子项目的发布人==父项目的接收人)
		User xcUser = userDao.getUserById(xcP.getUserName());
		List<User> userList = new ArrayList<User>();
		User user = userService.getByUserName(xc.getUserName());
		List<Materials> materialsList = materialsService.getByUser(user.getHelpUserName(), xc.getXmChildNumber());
		userList.add(user);
		xc.setXmChild(xcP);
		xcP.setUserList(userList);
		user.setMaterialsList(materialsList);
		// 保存子项目发布人姓名
		xc.setHelpName(xcUser.getHelpName());
		return xc;
	}
	
   /**
	* @Description: 通过子项目编号获取其子项目信息列表XX
	* @Author 莫东林
	* @date 2019年3月15日上午11:09:42
	* @return XmChild
	* @throws
	*/
	@Override
	public XmChild getByXmUnitChildNumberXX(String thisXmNumber, String unitId) {
		XmChild xc = xmChildDao.getChildByXmUser(thisXmNumber, unitId);
		List<XmChild> xcList = xmChildDao.getChildListByXmUser(thisXmNumber, unitId);
		List<User> userList = new ArrayList<User>();
		for(int i = 0; i < xcList.size(); i++){
			User user = userService.getByUserName(xcList.get(i).getUserName());
			List<Materials> materialsList = materialsService.getByUser(user.getHelpUserName(), xcList.get(i).getUnitId());
			user.setMaterialsList(materialsList);
			//关联用户与本项目信息
			user.setXmChild(xcList.get(i));
			userList.add(user);
		}
		xc.setUserList(userList);
		xc.setXmChildList(xcList);
		// 获取项目发布人信息
		User xcUser = userDao.getUserById(xc.getUserName());
		// 保存项目发布人姓名
		xc.setHelpName(xcUser.getHelpName());
		return xc;
	}
	
	/**
	 * 查询所有子项目list
	 * @author 叶城廷
	 * @version 2019.03.13
	 * @return
	 */
	public  List<XmChild> findAllXmChild() {
		//根据条件筛选获取子项目列表
		List<XmChild> allXmChild = xmChildDao.findAllXmChild();
		String xmChildNumber = null;
		//遍历子项目列表获取其父项目信息
		for(int i = 0; i < allXmChild.size(); i++){
			Xm xm = null;
			//如果此项目为教研室继续下发的子项目
			if(allXmChild.get(i).getAcceptstatus()==0){
				int userNameLength = allXmChild.get(i).getUserName().length();
				xmChildNumber = allXmChild.get(i).getXmChildNumber().substring(
						0, allXmChild.get(i).getXmChildNumber().length() - userNameLength);
				xm = xmDao.getByXmNumber(xmChildNumber);
				XmChild xc = xmChildDao.getChildByXmUser(xmChildNumber, allXmChild.get(i).getUnitParent());
		
				allXmChild.get(i).setXmChild(xc);
			}
			//院下发的任务
			else{
				xm = xmDao.getByXmNumber(allXmChild.get(i).getXmNumber());	
			}
			
			allXmChild.get(i).setXm(xm);
			
		}
		return allXmChild;
	}

	/**
	 * 查询对应User的所有子项目
	 * @param thisUserId 要查询的UserName
	 * @param thisInfoName
	 * @author 叶城廷
	 * @version 2019.03.13
	 * @return
	 */
	@Override
	public List<XmChild> findXmChildByNum(String thisUserId, String thisInfoName) {
//根据条件筛选获取子项目列表
		List<XmChild> allXmChild = xmChildDao.findXmChildByNum(thisUserId,thisInfoName);
		//遍历子项目列表获取其父项目信息
		for(int i = 0; i < allXmChild.size(); i++){
			Xm xm = null;
			//如果此项目为教研室继续下发的子项目
			if(allXmChild.get(i).getAcceptstatus()==0){
				int userNameLength = allXmChild.get(i).getUserName().length();
				String xmChildNumber = allXmChild.get(i).getXmChildNumber().substring(
						0, allXmChild.get(i).getXmChildNumber().length() - userNameLength);
				xm = xmDao.getByXmNumber(xmChildNumber);
				XmChild xc = xmChildDao.getChildByXmUser(xmChildNumber, allXmChild.get(i).getUnitParent());
				allXmChild.get(i).setXmChild(xc);
			}
			//院下发的任务
			else{
				xm = xmDao.getByXmNumber(allXmChild.get(i).getXmNumber());
			}

			allXmChild.get(i).setXm(xm);
		}
		return allXmChild;
	}

	/**
	 * 任务列表/我的任务 -查询该用户名下的所有子项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<XmChild> allViewXmChildByUser(String thisUserId) {
		//根据条件筛选获取子项目列表
		List<XmChild> allXmChild = xmChildDao.allViewXmChildByUser(thisUserId);
		//遍历子项目列表获取其父项目信息
		for(int i = 0; i < allXmChild.size(); i++){
			Xm xm = null;
			//如果此项目为教研室继续下发的子项目
			if(allXmChild.get(i).getAcceptstatus()==0){
				int userNameLength = allXmChild.get(i).getUserName().length();
				String xmChildNumber = allXmChild.get(i).getXmChildNumber().substring(
						0, allXmChild.get(i).getXmChildNumber().length() - userNameLength);
				xm = xmDao.getByXmNumber(xmChildNumber);
				XmChild xc = xmChildDao.getChildByXmUser(xmChildNumber, allXmChild.get(i).getUnitParent());
				allXmChild.get(i).setXmChild(xc);
			}
			//院下发的任务
			else{
				xm = xmDao.getByXmNumber(allXmChild.get(i).getXmNumber());
			}

			allXmChild.get(i).setXm(xm);
		}
		return allXmChild;
	}

	@Override
	public void saveQuality(String userName, String xmNumber, String quality) {
		xmChildDao.saveQuality(userName, xmNumber, quality);
	}


	@Override
	public void saveQualitys(String userName, String xmNumber, String degree) {
		xmChildDao.saveQualitys(userName, xmNumber, degree);
		
	}

	@Override
	public List<XmChild> findByLikeXmNumber(String xmNumber, String unitId) {
		return xmChildDao.findByLikeXmNumber(xmNumber,unitId);
	}

}
