package com.gxwzu.business.service.project;

import java.util.List;

import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmSource;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.sysVO.ViewAsk;
import com.gxwzu.sysVO.downViewAsk;

public interface XmSourceService extends BaseService<XmSource>{

	public abstract List<XmSource> findAll();

	public abstract Result<XmSource> find(XmSource model, int page, int size);

	public abstract List<XmSource> findByExample(XmSource model);

	public abstract XmSource findById(Integer findId);

	public abstract void add(XmSource model);

	public abstract void addAsk(XmInfo xmInfo, String thisPeopleId);

	public abstract List<ViewAsk> findAsk(String xsNumber);

	public abstract void delAsk(String thisXmNumber);

	public abstract void del(String thisXmSourceId);

	public abstract Result<ViewAsk> findListAsk(ViewAsk viewAsk, int page, int size);

	public abstract Result<ViewAsk> findTableAsk(ViewAsk viewAsk, int page, int size);

	public abstract downViewAsk tableAskDownload(ViewAsk thisViewAsk);

}