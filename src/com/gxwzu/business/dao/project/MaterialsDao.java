package com.gxwzu.business.dao.project;

import com.gxwzu.business.model.Materials;
import com.gxwzu.core.dao.BaseDao;

public interface MaterialsDao extends BaseDao<Materials>{

}
