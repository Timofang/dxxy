package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmTask;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmTaskDao extends BaseDao<XmTask>{

	public abstract Result<XmTask> find(XmTask model, int page, int size);

	public abstract List<XmTask> findByExample(XmTask model);

	public abstract XmTask findById(Integer id);

}