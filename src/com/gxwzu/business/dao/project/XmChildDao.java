package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.system.model.user.User;

public interface XmChildDao extends BaseDao<XmChild> {

	public abstract Result<XmChild> find(XmChild model, int page, int size);
	/**
	 * 查询个人教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public abstract List unitstatisticsDetails(String username);
	
	/**
	 * 统计所有任务的完成情况
	 * @author SunYi
	 * @Date 2019年3月18日下午9:52:35
	 * @return XmChild
	 */
	public abstract List<XmChild> getXmStatistics();
	
	/**
	 * 查询每个用户的所有任务信息
	 * @author SunYi
	 * @param size 
	 * @param page 
	 * @Date 2019年3月18日下午10:31:04
	 * @return Result<XmChild>
	 */
	public abstract List<XmChild> getUserXmChild(String userName);
	
	/**
	 * 根据子项目编号和接收人获取项目文件信息
	 * @author SunYi
	 * @Date 2019年3月11日下午11:09:10
	 * @return XmChild
	 */
	public abstract XmChild getFileByXmNumber(String xmChildNumber);
	/**
     * 查询员工个人所有任务
     * @author 叶城廷
     * @version 2019.03.03
     */
	public abstract Result<XmChild> selUserTask(User currentUser, Integer page, int size);

	/**
	 * 
	* @Description: 根据用户查找子项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午6:29:31
	* @return Result<XmChild>
	* @throws
	 */
	public abstract Result<XmChild> findByUser(XmChild xmChild, Integer page, int size);

	/*
	* @Description: 通过任务编号以及接收者获取任务信息
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
	public abstract XmChild getChildByXmUser(String thisXmNumber, String unitId);

	/**
	 * 
	* @Description: 根据用户查找其下发的任务信息
	* @Author 莫东林
	* @date 2019年3月13日下午4:08:23
	* @return Result<XmChild>
	* @throws
	 */
	public abstract Result<XmChild> findIssueByUser(XmChild xmChild, Integer page, int size);

	/**
	 * 
	* @Description: 根据项目编号和教研室编号获取子项目下发的项目列表
	* @Author 莫东林
	* @date 2019年3月13日下午11:20:09
	* @return List<XmChild>
	* @throws
	 */
	public abstract List<XmChild> getChildListByXmUser(String xmNumber, String unitId);


	XmChild getChildByXmUserX(String thisXmNumber, String unitId);


	List<XmChild> findXmChildByNum(String thisUserId, String thisInfoName);


	List<XmChild> allViewXmChildByUser(String thisUserId);


	List<XmChild> findMyIssueByUnit(String thisUserId);


	List<XmChild> findMyIssueByXmNumber(String thisUserId);


	public abstract List<XmChild> findAllXmChild();

	public abstract XmChild getByUserXmNumber(String userName, String xmNumber);

	public abstract void saveQuality(String userName, String xmNumber, String degree);

	XmChild getByUnitXmNumber(String unitId, String xmNumber);

	public abstract void saveQualitys(String userName, String xmNumber, String degree);
	public abstract List<XmChild> findByLikeXmNumber(String xmNumber, String unitId);
	public abstract List<XmChild> findMyIssueByXmNumber2(String thisUserId, String thisUserName);

	
}