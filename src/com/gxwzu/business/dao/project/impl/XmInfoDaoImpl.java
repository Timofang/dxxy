package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmInfoDao;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 项目基本信息DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInfoDaoImpl
 * <br>Date: 2016-11-21下午07:33:03
 * <br>log:
 */
@Repository("xmInfoDao")
public class XmInfoDaoImpl extends BaseDaoImpl<XmInfo> implements XmInfoDao {

	@Override
	public Result<XmInfo> find(XmInfo model, int page, int size) {
		String queryString="from XmInfo model where 1=1 and model.xmNumber not like'%FP%'";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmYear()!=null && !"".equals(model.getXmYear()) && !"-1".equals(model.getXmYear())){
	    	 queryString =queryString+" and model.xmYear=?";
	    	 params.add(model.getXmYear());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		if(model.getIsState()!=null && !"".equals(model.getIsState()) && !"-1".equals(model.getIsState())){
	    	 queryString =queryString+" and model.isState=?";
	    	 params.add(model.getIsState());
	     }
		if(model.getIsCompleted()!=null && !"".equals(model.getIsCompleted()) && !"-1".equals(model.getIsCompleted())){
	    	 queryString =queryString+" and model.isCompleted=?";
	    	 params.add(model.getIsCompleted());
	     }
		
		if(model.getBudgetType()!=null && !"".equals(model.getBudgetType()) && !"-1".equals(model.getBudgetType())){
			queryString =queryString+" and model.budgetType like '%"+model.getBudgetType()+"%'";
	     }
		if(model.getProjectType()!=null && !"".equals(model.getProjectType()) && !"-1".equals(model.getProjectType())){
	    	 queryString =queryString+" and model.projectType=?";
	    	 params.add(model.getProjectType());
	     }
		
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		queryString =queryString+" order by isClose,projectType desc";
		return (Result<XmInfo>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public Result<XmInfo> findPoor(XmInfo model, int page, int size) {
		String queryString="from XmInfo model where 1=1 and model.xmNumber like '%FP%' ";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmYear()!=null && !"".equals(model.getXmYear()) && !"-1".equals(model.getXmYear())){
	    	 queryString =queryString+" and model.xmYear=?";
	    	 params.add(model.getXmYear());
	     }
	     if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		if(model.getIsState()!=null && !"".equals(model.getIsState()) && !"-1".equals(model.getIsState())){
	    	 queryString =queryString+" and model.isState=?";
	    	 params.add(model.getIsState());
	     }
		if(model.getIsCompleted()!=null && !"".equals(model.getIsCompleted()) && !"-1".equals(model.getIsCompleted())){
	    	 queryString =queryString+" and model.isCompleted=?";
	    	 params.add(model.getIsCompleted());
	     }
		
		if(model.getBudgetType()!=null && !"".equals(model.getBudgetType()) && !"-1".equals(model.getBudgetType())){
			queryString =queryString+" and model.budgetType like '%"+model.getBudgetType()+"%'";
	     }
		if(model.getProjectType()!=null && !"".equals(model.getProjectType()) && !"-1".equals(model.getProjectType())){
	    	 queryString =queryString+" and model.projectType=?";
	    	 params.add(model.getProjectType());
	     }
		
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		queryString =queryString+" order by isClose,projectType desc";
		return (Result<XmInfo>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmInfo> findByExample(XmInfo model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmInfo model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmYear()!=null && !"".equals(model.getXmYear()) && !"-1".equals(model.getXmYear())){
	    	 queryString =queryString+" and model.xmYear=?";
	    	 params.add(model.getXmYear());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		if(model.getIsState()!=null && !"".equals(model.getIsState()) && !"-1".equals(model.getIsState())){
	    	 queryString =queryString+" and model.isState=?";
	    	 params.add(model.getIsState());
	     }
		if(model.getIsCompleted()!=null && !"".equals(model.getIsCompleted()) && !"-1".equals(model.getIsCompleted())){
	    	 queryString =queryString+" and model.isCompleted=?";
	    	 params.add(model.getIsCompleted());
	     }
		
		if(model.getBudgetType()!=null && !"".equals(model.getBudgetType()) && !"-1".equals(model.getBudgetType())){
			queryString =queryString+" and model.budgetType like '%"+model.getBudgetType()+"%'";
	     }
		if(model.getProjectType()!=null && !"".equals(model.getProjectType()) && !"-1".equals(model.getProjectType())){
	    	 queryString =queryString+" and model.projectType=?";
	    	 params.add(model.getProjectType());
	     }
		if(model.getXmSourceNumber()!=null && !"".equals(model.getXmSourceNumber())){
	    	 queryString =queryString+" and model.xmSourceNumber=?";
	    	 params.add(model.getXmSourceNumber());
	     }
		
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		queryString =queryString+" order by isClose,projectType desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public List<XmInfo> findCoordinate(XmInfo model) {
		List params = new ArrayList<Object>();
	     String queryString = "from XmInfo model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmYear()!=null && !"".equals(model.getXmYear()) && !"-1".equals(model.getXmYear())){
	    	 queryString =queryString+" and model.xmYear=?";
	    	 params.add(model.getXmYear());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		if(model.getIsState()!=null && !"".equals(model.getIsState()) && !"-1".equals(model.getIsState())){
	    	 queryString =queryString+" and model.isState=?";
	    	 params.add(model.getIsState());
	     }
		if(model.getIsCompleted()!=null && !"".equals(model.getIsCompleted()) && !"-1".equals(model.getIsCompleted())){
	    	 queryString =queryString+" and model.isCompleted=?";
	    	 params.add(model.getIsCompleted());
	     }
		
		if(model.getBudgetType()!=null && !"".equals(model.getBudgetType()) && !"-1".equals(model.getBudgetType())){
			queryString =queryString+" and model.budgetType like '%"+model.getBudgetType()+"%'";
	     }
		if(model.getProjectType()!=null && !"".equals(model.getProjectType()) && !"-1".equals(model.getProjectType())){
	    	 queryString =queryString+" and model.projectType=?";
	    	 params.add(model.getProjectType());
	     }
		if(model.getXmSourceNumber()!=null && !"".equals(model.getXmSourceNumber())){
	    	 queryString =queryString+" and model.xmSourceNumber=?";
	    	 params.add(model.getXmSourceNumber());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		queryString =queryString+" and model.coordinate is not null";
		queryString =queryString+" order by projectType desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	
	@Override
	public List<XmInfo> findPoorXmCoordinate(XmInfo model) {
		List params = new ArrayList<Object>();
	     String queryString = "from XmInfo model where 1=1 and model.xmNumber like'%FP%'";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmYear()!=null && !"".equals(model.getXmYear()) && !"-1".equals(model.getXmYear())){
	    	 queryString =queryString+" and model.xmYear=?";
	    	 params.add(model.getXmYear());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		if(model.getIsState()!=null && !"".equals(model.getIsState()) && !"-1".equals(model.getIsState())){
	    	 queryString =queryString+" and model.isState=?";
	    	 params.add(model.getIsState());
	     }
		if(model.getIsCompleted()!=null && !"".equals(model.getIsCompleted()) && !"-1".equals(model.getIsCompleted())){
	    	 queryString =queryString+" and model.isCompleted=?";
	    	 params.add(model.getIsCompleted());
	     }
		
		if(model.getBudgetType()!=null && !"".equals(model.getBudgetType()) && !"-1".equals(model.getBudgetType())){
			queryString =queryString+" and model.budgetType like '%"+model.getBudgetType()+"%'";
	     }
		if(model.getProjectType()!=null && !"".equals(model.getProjectType()) && !"-1".equals(model.getProjectType())){
	    	 queryString =queryString+" and model.projectType=?";
	    	 params.add(model.getProjectType());
	     }
		if(model.getXmSourceNumber()!=null && !"".equals(model.getXmSourceNumber())){
	    	 queryString =queryString+" and model.xmSourceNumber=?";
	    	 params.add(model.getXmSourceNumber());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		queryString =queryString+" and model.coordinate is not null";
		queryString =queryString+" order by projectType desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	
	@Override
	public XmInfo findById(Integer id) {
		log.debug("getting XmInfo instance with id: " + id);
		try {
			XmInfo instance = (XmInfo) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmInfo", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
