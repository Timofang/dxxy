package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmCapitalDao;
import com.gxwzu.business.model.XmCapital;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 资金信息DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmCapitalDaoImpl
 * <br>Date: 2017-8-17下午05:50:40
 * <br>log:
 */
@Repository("xmCapitalDao")
public class XmCapitalDaoImpl extends BaseDaoImpl<XmCapital> implements XmCapitalDao {

	@Override
	public Result<XmCapital> find(XmCapital model, int page, int size) {
		String queryString="from XmCapital model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		return (Result<XmCapital>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmCapital> findByExample(XmCapital model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmCapital model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmCapital findById(Integer id) {
		log.debug("getting XmCapital instance with id: " + id);
		try {
			XmCapital instance = (XmCapital) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmCapital", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
