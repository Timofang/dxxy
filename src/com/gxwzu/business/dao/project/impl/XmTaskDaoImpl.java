package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmTaskDao;
import com.gxwzu.business.model.XmTask;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 征地任务周报DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmTaskDaoImpl
 * <br>Date: 2017-9-6下午08:43:36
 * <br>log:
 */
@Repository("xmTaskDao")
public class XmTaskDaoImpl extends BaseDaoImpl<XmTask> implements XmTaskDao {

	@Override
	public Result<XmTask> find(XmTask model, int page, int size) {
		String queryString="from XmTask model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getTaskYear()!=null){
	    	 queryString =queryString+" and model.taskYear=?";
	    	 params.add(model.getTaskYear());
	     }
		if(model.getTaskMonth()!=null){
	    	 queryString =queryString+" and model.taskMonth=?";
	    	 params.add(model.getTaskMonth());
	     }
		if(model.getTaskType()!=null){
	    	 queryString =queryString+" and model.taskType=?";
	    	 params.add(model.getTaskType());
	     }
		
		queryString =queryString+" order by model.taskType desc";
		return (Result<XmTask>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmTask> findByExample(XmTask model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmTask model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getTaskYear()!=null){
	    	 queryString =queryString+" and model.taskYear=?";
	    	 params.add(model.getTaskYear());
	     }
		if(model.getTaskMonth()!=null){
	    	 queryString =queryString+" and model.taskMonth=?";
	    	 params.add(model.getTaskMonth());
	     }
		if(model.getTaskType()!=null){
	    	 queryString =queryString+" and model.taskType=?";
	    	 params.add(model.getTaskType());
	     }
		
		queryString =queryString+" order by model.taskType desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmTask findById(Integer id) {
		log.debug("getting XmTask instance with id: " + id);
		try {
			XmTask instance = (XmTask) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmTask", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
