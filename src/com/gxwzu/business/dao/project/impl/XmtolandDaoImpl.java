package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmtolandDao;
import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 项目-征地项目关联DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmtolandDaoImpl
 * <br>Date: 2017-9-5上午11:25:29
 * <br>log:
 */
@Repository("xmtolandDao")
public class XmtolandDaoImpl extends BaseDaoImpl<Xmtoland> implements XmtolandDao {

	@Override
	public Result<Xmtoland> find(Xmtoland model, int page, int size) {
		String queryString="from Xmtoland model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		return (Result<Xmtoland>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<Xmtoland> findByExample(Xmtoland model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from Xmtoland model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public Xmtoland findById(Integer id) {
		log.debug("getting Xmtoland instance with id: " + id);
		try {
			Xmtoland instance = (Xmtoland) getHibernateTemplate().get(
					"com.gxwzu.business.model.Xmtoland", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
