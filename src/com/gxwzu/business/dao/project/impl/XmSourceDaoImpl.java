package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmSourceDao;
import com.gxwzu.business.model.XmSource;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 督查事项批示来源DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmSourceDaoImpl
 * <br>Date: 2017-11-28上午11:23:50
 * <br>log:
 */
@Repository("xmSourceDao")
public class XmSourceDaoImpl extends BaseDaoImpl<XmSource> implements XmSourceDao{

	@Override
	public Result<XmSource> find(XmSource model, int page, int size) {
		String queryString="from XmSource model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXsNumber()!=null && !"".equals(model.getXsNumber())){
	    	 queryString =queryString+" and model.xsNumber=?";
	    	 params.add(model.getXsNumber());
	     }
	     if(model.getXsName()!=null && !"".equals(model.getXsName())){
	    	 queryString =queryString+" and model.xsName like '%" + model.getXsName() + "%'";
	     }
	     if(model.getXsLevel()!=null && !"".equals(model.getXsLevel())){
	    	 queryString =queryString+" and model.xsLevel like '%" + model.getXsLevel() + "%'";
	     }
	     if(model.getXsIndustry()!=null && !"".equals(model.getXsIndustry())){
	    	 queryString =queryString+" and model.xsIndustry like '%" + model.getXsIndustry() + "%'";
	     }
	     if(model.getXsArea()!=null && !"".equals(model.getXsArea())){
	    	 queryString =queryString+" and model.xsArea like '%" + model.getXsArea() + "%'";
	     }
	     queryString =queryString+" order by model.createTime desc";
		return (Result<XmSource>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmSource> findByExample(XmSource model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmSource model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXsNumber()!=null && !"".equals(model.getXsNumber())){
	    	 queryString =queryString+" and model.xsNumber=?";
	    	 params.add(model.getXsNumber());
	     }
	     if(model.getXsName()!=null && !"".equals(model.getXsName())){
	    	 queryString =queryString+" and model.xsName like '%" + model.getXsName() + "%'";
	     }
	     if(model.getXsLevel()!=null && !"".equals(model.getXsLevel())){
	    	 queryString =queryString+" and model.xsLevel like '%" + model.getXsLevel() + "%'";
	     }
	     if(model.getXsIndustry()!=null && !"".equals(model.getXsIndustry())){
	    	 queryString =queryString+" and model.xsIndustr like '%" + model.getXsIndustry() + "%'";
	     }
	     if(model.getXsArea()!=null && !"".equals(model.getXsArea())){
	    	 queryString =queryString+" and model.xsArea like '%" + model.getXsArea() + "%'";
	     }
	     queryString =queryString+" order by model.createTime desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmSource findById(Integer id) {
		log.debug("getting XmSource instance with id: " + id);
		try {
			XmSource instance = (XmSource) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmSource", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
}
