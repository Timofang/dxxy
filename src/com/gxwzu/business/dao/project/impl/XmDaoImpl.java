package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.AttachmentDao;
import com.gxwzu.business.dao.project.InstructionsDao;
import com.gxwzu.business.dao.project.MaterialsDao;
import com.gxwzu.business.dao.project.XmChildDao;
import com.gxwzu.business.dao.project.XmDao;
import com.gxwzu.business.dao.project.XmInfoDao;
import com.gxwzu.business.model.Attachment;
import com.gxwzu.business.model.Instructions;
import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.web.listener.UserBindingListener;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;

@Repository("xmDao")
public class XmDaoImpl extends BaseDaoImpl<Xm> implements XmDao {
    @Autowired
	private XmChildDao xmChildDao;
    @Autowired
    private MaterialsDao  materialsDao;
    @Autowired
    private AttachmentDao  attachmentDao;
    @Autowired
    private InstructionsDao  instructionsDao;
    

	/**
	 * 获取指定教研室所对应的项目信息
	 * @author SunYi
	 * @Date 2019年3月1日下午2:50:14
	 * @return
	 */
	@Override
	public Result<Xm> selXmByUnit(Xm model, int page, int size) {
		// 获取session存的当前用户信息，根据当前用户的用户ID查询对应的项目信息
		//HttpSession session = ServletActionContext.getRequest().getSession();
		//String helpUserName = (String) session.getAttribute("helpUserName");
		String queryString = "from XmChild model where 1= 1 ORDER BY model.publishTime DESC" ;
		int start=(page-1)*size;
		int limit =size;
		
		//String queryString2 = "from Xm model where xmNumber =" + "'xmNumber'";
		
		// 条件查询（暂时无法使用）
		List params =new ArrayList<Object>();
		return (Result<Xm>)super.find(queryString, params.toArray(),null, start, limit);
	}

	/**
	 * 获取所有项目信息
	 * @author SunYi
	 * @Date 2019年4月19日下午4:22:05
	 */
	@Override
	public Result<Xm> find(Xm model, int page, int size) {
		//获取登陆用户名
//		String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from Xm model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
/*		queryString = queryString + "and model.username = ?";
		params.add(username);*/
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
		}
		queryString  = queryString +" ORDER BY model.publishTime DESC";
		return (Result<Xm>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	/**
	 * 
	 * @Description 获取最大项目编号
	 * @author 莫东林
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */
	public Xm getMaxXmNumber(){
		String sql = "select xm_number from xm where id = (select max(id) from xm)";
		String sqls = "select username from xm where id = (select max(id) from xm)";
		Result rs = findBySQL(sql, null, 0, 0);
		Result rss = findBySQL(sqls, null, 0, 0);
		Xm xm = new Xm();
		//如果数据库无数据
		if(rs.getData().size() == 0 || rss.getData().size() == 0){
			return null;
		}else{
			xm.setXmNumber((String) rs.getData().get(0));
			xm.setUsername((String) rss.getData().get(0));
		}
		return xm;
	}

	/**
	 * 删除任务以及任务关联的所有信息
	 * @author SunYi
	 * @Date 2019年4月19日下午9:55:51
	 */
	@Transactional
	public void delete(String xmNumber) {
		Transaction tx = getSessionFactory().openSession().beginTransaction();
		try {
		 // 删除任务主表信息
	     List<Xm> find = find("from Xm where xmNumber='"+xmNumber+"'");
	     Xm xm = find.get(0);
	     remove(xm);
	     
	     // 删除院下发到教研室或个人的任务子表信息
	     List<XmChild> xmChildList = xmChildDao.find("from XmChild where xmNumber = '"+xmNumber+"'");
	     for (int i = 0; i < xmChildList.size(); i++) {
	    	 XmChild xmChild = xmChildList.get(i);
	    	 xmChildDao.remove(xmChild);
	     }
	     
	     // 删除教研室下发到个人的任务子表信息
	     List<XmChild> unitXmChildList = xmChildDao.find("from XmChild where xmChildNumber like '"+xmNumber+"%'");
	     for (int i = 0; i < unitXmChildList.size(); i++) {
	    	 XmChild xmChild = unitXmChildList.get(i);
	    	 xmChildDao.remove(xmChild);
	     }
	     
	     // 删除院下发材料信息
	     List<Materials> materialsList =materialsDao.find("from Materials where xmId = '"+xmNumber+"'");
	     for (int i = 0; i < materialsList.size(); i++) {
	    	 Materials materials =materialsList.get(i);
	    	 materialsDao.remove(materials);
	     }
	     
	     // 删除教研室下发材料信息
	     List<Materials> unitMaterialsList =materialsDao.find("from Materials where xmId like '"+xmNumber+"%'");
	     for (int i = 0; i < unitMaterialsList.size(); i++) {
	    	 Materials materials =unitMaterialsList.get(i);
	    	 materialsDao.remove(materials);
	     }
	     
	     // 删除院下发的附件信息
	     List<Attachment> attachmentList = attachmentDao.find("from Attachment where xmNumber = '"+xmNumber+"'");
	     for (int i = 0; i < attachmentList.size(); i++) {
	    	 Attachment attachment = attachmentList.get(i);
	    	 attachmentDao.remove(attachment);
	     }
	     // 删除教研室下发的附件信息
	     List<Attachment> unitAttachmentList = attachmentDao.find("from Attachment where xmNumber like '"+xmNumber+"%'");
	     for (int i = 0; i < unitAttachmentList.size(); i++) {
	    	 Attachment attachment = unitAttachmentList.get(i);
	    	 attachmentDao.remove(attachment);
	     }
	     
	     // 删除院下发的批示信息
	     List<Instructions> instructionsList = instructionsDao.find("from Instructions where xmNumber = '"+xmNumber+"'");
	     for (int i = 0; i < instructionsList.size(); i++) {
	    	 Instructions instructions = instructionsList.get(i);
	    	 instructionsDao.remove(instructions);
	     }
	     
	     // 删除教研室下发的批示信息
	     List<Instructions> unitInstructionsList = instructionsDao.find("from Instructions where xmNumber like '"+xmNumber+"%'");
	     for (int i = 0; i < unitInstructionsList.size(); i++) {
	    	 Instructions instructions = unitInstructionsList.get(i);
	    	 instructionsDao.remove(instructions);
	     }
	     tx.commit();
		}catch (Exception e) {
			tx.rollback();
		}
	     
	}

	/**
	 * 
	* @Description: 根据任务编号获取
	* @Author 莫东林
	* @date 2019年3月5日下午8:16:41
	* @throws
	 */
	@Override
	public Xm getByXmNumber(String xmNumber) {
		String hql = "from Xm model where 1 = 1 and xmNumber = '"+xmNumber+"' ORDER BY model.publishTime DESC ";
		List<Xm> list = super.excuteQuery(hql);
		return list.get(0);
	}


	/**
	 * 
	* @Description: 教研室下发任务时更新父级项目
	* @Author 莫东林
	* @date 2019年3月12日下午8:48:13
	* @throws
	 */
	@Override
	public void updateXmChild(XmChild c) {
		getHibernateTemplate().update(c);
	}

	/**
	 * 
	* @Description: 根据用户查找下发的任务信息
	* @Author 莫东林
	* @date 2019年3月13日下午5:36:47
	* @return Result<Xm>
	* @throws
	 */
	@Override
	public Result<Xm> findIssueByUser(Xm model, Integer page, int size) {
		String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from Xm model where 1=1 and username = ?";
		int start=(page-1)*size;
		int limit =size;
		List params =new ArrayList<Object>();
		params.add(username);
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		queryString  = queryString +" ORDER BY model.publishTime DESC";
		return (Result<Xm>)super.find(queryString, params.toArray(), null, start, limit);
	}
	/**
	 * 查询个人院任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public   List xmstatisticsDetails(String username){
		 
		List ylist = new ArrayList();
	
		/*接收人姓名*/
		String receiver=null;
		
		/*下发人姓名*/
		String Lowerrunner=null;
		/*查询接收者姓名*/
		String hql0="from User u where u.helpUserName='"+username+"'";
		List <User>u=excuteQuery(hql0);
		if(u.size()>0){
			receiver=u.get(0).getHelpName();
		}  

		/*查询院项目统计详情*/
		String hql="from Xm xm where xm.receiver like '%"+username+"%'";
		List <Xm>yl=excuteQuery(hql);		
		if(yl.size()>0){
	
		for(int i=0;i<yl.size();i++){
			Map yuData = new HashMap();
			/*查询下发人姓名*/
			String hql2="from User u where u.helpUserName='"+yl.get(i).getUsername()+"'";
			List <User>u2=excuteQuery(hql2);
			if(u2.size()>0){Lowerrunner=u2.get(0).getHelpName();}
			yuData.put("receiver",receiver);
			yuData.put("name",yl.get(i).getName());
			yuData.put("requires",yl.get(i).getRequires());
			yuData.put("status",yl.get(i).getStatus());
			yuData.put("Lowerrunner",Lowerrunner);
			yuData.put("degree",yl.get(i).getDegree());
			/*查询个人任务质量*/
			String hql3="from XmChild xc where xc.xmNumber='"+yl.get(i).getXmNumber()+"' and xc.userName='"+username+"'";
			List <XmChild>q=excuteQuery(hql3);
			if(q.size()>0){
			yuData.put("quality",q.get(0).getQuality());
			ylist.add(yuData);
			}
		}
		
		}
	
		return ylist;
	}
	/**
	 * 查询院发布给教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	
	public List xmToUnitStatisticsDetails(String username){
		List ylist = new ArrayList();
	
		/*接收人姓名*/
		String receiver=null;
		
		/*下发人姓名*/
		String Lowerrunner=null;
		/*查询个人所属教研室*/
		String hq="from Unit un where un.unitPerson='"+username+"'";
		List <Unit>u1=excuteQuery(hq);
		if(u1.size()>0){
		/*查询接收者姓名*/
		String hql0="from User u where u.helpUserName='"+username+"'";
		List <User>u=excuteQuery(hql0);
		if(u.size()>0){
			receiver=u.get(0).getHelpName();
		}  
		/*查询院项目统计详情*/
		String hql="from Xm xm where xm.receiverUnit like '%"+u1.get(0).getUnitId()+"%' ORDER BY xm.publishTime DESC";
		List <Xm>yl=excuteQuery(hql);
		if(yl.size()>0){
		for(int i=0;i<yl.size();i++){
			Map yuData = new HashMap();
			/*查询下发人姓名*/
			String hql2="from User u where u.helpUserName='"+yl.get(i).getUsername()+"'";
			List <User>u2=excuteQuery(hql2);
			if(u2.size()>0){Lowerrunner=u2.get(0).getHelpName();}
			yuData.put("receiver",receiver);
			yuData.put("name",yl.get(i).getName());
			yuData.put("requires",yl.get(i).getRequires());
			yuData.put("status",yl.get(i).getStatus());
			yuData.put("Lowerrunner",Lowerrunner);
			yuData.put("degree",yl.get(i).getDegree());
			/*查询个人任务质量*/
			String hql3="from XmChild xc where xc.xmNumber='"+yl.get(i).getXmNumber()+"' and xc.userName='"+username+"'";
			List <XmChild>q=excuteQuery(hql3);
			if(q.size()>0){
			yuData.put("quality",q.get(0).getQuality());
			ylist.add(yuData);
			}}}}
		return ylist;
	}
}
