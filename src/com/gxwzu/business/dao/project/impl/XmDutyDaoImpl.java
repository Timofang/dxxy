package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmDutyDao;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 责任单位/责任人DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmDutyDaoImpl
 * <br>Date: 2017-8-17下午05:50:31
 * <br>log:
 */
@Repository("xmDutyDao")
public class XmDutyDaoImpl extends BaseDaoImpl<XmDuty> implements XmDutyDao  {

	@Override
	public Result<XmDuty> find(XmDuty model, int page, int size) {
		String queryString="from XmDuty model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getPeopleId()!=null){
	    	 queryString =queryString+" and model.peopleId=?";
	    	 params.add(model.getPeopleId());
	     }
		if(model.getType()!=null){
	    	 queryString =queryString+" and model.type=?";
	    	 params.add(model.getType());
	     }
		if(model.getXbUnit()!=null){
	    	 queryString =queryString+" and model.xbUnit=?";
	    	 params.add(model.getXbUnit());
	     }
		
		queryString =queryString+" order by model.type desc";
		return (Result<XmDuty>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmDuty> findByExample(XmDuty model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmDuty model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
	     if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getPeopleId()!=null){
	    	 queryString =queryString+" and model.peopleId=?";
	    	 params.add(model.getPeopleId());
	     }
		if(model.getType()!=null){
	    	 queryString =queryString+" and model.type=?";
	    	 params.add(model.getType());
	     }
		if(model.getXbUnit()!=null){
	    	 queryString =queryString+" and model.xbUnit=?";
	    	 params.add(model.getXbUnit());
	     }
		
		queryString =queryString+" order by model.type desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmDuty findById(Integer id) {
		log.debug("getting XmDuty instance with id: " + id);
		try {
			XmDuty instance = (XmDuty) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmDuty", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
