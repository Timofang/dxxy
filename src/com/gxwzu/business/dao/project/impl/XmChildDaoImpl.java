package com.gxwzu.business.dao.project.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmChildDao;
import com.gxwzu.business.dao.project.XmDao;
import com.gxwzu.business.dao.project.XmInfoDao;
import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.web.listener.UserBindingListener;
import com.gxwzu.system.model.user.User;

@Repository("xmChildDao")
public class XmChildDaoImpl extends BaseDaoImpl<XmChild> implements XmChildDao {
	
	/**
	 * 统计所有任务的完成情况
	 * @author SunYi
	 * @Date 2019年3月18日下午9:53:11
	 */
	@Override
	public List<XmChild> getXmStatistics() {
		String queryString="from XmChild where 1=1 GROUP BY userName";
		List<XmChild> list = super.excuteQuery(queryString);
		return list;
	}
	
	
	/**
	 * 查询每个用户的所有任务信息
	 * @author SunYi
	 * @Date 2019年3月18日下午10:33:04
	 */
	@Override
	public List<XmChild> getUserXmChild(String userName) {
		String queryString = "from XmChild where userName = '"+userName+"'";
		List<XmChild> list = super.excuteQuery(queryString);
		return list;
		
	}
	
	
	/**
	 * 根据项目编号和接收人获取子项目文件信息
	 * @author SunYi
	 * @Date 2019年3月11日下午11:09:46
	 */
	@Override
	public XmChild getFileByXmNumber(String xmChildNumber) {
		String hql = "from XmChild where xmChildNumber='"+xmChildNumber+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}

	/**
	 * @throws
	 * @Description:
	 * @Author 莫东林
	 * @date 2019年3月12日下午9:30:16
	 */
	@Override
	public Result<XmChild> find(XmChild model, int page, int size) {
		String queryString;
		List params = new ArrayList<Object>();
		int start = (page - 1) * size;
		int limit = size;

		if (model.getXm() == null) {
			queryString = "from XmChild model where 1=1";

			if (model.getId() != null) {
				queryString = queryString + " and model.id=?";
				params.add(model.getId());
			}
			if (model.getXmNumber() != null) {
				queryString = queryString + " and model.xmNumber=?";
				params.add(model.getXmNumber());
			}
		}
		/**
		 * @Description: XmChild_projectList.jsp页面的条件查询
		 * @Author 黄结
		 */
		else {

			queryString = "from XmChild model where model.xmNumber in (select model2.xmNumber from Xm model2 where 1=1 ";

			//用户选择任务类型
			if (model.getXm().getType() != null) {
				String xmType = model.getXm().getType();
				logger.info("XmChildDaoImpl:***xmType***" + xmType);

				queryString = queryString + " and model2.type=?";
				params.add(xmType);
			}
			//用户选择项目完成情况
			if (model.getXm().getStatus() != null) {
				Integer xmStatus = model.getXm().getStatus();
				logger.info("XmChildDaoImpl:***xmStatus***" + xmStatus);

				queryString = queryString + " and model2.status=?";
				params.add(xmStatus);
			}
			//用户输入项目名称
			if (model.getXm().getName() != null
					&& !"".equals(model.getXm().getName())) {
				String xmName = model.getXm().getName();
				logger.info("XmChildDaoImpl:***xmName***" + xmName);

				queryString = queryString + " and model2.name like'%" + xmName + "%'";
			}
			queryString = queryString + ")";

		}

		return (Result<XmChild>) super.find(queryString, params.toArray(), null, start, limit);
	}

	/**
     * 查询员工个人所有任务
     * @author 叶城廷
     * @version 2019.03.03
     */
	@SuppressWarnings("unchecked")
	@Override
	public Result<XmChild> selUserTask(User currentUser, Integer page, int size) {
		/*String queryString ="select new XmChild(c.id,c.xmNumber,c.unitId,c.userName,c.unitParent,c.status,"+ 
				            "c.require) from XmChild c where m.xmNumber=c.xmNumber and c.userName="+currentUser.getHelpUserName()+"'";*/
		String queryString ="SELECT from XmChild c";
		int start=(page-1)*size;
		int limit =size;
	
		Result<XmChild> sample =(Result<XmChild>)super.find(queryString, null, null, start, limit);
		System.out.println(sample);
		//暂无查询条件
		return sample;
	}
	
	/**
	 * 
	* @Description: 根据用户查找子项目信息
	* @Author 莫东林
	* @date 2019年3月12日下午6:29:31
	* @return Result<XmChild>
	* @throws
	 */
	@Override
	public Result<XmChild> findByUser(XmChild model, Integer page, int size) {
		String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from XmChild model where 1=1 and userName = ? ";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		params.add(username);
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		queryString  = queryString +" ORDER BY model.publishTime DESC";
		return (Result<XmChild>)super.find(queryString, params.toArray(), null, start, limit);
	}


	/*
	* @Description: 通过任务编号以及接收者获取任务信息    方法同名，暂时
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
//	@Override
//	public XmChild getChildByXmUser(String thisXmNumber, String unitId) {
//		String hql = "from XmChild where 1 = 1 and xmNumber = '"+thisXmNumber+"' and unitId = '"+unitId+"'";
//		List<XmChild> list = super.excuteQuery(hql);
//		return list.get(0);
//	}
	
	/*
	* @Description: 通过任务编号以及接收者获取任务信息X
	* @Author 莫东林
	* @date 2019年3月12日下午7:21:19
	* @return String
	* @throws
	 */
	@Override
	public XmChild getChildByXmUserX(String thisXmNumber, String unitId) {
		String hql = "from XmChild where 1 = 1 and xmChildNumber = '"+thisXmNumber+"' and unitParent = '"+unitId+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}

	/**
	 * 
	* @Description: 根据用户查找其下发的任务信息
	* @Author 莫东林
	* @date 2019年3月13日下午4:08:23
	* @return Result<XmChild>
	* @throws
	 */
	@Override
	public Result<XmChild> findIssueByUser(XmChild model, Integer page, int size) {
		String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from XmChild model where 1=1 and userName = ? and issue = 1";
		int start=(page-1)*size;
		int limit =size;
		List params =new ArrayList<Object>();
		params.add(username);
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		queryString  = queryString +" ORDER BY model.publishTime DESC";
		return (Result<XmChild>)super.find(queryString, params.toArray(), null, start, limit);
	}

	/**
	 * 
	* @Description: 根据项目编号和教研室编号获取子项目下发的项目列表    方法同名，暂时注释
	* @Author 莫东林
	* @date 2019年3月13日下午11:20:09
	* @return List<XmChild>
	* @throws
	 */
//	@Override
//	public List<XmChild> getChildListByXmUser(String xmNumber, String unitId) {
//		String hql = "from XmChild where 1 = 1 and xmChildNumber  LIKE '%"+xmNumber+"%' and unitParent = '"+unitId+"'";
//		List<XmChild> list = super.excuteQuery(hql);
//		return list;
//	}
	
	/**
	 * 查询所有子项目信息
	 * @author 叶城廷（不同参查询方法）
	 * @version 2019.03.14
	 * @return
	 */
	public List<XmChild> findAllXmChild() {
//		String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from XmChild  ORDER BY publishTime DESC";
		return find(queryString);
	}

	/**
	 * 查询对应User的所有子项目
	 * @param thisUserId 要查询的UserName
	 * @param thisInfoName
	 * @author 叶城廷
	 * @version 2019.03.13
	 * @return
	 */
	@Override
	public List<XmChild> findXmChildByNum(String thisUserId, String thisInfoName) {
		String queryString="";
		if(thisUserId==null){
			 queryString="from XmChild where xmChildNumber='"+thisInfoName+"'  ORDER BY publishTime DESC";
		}else{
			 queryString="from XmChild where xmNumber='"+thisUserId+"'  ORDER BY publishTime DESC";
		}

		return find(queryString);
	}

	/**
	 * 根据用户Id查询所有子项目（同参）
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param thisXmNumber
	 * @param unitId
	 * @return
	 */
	@Override
	public XmChild getChildByXmUser(String thisXmNumber, String unitId) {
		String hql = "from XmChild where 1 = 1 and xmNumber = '"+thisXmNumber+"' and unitId = '"+unitId+"'  ORDER BY publishTime DESC";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}

	/**
	 * 查询该用户名下的所有子项目信息（不同参查询方法）
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<XmChild> allViewXmChildByUser(String thisUserId) {
		String queryString="from XmChild where userName='"+thisUserId+"'  ORDER BY publishTime DESC";
		return find(queryString);
	}

	/**
	 * 根据当前教研室领导账号查询所有下发任务
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<XmChild> findMyIssueByUnit(String thisUserId) {
		//String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from XmChild model where 1=1 and userName = '"+thisUserId+"' and issue = 1  ORDER BY publishTime DESC";
		return find(queryString);
	}


	public List<XmChild> getChildListByXmUser(String xmNumber, String unitId) {
		String hql = "from XmChild where 1 = 1 and xmChildNumber  LIKE '%"+xmNumber+"%' and unitParent = '"+unitId+"'  ORDER BY publishTime DESC";
		List<XmChild> list = super.excuteQuery(hql);
		return list;
	}
	

	/**
	 * 根据当前教研室领导点击的详细下发任务查询详细子项目信息
	 * @param thisUserId
	 * @return
	 */
	@Override
	public List<XmChild> findMyIssueByXmNumber(String thisUserId) {
		//String username = UserBindingListener.getUserBindingListener().getUsername();
		String queryString="from XmChild  where 1=1 and xmNumber = '"+thisUserId+"' and issue = 1  ORDER BY publishTime DESC";
		return find(queryString);

	}

	/**
	 * 
	* @Description: 根据用户工号和项目编号查询xmChild
	* @Author 莫东林
	* @date 2019年3月18日下午9:41:00
	* @return XmChild
	* @throws
	 */
	@Override
	public XmChild getByUserXmNumber(String userName, String xmNumber) {
		String hql = "from XmChild where 1 = 1 and userName = '"+userName+"' and xmNumber = '"+xmNumber+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}
	
	@Override
	public XmChild getByUnitXmNumber(String unitId, String xmNumber) {
		String hql = "from XmChild where 1 = 1 and unitId = '"+unitId+"' and xmNumber = '"+xmNumber+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}


	@Override
	public void saveQuality(String userName, String xmNumber, String quality) {
		XmChild xc = getByUserXmNumber(userName, xmNumber);
		xc.setQuality(quality);
		super.update(xc);
	}

	@Override
	public void saveQualitys(String userName, String xmNumber, String degree) {
		XmChild xc = getByUserXmNumbers(userName, xmNumber);
		xc.setQuality(degree);
		super.update(xc);
	}

	private XmChild getByUserXmNumbers(String userName, String xmNumber) {
		String hql = "from XmChild where 1 = 1 and userName = '"+userName+"' and xmChildNumber = '"+xmNumber+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list.get(0);
	}


	/**
	 * 查询个人教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public  List unitstatisticsDetails(String username){
	
		List unitlist = new ArrayList();
		
		/*接收人姓名*/
		String receiver=null;
		
		/*下发人姓名*/
		String Lowerrunner=null;
		/*查询接收者姓名*/
		String hql0="from User u where u.helpUserName='"+username+"'";
		List <User>u=excuteQuery(hql0);
		if(u.size()>0){
			receiver=u.get(0).getHelpName();
		}  
		/*查询个人所有教研室下发任务*/
		String hql1="from XmChild xc where xc.userName='"+username+"' and xc.acceptstatus=0 and xc.unitParent is not null";
		List <XmChild>un=excuteQuery(hql1);
		if(un.size()>0){
			
		for(int i=0;i<un.size();i++){
			
			String xmNumber=un.get(i).getXmChildNumber().replaceAll(un.get(i).getUserName(),"");
			String hq="from XmChild xc where xc.xmNumber='"+xmNumber+"'";
			List <XmChild>rq=excuteQuery(hq);
			String requires=rq.get(i).getRequires();
			
			
			Map yuData = new HashMap();
			yuData.put("receiver",receiver);
			yuData.put("name",un.get(i).getXmChildName());
			yuData.put("requires",requires);
			yuData.put("status",un.get(i).getStatus());
			
			/*根据unitparent查询Unit表用户名*/
			String hql2="from Unit u where u.unitId='"+un.get(i).getUnitParent()+"'";
			List<Unit> unit=excuteQuery(hql2);
			if(un.size()>0){
				/*根据Unit表用户名查询用户表用户姓名*/
				String hql3="from User u where u.helpUserName='"+unit.get(0).getUnitPerson()+"'";
				List <User>us=excuteQuery(hql3);
				if (us.size()>0){Lowerrunner=us.get(0).getHelpName();}
			}
			yuData.put("Lowerrunner",Lowerrunner);
			yuData.put("degree",un.get(i).getDegree());
			yuData.put("quality",un.get(i).getQuality());
			unitlist.add(yuData);
		}
		
			
		}
		   return unitlist;
	}


	/**
	 * 模糊查询所有与该项目相关的所有子项目
	* @Description: TODO
	* @Author 叶城廷
	* @date 2019年4月11日下午2:20:36
	* @throws
	 */
	@Override
	public List<XmChild> findByLikeXmNumber(String xmNumber,String unitId) {
		String hql = "from XmChild where 1 = 1 and xmChildNumber like '"+xmNumber+"%' and unitParent = '"+unitId+"'";
		List<XmChild> list = super.excuteQuery(hql);
		return list;
	}


	@Override
	public List<XmChild> findMyIssueByXmNumber2(String thisUserId, String thisUserName) {
				String queryString="from XmChild  where 1=1 and xmNumber = '"+thisUserId+"' and issue = 1 and userName='"+thisUserName+"' ORDER BY publishTime DESC";
				return find(queryString);
	}
	
}
