package com.gxwzu.business.dao.project.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.InstructionsDao;
import com.gxwzu.business.model.Instructions;
import com.gxwzu.core.dao.impl.BaseDaoImpl;

/**
 * 任务批示Dao层接口实现类
 * @ClassName: InstructionsDao
 * @author SunYi
 * @Date 2019年4月8日上午10:05:46
 */
@Repository
public class InstructionsDaoImpl extends BaseDaoImpl<Instructions> implements InstructionsDao {

	/**
	 * 获取指定任务编号的任务批示
	 * @author SunYi
	 * @Date 2019年4月8日上午11:23:00
	 */
	@Override
	public List<Instructions> getInstructionsByXmNumber(String thisXmNumber) {
		String hql = "from Instructions where xmNumber = '"+ thisXmNumber +"'"; 
		List<Instructions> list = super.excuteQuery(hql);
		return list;
	}

	
}
