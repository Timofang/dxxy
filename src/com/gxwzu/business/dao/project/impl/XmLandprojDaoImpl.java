package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmLandprojDao;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 征地项目DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmLandprojDaoImpl
 * <br>Date: 2017-9-5上午10:51:14
 * <br>log:
 */
@Repository("xmLandprojDao")
public class XmLandprojDaoImpl extends BaseDaoImpl<XmLandproj> implements XmLandprojDao{

	@Override
	public Result<XmLandproj> find(XmLandproj model, int page, int size) {
		String queryString="from XmLandproj model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		if(model.getLandType()!=null && !"".equals(model.getLandType())){
	    	 queryString =queryString+" and model.landType like '%"+model.getLandType()+"%'";
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getLaYear()!=null && !"".equals(model.getLaYear()) && !"-1".equals(model.getLaYear())){
	    	 queryString =queryString+" and model.laYear=?";
	    	 params.add(model.getLaYear());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		queryString =queryString+" order by model.createTime desc";
		return (Result<XmLandproj>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmLandproj> findByExample(XmLandproj model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmLandproj model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		if(model.getLandType()!=null && !"".equals(model.getLandType())){
	    	 queryString =queryString+" and model.landType like '%"+model.getLandType()+"%'";
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getLaYear()!=null && !"".equals(model.getLaYear()) && !"-1".equals(model.getLaYear())){
	    	 queryString =queryString+" and model.laYear=?";
	    	 params.add(model.getLaYear());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		queryString =queryString+" order by model.createTime desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public List<XmLandproj> findCoordinate(XmLandproj model) {
		List params = new ArrayList<Object>();
	     String queryString = "from XmLandproj model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getLandNumber()!=null){
	    	 queryString =queryString+" and model.landNumber=?";
	    	 params.add(model.getLandNumber());
	     }
		if(model.getLandType()!=null && !"".equals(model.getLandType())){
	    	 queryString =queryString+" and model.landType like '%"+model.getLandType()+"%'";
	     }
		if(model.getName()!=null && !"".equals(model.getName()) && !"-1".equals(model.getName())){
	    	 queryString =queryString+" and model.name like '%"+model.getName()+"%'";
	     }
		if(model.getLaYear()!=null && !"".equals(model.getLaYear()) && !"-1".equals(model.getLaYear())){
	    	 queryString =queryString+" and model.laYear=?";
	    	 params.add(model.getLaYear());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getAddress()!=null && !"".equals(model.getAddress()) && !"-1".equals(model.getAddress())){
	    	 queryString =queryString+" and model.address like '%"+model.getAddress()+"%'";
	     }
		queryString =queryString+" and model.coordinate is not null";
		queryString =queryString+" order by model.createTime desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmLandproj findById(Integer id) {
		log.debug("getting XmLandproj instance with id: " + id);
		try {
			XmLandproj instance = (XmLandproj) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmLandproj", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
