package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmInvestDao;
import com.gxwzu.business.model.XmInvest;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 资金投资进度DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmInvestDaoImpl
 * <br>Date: 2017-8-17下午06:10:39
 * <br>log:
 */
@Repository("xmInvestDao")
public class XmInvestDaoImpl extends BaseDaoImpl<XmInvest> implements XmInvestDao {

	@Override
	public Result<XmInvest> find(XmInvest model, int page, int size) {
		String queryString="from XmInvest model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		queryString =queryString+" order by investYear,investMonth desc";
		return (Result<XmInvest>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmInvest> findByExample(XmInvest model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmInvest model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
	     if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
	     if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
	     queryString =queryString+" order by investYear,investMonth desc";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmInvest findById(Integer id) {
		log.debug("getting XmInvest instance with id: " + id);
		try {
			XmInvest instance = (XmInvest) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmInvest", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
