package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.XmReplyDao;
import com.gxwzu.business.model.XmReply;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;

/**
 * 回复DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: XmReplyDaoImpl
 * <br>Date: 2017-9-9下午03:30:11
 * <br>log:
 */
@Repository("xmReplyDao")
public class XmReplyDaoImpl extends BaseDaoImpl<XmReply> implements XmReplyDao{

	@Override
	public Result<XmReply> find(XmReply model, int page, int size) {
		String queryString="from XmReply model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getScheduleId()!=null){
	    	 queryString =queryString+" and model.scheduleId=?";
	    	 params.add(model.getScheduleId());
	     }
		if(model.getReUserId()!=null){
	    	 queryString =queryString+" and model.reUserId=?";
	    	 params.add(model.getReUserId());
	     }
		queryString =queryString+" order by model.createTime ASC";
		return (Result<XmReply>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<XmReply> findByExample(XmReply model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from XmReply model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getXmNumber()!=null){
	    	 queryString =queryString+" and model.xmNumber=?";
	    	 params.add(model.getXmNumber());
	     }
		if(model.getScheduleId()!=null){
	    	 queryString =queryString+" and model.scheduleId=?";
	    	 params.add(model.getScheduleId());
	     }
		if(model.getReUserId()!=null){
	    	 queryString =queryString+" and model.reUserId=?";
	    	 params.add(model.getReUserId());
	     }
		queryString =queryString+" order by model.createTime ASC";
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public XmReply findById(Integer id) {
		log.debug("getting XmReply instance with id: " + id);
		try {
			XmReply instance = (XmReply) getHibernateTemplate().get(
					"com.gxwzu.business.model.XmReply", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	
}
