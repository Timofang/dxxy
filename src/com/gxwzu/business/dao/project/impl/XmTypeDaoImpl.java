package com.gxwzu.business.dao.project.impl;

import com.gxwzu.business.dao.project.XmTypeDao;
import com.gxwzu.business.model.XmType;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;
import org.springframework.stereotype.Repository;

/**
 * @Author: soldier
 * @Date: 2019/3/5 9:01
 * @Desc:
 */
@Repository("xmTypeDao")
public class XmTypeDaoImpl extends BaseDaoImpl<XmType> implements XmTypeDao {

    @Override
    public Result<XmType> find(XmType model, int page, int size) {
        String queryString="from XmType model where 1=1";
        int start=(page-1)*size;
        int limit =size;

        return (Result<XmType>)super.find(queryString, null, null, start, limit);
    }

    @Override
    public XmType findById(Integer typeId) {
        log.debug("getting XmType instance with id: " + typeId);
        try {
            XmType instance = (XmType) getHibernateTemplate().get(
                    "com.gxwzu.business.model.XmType", typeId);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
