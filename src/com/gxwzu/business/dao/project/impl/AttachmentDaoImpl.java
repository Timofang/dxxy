package com.gxwzu.business.dao.project.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.AttachmentDao;
import com.gxwzu.business.model.Attachment;
import com.gxwzu.core.dao.impl.BaseDaoImpl;

@Repository
public class AttachmentDaoImpl extends BaseDaoImpl<Attachment> implements AttachmentDao {

	/**
	 * 查询指定项目编号的附件信息
	 * @author SunYi
	 * @Date 2019年3月28日下午9:08:43
	 */
	@Override
	public List<Attachment> getAttachmentByXmNuber(String xmNumber) {
		String hql = "from Attachment where xmNumber = '"+xmNumber+"'";
		List<Attachment> list = super.excuteQuery(hql);
		return list;
	}

}
