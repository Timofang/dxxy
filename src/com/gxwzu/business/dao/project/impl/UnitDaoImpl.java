package com.gxwzu.business.dao.project.impl;

import java.util.ArrayList;
import java.util.List;

import com.gxwzu.core.pagination.Result;
import org.springframework.stereotype.Repository;

import com.gxwzu.business.model.Unit;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.business.dao.project.UnitDao;

@Repository("unitDao")
public class UnitDaoImpl extends BaseDaoImpl<Unit> implements UnitDao {
	/**
	 * 调取教研室所有数据
	 * @author 叶城廷
	 * @version 2019.2.28
	 * @return UnitList
	 */
	@Override
	public List<Unit> selAllUnit() {
		String queryString="from Unit";
		List<Unit> unitList = find(queryString);
		return unitList;
	}

	/**
	 * 教研室分页
	 * @author 黄结
	 */
	@Override
	public Result<Unit> find(Unit model, int page, int size) {
		String queryString="from Unit model where 1=1";
		int start=(page-1)*size;
		int limit =size;

		return (Result<Unit>)super.find(queryString, null, null, start, limit);
	}

	@Override
	public Unit findById(String unitId) {
		String queryString="from Unit model where unitId='"+unitId+"'";
		return super.find(queryString).get(0);
	}

	@Override
	public int[] getMaxUnitId() {
		String hql = "from Unit";
		List<Unit> unitList = super.find(hql);
		int[] unitIdList = new int[unitList.size()];
		for(int i = 0; i < unitList.size(); i++){
			unitIdList[i] = (Integer.parseInt(unitList.get(i).getUnitId()));
		}
		return unitIdList;
	}

	@Override
	public Unit getByUnitId(String unitId) {
		String hql = "from Unit where unitId = '"+unitId+"'";
		List<Unit> unitList = super.find(hql);
		return unitList.get(0);
	}

	@Override
	public Unit getByUserId(String userName) {
		String hql = "from Unit where unitPerson = '"+userName+"'";
		List<Unit> unitList = super.find(hql);
		return unitList.get(0);
	}

}
