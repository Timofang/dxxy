package com.gxwzu.business.dao.project.impl;

import org.springframework.stereotype.Repository;

import com.gxwzu.business.dao.project.MaterialsDao;
import com.gxwzu.business.model.Materials;
import com.gxwzu.core.dao.impl.BaseDaoImpl;

@Repository
public class MaterialsDaoImpl extends BaseDaoImpl<Materials> implements MaterialsDao {

}
