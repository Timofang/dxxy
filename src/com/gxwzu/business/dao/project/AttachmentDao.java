package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Attachment;
import com.gxwzu.core.dao.BaseDao;

public interface AttachmentDao extends BaseDao<Attachment>{

	/**
	 * 查询指定项目编号的附件信息
	 * @author SunYi
	 * @Date 2019年3月28日下午9:07:39
	 * @return List<Attachment>
	 */
	public abstract List<Attachment> getAttachmentByXmNuber(String xmNumber);

}
