package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Instructions;
import com.gxwzu.core.dao.BaseDao;

/**
 * 任务批示Dao层接口
 * @ClassName: InstructionsDao
 * @author SunYi
 * @Date 2019年4月8日上午10:05:46
 */
public interface InstructionsDao extends BaseDao<Instructions>{
	
	/**
	 * 获取指定任务编号的任务批示
	 * @author SunYi
	 * @Date 2019年4月8日上午11:22:23
	 * @return List<Instructions>
	 */
	public abstract List<Instructions> getInstructionsByXmNumber(String thisXmNumber);


}
