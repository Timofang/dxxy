package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmInvest;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmInvestDao extends BaseDao<XmInvest> {

	public abstract Result<XmInvest> find(XmInvest model, int page, int size);

	public abstract List<XmInvest> findByExample(XmInvest model);

	public abstract XmInvest findById(Integer id);

}