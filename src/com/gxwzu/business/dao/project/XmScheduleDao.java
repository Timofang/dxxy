package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmScheduleDao extends BaseDao<XmSchedule> {

	public abstract Result<XmSchedule> find(com.gxwzu.business.model.XmSchedule model, int page, int size);

	public abstract List<XmSchedule> findByExample(XmSchedule model);

	public abstract XmSchedule findById(Integer id);

}