package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmCapital;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmCapitalDao extends BaseDao<XmCapital> {

	public abstract Result<XmCapital> find(XmCapital model, int page, int size);

	public abstract List<XmCapital> findByExample(XmCapital model);

	public abstract XmCapital findById(Integer id);

}