package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmLandprojDao extends BaseDao<XmLandproj>{

	public abstract Result<XmLandproj> find(XmLandproj model, int page, int size);

	public abstract List<XmLandproj> findByExample(XmLandproj model);

	public abstract XmLandproj findById(Integer id);

	public abstract List<XmLandproj> findCoordinate(XmLandproj model);

}