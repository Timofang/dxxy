package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Xmtoland;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmtolandDao extends BaseDao<Xmtoland>{

	public abstract Result<Xmtoland> find(Xmtoland model, int page, int size);

	public abstract List<Xmtoland> findByExample(Xmtoland model);

	public abstract Xmtoland findById(Integer id);

}