package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmDuty;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmDutyDao extends BaseDao<XmDuty> {

	public abstract Result<XmDuty> find(XmDuty model, int page, int size);

	public abstract List<XmDuty> findByExample(XmDuty model);

	public abstract XmDuty findById(Integer id);

}