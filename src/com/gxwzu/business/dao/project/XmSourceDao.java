package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmSource;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmSourceDao extends BaseDao<XmSource>{

	public abstract Result<XmSource> find(XmSource model, int page, int size);

	public abstract List<XmSource> findByExample(XmSource model);

	public abstract XmSource findById(Integer id);

}