package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmSchedulepicDao extends BaseDao<XmSchedulepic> {

	public abstract Result<XmSchedulepic> find(
			com.gxwzu.business.model.XmSchedulepic model, int page, int size);

	public abstract List<XmSchedulepic> findByExample(XmSchedulepic model);

	public abstract XmSchedulepic findById(Integer id);

}