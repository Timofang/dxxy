package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface UnitDao  extends BaseDao<Unit>{
	/**
	 * 调取教研室所有数据
	 * @author 叶城廷
	 * @version 2019.2.28
	 * @return UnitList
	 */
	public List<Unit> selAllUnit();

	public abstract Result<Unit> find(Unit model, int page, int size);

	public abstract Unit findById(String unitId);

	public abstract int[] getMaxUnitId();
	
	public abstract Unit getByUnitId(String unitId);
	
	public Unit getByUserId(String userName);
}
