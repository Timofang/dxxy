package com.gxwzu.business.dao.project;

import com.gxwzu.business.model.XmType;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

/**
 * @Author: soldier
 * @Date: 2019/3/5 8:58
 * @Desc:
 */
public interface XmTypeDao extends BaseDao<XmType> {

    public abstract Result<XmType> find(XmType model, int page, int size);

    public abstract XmType findById(Integer typeId);
}
