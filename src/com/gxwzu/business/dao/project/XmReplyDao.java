package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmReply;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmReplyDao extends BaseDao<XmReply>{

	public abstract Result<XmReply> find(XmReply model, int page, int size);

	public abstract List<XmReply> findByExample(XmReply model);

	public abstract XmReply findById(Integer id);

}