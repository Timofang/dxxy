package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmInfoDao extends BaseDao<XmInfo> {

	public abstract Result<XmInfo> find(XmInfo model, int page, int size);
	
	public abstract Result<XmInfo> findPoor(XmInfo model, int page, int size);

	public abstract List<XmInfo> findByExample(XmInfo model);

	public abstract XmInfo findById(Integer id);

	public abstract List<XmInfo> findCoordinate(XmInfo model);
	
	public abstract List<XmInfo> findPoorXmCoordinate(XmInfo model);

}