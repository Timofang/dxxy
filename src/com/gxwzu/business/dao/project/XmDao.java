package com.gxwzu.business.dao.project;

import java.util.List;

import com.gxwzu.business.model.Xm;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;

public interface XmDao extends BaseDao<Xm> {

	public abstract Result<Xm> find(Xm model, int page, int size);
	
	/**
	 * 获取指定教研室所对应的项目信息
	 * @author SunYi
	 * @Date 2019年3月1日下午2:50:14
	 * @return List<Xm>
	 */
	public Result<Xm> selXmByUnit(Xm model, int page, int size);
	
	/**
	 * 
	 * @Description 获取最大项目编号的项目信息
	 * @author 莫东林
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */
	public Xm getMaxXmNumber();
	/**
	 * 
	 * @Description 删除院项目表
	 * @author 杨长官
	 * @date 2019年3月2日下午2:01:39
	 * String
	 */	
	public void delete(String xmNumber);

	/**
	 * 
	* @Description: 根据项目编号获取
	* @Author 莫东林
	* @date 2019年3月5日下午8:16:08
	* @return Xm
	* @throws
	 */
	public abstract Xm getByXmNumber(String xmNumber);

	public abstract void updateXmChild(XmChild c);

	/**
	 * 
	* @Description: TODO
	* @Author 莫东林
	* @date 2019年3月13日下午5:50:29
	* @return Result<Xm>
	* @throws
	 */
	public abstract Result<Xm> findIssueByUser(Xm xm, Integer page, int size);

	/**
	 * 查询个人院任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	public abstract  List xmstatisticsDetails(String username);
	/**
	 * 查询院发布给教研室任务统计详情
	 * @author 杨长官
	 * @Date 2019年3月7日下午9:49:49
	 * @return List
	 * @throws Exception 
	 */
	
public abstract  List xmToUnitStatisticsDetails(String username);
}