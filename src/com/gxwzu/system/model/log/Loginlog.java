package com.gxwzu.system.model.log;
/**
 * 登录日志类
 * @author amu_1115
 * @version 1.0
 */
public class Loginlog {
	
	private Integer id;
	private String loginSid;//登录session编号
	private String loginUser;//登录用户名
	private String loginIp;//访问IP
	private java.util.Date loginIn;//登录时间
	private java.util.Date loginOut;//退出时间
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLoginSid() {
		return loginSid;
	}
	public void setLoginSid(String loginSid) {
		this.loginSid = loginSid;
	}
	public String getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public java.util.Date getLoginIn() {
		return loginIn;
	}
	public void setLoginIn(java.util.Date loginIn) {
		this.loginIn = loginIn;
	}
	public java.util.Date getLoginOut() {
		return loginOut;
	}
	public void setLoginOut(java.util.Date loginOut) {
		this.loginOut = loginOut;
	}
}
