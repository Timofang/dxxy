package com.gxwzu.system.model.log;

import java.sql.Timestamp;

/**
 * Applog entity. @author MyEclipse Persistence Tools
 */

public class Applog implements java.io.Serializable {

	// Fields

	private Integer id;
	private String userId;
	private String userName;
	private String log;
	private Timestamp logTime;

	// Constructors

	/** default constructor */
	public Applog() {
	}

	/** full constructor */
	public Applog(String userId, String userName, String log, Timestamp logTime) {
		this.userId = userId;
		this.userName = userName;
		this.log = log;
		this.logTime = logTime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLog() {
		return this.log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public Timestamp getLogTime() {
		return logTime;
	}

	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}

}