package com.gxwzu.system.model.log;
/**
 * 操作日志
 * @author amu_1115
 * @version 1.0
 */
public class OperaLog {
	
	private String logId;//ID
	private String opMan;//操作人
	private java.util.Date opTime;//操作时间
	private String ipAdress;//IP地址
	private String logCon;//操作内容
	private String opName;//动作名称
	
	public String getLogId() {
		return logId;
	}
	public void setLogId(String logId) {
		this.logId = logId;
	}
	public String getOpMan() {
		return opMan;
	}
	public void setOpMan(String opMan) {
		this.opMan = opMan;
	}
	
	public String getIpAdress() {
		return ipAdress;
	}
	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	public String getLogCon() {
		return logCon;
	}
	public void setLogCon(String logCon) {
		this.logCon = logCon;
	}
	public String getOpName() {
		return opName;
	}
	public void setOpName(String opName) {
		this.opName = opName;
	}
	public java.util.Date getOpTime() {
		return opTime;
	}
	public void setOpTime(java.util.Date opTime) {
		this.opTime = opTime;
	}


}
