package com.gxwzu.system.model.user;

import java.util.List;

import com.gxwzu.business.model.Materials;
import com.gxwzu.business.model.XmChild;

/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields

	private Integer id;
	private String helpUserName;
	private String helpPassword;
	private String helpName;
	private String helpSex;
	private String helpUnit;
	private String helpPost;
	private String helpTelephone;
	private String portrait;
	private String sign;
	private String helpUnitType;
	private String helpSubjection;
	private String isReply;
	private String userType;
	private String jobNumber;
	private Integer leaderTypeId;
	private String unitId;
	
	private List<Materials> materialsList;

	private XmChild xmChild;
	private List<XmChild> xmChildList;
	
	// 个人任务的完成数量
	private int finishCount;
	// 个人任务的未完成数量
	private int unfinishedCount;
	// 个人任务总数
	private int allCount;
	// 个人任务的完成比例
	private String proportion;
	
	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String helpUserName, String helpPassword, String helpName) {
		this.helpUserName = helpUserName;
		this.helpPassword = helpPassword;
		this.helpName = helpName;
	}
	
	public User(int finishCount, int unfinishedCount, int allCount, String proportion) {
		super();
		this.finishCount = finishCount;
		this.unfinishedCount = unfinishedCount;
		this.allCount = allCount;
		this.proportion = proportion;
	}

	/** full constructor */
	public User(String helpUserName, String helpPassword, String helpName, String helpSex, String helpUnit,
			String helpPost, String helpTelephone, String portrait, String sign, String helpUnitType,
			String helpSubjection, String isReply, String userType, String jobNumber, Integer leaderTypeId,
			String unitId) {
		this.helpUserName = helpUserName;
		this.helpPassword = helpPassword;
		this.helpName = helpName;
		this.helpSex = helpSex;
		this.helpUnit = helpUnit;
		this.helpPost = helpPost;
		this.helpTelephone = helpTelephone;
		this.portrait = portrait;
		this.sign = sign;
		this.helpUnitType = helpUnitType;
		this.helpSubjection = helpSubjection;
		this.isReply = isReply;
		this.userType = userType;
		this.jobNumber = jobNumber;
		this.leaderTypeId = leaderTypeId;
		this.unitId = unitId;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHelpUserName() {
		return this.helpUserName;
	}

	public void setHelpUserName(String helpUserName) {
		this.helpUserName = helpUserName;
	}

	public String getHelpPassword() {
		return this.helpPassword;
	}

	public void setHelpPassword(String helpPassword) {
		this.helpPassword = helpPassword;
	}

	public String getHelpName() {
		return this.helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public String getHelpSex() {
		return this.helpSex;
	}

	public void setHelpSex(String helpSex) {
		this.helpSex = helpSex;
	}

	public String getHelpUnit() {
		return this.helpUnit;
	}

	public void setHelpUnit(String helpUnit) {
		this.helpUnit = helpUnit;
	}

	public String getHelpPost() {
		return this.helpPost;
	}

	public void setHelpPost(String helpPost) {
		this.helpPost = helpPost;
	}

	public String getHelpTelephone() {
		return this.helpTelephone;
	}

	public void setHelpTelephone(String helpTelephone) {
		this.helpTelephone = helpTelephone;
	}

	public String getPortrait() {
		return this.portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getSign() {
		return this.sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getHelpUnitType() {
		return this.helpUnitType;
	}

	public void setHelpUnitType(String helpUnitType) {
		this.helpUnitType = helpUnitType;
	}

	public String getHelpSubjection() {
		return this.helpSubjection;
	}

	public void setHelpSubjection(String helpSubjection) {
		this.helpSubjection = helpSubjection;
	}

	public String getIsReply() {
		return this.isReply;
	}

	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}

	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public Integer getLeaderTypeId() {
		return this.leaderTypeId;
	}

	public void setLeaderTypeId(Integer leaderTypeId) {
		this.leaderTypeId = leaderTypeId;
	}

	public String getUnitId() {
		return this.unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public List<Materials> getMaterialsList() {
		return materialsList;
	}

	public void setMaterialsList(List<Materials> materialsList) {
		this.materialsList = materialsList;
	}
	

	public XmChild getXmChild() {
		return xmChild;
	}

	public void setXmChild(XmChild xmChild) {
		this.xmChild = xmChild;
	}
	
	public List<XmChild> getXmChildList() {
		return xmChildList;
	}

	public void setXmChildList(List<XmChild> xmChildList) {
		this.xmChildList = xmChildList;
	}
	
	public int getFinishCount() {
		return finishCount;
	}

	public void setFinishCount(int finishCount) {
		this.finishCount = finishCount;
	}

	public int getUnfinishedCount() {
		return unfinishedCount;
	}

	public void setUnfinishedCount(int unfinishedCount) {
		this.unfinishedCount = unfinishedCount;
	}

	public String getProportion() {
		return proportion;
	}

	public void setProportion(String proportion) {
		this.proportion = proportion;
	}
	
	public int getAllCount() {
		return allCount;
	}

	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", helpUserName=" + helpUserName + ", helpPassword=" + helpPassword + ", helpName="
				+ helpName + ", helpSex=" + helpSex + ", helpUnit=" + helpUnit + ", helpPost=" + helpPost
				+ ", helpTelephone=" + helpTelephone + ", portrait=" + portrait + ", sign=" + sign + ", helpUnitType="
				+ helpUnitType + ", helpSubjection=" + helpSubjection + ", isReply=" + isReply + ", userType="
				+ userType + ", jobNumber=" + jobNumber + ", leaderTypeId=" + leaderTypeId + ", unitId=" + unitId
				+ ", materialsList=" + materialsList + ", xmChild=" + xmChild + ", xmChildList=" + xmChildList
				+ ", finishCount=" + finishCount + ", unfinishedCount=" + unfinishedCount + ", allCount=" + allCount
				+ ", proportion=" + proportion + "]";
	}

	

}