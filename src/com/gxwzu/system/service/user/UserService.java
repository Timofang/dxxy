package com.gxwzu.system.service.user;

import java.util.List;
import java.util.Map;

import com.gxwzu.business.model.Unit;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.system.model.user.User;

public interface UserService extends BaseService<User>{
	
	/**
	 * 通过教研处Id查找教职工
	 * @author SunYi
	 * @Date 2019年3月2日下午2:00:46
	 * @return List<User>
	 */
	public List<User> findUserByUnitId(String unitId, String jobNumber);
	

    public  List<User> findAll();

    public  Result<User> find(User model, int page, int size);

    public  List<User> findByExample(User model);

    public User add(User User);
    public  User findById(Integer findId);
    public void del(Integer delId);

    public  int modifyPassword(String loginNameValue, String sign, String userPassword, String newPassword, String confirmPassword);

    public  List<AutoCompleteHttpModel> likeAll(String keyword);
    /**
     *
     * @param model
     * @return
     */
    public  List updateUserInfo(User model);
    /**
     * 查找所有去除重复的单位
     * @return
     */
    public Map<String,String> findAllUnits();
    
	/**
	 * 
	 * @Description 根据用户名查找
	 * @author 莫东林
	 * @date 2019年3月7日下午5:05:32
	 * User
	 */
	public User getByUserName(String userName);

    public Map<String, String> findUnitsByID(String helpUnit);
    /**
     * 查找单位用户
     * @param helpUnit
     * @return
     */
    public List findUserByUnits(String helpUnit);
    /**
     * 根据单位名称查找未绑定贫困户的人员名单
     * @param helpUnit
     * @param c 绑定的人数
     * @return
     */
    public List<User> findByUseridAndPwd(String userid,String pwd);

    public List isLoginApp(String userId, String userPassword);

    /**
     * @author soldier
     */
    public List<User> findHaveJobNumberUser();

    public User findByJobNumber(String jobNumber);

    public User findByHelpUserName(String helpUserName);
    
    public int findByHelpUserType(String helpUserName);

	List<User> findJobNumberUser();
	public User getByUserName2(String userName);

}