package com.gxwzu.system.service.user.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.business.dao.project.UnitDao;
import com.gxwzu.business.model.Unit;
import com.gxwzu.core.context.SystemContext;
import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.core.util.UidUtils;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;

/**
 * 帮扶人员ServiceImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: UserServiceImpl
 * <br>Date: 2016-8-27下午09:46:10
 * <br>log:
 */
@Service("userService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public  class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	@Autowired
	private UnitDao unitDao;
	@Autowired
    private UserDao UserDao;
    private User User;
    public User getUser() {
        return User;
    }

    public void setUser(User User) {
        this.User = User;
    }

    @Autowired
    public void setUserDao(UserDao UserDao) {
        this.UserDao = UserDao;
    }

    @Override
    public BaseDao<User> getDao() {
        return this.UserDao;
    }
    

    /*************************************service方法体**********************************************/
    /**
     * 通过教研室Id查找教职工
     * @author SunYi
     * @Date 2019年3月2日下午2:10:14
     */
    @Override
	public List<User> findUserByUnitId(String unitId,  String jobNumber) {
		return this.UserDao.findUserByUnitId(unitId, jobNumber);
	}

    @Override
    public Result<User> find(User model, int page, int size) {

        return UserDao.find(model, page, size);
    }

   /* @Override
    public List<User> findAll() {
        return UserDao.getAll(User.class);
    }*/
    
    /**
     * 获取所有用户信息
     * @author SunYi
     * @Date 2019年4月19日上午11:19:49
     */
    @Override
    public List<User> findAll() {
		List<User> userList = UserDao.findAllUser();
        return userList;
    }

    public List<User> findByUseridAndPwd(String userid,String pwd) {

        String queryString = "from User model where 1=1";

        queryString =queryString+" and (model.helpUserName=? or model.jobNumber = ?) and model.helpPassword=? ";
        Object[]params={userid,userid,pwd};
        return UserDao.findByHql(queryString, params);

    }

    /**
     * @author soldier
     * @date 19-3-16下午10:50
     * @description: 1、用于用户输入信息查询
     * 				  2、用于User_add.jsp页面【js检验用户信息】
     */
    @Override
    public List<User> findByExample(User model) {
        List params = new ArrayList<Object>();
        String queryString = "from User model where 1=1";
        if(model.getId()!=null){
            queryString =queryString+" and model.id=?";
            params.add(model.getId());
        }
        if(model.getHelpUserName()!=null){
            queryString =queryString+" and model.helpUserName like'%"+model.getHelpUserName()+"%'";
        }
        if(model.getHelpPassword()!=null){
            queryString =queryString+" and model.helpPassword=?";
            params.add(model.getHelpPassword());
        }
        if(model.getHelpName()!=null && !"".equals(model.getHelpName())){
            queryString =queryString+" and model.helpName like'%"+model.getHelpName()+"%'";
        }

        if(model.getHelpSex()!=null){
            queryString =queryString+" and model.helpSex=?";
            params.add(model.getHelpSex());
        }
        if(model.getHelpUnit()!=null && !"".equals(model.getHelpUnit())){
            queryString =queryString+" and model.helpUnit like'%"+model.getHelpUnit()+"%'";

        }
        if(model.getHelpPost()!=null){
            queryString =queryString+" and model.helpPost=?";
            params.add(model.getHelpPost());
        }
        if(model.getHelpTelephone()!=null){
            queryString =queryString+" and model.helpTelephone=?";
            params.add(model.getHelpTelephone());
        }
        return UserDao.findByHql(queryString, params.toArray());
    }

    public User add(User user){
        try{
        	//根据领导类型变更用户类型
        	//角色类型：1：学院领导；2：教研室/实验室负责人；3：普通教师
        	//用户类型：1学院任务，2任务列表，3我的任务，4我的下发，5问题反馈， 6用户管理，7教研室管理，8项目类型管理
        	user.setJobNumber(user.getHelpUserName());
        	switch(user.getLeaderTypeId()){
        	case 1:
        		user.setUserType("1_2_3_4_5_6_7_8");
        		break;
        	case 2:
        		user.setUserType("2_3_4_5");
        		break;
        	case 3:
        		user.setUserType("2_3_5");
        		break;
        	}
            UserDao.save(user);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return User;

    }
    public  User findById(Integer findId){
        return UserDao.findById(findId);
    }
    public void del(Integer delId){
        User User=UserDao.findById(delId);
        UserDao.remove(User);
    }


    @Override
    public int modifyPassword(String loginNameValue, String sign, String userPassword, String newPassword, String confirmPassword) {
        int actionState = 0;
        try {
            User findUser = new User();
            findUser.setHelpUserName(loginNameValue);
            findUser.setHelpPassword(userPassword);
            List<User> listUser = UserDao.findByExample(findUser);
            if(listUser.size()==1){
                if(newPassword!=null && confirmPassword!=null && newPassword.equals(confirmPassword)){
                    findUser = listUser.get(0);
                    findUser.setHelpPassword(newPassword);
                    UserDao.update(findUser);
                    actionState = 1;
                    return actionState;
                }else{
                    return actionState;
                }
            }else{
                return actionState;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return actionState;
    }

    @Override
    public List<AutoCompleteHttpModel> likeAll(String keyword) {
        List<AutoCompleteHttpModel> results=new ArrayList<AutoCompleteHttpModel> ();
        try {

            String hql="select distinct id,helpName,helpUnit,helpPost,portrait " +
                    "from user where (helpName like'%"+keyword+"%' or helpUnit like'%"+keyword+"%')";
            System.out.println(hql);
            List<Object[]> list=  UserDao.query(hql);
            for(int i=0;i<list.size();i++){
                AutoCompleteHttpModel model =new AutoCompleteHttpModel();

                Object[] obj = list.get(i);

                model.setItemId(((Integer)obj[0]).toString());
                model.setItemName((String)obj[1]+"_"+(String)obj[2]+"_"+(String)obj[3]);
                model.setItemValue(((String)obj[1]).toString());
                results.add(model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    @Override
    public List updateUserInfo(User model) {
        List<Object> list = new ArrayList<Object>();
        User User = new User();
        try {
            //获取原User数据
            User thisUser = UserDao.findUserInfoById(model.getId());
            if(null != thisUser){
                thisUser.setHelpSex(model.getHelpSex());
                thisUser.setHelpUnit(model.getHelpUnit());
                thisUser.setHelpPost(model.getHelpPost());
                thisUser.setHelpTelephone(model.getHelpTelephone());
                UserDao.update(thisUser);

                User = UserDao.findUserInfoById(model.getId());
                list.add("1");
                list.add(User);
                return list;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Map<String,String> findAllUnits() {
        Map<String,String> results=new LinkedHashMap<String,String>();
        String hql="select distinct helpUnit from user";

        List list =UserDao.query(hql);
        for(int i=0;i<list.size();i++){
            Object oc=list.get(i);
            String key=(String)oc;
            String value=(String)oc;
            results.put(key,(i+1)+"."+value);
        }
        return results;
    }

    public List findUserByUnits(String helpUnit) {

        String hql="select helpName,helpUserName,helpPassword,helpUnit,helpPost,helpTelephone " +
                " from user where 1=1";
        if(helpUnit!=null&&!"".equals(helpUnit)){

            helpUnit =" and helpUnit='"+helpUnit+"'";
        }


        List list =UserDao.query(hql);

        return list;
    }

    @Override
    public Map<String, String> findUnitsByID(String helpUnit) {
        Map<String,String> results=new LinkedHashMap<String,String>();
        String hql="select distinct helpUnit from user where 1=1 and helpUnit like'%"+helpUnit+"%'";

        List list =UserDao.query(hql);
        for(int i=0;i<list.size();i++){
            Object oc=list.get(i);
            String key=(String)oc;
            String value=(String)oc;
            results.put(key,(i+1)+"."+value);
        }
        return results;
    }

    /**
     * 用户登录（app端）
     * @param userId 手机号码/账号，（后续有其他也是用这个参数）
     * @param userPassword 密码
     * @return 列表（三个参数），如下：
     *  第一个参数：1-登录成功；0-登录失败；2-账号不存在。
     *  第二个参数：Users 对象
     *  第三个参数：令牌
     */
    @Override
    public List isLoginApp(String userId,String userPassword){
        try{
            List<Object> list = new ArrayList<Object>();
            int numT = 0;//初始化临时变量
            User user = null;

            if((!UserDao.isHadValue("helpTelephone", userId)) ){//不存在
                if((!UserDao.isHadValue("helpUserName", userId))){//这写法不好，后面改一下
                    numT = 2;
                }
            }

            //第一步先用手机查询，第二步用用户名
            if(0 == numT){
                user = UserDao.findUserByProperty("helpTelephone", userId);//手机
                if(null == user){
                    user = UserDao.findUserByProperty("helpUserName", userId);//手机
                }
                if(null != user){
                    //校验用户名密码
                    if(user.getHelpPassword().equals(userPassword)){
                        numT = 1;//测试
                    }
                }
            }
            list.add(numT);//第一个参数：1-登录成功；0-登录失败；2-账号不存在。
            if(1 == numT){//只有登录成功才返回信息

                User thisUserHelp = UserDao.findById(user.getId());

                if(user.getHelpName() == null || "".equals(user.getHelpName()) || "NULL".equals(user.getHelpName())){
                    user.setHelpName("未填写");
                }else{
                    user.setHelpName(user.getHelpName().replaceAll(" ", ""));
                }

                if(user.getHelpUnit() == null || "".equals(user.getHelpUnit()) || "NULL".equals(user.getHelpUnit())){
                    user.setHelpUnit("未填写");
                }else{
                    user.setHelpUnit(user.getHelpUnit().replaceAll(" ", ""));
                }

                if(user.getHelpPost() == null || "".equals(user.getHelpPost()) || "NULL".equals(user.getHelpPost())){
                    user.setHelpPost("未填写");
                }else{
                    user.setHelpPost(user.getHelpPost().replaceAll(" ", ""));
                }

                if(user.getHelpTelephone() == null || "".equals(user.getHelpTelephone()) || "NULL".equals(user.getHelpTelephone())){
                    user.setHelpTelephone("未填写");
                }else{
                    user.setHelpTelephone(user.getHelpTelephone().replaceAll(" ", ""));
                }

                if(user.getPortrait() == null || "".equals(user.getPortrait()) || "NULL".equals(user.getPortrait())){
                    user.setHelpTelephone("无证件照");
                }else{
                    user.setPortrait(user.getPortrait().replaceAll(" ", ""));
                }


                list.add(user);//第二个参数：Users 对象

                //修改：
                if(null != thisUserHelp){
                    //原理是：使用UUID作为令牌明文，给用户返回令牌明文，存数据库存令牌密文
                    //每次登陆成功，都生成一次令牌，然后保存到数据库。注意，令牌也加密，加密方式跟密码一致
                    String uuid_sign = UidUtils.UID();

                    thisUserHelp.setSign(SystemContext.passwdEncryption(uuid_sign));//加密
                    //更新数据库

                    UserDao.update(thisUserHelp);
                    //给APP返回的令牌是明文，下次APP传明文过来的时候直接调用解密匹配
                    list.add(uuid_sign);// 第三个参数：令牌
                }
            }else{
                list.add(null);//第二个参数：Users 对象
            }


            return list;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<User> findHaveJobNumberUser() {
        String queryString="from User model where jobNumber is not null";
        return UserDao.find(queryString);
    }
    
    @Override
    public List<User> findJobNumberUser() {
    	List<Unit> userIdList = unitDao.find("from Unit");
    	String unitId = "(";
    	for(int i = 0; i < userIdList.size(); i++){
    		unitId += userIdList.get(i).getUnitPerson()+",";
    	}
    	unitId = unitId.substring(0, unitId.length() - 1) + ")";
    	System.out.println(unitId);
        String queryString="from User model where jobNumber is not null and jobNumber not in "+unitId+"";
        System.out.println(queryString);
        return UserDao.find(queryString);
    }

    @Override
    public User findByJobNumber(String jobNumber) {
        String queryString="from User model where jobNumber = "+jobNumber;
        List<User> userList = UserDao.find(queryString);
        return userList.get(0);
    }

    @Override
    public User findByHelpUserName(String helpUserName) {
        String queryString="from User model where helpUserName = "+helpUserName;
        List<User> userList = UserDao.find(queryString);
        return userList.get(0);
    }
    
    @Override
    public int findByHelpUserType(String helpUserName){
        String queryString="from User where helpUserName = "+helpUserName;
        List<User> userList = UserDao.find(queryString);
        int helpUserName1 = Integer.valueOf(userList.get(0).getLeaderTypeId());
		return helpUserName1;
    }

    /**
	 * 
	 * @Description 根据用户名查找
	 * @author 莫东林
	 * @date 2019年3月7日下午5:05:32
	 * User
	 */
	@Override
	public User getByUserName(String userName) {
		return UserDao.find("from User where helpUserName = '"+userName+"'").get(0);
	}
	@Override
	public User getByUserName2(String userName) {
		return UserDao.find("from User where helpName = '"+userName+"'").get(0);
	}

}
