package com.gxwzu.system.service.log;

import java.util.List;

import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.BaseService;
import com.gxwzu.system.model.log.Applog;

public interface ApplogService extends BaseService<Applog> {

	public abstract List<Applog> findAll();

	public abstract Result<Applog> find(Applog model, int page, int size);

	public abstract List<Applog> findByExample(Applog model);

	public abstract Applog findById(Integer id);

}