package com.gxwzu.system.service.log;

import com.gxwzu.core.service.BaseService;
import com.gxwzu.system.model.log.OperaLog;

public interface OperaLogService extends BaseService<OperaLog> {

}
