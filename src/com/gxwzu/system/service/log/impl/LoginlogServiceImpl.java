package com.gxwzu.system.service.log.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.model.log.Loginlog;
import com.gxwzu.system.service.log.LoginlogService;
/**
 * 
 * @author amu_1115
 *
 */
@Service("loginlogService")
public class LoginlogServiceImpl extends BaseServiceImpl<Loginlog> implements
		LoginlogService {

	private BaseDao<Loginlog>loginlogDao;
	@Override
	public BaseDao<Loginlog> getDao() {
		
		return loginlogDao;
	}
	@Autowired
	public void setLoginlogDao(BaseDao<Loginlog> loginlogDao) {
		this.loginlogDao = loginlogDao;
	}
	@Override
	public Loginlog findByJsessionid(String jsessionid) {
		String hql="from Loginlog where loginSid=?";
		Object[]params={jsessionid};
		List<Loginlog> logs= loginlogDao.findByHql(hql, params);
		Loginlog log=null;
		if(!logs.isEmpty()){
			 log=logs.get(0);
			
		}
		return log;
	}}
