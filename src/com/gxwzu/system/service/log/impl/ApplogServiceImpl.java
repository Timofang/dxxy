package com.gxwzu.system.service.log.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.dao.log.ApplogDao;
import com.gxwzu.system.model.log.Applog;
import com.gxwzu.system.service.log.ApplogService;

@Service("applogService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor =RuntimeException.class)
public class ApplogServiceImpl extends BaseServiceImpl<Applog> implements ApplogService {

	private ApplogDao applogDao;

	@Autowired
	public void setApplogDao(ApplogDao applogDao) {
		super.setDao(dao);
		this.applogDao = applogDao;
	}

	@Override
	public BaseDao<Applog> getDao() {
		return this.applogDao;
	}
	
	@Override
	public List<Applog> findAll() {
		
		return applogDao.getAll(Applog.class);
	}

	@Override
	public Result<Applog> find(Applog model, int page, int size) {
	
		return applogDao.find(model, page, size);
	}
	
	@Override
	public List<Applog> findByExample(Applog model) {
	
		return applogDao.findByExample(model);
	}
	
	@Override
	public Applog findById(Integer id) {
	
		return applogDao.findById(id);
	}
	
}
