package com.gxwzu.system.service.log.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.service.impl.BaseServiceImpl;
import com.gxwzu.system.model.log.OperaLog;
import com.gxwzu.system.service.log.OperaLogService;
/**
 * 
 * @author amu_1115
 *
 */
@Service("operaLogService")
public class OperaLogServiceImpl extends BaseServiceImpl<OperaLog> implements
		OperaLogService {
	private BaseDao<OperaLog> operaLogDao;

	@Override
	public BaseDao<OperaLog> getDao() {
		return operaLogDao;
	}
@Autowired
	public void setOperaLogDao(BaseDao<OperaLog> operaLogDao) {
		this.operaLogDao = operaLogDao;
	}}
