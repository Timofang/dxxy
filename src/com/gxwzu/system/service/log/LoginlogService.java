package com.gxwzu.system.service.log;

import com.gxwzu.core.service.BaseService;
import com.gxwzu.system.model.log.Loginlog;
/**
 * 
 * @author amu_1115
 *
 */
public interface LoginlogService extends BaseService<Loginlog> {
	/**
	 * 根据会话ID查找登录记录
	 * @param jsessionid
	 * @return
	 */
	public Loginlog findByJsessionid(String jsessionid);

}
