package com.gxwzu.system.dao.user;

import java.util.List;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.system.model.user.User;

public interface UserDao extends BaseDao<User>{

	public abstract Result<User> find(User model, int page, int size);

	public abstract List<User> findByExample(User model);

	public abstract boolean isHadValue(String property, String value);

	public abstract User findUserByProperty(String property, String value);

	public abstract User findById(Integer id);

	public abstract User findUserInfoById(Integer id);
	
	/**
	 * 获取所有用户信息
	 * @author SunYi
	 * @Date 2019年4月19日上午11:06:43
	 * @return User
	 */
	public abstract List<User> findAllUser();
	

	
	/**
	 * 通过教研室Id查找教职工
	 * @author SunYi
	 * @Date 2019年3月2日下午2:26:58
	 * @return List<User>
	 */
	public abstract List<User> findUserByUnitId(String unitId, String jobNumber);

	/**
	 * 通过子任务表的接收人查找用户
	 * @author SunYi
	 * @Date 2019年3月19日下午9:02:53
	 * @return void
	 */
	public abstract User getUserById(String userName);


}