package com.gxwzu.system.dao.user.impl;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.system.dao.user.UserDao;
import com.gxwzu.system.model.user.User;

/**
 * 帮扶人员DAOImpl实现类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: UserDaoImpl
 * <br>Date: 2016-8-27下午09:39:45
 * <br>log:
 */
@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{
	
	/**
	 * 获取所有用户信息
	 * @author SunYi
	 * @Date 2019年4月19日上午11:07:37
	 */
	@Override
	public List<User> findAllUser() {
		String hql = "from User model where 1=1  ORDER BY model.helpName ASC";
		List<User> userList = super.find(hql);
		userList = super.sortByHelpName(userList);
		return userList;
	}
	

	/**
	 * 通过任务子表的接收人查找用户
	 * @author SunYi
	 * @Date 2019年3月19日下午9:03:57
	 */
	@Override
	public User getUserById(String userName) {
		String hql = "from User where helpUserName = '"+userName+"'";
		List<User> list = super.find(hql);
		return list.get(0);
	}
	
	/**
	 * 通过教研室Id查找教职工(自己不能查找到自己,因为不能把任务指派给自己)
	 * @author SunYi
	 * @Date 2019年3月2日下午2:28:49
	 */
	@Override
	public List<User> findUserByUnitId(String unitId, String userName) {
		String hql = "from User where unitId like '%"+unitId+"%' and helpUserName != '"+userName+"'";
		List<User> userList = this.getHibernateTemplate().find(hql);
		if(null != userList && 0<userList.size()){
			return userList;
		}
		return null;
	}

	@Override
	public Result<User> find(User model, int page, int size) {
		String queryString="from User model where jobNumber is not null";
		int start=(page-1)*size;
		int limit =size;

		if(model.getId()!=null){
			queryString =queryString+" and model.id="+model.getId();
		}
		if(model.getHelpUserName()!=null){
			queryString =queryString+" and model.helpUserName='"+model.getHelpUserName()+"'";
		}
		if(model.getHelpPassword()!=null){
			queryString =queryString+" and model.helpPassword='"+model.getHelpPassword()+"'";
		}
		if(model.getHelpName()!=null && !"".equals(model.getHelpName())){
			queryString =queryString+" and model.helpName='"+model.getHelpName()+"'";
		}

		if(model.getHelpSex()!=null){
			queryString =queryString+" and model.helpSex='"+model.getHelpSex()+"'";
		}
		if(model.getHelpUnit()!=null && !"".equals(model.getHelpUnit())){
			queryString =queryString+" and model.helpUnit='"+model.getHelpUnit()+"'";
		}
		if(model.getHelpPost()!=null){
			queryString =queryString+" and model.helpPost='"+model.getHelpPost()+"'";
		}
		if(model.getHelpTelephone()!=null){
			queryString =queryString+" and model.helpTelephone='"+model.getHelpTelephone()+"'";
		}
		if(model.getIsReply()!=null){
			queryString =queryString+" and model.isReply='"+model.getIsReply()+"'";
		}
		return (Result<User>)super.find(queryString, null, null, start, limit);
	}

	@Override
	public List<User> findByExample(User model) {

		List params = new ArrayList<Object>();
		String queryString = "from User model where 1=1";
		if(model.getId()!=null){
			queryString =queryString+" and model.id="+model.getId();
		}
		if(model.getHelpUserName()!=null){
			queryString =queryString+" and model.helpUserName='"+model.getHelpUserName()+"'";
		}
		if(model.getHelpPassword()!=null){
			queryString =queryString+" and model.helpPassword='"+model.getHelpPassword()+"'";
		}
		if(model.getHelpName()!=null && !"".equals(model.getHelpName())){
			queryString =queryString+" and model.helpName='"+model.getHelpName()+"'";
		}
		if(model.getHelpSex()!=null){
			queryString =queryString+" and model.helpSex='"+model.getHelpSex()+"'";
		}
		if(model.getHelpUnit()!=null && !"".equals(model.getHelpUnit())){
			queryString =queryString+" and model.helpUnit='"+model.getHelpUnit()+"'";
		}
		if(model.getHelpPost()!=null){
			queryString =queryString+" and model.helpPost='"+model.getHelpPost()+"'";
		}
		if(model.getHelpTelephone()!=null){
			queryString =queryString+" and model.helpTelephone='"+model.getHelpTelephone()+"'";
		}
		if(model.getIsReply()!=null){
			queryString =queryString+" and model.isReply='"+model.getIsReply()+"'";
		}
		return this.findByExample(queryString, null);
	}

	@Override
	public User findById(Integer id) {
		log.debug("getting User instance with id: " + id);
		try {
			User instance = (User) getHibernateTemplate().get(
					"com.gxwzu.system.model.user.User", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	/**
	 * 判断服务器是否存在该值（用户名是否重复，手机号码是否重复等）
	 * @param property SysUser对象的属性
	 * @param value 值
	 * @return true -重复了，false-不重复
	 */
	@Override
	public boolean isHadValue(String property,String value){
		boolean isT = false;
		String hql = "from User where "+property+" = '"+value+"'";
		List<User> list = this.getHibernateTemplate().find(hql);
		if(null != list && 0<list.size()){
			isT = true;
		}
		return isT;
	}

	/**
	 * 根据property属性的值value获取对象
	 * @param property SysUser对象的属性
	 * @param value 值
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public User findUserByProperty(String property,String value){
		User User = null;
		String hql = "from User where "+property+" = '"+value+"'";
		List<User> list = this.getHibernateTemplate().find(hql);
		if(null != list && 0<list.size()){
			User = list.get(0);
		}
		return User;
	}

	@Override
	public User findUserInfoById(Integer id) {

		try {
			User User = (User) getHibernateTemplate().get(
					"com.gxwzu.system.model.user.User", id);
			return User;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}


}
