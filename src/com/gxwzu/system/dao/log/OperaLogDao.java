package com.gxwzu.system.dao.log;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.system.model.log.OperaLog;

public interface OperaLogDao extends BaseDao<OperaLog> {

}
