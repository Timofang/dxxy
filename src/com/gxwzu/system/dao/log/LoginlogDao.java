package com.gxwzu.system.dao.log;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.system.model.log.Loginlog;

public interface LoginlogDao extends BaseDao<Loginlog> {

}
