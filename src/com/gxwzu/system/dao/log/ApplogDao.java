package com.gxwzu.system.dao.log;

import java.util.List;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.system.model.log.Applog;

public interface ApplogDao extends BaseDao<Applog>{

	public abstract Result<Applog> find(Applog model, int page, int size);

	public abstract List<Applog> findByExample(Applog model);

	public abstract Applog findById(Integer id);

}