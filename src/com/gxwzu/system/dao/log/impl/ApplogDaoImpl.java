package com.gxwzu.system.dao.log.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.system.dao.log.ApplogDao;
import com.gxwzu.system.model.log.Applog;

/**
 * APP日志DAO类
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: ApplogDaoImpl
 * <br>Date: 2016-12-4上午10:49:20
 * <br>log:
 */
@Repository("applogDao")
public class ApplogDaoImpl extends BaseDaoImpl<Applog> implements ApplogDao{

	@Override
	public Result<Applog> find(Applog model, int page, int size) {
		String queryString="from Applog model where 1=1";
		int start=(page-1)*size;
		int limit =size;
		
		List params =new ArrayList<Object>();
		
		if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getUserName()!=null){
	    	 queryString =queryString+" and model.userName=?";
	    	 params.add(model.getUserName());
	     }
		return (Result<Applog>)super.find(queryString, params.toArray(), null, start, limit);
	}
	
	@Override
	public List<Applog> findByExample(Applog model) {
		
		 List params = new ArrayList<Object>();
	     String queryString = "from Applog model where 1=1";
	     if(model.getId()!=null){
	    	 queryString =queryString+" and model.id=?";
	    	 params.add(model.getId());
	     }
		if(model.getUserId()!=null){
	    	 queryString =queryString+" and model.userId=?";
	    	 params.add(model.getUserId());
	     }
		if(model.getUserName()!=null){
	    	 queryString =queryString+" and model.userName=?";
	    	 params.add(model.getUserName());
	     }
		return this.findByExample(queryString, params.toArray());
	}
	
	@Override
	public Applog findById(Integer id) {
		log.debug("getting Applog instance with id: " + id);
		try {
			Applog instance = (Applog) getHibernateTemplate().get(
					"com.gxwzu.system.model.log.Applog", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
}
