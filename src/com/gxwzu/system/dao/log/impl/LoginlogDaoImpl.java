package com.gxwzu.system.dao.log.impl;

import org.springframework.stereotype.Repository;

import com.gxwzu.core.dao.BaseDao;
import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.system.model.log.Loginlog;
@Repository("loginlogDao")
public class LoginlogDaoImpl extends BaseDaoImpl<Loginlog> implements
		BaseDao<Loginlog> {

}
