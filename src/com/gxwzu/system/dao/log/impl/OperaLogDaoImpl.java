package com.gxwzu.system.dao.log.impl;

import org.springframework.stereotype.Repository;

import com.gxwzu.core.dao.impl.BaseDaoImpl;
import com.gxwzu.system.dao.log.OperaLogDao;
import com.gxwzu.system.model.log.OperaLog;
/**
 * 
 * @author amu_1115
 *
 */
@Repository("operaLogDao")
public class OperaLogDaoImpl extends BaseDaoImpl<OperaLog> implements
		OperaLogDao {}
