package com.gxwzu.system.action.user;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.gxwzu.business.model.Unit;
import com.gxwzu.business.service.project.UnitService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.core.message.ErrorMessage;
import com.gxwzu.core.model.AutoCompleteHttpModel;
import com.gxwzu.core.model.ResponeJson;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.ImageUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.ModelDriven;

public class UserAction extends BaseAction implements ModelDriven<User> {

	private static final long serialVersionUID = 518531192299350300L;

	protected final Log logger = LogFactory.getLog(getClass());

	/***********************实例化User******************************/
	private User model=new User();

	@Override
	public User getModel() {
		return model;
	}
	public void setModel(User model) {
		this.model = model;
	}

	/***********************注入Service******************************/
	private  UserService UserService;
	private  UnitService UnitService;
	private XmInfoService xmInfoService;

	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setUserService(UserService UserService) {
		this.UserService = UserService;
	}
	public void setUnitService(UnitService unitService) {
		UnitService = unitService;
	}

	/***********************声明参数******************************/
	private ResponeJson dataJson;//JSON格式
	private List<User> listUser;
	private List<Unit> listUnit;
	private Result<User> pageResult;//分页

	private Map<String,String>  unitsList;

	private String id;
	private String thisState;
	private String isMenu;//1.贫困户管理，2.家庭成员管理，3.帮扶人员管理

	private String[] thisHelpUserType;//用户类型
	private String[] thisHelpUserDuty;//用户职责
	private String thisUserDuty;
	private String thisUserId;
	private String delUserId;
	private InputStream is;
	private String thisUserType;
	private String thisUnit;
	private String isExample = "0";
	private String isOneAction;

	/**************************文件上传参数Start**************************/
	private String title;//封装文件标题请求参数的属性
	private File upload;//封装上传文件域的属性
	private String uploadContentType;//封装上传文件类型属性
	private String uploadFileName;//封装上传文件名属性
	private String savePath;//直接在struts.xml文件中配置的属性
	/**************************文件上传参数End**************************/
	
	
	
	/********************方法类*************************/
	/**
	 * 根据教研室Id查找教职工
	 * @author SunYi
	 * @Date 2019年3月2日上午10:45:54
	 * @return String
	 */
	public String findUserByUnit() throws Exception{
		
		try {
			String unitId = (String) getSession().getAttribute("unitId");
			String jobNumber = (String) getSession().getAttribute("jobNumber");
			listUser = UserService.findUserByUnitId(unitId, jobNumber);
			getSession().setAttribute("listUser", listUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	
	private void init(){

		unitsList=UserService.findAllUnits();
	}

	/**
	 * @Author: soldier
	 * @Date: 2019/3/1 22:32
	 * @Desc: 每次页面请求都重新获取数据库‘教研室’数据
	 */
	private void RequestAddlistUnit() {

		listUnit = UnitService.findAll();
		getRequest().setAttribute("listUnit", listUnit);
	}
	
	public String allUnits()throws Exception{
		try{
			dataJson=new ResponeJson();
			//读取模板编号
			String q=getRequest().getParameter("q");

			String keyword =URLDecoder.decode(q,"UTF-8");
			logger.info("#allUnits:q:"+keyword);
			unitsList=UserService.findUnitsByID(keyword);
			List<AutoCompleteHttpModel> rs =new ArrayList<AutoCompleteHttpModel>();
			for(Map.Entry<String, String> entry:unitsList.entrySet()){
				AutoCompleteHttpModel model=new AutoCompleteHttpModel();
				model.setItemId(entry.getKey());
				model.setItemName(entry.getKey());
				model.setItemValue(entry.getKey());
				rs.add(model);
			}
			dataJson.setObj(rs);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
			dataJson.setSuccess(true);


		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	/**
	 * 校验用户信息
	 * @return
	 * @throws Exception
	 */
	public String checkLoginName() throws Exception{
		logger.info("## 校验用户名    ");
		PrintWriter out=getResponse().getWriter();

		listUser=UserService.findByExample(model);
		if(null!=listUser && 0<listUser.size()){//不可用
			out.print("false");
		}else{//可用
			out.print("true");
		}
		out.flush();
		out.close();
		return null;
	}

	public String list() throws Exception{
		try{
			logger.info("##用户管理列表");
			init();
			RequestAddlistUnit();

			pageResult= UserService.find(model, getPage(), getRow());
			footer=PageUtil.pageFooter(pageResult, getRequest());

		}catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	public String findByExample() throws Exception{
		try{
			logger.info("##用户管理列表");
			init();
			RequestAddlistUnit();
			if ((model.getHelpName() != null && !"".equals(model.getHelpName()))
					|| (model.getHelpUnit() != null
					&& !"".equals(model.getHelpUnit()))) {

				if(thisUnit!=null && !"".equals(thisUnit)){
					model.setHelpUnit(thisUnit);
				}
				listUser= UserService.findByExample(model);
				isExample = "1";
			}else{

				if(thisUnit!=null && !"".equals(thisUnit)){
					model.setHelpUnit(thisUnit);
				}
				pageResult= UserService.find(model, getPage(), getRow());
				footer=PageUtil.pageFooter(pageResult, getRequest());
				isExample = "0";
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	public String openAdd() throws Exception{
		try{
			RequestAddlistUnit();

			//查询省，用于界面显示
		}catch (Exception e){
			e.printStackTrace();
		}
		return resUri;
	}

	/**
	 * 新增用户
	 * @return
	 * @throws Exception
	 */
	public String add()throws Exception{
		try{
			logger.info("##添加用户信息");

			if(null!=getUpload()){
				String thisUri = uploadPortrait(model.getHelpUserName()+"");
				model.setPortrait(thisUri);  //头像
			}

			UserService.add(model);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	public String openEdit() throws Exception{
		try {
			logger.info("## 打开用户修改界面");
			model = UserService.findById(Integer.parseInt(thisUserId));
			RequestAddlistUnit();


		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	public String edit() throws Exception{
		try{
			logger.info("##管理员修改用户信息");
			RequestAddlistUnit();

			if(null!=getUpload()){
				String thisUri = uploadPortrait(model.getHelpUserName()+"");
				model.setPortrait(thisUri);

				//查询项目，替换头像
				XmInfo findXmInfo = new XmInfo();
				findXmInfo.setUserId(model.getId()+"");
				List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
				for (int i = 0; i < listXmInfo.size(); i++) {
					findXmInfo = listXmInfo.get(i);

					findXmInfo.setPortrait(thisUri);

					xmInfoService.update(findXmInfo);
				}
			}


			/**
			 * 修改用户类型：'1：学院领导；2：教研室/实验室负责人；3：普通教师'
			 */
			if (model.getLeaderTypeId() == 1) {  //如果是学院领导
				model.setUserType("1_2_3_4_5_6_7_8");
			}
			if (model.getLeaderTypeId() == 2) {  //教研室/实验室负责人
				model.setUserType("2_3_4_5");
			}
			if (model.getLeaderTypeId() == 3) {  //普通教师
				model.setUserType("2_3_5");
			}
			UserService.update(model);

		}catch (Exception e) {
			e.printStackTrace();
		}
		thisState="1";
		return resUri;
	}

	public String del() throws Exception{
		logger.info("##删除用户信息");
		UserService.del(Integer.parseInt(delUserId));
		RequestAddlistUnit();
		return list();
	}

	public String openEditInfo() throws Exception{
		try {
			logger.info("## 打开个人信息修改界面");
			User User=(User) getSession().getAttribute("userinfo");
			model = UserService.findById(User.getId());

			RequestAddlistUnit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	public String editInfo() {
		try {
			logger.info("## 用户个人信息修改");

			User thisUser = UserService.findById(model.getId());
			RequestAddlistUnit();

			thisUser.setHelpPassword(model.getHelpPassword());
			thisUser.setHelpName(model.getHelpName());
			thisUser.setHelpUnit(model.getHelpUnit());
			thisUser.setHelpPost(model.getHelpPost());
			thisUser.setHelpTelephone(model.getHelpTelephone());
			thisUser.setHelpSex(model.getHelpSex());

			if(null!=getUpload()){
				String thisUri = uploadPortrait(thisUser.getHelpUserName()+"");
				thisUser.setPortrait(thisUri);

				//查询项目，替换头像
				XmInfo findXmInfo = new XmInfo();
				findXmInfo.setUserId(model.getId()+"");
				List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
				for (int i = 0; i < listXmInfo.size(); i++) {
					findXmInfo = listXmInfo.get(i);

					findXmInfo.setPortrait(thisUri);

					xmInfoService.update(findXmInfo);
				}
			}

			/**
			 * 修改用户类型：'1：学院领导；2：教研室/实验室负责人；3：普通教师'
			 */
			if (model.getLeaderTypeId() == 1) {  //如果是学院领导
				thisUser.setUserType("1_2_3_4_5_6_7_8");
			}
			if (model.getLeaderTypeId() == 2) {  //教研室/实验室负责人
				thisUser.setUserType("2_3_4_5");
			}
			if (model.getLeaderTypeId() == 3) {  //普通教师
				thisUser.setUserType("2_3_5");
			}
			UserService.update(thisUser);

			getRequest().getSession().removeAttribute("onlineUser");
			getRequest().getSession().removeAttribute("jsId");
			getRequest().getSession().removeAttribute("userinfo");
			getRequest().getSession().removeAttribute("gh");
			getRequest().getSession().removeAttribute("fullName");
			getRequest().getSession().removeAttribute("grant");
			getRequest().getSession().invalidate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}

	/**
	 * 文件上传
	 * @return
	 * @author liqing
	 * @date 2016.03.15
	 * @throws Exception
	 */
	public String uploadPortrait(String userID) throws Exception{
		try {
			User userHelp=(User) getSession().getAttribute("userinfo");

			DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String dateStr = sdf.format(new Date());

			int pos = getUploadFileName().lastIndexOf( "." );

			String saveUrl="/attached/user/portrait/"+userID+"_"+dateStr+getUploadFileName().substring(pos);

			if(null!=getUpload()){

				byte[] bytes = ImageUtils.resize(ImageIO.read(getUpload()), 400, 1f, false);

				FileOutputStream fos = new FileOutputStream(getSavePath()+"\\"+userID+"_"+dateStr+getUploadFileName().substring(pos));

				// 将字节数组bytes中的数据，写入文件输出流fos中
				fos.write(bytes);
				fos.flush();
			}

			return saveUrl;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String likeAll() {
		logger.info("likeAll11 user...");
		dataJson=new ResponeJson();
		RequestAddlistUnit();
		try{
			//读取模板编号
			String q=getRequest().getParameter("q");
			logger.info("#检索所有的模板:q:"+q);
			String keyword =URLDecoder.decode(q,"UTF-8");
			logger.info("#检索所有的模板:转码:"+keyword);
			List<AutoCompleteHttpModel> rs =UserService.likeAll(keyword);
			dataJson.setObj(rs);
			dataJson.setMsg(ErrorMessage.WEB_SUC_000);
			dataJson.setSuccess(true);

		}catch(Exception e){
			dataJson.setMsg(ErrorMessage.WEB_ERR_014);
			dataJson.setSuccess(false);
		}
		return SUCCESS;
	}
	/****************参数的getter和setter方法****************/
	public ResponeJson getDataJson() {
		return dataJson;
	}
	public void setDataJson(ResponeJson dataJson) {
		this.dataJson = dataJson;
	}
	public List<User> getListUser() {
		return listUser;
	}
	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

	public List<Unit> getListUnit() {
		return listUnit;
	}

	public void setListUnit(List<Unit> listUnit) {
		this.listUnit = listUnit;
	}

	public Result<User> getPageResult() {
		return pageResult;
	}
	public void setPageResult(Result<User> pageResult) {
		this.pageResult = pageResult;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getThisState() {
		return thisState;
	}
	public void setThisState(String thisState) {
		this.thisState = thisState;
	}
	public String getIsMenu() {
		return isMenu;
	}
	public void setIsMenu(String isMenu) {
		this.isMenu = isMenu;
	}
	public String[] getThisHelpUserType() {
		return thisHelpUserType;
	}
	public void setThisHelpUserType(String[] thisHelpUserType) {
		this.thisHelpUserType = thisHelpUserType;
	}
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	public String getDelUserId() {
		return delUserId;
	}
	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}
	public String[] getThisHelpUserDuty() {
		return thisHelpUserDuty;
	}
	public void setThisHelpUserDuty(String[] thisHelpUserDuty) {
		this.thisHelpUserDuty = thisHelpUserDuty;
	}
	public String getIsExample() {
		return isExample;
	}
	public void setIsExample(String isExample) {
		this.isExample = isExample;
	}
	public Map<String, String> getUnitsList() {
		return unitsList;
	}
	public void setUnitsList(Map<String, String> unitsList) {
		this.unitsList = unitsList;
	}
	public InputStream getIs() {
		return is;
	}
	public void setIs(InputStream is) {
		this.is = is;
	}
	public String getThisUnit() {
		return thisUnit;
	}
	public void setThisUnit(String thisUnit) {
		this.thisUnit = thisUnit;
	}
	public String getIsOneAction() {
		return isOneAction;
	}
	public void setIsOneAction(String isOneAction) {
		this.isOneAction = isOneAction;
	}
	public String getThisUserType() {
		return thisUserType;
	}
	public void setThisUserType(String thisUserType) {
		this.thisUserType = thisUserType;
	}
	public String getThisUserDuty() {
		return thisUserDuty;
	}
	public void setThisUserDuty(String thisUserDuty) {
		this.thisUserDuty = thisUserDuty;
	}
	/****************上传的getter和setter方法Start****************/
	//接受struts.xml文件配置的方法
	public void setSavePath(String value) {
		this.savePath = value;
	}

	//返回上传文件保存位置
	public String getSavePath() throws Exception{
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}

	public String getTitle() {
		return (this.title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUpload() {
		return (this.upload);
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return (this.uploadContentType);
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return (this.uploadFileName);
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	/****************上传的getter和setter方法End****************/
}
