package com.gxwzu.system.action.login;

import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.business.model.XmChild;
import com.gxwzu.business.service.project.UnitService;
import com.gxwzu.business.service.project.XmChildService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.util.SysConstant;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.core.web.listener.UserBindingListener;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.user.UserService;

/**
 * 登陆
 * @author  liqing
 * @version  1.0
 * <br>Copyright (C), 2015, 梧州学院 软件研发中心
 * <br>This program is protected by copyright laws.
 * <br>Program Name: LoginAction
 * <br>Date: 2015-8-28下午06:39:24
 * <br>log:1.创建登陆方法类（liqing）
 */
public class LoginAction extends BaseAction {

	private static final long serialVersionUID = -2874663229964813880L;

	protected final Log logger = LogFactory.getLog(getClass());

	private static final String VIEW="view";
	private String view;
	private Result<XmChild> childPageResult;
	private XmChild xmChild = new XmChild();

	/***********************注入Service******************************/
	private UserService userService;
	private UnitService unitService;
	@Autowired
	private XmChildService xmChildService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}
	

	/***********************参数列表******************************/
	private String fullName;
	/**************************方法类Start**************************/
	@Override
	public String execute() throws Exception {

		return SUCCESS;
	}

	/**
	 * 登陆
	 * @return
	 * @throws Exception
	 * @log：1.创建方法（mengyiwen）
	 */
	public String login() throws Exception{
		try {
			String LoginName = getParameters("LoginName");
			String Password = getParameters("Password");
			
			if(null!=LoginName && !"".equals(LoginName) && null!=Password && !"".equals(Password)){

				List<User> listUser = userService.findByUseridAndPwd(LoginName, Password);

				if(!listUser.isEmpty()){//登录成功
					User currentUser=listUser.get(0);
					getSession().setAttribute("userinfo", currentUser);
					getSession().setAttribute("unitId", currentUser.getUnitId());// 用户教研室Id
					getSession().setAttribute("jobNumber", currentUser.getJobNumber());// 用户工号
					getSession().setAttribute("username", currentUser.getHelpUserName());// 用户姓名
					getSession().setAttribute("UserType", currentUser.getUserType());//用户角色
					getSession().setAttribute("grant", SysConstant.GRANT);
					getSession().setAttribute("GG_USERID", currentUser.getHelpName());//姓名
					getSession().setAttribute("HELP_USERID", currentUser.getId());//用户编号
					getSession().setAttribute("GRANT", SysConstant.GRANT);//session 有效的标识
					getSession().setAttribute("GRANT", SysConstant.GRANT);//session 有效的标识
					getSession().setAttribute("leaderTypeId", currentUser.getLeaderTypeId());//学院领导编号
					
					// 绑定会话登录用户,监听用户是否退出 系统
					String jsessionid = getSession().getId();
					String userid = currentUser.getHelpName();
					String username = currentUser.getHelpUserName();
					String ipAdress = getRequest().getRemoteAddr();
					UserBindingListener onlineUser = new UserBindingListener(jsessionid, userid, username, ipAdress);
					UserBindingListener.setUserBindingListener(onlineUser);
					getSession().setAttribute("onlineUser", onlineUser);
					System.out.println(SUCCESS+"////");
					return SUCCESS;

				}else{
					return errorHint(LoginName, Password, "用户名或密码错误");
				}
			}else{
				return errorHint(LoginName, Password, "用户名或密码不能为空");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 错误提示
	 * @param loginName  登陆账号
	 * @param password   密码
	 * @param errorInfo  错误提示信息
	 * @return
	 */
	private String errorHint(String loginName, String password, String errorInfo){
		getRequest().setAttribute("LoginName", loginName);
		getRequest().setAttribute("Password", password);
		getRequest().setAttribute("errorInfo", errorInfo);
		return LOGOUT;
	}
	/**
	 * 系统页面跳转
	 * @return
	 * @throws Exception
	 */
	public String view() throws Exception{
		try {
			if(null!=getSession().getAttribute("userinfo")){
				User User = (User) getSession().getAttribute("userinfo");
				if(view == "index2" || view.equals("index2")) {
					try {
						childPageResult=xmChildService.findByUser(xmChild, page, 10);
						footer=PageUtil.pageFooter(childPageResult, getRequest());

						List<User> users = userService.findAll();
						getRequest().setAttribute("users", users);
						List<Unit> units= unitService.findAll();
						getRequest().setAttribute("units", units);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return VIEW;
			}else{
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String toIndex() {

		return "toIndex";
	}

	/**
	 * 退出
	 * @return
	 * @throws Exception
	 * @log：1.创建方法（liqing）
	 */
	public String logout()throws Exception{

		getRequest().getSession().removeAttribute("onlineUser");
		getRequest().getSession().removeAttribute("jsId");
		getRequest().getSession().removeAttribute("userinfo");
		getRequest().getSession().removeAttribute("gh");
		getRequest().getSession().removeAttribute("fullName");
		getRequest().getSession().removeAttribute("grant");
		getRequest().getSession().invalidate();

		return LOGOUT;
	}
	/**************************方法类End**************************/



	/****************参数的getter和setter方法Start****************/
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	public Result<XmChild> getChildPageResult() {
		return childPageResult;
	}
	public void setChildPageResult(Result<XmChild> childPageResult) {
		this.childPageResult = childPageResult;
	}
	public XmChild getXmChild() {
		return xmChild;
	}
	public void setXmChild(XmChild xmChild) {
		this.xmChild = xmChild;
	}
	
}
