package com.gxwzu.app.project;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmInvest;
import com.gxwzu.business.service.project.XmDutyService;
import com.gxwzu.business.service.project.XmInvestService;
import com.gxwzu.core.web.action.BaseAction;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmDutyAction extends BaseAction implements Preparable,ModelDriven<XmDuty>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmDuty model=new XmDuty();
	@Override
	public XmDuty getModel() {
		return model;
	}
	public void setModel(XmDuty model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmDuty();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmDutyService xmDutyService;
	
	public void setXmDutyService(XmDutyService xmDutyService) {
		this.xmDutyService = xmDutyService;
	}
	/***********************声明参数******************************/
	private String actionState;
	private String jsonXmDuty;
	
	private List<XmDuty> listXmDuty;
	
	private String thisXmNumber;
	private String thisXmDutyId;
	
	/**************************方法类**************************/
	/**
	 * 查询责任人记录，通过项目编号：http://127.0.0.1:8080/dxxy/appf/appXmDuty_findXmDutyByNum.action?
	 * 参数：thisXmNumber（项目编号）
	 * 返回值：json（com.gxwzu.business.model.XmDuty对象的List）
	 */
	public String findXmDutyByNum() {
		List<XmDuty> listXmDuty = new ArrayList<XmDuty>();
		try {
			XmDuty findXmDuty = new XmDuty();
			findXmDuty.setXmNumber(thisXmNumber);
			listXmDuty = xmDutyService.findByExample(findXmDuty);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listXmDuty);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 添加责任人记录：http://127.0.0.1:8080/dxxy/appf/appXmDuty_add.action?
	 * 关键数据（com.gxwzu.business.model.XmDuty，封装json对象：jsonXmDuty）
	 *        ：xmNumber（项目编号）
	 *          unit（责任单位）
	 *          people（责任人）
	 * 返回：actionState：1成功；0失败
	 */
	public String add() {
		try {
			Gson gson = new Gson();
			model = gson.fromJson(jsonXmDuty, XmDuty.class);
			if(model.getXmNumber() != null && !"".equals(model.getXmNumber())){
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = sdf.format(new Date());
				model.setCreateTime(Timestamp.valueOf(dateStr));
				xmDutyService.save(model);
				
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除责任人记录：http://127.0.0.1:8080/dxxy/appf/appXmDuty_del.action?
	 * 参数： thisXmDutyId（责任人记录ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String  del() {
		try {
			XmDuty delXmDuty = xmDutyService.findById(Integer.parseInt(thisXmDutyId));
			if(delXmDuty != null){
				xmDutyService.remove(delXmDuty);
			}
			actionState="1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/****************参数的getter和setter方法****************/
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public String getActionState() {
		return actionState;
	}
	public void setActionState(String actionState) {
		this.actionState = actionState;
	}
	public String getJsonXmDuty() {
		return jsonXmDuty;
	}
	public void setJsonXmDuty(String jsonXmDuty) {
		this.jsonXmDuty = jsonXmDuty;
	}
	public List<XmDuty> getListXmDuty() {
		return listXmDuty;
	}
	public void setListXmDuty(List<XmDuty> listXmDuty) {
		this.listXmDuty = listXmDuty;
	}
	public String getThisXmDutyId() {
		return thisXmDutyId;
	}
	public void setThisXmDutyId(String thisXmDutyId) {
		this.thisXmDutyId = thisXmDutyId;
	}
	
}
