package com.gxwzu.app.project;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmDuty;
import com.gxwzu.business.model.XmSource;
import com.gxwzu.business.service.project.XmDutyService;
import com.gxwzu.business.service.project.XmScheduleService;
import com.gxwzu.business.service.project.XmSourceService;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewAsk;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmSourceAction extends BaseAction implements Preparable,ModelDriven<XmSource>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmSource model=new XmSource();
	@Override
	public XmSource getModel() {
		return model;
	}
	public void setModel(XmSource model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmSource();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmSourceService xmSourceService;
	private XmScheduleService xmScheduleService;
	private XmDutyService xmDutyService;
	private UserService userService;
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setXmDutyService(XmDutyService xmDutyService) {
		this.xmDutyService = xmDutyService;
	}

	public void setXmScheduleService(XmScheduleService xmScheduleService) {
		this.xmScheduleService = xmScheduleService;
	}

	public void setXmSourceService(XmSourceService xmSourceService) {
		this.xmSourceService = xmSourceService;
	}
	/***********************声明参数******************************/
	private String jsonXmSource;
	
	private List<XmSource> listXmSource;//回复列表
	private String thisXmNumber;
	private String thisUserId;
	private String thisType;//类型：01工作进度，02存在问题，03下一步工作计划
	
	private String title;//封装文件标题请求参数的属性
	private File upload;//封装上传文件域的属性
	private String uploadContentType;//封装上传文件类型属性
	private String uploadFileName;//封装上传文件名属性
	private String savePath;//直接在struts.xml文件中配置的属性
	
	/**************************方法类**************************/
	/**
	 * 查询批示内容（工作要求）：http://127.0.0.1:8080/dxxy/appf/appXmSource_listAsk.action?
	 * 参数：thisUserId（用户ID）
	 * 返回值：json（ccom.gxwzu.sysVO.ViewAsk对象的List）
	 */
	public String listAsk() {
		Result<ViewAsk> pageViewAsk = new Result<ViewAsk>();
		try {
			if(thisUserId != null && !"".equals(thisUserId)){
				ViewAsk viewAsk = new ViewAsk();
				viewAsk.setPeopleId(Integer.parseInt(thisUserId));
				pageViewAsk=xmSourceService.findListAsk(viewAsk, getPage(), 20);
				footer=PageUtil.pageFooter(pageViewAsk, getRequest());
			}else{
				ViewAsk viewAsk = new ViewAsk();
				pageViewAsk=xmSourceService.findListAsk(viewAsk, getPage(), 20);
				footer=PageUtil.pageFooter(pageViewAsk, getRequest());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(pageViewAsk);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询对应交办事项下的责任人：http://127.0.0.1:8080/dxxy/appf/appXmSource_listDuty.action?
	 * 参数：thisXmNumber（项目编号）
	 * 返回值：json（com.gxwzu.business.model.XmDuty对象的List）
	 */
	public String listDuty() {
		List<XmDuty> listXmDuty = new ArrayList<XmDuty>();
		try {
			if(thisXmNumber != null && !"".equals(thisXmNumber)){
				XmDuty findXmDuty = new XmDuty();
				findXmDuty.setXmNumber(thisXmNumber);
				listXmDuty = xmDutyService.findByExample(findXmDuty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listXmDuty);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/****************参数的getter和setter方法****************/
	//接受struts.xml文件配置的方法
	public void setSavePath(String value) {
		this.savePath = value;
	}

	//返回上传文件保存位置
	public String getSavePath() throws Exception{
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}
	
	public String getTitle() {
		return (this.title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUpload() {
		return (this.upload);
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return (this.uploadContentType);
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return (this.uploadFileName);
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	public String getJsonXmSource() {
		return jsonXmSource;
	}
	public void setJsonXmSource(String jsonXmSource) {
		this.jsonXmSource = jsonXmSource;
	}
	public List<XmSource> getListXmSource() {
		return listXmSource;
	}
	public void setListXmSource(List<XmSource> listXmSource) {
		this.listXmSource = listXmSource;
	}
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public String getThisType() {
		return thisType;
	}
	public void setThisType(String thisType) {
		this.thisType = thisType;
	}
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	
}
