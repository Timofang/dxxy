package com.gxwzu.app.project;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.gxwzu.sysVO.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.service.project.XmCapitalService;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.web.action.BaseAction;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmInfoAction extends BaseAction implements Preparable,ModelDriven<XmInfo>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());

	/***********************实例化ModelDriven******************************/
	private XmInfo model=new XmInfo();
	@Override
	public XmInfo getModel() {
		return model;
	}
	public void setModel(XmInfo model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmInfo();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmInfoService xmInfoService;
	private XmCapitalService xmCapitalService;
	private XmLandprojService xmLandprojService;
	
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}
	public void setXmCapitalService(XmCapitalService xmCapitalService) {
		this.xmCapitalService = xmCapitalService;
	}
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	/***********************声明参数******************************/
	private String jsonUserHelp;
	
	private List<XmInfo> listXmInfo;//回复列表
	
	private String title;//封装文件标题请求参数的属性
	private File upload;//封装上传文件域的属性
	private String uploadContentType;//封装上传文件类型属性
	private String uploadFileName;//封装上传文件名属性
	private String savePath;//直接在struts.xml文件中配置的属性
	
	private List<ViewXmInfo> listViewXmInfo;
	private String thisYear;
	private String thisUserId;
	private String thisBudgetType;
	private String thisProjectType;
	private String thisXmNumber;
	private String thisInfoName;
	private String thisPsNumber;
	private String thisUserName;
	
	private List<ViewToLandproj> listViewToLandproj;
	
	/**************************方法类**************************/
	/**
	 * 模糊查询，查询项目，并统计投资完成率：http://127.0.0.1:8080/dxxy/appf/appXmInfo_likeViewXmInfoByYear.action?
	 * 参数：thisInfoName（项目名称）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmInfo对象的List）
	 */
	public String likeViewXmInfoByYear() {
		List<ViewXmInfo> allViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			allViewXmInfo = xmInfoService.allViewXmInfoByYear(null, null, null, thisInfoName, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(allViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String allViewXmType(){
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String items=xmInfoService.allViewXmType();
        PrintWriter pw = getPrintWriter();
        pw.print(items);
        pw.flush();
        pw.close();
		return null;
	}

	/**
	 * 学院任务-点击头像查看相应用户的项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public String allViewXmByUser() {
		
		logger.info(jsonUserHelp, null);
		List<ViewXm> allViewXm = new ArrayList<ViewXm>();
		try {
			allViewXm = xmInfoService.allViewXmByUser(thisUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(allViewXm);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 学院任务-学院任务首页查看所有总项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public String allViewXm() {
		
		logger.info(jsonUserHelp, null);
		List<ViewXm> allViewXm = new ArrayList<ViewXm>();
		try {
			allViewXm = xmInfoService.allViewXm(thisBudgetType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(allViewXm);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询所项目，通过年份，并统计投资完成率：http://127.0.0.1:8080/dxxy/appf/appXmInfo_allViewXmInfoByYear.action?
	 * 参数：thisYear（年份）
	 * 		thisBudgetType（项目层次（大类）：01中央预算，02自治区层面，03市级层面，04项目建设丰收年，05现代服务业重点项目（复选：01_02_03））
	 * 		thisProjectType（项目性质（小类）：01新开工项目，02续建项目，03重大前期工作，04竣工或部分竣工）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmInfo对象的List）
	 */
	public String allViewXmInfoByYear() {
		
		logger.info(thisYear+thisBudgetType+thisProjectType);
		List<ViewXmInfo> allViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			allViewXmInfo = xmInfoService.allViewXmInfoByYear(thisYear, null, thisBudgetType, null, thisProjectType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(allViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String allViewXmPoorInfoByYear() {
		
		List<ViewXmInfo> allViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			allViewXmInfo = xmInfoService.allViewXmPoorInfoByYear(thisYear, null, thisBudgetType, null, thisProjectType);
			logger.info(new Gson().toJson(allViewXmInfo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(allViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public String allViewXmInfoByUser() {
		
		List<ViewXmInfo> userViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			userViewXmInfo = xmInfoService.allViewXmInfoByYear(thisYear, thisUserId, thisBudgetType, null, thisProjectType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(userViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 学院任务-学院任务首页查看所有总项目信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 *
	 * @return
	 */
public String findXmByNum() {
		
		List<ViewXm> userViewXm = new ArrayList<ViewXm>();
		try {
			userViewXm = xmInfoService.findXmByNum(thisXmNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(userViewXm);
	        System.out.println(json);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String allViewXmPoorInfoByUser() {
		
		logger.info("allViewXmPoorInfoByUser:"+thisYear+thisBudgetType+thisUserId);
		
		List<ViewXmInfo> userViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			userViewXmInfo = xmInfoService.allViewXmPoorInfoByYear(thisYear, thisUserId, thisBudgetType, null, thisProjectType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(userViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 用户-项目列表（取最新一条）：http://127.0.0.1:8080/dxxy/appf/appXmInfo_userViewXmInfoByYear.action?
	 * 参数：thisYear（年份）
	 * 		thisBudgetType（项目层次（大类）：01中央预算，02自治区层面，03市级层面，04项目建设丰收年，05现代服务业重点项目（复选：01_02_03））
	 * 		thisProjectType（项目性质（小类）：01新开工项目，02续建项目，03重大前期工作，04竣工或部分竣工）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmInfo对象的List）
	 */
	public String userViewXmInfoByYear() {
		List<ViewXmInfo> userViewXmInfo = new ArrayList<ViewXmInfo>();
		try {
			userViewXmInfo = xmInfoService.userViewXmInfoByYear(thisYear, thisBudgetType, thisProjectType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(userViewXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 通过项目编号，查询项目基本信息：http://127.0.0.1:8080/dxxy/appf/appXmInfo_findXmInfoByNum.action?
	 * 参数：thisXmNumber（项目编号）
	 * 返回：json（com.gxwzu.business.model.XmInfo对象）
	 * @return
	 */
	public String findXmInfoByNum() {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ViewInfo thisXmInfo = new ViewInfo();
		try {
			XmInfo findXmInfo = new XmInfo();
			findXmInfo.setXmNumber(thisXmNumber);
			List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
			
			if(listXmInfo.size()>0){
				findXmInfo = listXmInfo.get(0);
				
				String dateStart = "";
				if(findXmInfo.getStartTime() != null && !"".equals(findXmInfo.getStartTime())){
					dateStart = sdf.format(findXmInfo.getStartTime());
				}
				
				String dateCompletionTime = "";
				if(findXmInfo.getCompletionTime() != null && !"".equals(findXmInfo.getCompletionTime())){
					dateCompletionTime = sdf.format(findXmInfo.getCompletionTime());
				}
				
				String dateCreateTime = "";
				if(findXmInfo.getCreateTime() != null && !"".equals(findXmInfo.getCreateTime())){
					dateCreateTime = sdf.format(findXmInfo.getCreateTime());
				}
				
				thisXmInfo.setId(findXmInfo.getId());
				thisXmInfo.setXmNumber(findXmInfo.getXmNumber());
				thisXmInfo.setXmYear(findXmInfo.getXmYear());
				thisXmInfo.setName(findXmInfo.getName());
				thisXmInfo.setScale(findXmInfo.getScale());
				thisXmInfo.setPeriod(findXmInfo.getPeriod());
				thisXmInfo.setCycle(findXmInfo.getCycle());
				thisXmInfo.setOwner(findXmInfo.getOwner());
				thisXmInfo.setCounty(findXmInfo.getCounty());
				thisXmInfo.setStartTime(dateStart);
				thisXmInfo.setCompletionTime(dateCompletionTime);
				thisXmInfo.setAddress(findXmInfo.getAddress());
				thisXmInfo.setIndustry(findXmInfo.getIndustry());
				thisXmInfo.setIsState(findXmInfo.getIsState());
				thisXmInfo.setIsCompleted(findXmInfo.getIsCompleted());
				thisXmInfo.setBudgetType(findXmInfo.getBudgetType());
				thisXmInfo.setProjectType(findXmInfo.getProjectType());
				thisXmInfo.setUserId(findXmInfo.getUserId());
				thisXmInfo.setUserName(findXmInfo.getUserName());
				thisXmInfo.setPortrait(findXmInfo.getPortrait());
				thisXmInfo.setCreateTime(dateCreateTime);
				thisXmInfo.setCoordinate(findXmInfo.getCoordinate());
				thisXmInfo.setCoordinatePly(findXmInfo.getCoordinatePly());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(thisXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 任务列表-任务列表首页查看所有子项目任务
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String findAllXmChild(){

		List<ViewXmChild> ViewXmChild = new ArrayList<ViewXmChild>();
		try {
			ViewXmChild = xmInfoService.findAllXmChild();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXmChild);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 任务列表-点击项目查看详细信息
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String findXmChildByNum(){

		List<ViewXmChild> ViewXmChild = new ArrayList<ViewXmChild>();
		try {
			ViewXmChild = xmInfoService.findXmChildByNum(thisUserId,thisInfoName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXmChild);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 任务列表-点击头像查询所有子项目任务
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String allViewXmChildByUser(){

		List<ViewXmChild> ViewXmChild = new ArrayList<ViewXmChild>();
		try {
			ViewXmChild = xmInfoService.allViewXmChildByUser(thisUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXmChild);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 我的下发-我的下发首页查询所有我下发的项目（院领导）
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String findMyIssueByCollager(){

		List<ViewXm> ViewXm= new ArrayList<ViewXm>();
		try {
			ViewXm = xmInfoService.findMyIssueByCollager(thisUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXm);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 我的下发-查看详细项目（院领导）
	 * @author 叶城廷
	 * @version 2019.03.15
	 * @return
	 */
	public  String findMyIssueByCollagerByNum(){

		List<ViewXm> ViewXm= new ArrayList<ViewXm>();
		try {
			ViewXm = xmInfoService.findMyIssueByCollagerByNum(thisUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXm);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 我的下发-我的下发首页查询所有我下发的项目（教研室领导）
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String findMyIssueByUnit(){

		List<ViewXmChild> ViewXmChild = new ArrayList<ViewXmChild>();
		try {
			ViewXmChild = xmInfoService.findMyIssueByUnit(thisUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXmChild);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 我的下发-我的下发首页查询所有我下发的项目（教研室领导）
	 * @author 叶城廷
	 * @version 2019.03.14
	 * @return
	 */
	public  String findMyIssueByUnitByNum(){

		List<ViewXmChild> ViewXmChild = new ArrayList<ViewXmChild>();
		try {
			ViewXmChild = xmInfoService.findMyIssueByUnitByNum(thisInfoName,thisUserName,thisPsNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = g.toJson(ViewXmChild);
			System.out.println(json);
			PrintWriter pw = getPrintWriter();
			pw.print(json);
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * 通过项目编号，查询项目资金信息：http://127.0.0.1:8080/dxxy/appf/appXmInfo_findXmCapitalByNum.action?
	 * 参数：thisXmNumber（项目编号）
	 * 		thisYear（年份）
	 * 返回：json（com.gxwzu.sysVO.ViewXmCapital对象）
	 * @return
	 */
	public String findXmCapitalByNum() {
		ViewXmCapital thisViewXmCapital = new ViewXmCapital();
		try {
			List<ViewXmCapital> listViewXmCapital = xmCapitalService.findCapitalInRate(thisXmNumber, thisYear);
			if(listViewXmCapital.size()>0){
				thisViewXmCapital = listViewXmCapital.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(thisViewXmCapital);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 项目所关联的征地信息，通过项目编号查询：http://127.0.0.1:8080/dxxy/appf/appXmInfo_findToLandproj.action?
	 * 参数：thisXmNumber（项目编号）
	 * 返回：json（com.gxwzu.sysVO.ViewToLandproj对象list）
	 * @return
	 */
	public String findToLandproj() {
		try {
			listViewToLandproj = xmLandprojService.toLandproj(thisXmNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewToLandproj);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/****************参数的getter和setter方法****************/
	//接受struts.xml文件配置的方法
	public void setSavePath(String value) {
		this.savePath = value;
	}

	//返回上传文件保存位置
	public String getSavePath() throws Exception{
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}
	
	public String getTitle() {
		return (this.title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUpload() {
		return (this.upload);
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return (this.uploadContentType);
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return (this.uploadFileName);
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	
	public String getJsonUserHelp() {
		return jsonUserHelp;
	}
	public void setJsonUserHelp(String jsonUserHelp) {
		this.jsonUserHelp = jsonUserHelp;
	}
	public List<XmInfo> getListXmInfo() {
		return listXmInfo;
	}
	public void setListXmInfo(List<XmInfo> listXmInfo) {
		this.listXmInfo = listXmInfo;
	}
	public List<ViewXmInfo> getListViewXmInfo() {
		return listViewXmInfo;
	}
	public void setListViewXmInfo(List<ViewXmInfo> listViewXmInfo) {
		this.listViewXmInfo = listViewXmInfo;
	}
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	public String getThisYear() {
		return thisYear;
	}
	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}
	public String getThisBudgetType() {
		return thisBudgetType;
	}
	public void setThisBudgetType(String thisBudgetType) {
		this.thisBudgetType = thisBudgetType;
	}
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public List<ViewToLandproj> getListViewToLandproj() {
		return listViewToLandproj;
	}
	public void setListViewToLandproj(List<ViewToLandproj> listViewToLandproj) {
		this.listViewToLandproj = listViewToLandproj;
	}
	public String getThisInfoName() {
		return thisInfoName;
	}
	public void setThisInfoName(String thisInfoName) {
		this.thisInfoName = thisInfoName;
	}
	public String getThisProjectType() {
		return thisProjectType;
	}
	public void setThisProjectType(String thisProjectType) {
		this.thisProjectType = thisProjectType;
	}
	public String getThisUserName() {
		return thisUserName;
	}
	public void setThisUserName(String thisUserName) {
		this.thisUserName = thisUserName;
	}
	public String getThisPsNumber() {
		return thisPsNumber;
	}
	public void setThisPsNumber(String thisPsNumber) {
		this.thisPsNumber = thisPsNumber;
	}
	
	
}
