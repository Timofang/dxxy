package com.gxwzu.app.project;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewToXmInfo;
import com.gxwzu.sysVO.ViewXmLandproj;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmLandprojAction extends BaseAction implements Preparable,ModelDriven<XmLandproj>,ServletContextAware{

	private static final long serialVersionUID = -5047956014372103030L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmLandproj model=new XmLandproj();
	@Override
	public XmLandproj getModel() {
		return model;
	}
	public void setModel(XmLandproj model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmLandproj();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmLandprojService xmLandprojService;
	private XmInfoService xmInfoService;
	
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}
	/***********************声明参数******************************/
	private List<XmLandproj> listXmLandproj;
	private List<ViewXmLandproj> listViewXmLandproj;
	private List<ViewToXmInfo> listViewToXmInfo;
	
	private String thisYear;
	private String thisUserId;
	
	private String thisXmNumber;
	private String thisLandNumber;
	private String thisLandName;
	
	/**************************方法类**************************/
	/**
	 * 模糊查询，查询征地，并统计投资完成率：http://127.0.0.1:8080/dxxy/appf/appXmLandproj_likeXmLandproj.action?
	 * 参数：thisLandName（征地项目名称）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmLandproj对象的List）
	 */
	public String likeXmLandproj() {
		try {
			listViewXmLandproj = xmLandprojService.findXmLandproj(null, null, null, thisLandName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewXmLandproj);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询所征地，通过年份，并统计投资完成率：http://127.0.0.1:8080/dxxy/appf/appXmLandproj_findXmLandproj.action?
	 * 参数：thisYear（年份）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmLandproj对象的List）
	 */
	public String findXmLandproj() {
		try {
			listViewXmLandproj = xmLandprojService.findXmLandproj(thisYear, null, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewXmLandproj);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询对应用户，当年所有征地，通过用户ID，并统计投资完成率：http://127.0.0.1:8080/dxxy/appf/appXmLandproj_findXmLandprojByUser.action?
	 * 参数：thisYear（年份）
	 *       thisUserId（用户ID）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmLandproj对象的List）
	 */
	public String findXmLandprojByUser() {
		try {
			listViewXmLandproj = xmLandprojService.findXmLandproj(thisYear, thisUserId, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewXmLandproj);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 通过征地编号，查询项目基本信息：http://127.0.0.1:8080/dxxy/appf/appXmLandproj_findXmLandprojByNum.action?
	 * 参数：thisXmNumber（项目编号）
	 * 返回：json（com.gxwzu.business.model.XmLandproj对象）
	 * @return
	 */
	public String findXmLandprojByNum() {
		XmLandproj thisXmLandproj = new XmLandproj();
		try {
			XmLandproj findXmLandproj = new XmLandproj();
			findXmLandproj.setLandNumber(thisXmNumber);
			List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(findXmLandproj);
			if(listXmLandproj.size()>0){
				thisXmLandproj = listXmLandproj.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(thisXmLandproj);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 征地所关联的项目信息，通过项目编号查询：http://127.0.0.1:8080/dxxy/appf/appXmLandproj_findToXmInfo.action?
	 * 参数：thisLandNumber（征地编号）
	 * 返回：json（com.gxwzu.sysVO.ViewToXmInfo对象list）
	 * @return
	 */
	public String findToXmInfo() {
		try {
			listViewToXmInfo = xmInfoService.toXmInfo(thisLandNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(listViewToXmInfo);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/****************参数的getter和setter方法****************/
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	public String getThisYear() {
		return thisYear;
	}
	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}
	public List<XmLandproj> getListXmLandproj() {
		return listXmLandproj;
	}
	public void setListXmLandproj(List<XmLandproj> listXmLandproj) {
		this.listXmLandproj = listXmLandproj;
	}
	public List<ViewXmLandproj> getListViewXmLandproj() {
		return listViewXmLandproj;
	}
	public void setListViewXmLandproj(List<ViewXmLandproj> listViewXmLandproj) {
		this.listViewXmLandproj = listViewXmLandproj;
	}
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public List<ViewToXmInfo> getListViewToXmInfo() {
		return listViewToXmInfo;
	}
	public void setListViewToXmInfo(List<ViewToXmInfo> listViewToXmInfo) {
		this.listViewToXmInfo = listViewToXmInfo;
	}
	public String getThisLandNumber() {
		return thisLandNumber;
	}
	public void setThisLandNumber(String thisLandNumber) {
		this.thisLandNumber = thisLandNumber;
	}
	public String getThisLandName() {
		return thisLandName;
	}
	public void setThisLandName(String thisLandName) {
		this.thisLandName = thisLandName;
	}
	
}
