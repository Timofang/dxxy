package com.gxwzu.app.project;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmInvest;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmInvestService;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewInvest;
import com.gxwzu.sysVO.ViewXmInvest;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmInvestAction extends BaseAction implements Preparable,ModelDriven<XmInvest>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmInvest model=new XmInvest();
	@Override
	public XmInvest getModel() {
		return model;
	}
	public void setModel(XmInvest model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmInvest();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmInvestService xmInvestService;
	private XmInfoService xmInfoService;
	
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setXmInvestService(XmInvestService xmInvestService) {
		this.xmInvestService = xmInvestService;
	}
	/***********************声明参数******************************/
	private String actionState;
	private String jsonXmInvest;
	
	private List<XmInvest> listXmInvest;
	
	private String thisYear;
	private String thisUserId;
	private String thisXmNumber;
	private String thisXmInvestId;
	private String thisAllPay;
	
	/**************************方法类**************************/
	/**
	 * 查询投资记录，通过项目编号，年份：http://127.0.0.1:8080/dxxy/appf/appXmInvest_findXmInvestByNum.action?
	 * 参数：thisYear（年份）
	 * 		thisXmNumber（项目编号）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmInvest对象）
	 */
	public String findXmInvestByNum() {
		ViewXmInvest viewXmInvest = new ViewXmInvest();
		try {
			//1.获取投资进度列表
			List<ViewInvest> listViewInvest = new ArrayList<ViewInvest>();
			XmInvest findXmInvest = new XmInvest();
			findXmInvest.setXmNumber(thisXmNumber);
			findXmInvest.setInvestYear(thisYear);
			List<XmInvest> listXmInvest = xmInvestService.findByExample(findXmInvest);
			for (int i = 0; i < listXmInvest.size(); i++) {
				findXmInvest = listXmInvest.get(i);
				
				ViewInvest viewInvest = new ViewInvest();
				
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateCreateTime = sdf.format(findXmInvest.getCreateTime());
				
				viewInvest.setId(findXmInvest.getId());
				viewInvest.setXmNumber(findXmInvest.getXmNumber());
				viewInvest.setInvestment(findXmInvest.getInvestment());
				viewInvest.setPayment(findXmInvest.getPayment());
				viewInvest.setInvestYear(findXmInvest.getInvestYear());
				viewInvest.setInvestMonth(findXmInvest.getInvestMonth());
				viewInvest.setUserId(findXmInvest.getUserId());
				viewInvest.setUserName(findXmInvest.getUserName());
				viewInvest.setCreateTime(dateCreateTime);
				
				listViewInvest.add(viewInvest);
			}
			viewXmInvest.setListViewInvest(listViewInvest);
			
			//2.获取累计完成投资数，查项目表
			XmInfo thisXmInfo = new XmInfo();
			thisXmInfo.setXmNumber(thisXmNumber);
			List<XmInfo> listXmInfo = xmInfoService.findByExample(thisXmInfo);
			if(listXmInfo.size()>0){
				thisXmInfo = listXmInfo.get(0);
				viewXmInvest.setAllPay(thisXmInfo.getAllPay());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(viewXmInvest);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 添加投资记录：http://127.0.0.1:8080/dxxy/appf/appXmInvest_add.action?
	 * 关键数据1.（com.gxwzu.business.model.XmInvest，封装json对象：jsonXmInvest）
	 *        ：xmNumber（项目编号）
	 *          investment（当年完成投资）
	 *          payment（当年完成支付）
	 *          investYear（年份）
	 *          investMonth（月份）
	 *          userId（上传人编号）
	 *          userName（上传人姓名）
	 *        2.thisAllPay(累计完成投资，存项目表)
	 * 返回：actionState：1成功；0失败
	 */
	public String add() {
		try {
			Gson gson = new Gson();
			model = gson.fromJson(jsonXmInvest, XmInvest.class);
			if(model.getXmNumber() != null && !"".equals(model.getXmNumber())){
				//1.存储投资进度
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = sdf.format(new Date());
				model.setCreateTime(Timestamp.valueOf(dateStr));
				xmInvestService.save(model);
				
				//2.存储累计完成投资进度数，修改项目表
				if(thisAllPay != null && !"".equals(thisAllPay)){
					XmInfo findXmInfo = new XmInfo();
					findXmInfo.setXmNumber(model.getXmNumber());
					List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
					if(listXmInfo.size()>0){
						findXmInfo = listXmInfo.get(0);
						findXmInfo.setAllPay(thisAllPay);
						
						xmInfoService.update(findXmInfo);
					}
				}
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除投资记录：http://127.0.0.1:8080/dxxy/appf/appXmInvest_del.action?
	 * 参数： thisXmInvestId（投资记录ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String  del() {
		try {
			if(thisXmInvestId != null && !"".equals(thisXmInvestId)){
				XmInvest delXmInvest = xmInvestService.findById(Integer.parseInt(thisXmInvestId));
				if(delXmInvest != null){
					xmInvestService.remove(delXmInvest);
					actionState="1";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/****************参数的getter和setter方法****************/
	public String getThisUserId() {
		return thisUserId;
	}
	public void setThisUserId(String thisUserId) {
		this.thisUserId = thisUserId;
	}
	public String getThisYear() {
		return thisYear;
	}
	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}
	public String getJsonXmInvest() {
		return jsonXmInvest;
	}
	public void setJsonXmInvest(String jsonXmInvest) {
		this.jsonXmInvest = jsonXmInvest;
	}
	public List<XmInvest> getListXmInvest() {
		return listXmInvest;
	}
	public void setListXmInvest(List<XmInvest> listXmInvest) {
		this.listXmInvest = listXmInvest;
	}
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public String getActionState() {
		return actionState;
	}
	public void setActionState(String actionState) {
		this.actionState = actionState;
	}
	public String getThisXmInvestId() {
		return thisXmInvestId;
	}
	public void setThisXmInvestId(String thisXmInvestId) {
		this.thisXmInvestId = thisXmInvestId;
	}
	public String getThisAllPay() {
		return thisAllPay;
	}
	public void setThisAllPay(String thisAllPay) {
		this.thisAllPay = thisAllPay;
	}
	
}
