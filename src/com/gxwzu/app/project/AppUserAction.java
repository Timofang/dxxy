package com.gxwzu.app.project;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlAccessOrder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.gxwzu.business.model.XmInfo;
import com.gxwzu.business.model.XmLandproj;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmLandprojService;
import com.gxwzu.core.util.ImageUtils;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.system.model.log.Applog;
import com.gxwzu.system.model.user.User;
import com.gxwzu.system.service.log.ApplogService;
import com.gxwzu.system.service.user.UserService;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppUserAction extends BaseAction implements Preparable,ModelDriven<User>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private User model=new User();
	@Override
	public User getModel() {
		return model;
	}
	public void setModel(User model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new User();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private UserService userService;
	private ApplogService applogService;
	private XmInfoService xmInfoService;
	private XmLandprojService xmLandprojService;
	
	public void setXmLandprojService(XmLandprojService xmLandprojService) {
		this.xmLandprojService = xmLandprojService;
	}
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	public void setApplogService(ApplogService applogService) {
		this.applogService = applogService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/***********************声明参数******************************/
	private String jsonUser;
	
	private String loginNameValue;//手机、账号等。
	private String mobileVerCode;//手机验证码
	private String userPassword;//密码
	private String newPassword;//新密码
	private String confirmPassword;//密码确认
	private int loginType;//登录方式：1-qq，2-微信，3-微博
	private int actionState;//操作状态。1-操作成功（保存成功）
	private String verCodeForPhone;//手机验证码
	private String sign;//用户令牌
	private List<User> listUser;//回复列表
	private String feedbackContent;//用户反馈内容
	private String feedbackContact;//用户联系方式
	
	private String title;//封装文件标题请求参数的属性
	private File upload;//封装上传文件域的属性
	private String uploadContentType;//封装上传文件类型属性
	private String uploadFileName;//封装上传文件名属性
	private String savePath;//直接在struts.xml文件中配置的属性
	
	private Applog applog = new Applog();
	private String logUserId;
	private String logUserName;
	private String logText;
	
	private List<Applog> listApplog;
	private String ApplogId;
	/**************************方法类**************************/
	/**
	 * 用户登录：http://127.0.0.1:8080/dxxy/appf/appLogin_islogin.action?
	 * 参数：loginNameValue, userPassword
	 * 返回值：list（参数对象）
	 * @return
	 */
	public String islogin(){
		List list = null;
		try {
			list = userService.isLoginApp(loginNameValue, userPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(list);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			System.out.println("Gson出错："+e);
		}
		return null;
	} 
	
	/**
	 * 修改密码:http://127.0.0.1:8080/dxxy/appf/appLogin_modifyPassword.action?
	 * 参数：1.用户名：loginNameValue
	 * 		2.秘钥：sign
	 * 		3.原始密码：userPassword
	 * 		3.新密码：newPassword
	 * 		4.密码确认：confirmPassword
	 * 返回：actionState
	 * @return
	 */
	public String modifyPassword() {
		try {
			actionState = userService.modifyPassword(loginNameValue, sign, userPassword, newPassword, confirmPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			System.out.println("Gson出错："+e);
		}
		return null;

	}
	
	/**
	 * 修改相关信息:http://127.0.0.1:8080/dxxy/appf/appLogin_updateUserHelpInfo.action?
	 * 参数：对象json：jsonUser
	 * 返回值：list（参数对象）
	 * @return
	 */
	public String updateUserHelpInfo(){
		List list = null;
		try {
			Gson gson = new Gson();
			model = gson.fromJson(jsonUser, User.class);
			
			list = userService.updateUserInfo(model); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(list);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 上传头像：http://127.0.0.1:8080/dxxy/appf/appLogin_updatePortrait.action?
	 * 参数：1.User对象：jsonUser
	 * 		2.文件：upload
	 * 返回：user对象：json
	 * @return
	 */
	public String updatePortrait(){
		User thisUser = new User();
		try {
			//jsonOvestep = new String(getRequest().getParameter("jsonOvestep").getBytes("ISO-8859-1"),"UTF-8"); 
			Gson gson = new Gson();
			model = gson.fromJson(jsonUser, User.class);
			
			String saveUrl = uploadPortrait(model);//文件上传
			
			model.setPortrait(saveUrl);
			
			userService.update(model);
			
			//查询项目，替换头像
			XmInfo findXmInfo = new XmInfo();
			findXmInfo.setUserId(model.getId()+"");
			List<XmInfo> listXmInfo = xmInfoService.findByExample(findXmInfo);
			
			for (int i = 0; i < listXmInfo.size(); i++) {
				findXmInfo = listXmInfo.get(i);
				findXmInfo.setPortrait(saveUrl);
				xmInfoService.update(findXmInfo);
			}
			
			//查询征地，替换头像
			XmLandproj findXmLandproj = new XmLandproj();
			findXmLandproj.setUserId(model.getId()+"");
			List<XmLandproj> listXmLandproj = xmLandprojService.findByExample(findXmLandproj);
			
			for (int i = 0; i < listXmLandproj.size(); i++) {
				findXmLandproj = listXmLandproj.get(i);
				findXmLandproj.setPortrait(saveUrl);
				xmLandprojService.update(findXmLandproj);
			}
			
			thisUser = userService.findById(model.getId());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(thisUser);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 上传头像：
	 * @return
	 * @author liqing
	 * @date 2016.03.15
	 * @throws Exception 
	 */
	public String uploadPortrait(User model) throws Exception{
		try {
			
			DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String dateStr = sdf.format(new Date());
			
			int pos = getUploadFileName().lastIndexOf( "." );   
			
			String saveUrl="/attached/user/portrait/"+model.getId()+"_"+dateStr+getUploadFileName().substring(pos);
		
			if(null!=getUpload()){
			
				byte[] bytes = ImageUtils.resize(ImageIO.read(getUpload()), 400, 1f, false);
				
				FileOutputStream fos = new FileOutputStream(getSavePath()+"\\"+model.getId()+"_"+dateStr+getUploadFileName().substring(pos));
				
				// 将字节数组bytes中的数据，写入文件输出流fos中
				fos.write(bytes);
				fos.flush();
			}
			
			return saveUrl;
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String saveLog() throws Exception {
		try {
			
			applog.setUserId(logUserId);
			applog.setUserName(logUserName);
			applog.setLog(logText);
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			applog.setLogTime(Timestamp.valueOf(dateStr));
			
			applogService.save(applog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(applog);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String findAppLog() {
		try {
			listApplog = applogService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	
	public String appLogInfo() {
		try {
			if(ApplogId != null && !"".equals(ApplogId)){
				applog = applogService.findById(Integer.parseInt(ApplogId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resUri;
	}
	/****************参数的getter和setter方法****************/
	//接受struts.xml文件配置的方法
	public void setSavePath(String value) {
		this.savePath = value;
	}

	//返回上传文件保存位置
	public String getSavePath() throws Exception{
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}
	
	public String getTitle() {
		return (this.title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUpload() {
		return (this.upload);
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return (this.uploadContentType);
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return (this.uploadFileName);
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	
	
	public String getLoginNameValue() {
		return loginNameValue;
	}
	public void setLoginNameValue(String loginNameValue) {
		this.loginNameValue = loginNameValue;
	}
	public String getMobileVerCode() {
		return mobileVerCode;
	}
	public void setMobileVerCode(String mobileVerCode) {
		this.mobileVerCode = mobileVerCode;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public int getLoginType() {
		return loginType;
	}
	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}
	public int getActionState() {
		return actionState;
	}
	public void setActionState(int actionState) {
		this.actionState = actionState;
	}
	public String getVerCodeForPhone() {
		return verCodeForPhone;
	}
	public void setVerCodeForPhone(String verCodeForPhone) {
		this.verCodeForPhone = verCodeForPhone;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getFeedbackContent() {
		return feedbackContent;
	}
	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}
	public String getFeedbackContact() {
		return feedbackContact;
	}
	public void setFeedbackContact(String feedbackContact) {
		this.feedbackContact = feedbackContact;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getLogUserId() {
		return logUserId;
	}
	public void setLogUserId(String logUserId) {
		this.logUserId = logUserId;
	}
	public String getLogUserName() {
		return logUserName;
	}
	public void setLogUserName(String logUserName) {
		this.logUserName = logUserName;
	}
	public String getLogText() {
		return logText;
	}
	public void setLogText(String logText) {
		this.logText = logText;
	}
	public Applog getApplog() {
		return applog;
	}
	public void setApplog(Applog applog) {
		this.applog = applog;
	}
	public List<Applog> getListApplog() {
		return listApplog;
	}
	public void setListApplog(List<Applog> listApplog) {
		this.listApplog = listApplog;
	}
	public String getApplogId() {
		return ApplogId;
	}
	public void setApplogId(String applogId) {
		ApplogId = applogId;
	}
	public String getJsonUser() {
		return jsonUser;
	}
	public void setJsonUser(String jsonUser) {
		this.jsonUser = jsonUser;
	}
	public List<User> getListUser() {
		return listUser;
	}
	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}
	
}
