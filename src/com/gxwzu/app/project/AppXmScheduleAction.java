package com.gxwzu.app.project;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.ServletContextAware;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.model.XmSchedule;
import com.gxwzu.business.model.XmSchedulepic;
import com.gxwzu.business.service.project.XmInfoService;
import com.gxwzu.business.service.project.XmReplyService;
import com.gxwzu.business.service.project.XmScheduleService;
import com.gxwzu.business.service.project.XmSchedulepicService;
import com.gxwzu.core.pagination.Result;
import com.gxwzu.core.util.ImageUtils;
import com.gxwzu.core.util.PageUtil;
import com.gxwzu.core.web.action.BaseAction;
import com.gxwzu.sysVO.ViewPoorXmSchedule;
import com.gxwzu.sysVO.ViewPsInfoModel;
import com.gxwzu.sysVO.ViewXmSchedule;
import com.gxwzu.sysVO.ViewXmScheduleInInfo;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class AppXmScheduleAction extends BaseAction implements Preparable,ModelDriven<XmSchedule>,ServletContextAware{

	private static final long serialVersionUID = 8733953490279794383L;

	protected final Log logger = LogFactory.getLog(getClass());
	
	/***********************实例化ModelDriven******************************/
	private XmSchedule model=new XmSchedule();
	@Override
	public XmSchedule getModel() {
		return model;
	}
	public void setModel(XmSchedule model) {
		this.model = model;
	}
	@Override
	public void prepare() throws Exception {
		if(null == model){
			model = new XmSchedule();
		}
	}
	@Override
	public void setServletContext(ServletContext arg0) {
		
	}
	/***********************注入Service******************************/
	private XmInfoService xmInfoService;
	private XmScheduleService xmScheduleService;
	private XmSchedulepicService xmSchedulepicService;
	private XmReplyService xmReplyService;
	
	public void setXmReplyService(XmReplyService xmReplyService) {
		this.xmReplyService = xmReplyService;
	}
	public void setXmSchedulepicService(XmSchedulepicService xmSchedulepicService) {
		this.xmSchedulepicService = xmSchedulepicService;
	}
	public void setXmScheduleService(XmScheduleService xmScheduleService) {
		this.xmScheduleService = xmScheduleService;
	}
	/***********************声明参数******************************/
	private String actionState;
	private String jsonXmSchedule;
	private String jsonXmReply;
	
	private List<XmSchedule> listXmSchedule;
	private List<ViewXmScheduleInInfo> listViewXmScheduleInInfo;
	private Result<ViewXmSchedule> ResultViewXmSchedule;
	private Result<ViewPsInfoModel> ResultViewPoorXmSchedule;
	
	private String thisXmNumber;
	private String thisXmScheduleId;
	private String thisType;
	private String thisLoginUserId;
	private String thisReplyId;
	private String isReply; //是否有权限批示  00 否  03是
	
	private String title;//封装文件标题请求参数的属性
	private File upload;//封装上传文件域的属性
	private String uploadContentType;//封装上传文件类型属性
	private String uploadFileName;//封装上传文件名属性
	private String savePath;//直接在struts.xml文件中配置的属性
	
	private String jsonUrlId;//上传图片地址的ID号，List
	
	private String thisPicId;
	/**************************方法类**************************/
	/**
	 * 工作进度/问题列表（前十条），带项目基本信息（首页）：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_viewXmScheduleInInfo.action?
	 * 返回值：json（com.gxwzu.sysVO.ViewXmScheduleInInfo对象的List）
	 * 			budgetType判断是否为07（领导交办）
	 */
	public String viewXmScheduleInInfo() {
		try {
			ResultViewXmSchedule = xmScheduleService.viewXmScheduleInInfo(getPage(), 10);
			footer=PageUtil.pageFooter(ResultViewXmSchedule, getRequest());	
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(ResultViewXmSchedule);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String viewPoorXmScheduleInInfo() {
		
		try {
//			if("03".equals(isReply)){
//				System.out.println("aaaaaaaaaa");
//			    ResultViewXmSchedule = xmScheduleService.viewXmScheduleInInfo(getPage(), 10);
//			    footer=PageUtil.pageFooter(ResultViewXmSchedule, getRequest());	
//			}else if("00".equals(isReply)){
//				ResultViewPoorXmSchedule = xmScheduleService.viewPoorXmScheduleInInfo(getPage(), 10, Integer.parseInt(thisLoginUserId));
//				footer=PageUtil.pageFooter(ResultViewPoorXmSchedule, getRequest());
//			}
			ResultViewPoorXmSchedule = xmScheduleService.viewPoorXmScheduleInInfo(getPage(), 10);
			footer=PageUtil.pageFooter(ResultViewPoorXmSchedule, getRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
			String json = null;
			if(ResultViewXmSchedule != null){
	            json = g.toJson(ResultViewXmSchedule);
			}else if(ResultViewPoorXmSchedule != null){
			 	json = g.toJson(ResultViewPoorXmSchedule);
			}
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询工作进度/存在问题，通过项目编号、类型：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_findXmScheduleByNum.action?
	 * 参数：thisXmNumber（项目编号）
	 * 		thisLoginUserId（当前登录用户ID）
	 *       thisType（类型：01工作进度，02存在问题，03下一步工作计划，04督查情况）
	 * 返回值：json（com.gxwzu.sysVO.ViewXmSchedule对象的Result）
	 */
	public String findXmScheduleByNum() {
		try {
				
			ResultViewXmSchedule = xmScheduleService.findXmScheduleByNum(thisXmNumber, thisType, thisLoginUserId, getPage(), 10);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g = new Gson();
	        String json = g.toJson(ResultViewXmSchedule);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除工作进度/存在问题：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_del.action?
	 * 参数： thisXmScheduleId（工作进度/存在问题ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String  del() {
		try {
			if(thisXmScheduleId != null && !"".equals(thisXmScheduleId)){
				xmScheduleService.del(thisXmScheduleId);
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 添加工作进度/存在问题：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_add.action?
	 * 关键数据：1.jsonXmSchedule：（com.gxwzu.business.model.XmSchedule，封装json对象：）
	 * 				xmInfoName(项目名称)
	 *        		xmNumber（项目编号）
	 *          	content（工作进度）
	 *          	type（类型：01工作进度，02存在问题）
	 *          	userId（上传人ID）
	 *          	userName（上传人姓名）
	 *          2.jsonUrlId（上传图片的ID号）
	 * 返回：actionState：1成功；0失败
	 */
	public String add() {
		try {
			Gson gson = new Gson();
			model = gson.fromJson(jsonXmSchedule, XmSchedule.class);
			
			if(model.getXmNumber() != null && !"".equals(model.getXmNumber())){
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = sdf.format(new Date());
				model.setCreatTime(Timestamp.valueOf(dateStr));
				
				//1.保存工作进度/存在问题数据
				XmSchedule newXmSchedule= xmScheduleService.save(model);
				
				//2.保存图片信息：找出对应的图片上传记录，写入工作进度/存在问题的关联ID号，修改数据
				List<String> listUrl = gson.fromJson(jsonUrlId, new TypeToken<List<String>>(){}.getType());
				for (int i = 0; i < listUrl.size(); i++) {
					String saveUrlId = listUrl.get(i);
					
					if(saveUrlId != null && !"".equals(saveUrlId)){
						XmSchedulepic thisXmSchedulepic = xmSchedulepicService.findById(Integer.parseInt(saveUrlId));
						
						thisXmSchedulepic.setScheduleId(newXmSchedule.getId());
						
						xmSchedulepicService.update(thisXmSchedulepic);
					}
				}
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 上传工作进度/存在问题图片：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_uploadPIC.action?
	 * 返回：json（picId：上传图片的ID）
	 * @throws Exception
	 */
	public String uploadPIC() throws Exception{
		Integer picId = -1;
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(new Date());
			
			if(null!=getUpload()){
				
				DateFormat sdfUrl = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String dateStrUrl = sdfUrl.format(new Date());
				
				int randomNumber = (int) Math.round(Math.random()*(99-1)+1); 
			
				int pos = getUploadFileName().lastIndexOf( "." );   
					
				String saveUrl="/attached/project/schedule/"+dateStrUrl+"-"+randomNumber+getUploadFileName().substring(pos);
					
				byte[] bytes = ImageUtils.resize(ImageIO.read(getUpload()), 400, 1f, false);
					
				FileOutputStream fos = new FileOutputStream(getSavePath()+"\\"+dateStrUrl+"-"+randomNumber+getUploadFileName().substring(pos));
					
				// 将字节数组bytes中的数据，写入文件输出流fos中
				fos.write(bytes);
				fos.flush();
				
				//保存图片信息
				XmSchedulepic xmSchedulepic = new XmSchedulepic();
				xmSchedulepic.setPicLink(saveUrl);
				xmSchedulepic.setCreateTime(Timestamp.valueOf(dateStr));
				
				XmSchedulepic newXmSchedulepic = xmSchedulepicService.save(xmSchedulepic);
				
				picId = newXmSchedulepic.getId();
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(picId);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除图片：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_delPic.action?
	 * 参数： thisPicId（图片ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String delPic() {
		try {
			if(thisPicId != null && !"".equals(thisPicId)){
				xmSchedulepicService.delPic(thisPicId);
				actionState="1";
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 添加回复：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_addReply.action?
	 * 关键数据：1.jsonXmReply：（com.gxwzu.business.model.XmReply，封装json对象：）
	 *        		xmNumber（项目编号）
	 *        		scheduleId（工作进度/存在问题ID）
	 *          	content（回复内容）
	 *          	reUserId（回复人ID）
	 *          	reUserName（回复人姓名）
	 * 返回：actionState：0失败；批示成功,返回添加成功的对象Id
	 */
	public String addReply() {
		try {
			Gson gson = new Gson();
			XmReply xmReply = gson.fromJson(jsonXmReply, XmReply.class);
			
			if(xmReply.getScheduleId() != null && !"".equals(xmReply.getScheduleId())){
				
				if(xmReply.getXmNumber() != null && !"".equals(xmReply.getXmNumber())){
					actionState = xmScheduleService.addReply(xmReply);
				}else{
					actionState="0";
				}
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除评论：http://127.0.0.1:8080/dxxy/appf/appXmSchedule_delReply.action?
	 * 参数： thisReplyId（评论ID）
	 * 返回：actionState：1成功；0失败
	 * @return
	 */
	public String delReply() {
		try {
			if(thisReplyId != null && !"".equals(thisReplyId)){
				XmReply xmReply = xmReplyService.findById(Integer.parseInt(thisReplyId));
				if(xmReply != null){
					xmReplyService.remove(xmReply);
					actionState="1";
				}else{
					actionState="0";
				}
			}else{
				actionState="0";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			Gson g  = new Gson();
	        String json = g.toJson(actionState);
	        PrintWriter pw = getPrintWriter();
	        pw.print(json);
	        pw.flush();
	        pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/****************参数的getter和setter方法****************/
	public String getThisXmNumber() {
		return thisXmNumber;
	}
	public void setThisXmNumber(String thisXmNumber) {
		this.thisXmNumber = thisXmNumber;
	}
	public String getActionState() {
		return actionState;
	}
	public void setActionState(String actionState) {
		this.actionState = actionState;
	}
	public String getJsonXmSchedule() {
		return jsonXmSchedule;
	}
	public void setJsonXmSchedule(String jsonXmSchedule) {
		this.jsonXmSchedule = jsonXmSchedule;
	}
	public List<XmSchedule> getListXmSchedule() {
		return listXmSchedule;
	}
	public void setListXmSchedule(List<XmSchedule> listXmSchedule) {
		this.listXmSchedule = listXmSchedule;
	}
	public String getThisXmScheduleId() {
		return thisXmScheduleId;
	}
	public void setThisXmScheduleId(String thisXmScheduleId) {
		this.thisXmScheduleId = thisXmScheduleId;
	}
	public String getThisType() {
		return thisType;
	}
	public void setThisType(String thisType) {
		this.thisType = thisType;
	}
	public List<ViewXmScheduleInInfo> getListViewXmScheduleInInfo() {
		return listViewXmScheduleInInfo;
	}
	public void setListViewXmScheduleInInfo(
			List<ViewXmScheduleInInfo> listViewXmScheduleInInfo) {
		this.listViewXmScheduleInInfo = listViewXmScheduleInInfo;
	}
	
	public String getJsonUrlId() {
		return jsonUrlId;
	}
	public void setJsonUrlId(String jsonUrlId) {
		this.jsonUrlId = jsonUrlId;
	}
	public String getThisPicId() {
		return thisPicId;
	}
	public void setThisPicId(String thisPicId) {
		this.thisPicId = thisPicId;
	}
	//接受struts.xml文件配置的方法
	public void setSavePath(String value) {
		this.savePath = value;
	}

	//返回上传文件保存位置
	public String getSavePath() throws Exception{
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}
	
	public String getTitle() {
		return (this.title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUpload() {
		return (this.upload);
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return (this.uploadContentType);
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return (this.uploadFileName);
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	public Result<ViewXmSchedule> getResultViewXmSchedule() {
		return ResultViewXmSchedule;
	}
	public void setResultViewXmSchedule(Result<ViewXmSchedule> resultViewXmSchedule) {
		ResultViewXmSchedule = resultViewXmSchedule;
	}
	public String getJsonXmReply() {
		return jsonXmReply;
	}
	public void setJsonXmReply(String jsonXmReply) {
		this.jsonXmReply = jsonXmReply;
	}
	public String getThisLoginUserId() {
		return thisLoginUserId;
	}
	public void setThisLoginUserId(String thisLoginUserId) {
		this.thisLoginUserId = thisLoginUserId;
	}
	public String getThisReplyId() {
		return thisReplyId;
	}
	public void setThisReplyId(String thisReplyId) {
		this.thisReplyId = thisReplyId;
	}
	public String getIsReply() {
		return isReply;
	}
	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}
	public XmInfoService getXmInfoService() {
		return xmInfoService;
	}
	public void setXmInfoService(XmInfoService xmInfoService) {
		this.xmInfoService = xmInfoService;
	}
	
	
}
