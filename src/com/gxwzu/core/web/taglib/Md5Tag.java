package com.gxwzu.core.web.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * 部分字符加密标签
 * @author <a href=mailto:amu_1115@126.com> amu_1115 </a>
 * @version $ Md5Tag.java 2015-7-7 上午08:40:44
 */
public class Md5Tag extends SimpleTagSupport {

    /**
     * 字符串
     */
	private String  value;
	/**
	 * 加密起始小标,默认从0开始
	 */
	private int start;
	/**
	 * 加密最后下标，默认字符串长度
	 */
	private int size;

	public void doTag() throws JspException, IOException {
	
		try{String outValue="";
		
        if(value!=null&&value.length()>(start+size)){
        
        	String startStr=value.substring(0, start);
        	String endStr=value.substring(value.length()-size);
        	StringBuffer strBuffer=new StringBuffer(startStr);

        	for(int i=start;i<value.length()-size;i++){
        		strBuffer.append("*");
        		
        	}
        	strBuffer.append(endStr);
        	outValue=strBuffer.toString();
        }
    
		this.getJspContext().getOut().write(outValue+"");
		}catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}




}
