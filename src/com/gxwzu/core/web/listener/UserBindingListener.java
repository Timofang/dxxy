package com.gxwzu.core.web.listener;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import com.gxwzu.core.util.AppConextUtil;
import com.gxwzu.system.model.log.Loginlog;
import com.gxwzu.system.service.log.LoginlogService;

/**
 * 绑定用户session
 * 
 * @author amu_1115
 *
 */
public class UserBindingListener implements HttpSessionBindingListener {

	private String jsessionid;
	private String userid;
	private String username;
	private String ipAdress;
	
	private static UserBindingListener userBindingListener;
	
	

	public static void setUserBindingListener(UserBindingListener userBindingListener) {
		UserBindingListener.userBindingListener = userBindingListener;
	}

	public static UserBindingListener getUserBindingListener(){
		return userBindingListener;
	}
	
	public UserBindingListener(String jsessionid, String userid, String username,
			String ipAdress) {
		this.jsessionid = jsessionid;
		this.userid = userid;
		this.username = username;
		this.ipAdress = ipAdress;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		try {
			LoginlogService logService = (LoginlogService) AppConextUtil
					.getBean("loginlogService");
			Loginlog log = logService.findByJsessionid(jsessionid);
			if (log == null) {
				log = new Loginlog();
				log.setLoginSid(jsessionid);
				log.setLoginIp(ipAdress);
				log.setLoginIn(new java.util.Date());
				log.setLoginUser(userid);
			}
			logService.save(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {

		try {
			LoginlogService logService = (LoginlogService) AppConextUtil
					.getBean("loginlogService");
			Loginlog log = logService.findByJsessionid(jsessionid);
			if (log != null) {
				log.setLoginOut(new java.util.Date());
				logService.update(log);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	public String getJsessionid() {
		return jsessionid;
	}

	public void setJsessionid(String jsessionid) {
		this.jsessionid = jsessionid;
	}

}
