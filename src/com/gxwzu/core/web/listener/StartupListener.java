package com.gxwzu.core.web.listener;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ServletContextAware;

import com.gxwzu.core.context.ConfigInfo;
import com.gxwzu.core.context.SystemContext;


/**
 * 启动监听器类主要负责加载系统初始化数据
 * @author amu_1115
 *
 */
public class StartupListener implements InitializingBean, ServletContextAware{

	private static final Log log = LogFactory.getLog(StartupListener.class);
	
	private ConfigInfo configInfo;
	@Override
	public void setServletContext(ServletContext servletContext) {
		//读取软件版本配置信息
		log.info("加载软件版本配置信息...");
		SystemContext.AREACODE=configInfo.getAreacode();
		SystemContext.PROJECTNAME=configInfo.getProjectname();
		SystemContext.SYSNAME=configInfo.getSysname();
		SystemContext.PROJECT_VERSION=configInfo.getVersion();
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
		
	}
	@Autowired
	public void setConfigInfo(ConfigInfo configInfo) {
		this.configInfo = configInfo;
	}



}
