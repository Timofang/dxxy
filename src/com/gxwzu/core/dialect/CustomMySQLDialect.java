package com.gxwzu.core.dialect;

import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.dialect.MySQL5Dialect;
/**
 * 自定义针对MySQL的方言支持
 * @author amu_1115
 * @since 2017-
 */
public class CustomMySQLDialect extends MySQL5Dialect {
  
   public CustomMySQLDialect(){
	   super();
	   //注册长字符类型
	  this.registerHibernateType(Types.LONGVARCHAR, Hibernate.STRING.getName());
   }
	
}
