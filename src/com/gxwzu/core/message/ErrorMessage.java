package com.gxwzu.core.message;
/**
 * 系统异常提示类
 * @author author
 * @version 1.0
 */
public final class ErrorMessage {
	
	public static final  String WEB_SUC_000="处理成功";
	/*WEB层出错提示*/
	public static final  String WEB_ERR_TIMEOUT="访问超时";
	public static final  String WEB_ERR_001="用户名或者密码不对";
	public static final  String WEB_ERR_014="贫困户管理出错!";
	public static final  String WEB_ERR_023="删除基础数据出错!";
	public static final  String WEB_ERR_024="无法连接服务器!";
	public static final  String WEB_ERR_026="添加每月项目进度出错!";
	public static final  String WEB_ERR_027="删除每月项目进度出错!";
	public static final  String WEB_ERR_028="项目资金已分配，不能再回收!";
	
	/*业务层出错提示*/
	public static final  String BIZ_ERR_001="业务流程:添加每月项目进度出错";
	

}
