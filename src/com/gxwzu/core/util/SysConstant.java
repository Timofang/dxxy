package com.gxwzu.core.util;
/**
 *系统配置常量
 * @author MR.AMU
 * @version 1.0
 * @since 2014.04.22
 */
public class SysConstant {
	/**
	 * default page size 12
	 */
	public final static int DEFAULT_PAGESIZE=18;
	/**
	 * grant flag if flag is null ,logout
	 */
	public final static String GRANT="grant";
	/**
	 * 
	 */
	public final static String LOGOUT="loginPage";
}
