package com.gxwzu.core.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

/**
 * Date Utility Class
 * @version $Revision: 1.7 $ $Date: 2005/05/04 04:57:41 $
 */
public class DateUtils {

	protected final Log log =LogFactory.getLog(getClass()); 
	
    public final static String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    
    public final static String DEFAULT_TIMES_FORMAT = "yyyyMMddHHmmss";

    public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public final static DateFormat DEFAULT_TIME_FORMATER = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
    public final static DateFormat DEFAULT_TIMES_FORMATER = new SimpleDateFormat(DEFAULT_TIMES_FORMAT);

    public final static DateFormat DEFAULT_DATE_FORMATER = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

    public final static String FORMAT_YYYY = "yyyy";

    public final static String FORMAT_YYYYMM = "yyyyMM";

    public final static String FORMAT_YYYYMMDD = "yyyyMMdd";

    public final static DateFormat FORMAT_YYYY_FORMATER = new SimpleDateFormat(FORMAT_YYYY);

    public final static DateFormat FORMAT_YYYYMM_FORMATER = new SimpleDateFormat(FORMAT_YYYYMM);

    public final static DateFormat FORMAT_YYYYMMDD_FORMATER = new SimpleDateFormat(FORMAT_YYYYMMDD);
    
    public static String formatYear(Date date) {
        if (date == null) {
            return null;
        }
        return FORMAT_YYYY_FORMATER.format(date);
    }
    public static int getYear(){
    	Calendar calendar=Calendar.getInstance();
    	return calendar.get(Calendar.YEAR);
    }
    public static int getMonth(){
    	Calendar calendar=Calendar.getInstance();
    	return calendar.get(Calendar.MONTH)+1;
    }
    public static String getMonthToStr(){
    	Calendar calendar=Calendar.getInstance();
    	int m= calendar.get(Calendar.MONTH)+1;
    	if(m<10){
    		return "0"+m;
    	}else{
    		return ""+m;
    	}
    }
    public static String formatDate(Date date) {
        if (date == null) {
            return null;
        }
        return DEFAULT_DATE_FORMATER.format(date);
    }
    public static String formatDate2str() {
    	Date date=new Date();
        return DEFAULT_TIMES_FORMATER.format(date);
    }

    public static String formatDate(Date date, String format) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(format).format(date);
    }

    public static String formatTime(Date date) {
        if (date == null) {
            return null;
        }
        return DEFAULT_TIME_FORMATER.format(date);
    }

    public static String formatDateNow() {
        return formatDate(new Date());
    }

    public static String formatTimeNow() {
        return formatTime(new Date());
    }

    public static Date parseDate(String date, DateFormat df) {
        if (date == null) {
            return null;
        }
        try {
            return df.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static Date parseTime(String date, DateFormat df) {
        if (date == null) {
            return null;
        }
        try {
            return df.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * 校验日期格式是否正确
     * @param str
     * @return
     */
	public static boolean isValidDate(String str) {
		boolean convertSuccess = true;
		// 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			// 设置lenient为false.
			// 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
			format.setLenient(false);
			format.parse(str);
		} catch (ParseException e) {

			convertSuccess = false;
		}
		return convertSuccess;
	}
    public static Date parseDate(String date) {
        return parseDate(date, DEFAULT_DATE_FORMATER);
    }

    public static Date parseTime(String date) {
        return parseTime(date, DEFAULT_TIME_FORMATER);
    }

    public static String plusOneDay(String date) {
        DateTime dateTime = new DateTime(parseDate(date).getTime());
        return formatDate(dateTime.plusDays(1).toDate());
    }

    public static String plusOneDay(Date date) {
        DateTime dateTime = new DateTime(date.getTime());
        return formatDate(dateTime.plusDays(1).toDate());
    }

    public static String getHumanDisplayForTimediff(Long diffMillis) {
        if (diffMillis == null) {
            return "";
        }
        long day = diffMillis / (24 * 60 * 60 * 1000);
        long hour = (diffMillis / (60 * 60 * 1000) - day * 24);
        long min = ((diffMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long se = (diffMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        StringBuilder sb = new StringBuilder();
        if (day > 0) {
            sb.append(day + "D");
        }
        DecimalFormat df = new DecimalFormat("00");
        sb.append(df.format(hour) + ":");
        sb.append(df.format(min) + ":");
        sb.append(df.format(se));
        return sb.toString();
    }
    /**
	 * 整型月份转换为字符月份
	 * @param number
	 * @return
	 */
	public static String changeIntToString(int number){
		String[]str=new String[]{
				"01","02","03","04","05",
				"06","07","08","09"
		};
		if(number<10){
			return str[number-1];
		}else{
			return String.valueOf(number);
		}
	}
	/**
	 * 获取最近12个月的月份
	 * @return
	 */
    public static String[] getLast12Months(){  
        
        String[] last12Months = new String[12];  
        Calendar cal = Calendar.getInstance();  
        //默认当年11月之前的12个月
        cal.set(Calendar.MONTH, 11); //要先+1,才能把本月的算进去</span>  
        for(int i=0; i<12; i++){  
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); //逐次往前推1个月  
            last12Months[11-i] = cal.get(Calendar.YEAR)+ "-" +(cal.get(Calendar.MONTH)+1);  
        }  
        return last12Months;  
    }  
    public static void main(String[]args){
    	String[] months=DateUtils.getLast12Months();
    	for(String m:months){
    		System.out.println("m:"+m);
    	}
    }
}
