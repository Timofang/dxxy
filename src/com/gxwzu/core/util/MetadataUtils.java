package com.gxwzu.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.jpeg.JpegDirectory;

public class MetadataUtils {
    private File imgFile;
	
    public MetadataUtils(File imgFile){
    	
    	this.imgFile=imgFile;
    }
    /**
     * 根据
     * @param directoryType-[1:图片信息,2:拍摄信息,3:设备信息,4:GPS]
     * @param tagType 1-表示拍摄时间
     * @return
     */
	public  String getMetadata(int directoryType,int tagType){
		
		InputStream is = null;
		Directory dr=null;
		String tagValue="";
		try {
			is = new FileInputStream(imgFile);
			//读取图片元信息
			Metadata metadata = ImageMetadataReader.readMetadata(is);
            switch (directoryType) {
			case 1://图片信息
				dr = metadata.getDirectory(JpegDirectory.class);
				break;
			case 2://拍摄信息
				dr = metadata.getDirectory(ExifSubIFDDirectory.class);
				
				break;
			case 3://设备信息
				dr = metadata.getDirectory(ExifIFD0Directory.class);
				break;
			case 4://GPS
				dr = metadata.getDirectory(GpsDirectory.class);
				break;
				
			default:
				break;
			}
           
            if(tagType==1){ //拍摄时间
            	tagValue= dr.getString(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
            }else if(tagType==2){//获取经纬度
            	tagValue= dr.getString(GpsDirectory.TAG_LONGITUDE);
            }else{
            	tagValue= "-1";
            }
           
		   
		
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			
		}catch (ImageProcessingException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return tagValue;
		
	}
	

}
