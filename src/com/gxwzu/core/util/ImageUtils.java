package com.gxwzu.core.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;

public class ImageUtils {

	public static byte[] resize(BufferedImage srcBufferedImage, int targetWidth)
			throws IOException {
		return resize(srcBufferedImage, targetWidth, 1f, false);
	}

	public static byte[] resize(BufferedImage srcBufferedImage,
			int targetWidth, boolean square) throws IOException {
		return resize(srcBufferedImage, targetWidth, square);
	}

	public static byte[] resize(BufferedImage srcBufferedImage,
			int targetWidth, float quality, boolean square) throws IOException {
		if (quality > 1) {
			throw new IllegalArgumentException(
					"Quality has to be between 0 and 1");
		}
		if (square) {
			// 正方形，需要提前进行裁剪
			int width = srcBufferedImage.getWidth();
			int height = srcBufferedImage.getHeight();
			if (width > height) {
				int x = (width - height) / 2;
				srcBufferedImage = srcBufferedImage.getSubimage(x, 0, height,
						height);
			} else if (width < height) {
				int y = (height - width) / 2;
				srcBufferedImage = srcBufferedImage.getSubimage(0, y, width,
						width);
			}
		}

		Image resizedImage = null;
		int iWidth = srcBufferedImage.getWidth();
		int iHeight = srcBufferedImage.getHeight();

		if (iWidth > iHeight) {
			resizedImage = srcBufferedImage.getScaledInstance(targetWidth,
					(targetWidth * iHeight) / iWidth, Image.SCALE_SMOOTH);
		} else {
			resizedImage = srcBufferedImage.getScaledInstance(
					(targetWidth * iWidth) / iHeight, targetWidth,
					Image.SCALE_SMOOTH);
		}

		// This code ensures that all the pixels in the image are loaded.
		Image temp = new ImageIcon(resizedImage).getImage();

		// Create the buffered image.
		BufferedImage bufferedImage = new BufferedImage(temp.getWidth(null),
				temp.getHeight(null), BufferedImage.TYPE_INT_RGB);

		// Copy image to buffered image.
		Graphics g = bufferedImage.createGraphics();

		// Clear background and paint the image.
		g.setColor(Color.white);
		g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
		g.drawImage(temp, 0, 0, null);
		g.dispose();

		// Soften.
		float softenFactor = 0.05f;
		float[] softenArray = { 0, softenFactor, 0, softenFactor,
				1 - (softenFactor * 4), softenFactor, 0, softenFactor, 0 };
		Kernel kernel = new Kernel(3, 3, softenArray);
		ConvolveOp cOp = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
		bufferedImage = cOp.filter(bufferedImage, null);

		ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
		ImageWriteParam param = writer.getDefaultWriteParam();
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(1.0F); // Highest quality
		// Write the JPEG to our ByteArray stream
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageOutputStream imageOutputStream = ImageIO
				.createImageOutputStream(byteArrayOutputStream);
		writer.setOutput(imageOutputStream);
		writer.write(null, new IIOImage(bufferedImage, null, null), param);
		return byteArrayOutputStream.toByteArray();
	}

	// Example usage
	public static void main(String[] args) throws IOException {

		File originalImage = new File("C:/Users/liqing/Desktop/证书扫描件/李庆-工程硕士录取通知书.jpg");
		byte[] bytes = resize(ImageIO.read(originalImage), 400, 1f, false);
		FileOutputStream out = new FileOutputStream(new File("E:/66.jpg"));
		out.write(bytes);
		out.close();
		System.out.println("Ok");
	}
	
	/*
	 * public static void graphics(String text, String imgurl) { try{ int
	 * imageWidth = 100;// 图片的宽度
	 * 
	 * int imageHeight = 50;// 图片的高度 BufferedImage image; image = new
	 * BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
	 * Graphics graphics = image.getGraphics(); graphics.setColor(Color.WHITE);
	 * graphics.fillRect(0, 0, imageWidth, imageHeight);
	 * graphics.setColor(Color.BLACK); graphics.setFont(new
	 * Font("宋体",Font.BOLD,14)); graphics.drawString(text, 30, 30);
	 * 
	 * // 改成这样: BufferedImage bimg = null; try { bimg =
	 * javax.imageio.ImageIO.read(new java.io.File(imgurl)); } catch (Exception
	 * e) { }
	 * 
	 * if (bimg != null) graphics.drawImage(bimg, 230, 0, null);
	 * graphics.dispose();
	 * 
	 * 
	 * FileOutputStream fos = new FileOutputStream(imgurl); BufferedOutputStream
	 * bos = new BufferedOutputStream(fos); JPEGImageEncoder encoder =
	 * JPEGCodec.createJPEGEncoder(bos); encoder.encode(image); bos.close();
	 * }catch(Exception e){ e.printStackTrace(); }
	 * 
	 * } public static void main(String[]args){ ImageUtils.graphics("许传本",
	 * "d:\\bb.jpg"); }
	 */

}
