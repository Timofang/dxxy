package com.gxwzu.core.util.tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class HistoryUtil {

	
	private static final Log log = LogFactory.getLog(HistoryUtil.class);
	//校验的属性集合
	private Map<String,String> filedMap=new HashMap<String, String>();
	
	/**
	 * 根据属性名获取属性值
	 * */
    private static  Object getFieldValueByName(String fieldName, Object o) {
        try {  
            String firstLetter = fieldName.substring(0, 1).toUpperCase();  
            String getter = "get" + firstLetter + fieldName.substring(1);  
            Method method = o.getClass().getMethod(getter, new Class[] {});  
            Object value = method.invoke(o, new Object[] {});  
            return value;  
        } catch (Exception e) {  
            log.error(e.getMessage(),e);  
            return null;  
        }  
    } 

    
    /**
     * 获取属性名数组
     * */
    public static String[] getFiledName(Object o){
    	Field[] fields=o.getClass().getDeclaredFields();
       	String[] fieldNames=new String[fields.length];
    	for(int i=0;i<fields.length;i++){
    		fieldNames[i]=fields[i].getName();
    	}
    	return fieldNames;
    }
    
    /**
     * 获取属性类型(type)，属性名(name)，属性值(value)的String组成的list
     * */
    private static List<String> getFiledsInfo(Object o){
    	Field[] fields=o.getClass().getDeclaredFields();
       	List<String> list = new ArrayList<String>();
    	for(int i=0;i<fields.length;i++){	
    		String type=fields[i].getType().toString();
    		String name= fields[i].getName();
    		Object value=getFieldValueByName(fields[i].getName(), o);
    		StringBuffer buffer=new StringBuffer();
    		String data=value==null?"0":value+"";
    		int length=data.length();
    		buffer.append("[");
    		buffer.append("type:"+type+",");
    		buffer.append("name:"+name+",");
    		buffer.append("value:"+value+",");
    		buffer.append("length:"+length+"]");
    		list.add(buffer.toString());
    	}
    	return list;
    }
    
    /**
     * 获取对象的所有属性值，返回一个对象数组
     * */
    public static Object[] getFiledValues(Object o){
    	String[] fieldNames=getFiledName(o);
    	Object[] value=new Object[fieldNames.length];
    	for(int i=0;i<fieldNames.length;i++){
    		value[i]=getFieldValueByName(fieldNames[i], o);
    	}
    	return value;
    }
    /**
     * 添加校验字段
     * @param name
     * @param value
     */
    public void addfield(String name,String value){
    	filedMap.put(name, value);
    }
    /**
     * 校验过程
     * @param original
     * @param other
     */
    public void verify(Object original,Object other ){
 
    	Field[] originalfields=original.getClass().getDeclaredFields();
      
    	for(int i=0;i<originalfields.length;i++){	
    
    		String name= originalfields[i].getName();
    		//如果在校验的字段范围内，比较两个属性的值的变化
    		if(filedMap.containsKey(name)){
    			Object value=getFieldValueByName(originalfields[i].getName(),original);
    			
    		}    		
    		
    		
    	}
    
    
    	
    }

}
