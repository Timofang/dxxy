package com.gxwzu.core.context;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;



/**
 * 全局常量配置
 * @author Mr.xucb
 * @version 1.0
 */
public final class SystemContext {
	
	/*配置软件版本信息*/
	
	public static  String AREACODE="450421";//默认行政区域450802港北区|450821（平南）|450804（覃塘）|450423（蒙山）|450421（苍梧），系统自动读取参数覆盖
	public static  String SYSNAME="dxxy";//系统名称
	public static  String PROJECTNAME="项目综合信息管理平台";

	/*public static  String AREACODE="451021";//默认行政区域450802港北区|450881（桂平），系统自动读取参数覆盖
	public static  String SYSNAME="tyfp";//系统名称
	public static  String PROJECTNAME="白色市田阳县扶贫辅助系统";//桂平市扶贫辅助系统|贵港市港北区扶贫辅助系统
*/	
	/*public static  String AREACODE="450802";//默认行政区域450802港北区|450881（桂平），系统自动读取参数覆盖
	public static  String SYSNAME="gpfp";//系统名称
	public static  String PROJECTNAME="贵港市港北区扶贫辅助系统";//桂平市扶贫辅助系统|贵港市港北区扶贫辅助系统
*/
	/*public static  String AREACODE="450881";//默认行政区域450802港北区|450881（桂平），系统自动读取参数覆盖
	public static  String SYSNAME="gpfp";//系统名称
	public static  String PROJECTNAME="桂平市脱贫攻坚辅助数据平台";//桂平市扶贫辅助系统|贵港市港北区扶贫辅助系统
*/
	public static  String PROJECT_VERSION="V1.0";
	
	/*public static  String AREACODE="450804";//默认行政区域450802港北区|450881（桂平），系统自动读取参数覆盖
	public static  String SYSNAME="qtfp";//系统名称
	public static  String PROJECTNAME="贵港市覃塘区扶贫辅助系统";//桂平市扶贫辅助系统|贵港市港北区扶贫辅助系统
	public static  String PROJECT_VERSION="V1.0";*/
	
	public static final Integer DEFUALT_PAGE_SIZE=12;//默认分页数,Result有一个BUG，
	public static final Integer DEFUALT_PAGE_NUM=1;//默认分页码
	public static final String ACTION_CONTENT_TYPE="application/json;charset=utf-8";//默认传输编码，注意utf-8
	
	public static final int TIME_INTERVAL_MSG = 900;//单位，秒。2次发送短信的时间间隔不能少于900秒
	


	/**
	 * 获取用户头像的路径
	 * @return 用户头像上传图片的路径
	 */
	public static String getUserImgURL(){
		String root_url = "/attached/persinfo";
		return root_url;
	}
	private static final String SITE_WIDE_SECRET = "GXUWZ_0774_KEY";  
	private static final PasswordEncoder encoder = new StandardPasswordEncoder(SITE_WIDE_SECRET); 
	
	/**
	 * 密码加密
	 * @param rawPasswd 需要加密的密码
	 * @return
	 */
	public static String passwdEncryption(String rawPasswd){
		
		String str = encoder.encode(rawPasswd);
		return str;
	}

	/**
	 * 密码校验
	 * @param rawPassword 明文密码（需要匹配的用户输入的那份）
	 * @param password 数据库保存的加密的密码
	 * @return true = 通过，fasle=失败
	 */
	public static boolean passwdDecryption(String rawPasswd, String password){

		boolean isT = false;
		try {
			isT = encoder.matches(rawPasswd, password);
		} catch (Exception e) {
			
			System.out.println("密码匹配："+e);
		}
		return isT;
	}
	
	

}
