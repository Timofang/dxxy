package com.gxwzu.core.context;
/**
 * 配置信息
 * @author amu_1115
 *
 */
public class ConfigInfo {
	
	public static String DEFAULT_AREACODE;
	public static String DEFAULT_VERSION;
	public static String DEFAULT_SYSNAME;
	public static String DEFAULT_PROJECTNAME;
	
	
	private String areacode;

	private String version;
	private String sysname;
	private String projectname;

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSysname() {
		return sysname;
	}

	public void setSysname(String sysname) {
		this.sysname = sysname;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

}
