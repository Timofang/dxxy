package com.gxwzu.core.model;

import java.util.ArrayList;
import java.util.List;

public abstract class ViewbaseModel {

	private List data;
	private List sortdata=new ArrayList<Object>();
	private List convertdata=new ArrayList<Object>();
    private String tplid;//导出模板编号
    private String tplName;//导出模板名称
    
    public abstract void total();
    //重新排序
    public abstract void sort();
    //转换为输出模型
    public abstract void convert();
    
	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public String getTplid() {
		return tplid;
	}

	public void setTplid(String tplid) {
		this.tplid = tplid;
	}

	public String getTplName() {
		return tplName;
	}

	public void setTplName(String tplName) {
		this.tplName = tplName;
	}
	public List getSortdata() {
		return sortdata;
	}
	public void setSortdata(List sortdata) {
		this.sortdata = sortdata;
	}
	public List getConvertdata() {
		return convertdata;
	}
	public void setConvertdata(List convertdata) {
		this.convertdata = convertdata;
	}
	

	
}
