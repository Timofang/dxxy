package com.gxwzu.sysVO;

import java.math.BigInteger;
import java.util.List;

import com.gxwzu.core.model.ViewbaseModel;

public class downViewXmInfo extends ViewbaseModel{

	@Override
	public void total() {
		
	}

	@Override
	public void sort() {
		
		List list=super.getData();
		
		for(int i=0;list!=null&&i<list.size();i++){
			Object[]oc=(Object[])list.get(i);
			
			String budgetType = (String)oc[11];
			String strBudgetType = "";
			if(budgetType != null && !"".equals(budgetType)){
				if(budgetType.contains("01")){
					strBudgetType += "中央预算；";
				}
				if(budgetType.contains("02")){
					strBudgetType += "自治区层面；";
				}
				if(budgetType.contains("03")){
					strBudgetType += "市级层面；";
				}
				if(budgetType.contains("04")){
					strBudgetType += "项目建设丰收年；";
				}
				if(budgetType.contains("05")){
					strBudgetType += "现代服务业重点项目；";
				}
			}
			
			String strProjectType = "";
			if("01".equals((String)oc[12])){
				strProjectType = "新开工项目";
			}
			if("02".equals((String)oc[12])){
				strProjectType = "续建项目";
			}
			if("03".equals((String)oc[12])){
				strProjectType = "重大前期工作";
			}
			if("04".equals((String)oc[12])){
				strProjectType = "竣工或部分竣工";
			}
			
			String strIsState = "";
			if("00".equals((String)oc[9])){
				strIsState = "未开工";
			}
			if("01".equals((String)oc[9])){
				strIsState = "已开工";
			}
			
			String strIsCompleted = "";
			if("00".equals((String)oc[10])){
				strIsCompleted = "未竣工";
			}
			if("01".equals((String)oc[10])){
				strIsCompleted = "已竣工";
			}
			
			String strIsOverdue = "";
			if("0".equals((BigInteger)oc[30]+"")){
				strIsOverdue = "未超期";
			}
			if("1".equals((BigInteger)oc[30]+"")){
				strIsOverdue = "超期";
			}
			
			Object[] sortOc = { oc[26], oc[4], oc[0], oc[8], strBudgetType, strProjectType,
					oc[1], oc[2], oc[3], oc[7], oc[19], oc[20], oc[21],
					oc[22], oc[23], oc[24], oc[5], oc[6], strIsState, strIsCompleted,
					oc[27], strIsOverdue };

			super.getSortdata().add(sortOc);
			
		}
	}

	@Override
	public void convert() {
		
	}
}
