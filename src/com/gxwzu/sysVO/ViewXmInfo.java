package com.gxwzu.sysVO;

import java.sql.Timestamp;

public class ViewXmInfo {

	private Integer id;
	private String xmNumber;
	private String xmYear;
	private String name;
	private String scale;
	private String period;
	private String cycle;
	private String owner;
	private String county;
	private String startTime;
	private String completionTime;
	private String address;
	private String industry;
	private String isState;
	private String isCompleted;
	private String budgetType;
	private String projectType;
	private String allPay;
	private String userId;
	private String userName;
	private String portrait;
	private String createTime;
	
	private String coordinate;
	private String coordinatePly;
	
	private String investTotal;
	private String investSource;
	private String investPlan;
	private String investComplete;
	
	private String sInvestment;
	private String cRate;
	
	private String maxScheduleTime;//最新工作进度（时间）
	private String nowCycle;//当前时间-最新工作进度
	private String isOverdue;//是否超期：0不超期，1超期
	
	public ViewXmInfo() {
		super();
	}

	public ViewXmInfo(Integer id, String xmNumber, String xmYear, String name,
			String scale, String period, String cycle, String owner,
			String county, String startTime, String completionTime,
			String address, String industry, String isState, String allPay,
			String isCompleted, String budgetType, String projectType,
			String userId, String userName, String portrait, String createTime,
			String coordinate, String coordinatePly, String investTotal,
			String investSource, String investPlan, String investComplete,
			String sInvestment, String cRate, String maxScheduleTime,
			String nowCycle, String isOverdue) {
		super();
		this.id = id;
		this.xmNumber = xmNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.scale = scale;
		this.period = period;
		this.cycle = cycle;
		this.owner = owner;
		this.county = county;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.address = address;
		this.industry = industry;
		this.isState = isState;
		this.isCompleted = isCompleted;
		this.budgetType = budgetType;
		this.projectType = projectType;
		this.allPay = allPay;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
		this.investTotal = investTotal;
		this.investSource = investSource;
		this.investPlan = investPlan;
		this.investComplete = investComplete;
		this.sInvestment = sInvestment;
		this.cRate = cRate;
		this.maxScheduleTime = maxScheduleTime;
		this.nowCycle = nowCycle;
		this.isOverdue = isOverdue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(String completionTime) {
		this.completionTime = completionTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}

	public String getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	public String getInvestTotal() {
		return investTotal;
	}

	public void setInvestTotal(String investTotal) {
		this.investTotal = investTotal;
	}

	public String getInvestSource() {
		return investSource;
	}

	public void setInvestSource(String investSource) {
		this.investSource = investSource;
	}

	public String getInvestPlan() {
		return investPlan;
	}

	public void setInvestPlan(String investPlan) {
		this.investPlan = investPlan;
	}

	public String getInvestComplete() {
		return investComplete;
	}

	public void setInvestComplete(String investComplete) {
		this.investComplete = investComplete;
	}

	public String getSInvestment() {
		return sInvestment;
	}

	public void setSInvestment(String sInvestment) {
		this.sInvestment = sInvestment;
	}

	public String getCRate() {
		return cRate;
	}

	public void setCRate(String cRate) {
		this.cRate = cRate;
	}

	public String getMaxScheduleTime() {
		return maxScheduleTime;
	}

	public void setMaxScheduleTime(String maxScheduleTime) {
		this.maxScheduleTime = maxScheduleTime;
	}

	public String getNowCycle() {
		return nowCycle;
	}

	public void setNowCycle(String nowCycle) {
		this.nowCycle = nowCycle;
	}

	public String getIsOverdue() {
		return isOverdue;
	}

	public void setIsOverdue(String isOverdue) {
		this.isOverdue = isOverdue;
	}

	public String getAllPay() {
		return allPay;
	}

	public void setAllPay(String allPay) {
		this.allPay = allPay;
	}

	public String getsInvestment() {
		return sInvestment;
	}

	public void setsInvestment(String sInvestment) {
		this.sInvestment = sInvestment;
	}

	public String getcRate() {
		return cRate;
	}

	public void setcRate(String cRate) {
		this.cRate = cRate;
	}
}
