package com.gxwzu.sysVO;

import java.sql.Timestamp;

public class ViewXmScheduleInInfo {

	private Integer id;
	private String content;
	private String type;
	private Integer userId;
	private String userName;
	private Timestamp creatTime;
	private String xmNumber;
	
	private String xmYear;
	private String name;
	private String scale;
	private String period;
	private String cycle;
	private String owner;
	private String county;
	private Timestamp startTime;
	private Timestamp completionTime;
	private String address;
	private String industry;
	private String isState;
	private String isCompleted;
	private String budgetType;
	private String projectType;
	private String xmUserId;
	private String xmUserName;
	private String portrait;
	private Timestamp createTime;
	
	private String coordinate;
	private String coordinatePly;
	
	public ViewXmScheduleInInfo() {
		super();
	}

	public ViewXmScheduleInInfo(Integer id, String content, String type,
			Integer userId, String userName, Timestamp creatTime,
			String xmNumber, String xmYear, String name, String scale,
			String period, String owner, String county, Timestamp startTime,
			Timestamp completionTime, String address, String industry,
			String isState, String isCompleted, String budgetType,
			String projectType, String xmUserId, String xmUserName,
			String portrait, Timestamp createTime, String coordinate,
			String coordinatePly, String cycle) {
		super();
		this.id = id;
		this.content = content;
		this.type = type;
		this.userId = userId;
		this.userName = userName;
		this.creatTime = creatTime;
		this.xmNumber = xmNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.scale = scale;
		this.period = period;
		this.cycle = cycle;
		this.owner = owner;
		this.county = county;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.address = address;
		this.industry = industry;
		this.isState = isState;
		this.isCompleted = isCompleted;
		this.budgetType = budgetType;
		this.projectType = projectType;
		this.xmUserId = xmUserId;
		this.xmUserName = xmUserName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(Timestamp completionTime) {
		this.completionTime = completionTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}

	public String getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getXmUserId() {
		return xmUserId;
	}

	public void setXmUserId(String xmUserId) {
		this.xmUserId = xmUserId;
	}

	public String getXmUserName() {
		return xmUserName;
	}

	public void setXmUserName(String xmUserName) {
		this.xmUserName = xmUserName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
}
