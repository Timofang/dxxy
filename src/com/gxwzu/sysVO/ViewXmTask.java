package com.gxwzu.sysVO;

public class ViewXmTask {

	private Integer userId;
	private String userName;
	private String landNumber;
	private String taskYear;
	private String taskMonth;
	
	private String landTask;
	private String houseTask;
	private String mountainTask;
	
	public ViewXmTask() {
		super();
	}

	public ViewXmTask(Integer userId, String userName, String landNumber,
			String taskYear, String taskMonth, String landTask,
			String houseTask, String mountainTask) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.landNumber = landNumber;
		this.taskYear = taskYear;
		this.taskMonth = taskMonth;
		this.landTask = landTask;
		this.houseTask = houseTask;
		this.mountainTask = mountainTask;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLandNumber() {
		return landNumber;
	}

	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}

	public String getTaskYear() {
		return taskYear;
	}

	public void setTaskYear(String taskYear) {
		this.taskYear = taskYear;
	}

	public String getTaskMonth() {
		return taskMonth;
	}

	public void setTaskMonth(String taskMonth) {
		this.taskMonth = taskMonth;
	}

	public String getLandTask() {
		return landTask;
	}

	public void setLandTask(String landTask) {
		this.landTask = landTask;
	}

	public String getHouseTask() {
		return houseTask;
	}

	public void setHouseTask(String houseTask) {
		this.houseTask = houseTask;
	}

	public String getMountainTask() {
		return mountainTask;
	}

	public void setMountainTask(String mountainTask) {
		this.mountainTask = mountainTask;
	}
	
}
