package com.gxwzu.sysVO;

import java.util.List;

import com.gxwzu.business.model.XmReply;
import com.gxwzu.business.model.XmSchedulepic;

public class ViewXmSchedule {

	private Integer id;
	private String xmInfoName;
	private String content;
	private String type;
	private Integer userId;
	private String userName;
	private String portrait;
	private String creatTime;
	private String xmNumber;
	
	private String loginUserType;
	
	private List<XmSchedulepic> listXmSchedulepic;
	private List<XmReply> listXmReply;
	
	private String isReply;//0没有，1有
	
	private String budgetType;

	public ViewXmSchedule() {
		super();
	}

	public ViewXmSchedule(Integer id, String xmInfoName, String content,
			String type, Integer userId, String userName, String portrait,
			String creatTime, String xmNumber, String loginUserType,
			List<XmSchedulepic> listXmSchedulepic, List<XmReply> listXmReply,
			String isReply, String budgetType) {
		super();
		this.id = id;
		this.xmInfoName = xmInfoName;
		this.content = content;
		this.type = type;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.creatTime = creatTime;
		this.xmNumber = xmNumber;
		this.loginUserType = loginUserType;
		this.listXmSchedulepic = listXmSchedulepic;
		this.listXmReply = listXmReply;
		this.isReply = isReply;
		this.budgetType = budgetType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmInfoName() {
		return xmInfoName;
	}

	public void setXmInfoName(String xmInfoName) {
		this.xmInfoName = xmInfoName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getLoginUserType() {
		return loginUserType;
	}

	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}

	public List<XmSchedulepic> getListXmSchedulepic() {
		return listXmSchedulepic;
	}

	public void setListXmSchedulepic(List<XmSchedulepic> listXmSchedulepic) {
		this.listXmSchedulepic = listXmSchedulepic;
	}

	public List<XmReply> getListXmReply() {
		return listXmReply;
	}

	public void setListXmReply(List<XmReply> listXmReply) {
		this.listXmReply = listXmReply;
	}

	public String getIsReply() {
		return isReply;
	}

	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	
}
