package com.gxwzu.sysVO;

import java.sql.Timestamp;

public class ViewXmLandproj {

	private Integer id;
	private String landNumber;
	private String type;
	private String name;
	private String laYear;
	private String cycle;
	private String approval;
	private String landTotal;//土地征收（亩）项目总任务
	private String landSurplus;//土地征收（亩）剩余待完成任务
	private String landCurrent;//土地征收（亩）本年任务
	private String houseTotal;//房屋征收（m2）项目总任务
	private String houseSurplus;//房屋征收（m2）剩余待完成任务
	private String houseCurrent;//房屋征收（m2）本年任务
	private String mountainTotal;//坟山迁移（座）项目总任务
	private String mountainSurplus;//坟山迁移（座）剩余待完成任务
	private String mountainCurrent;//坟山迁移（座）本年任务
	private String largeInvest;
	private String owner;
	private String address;
	private String coordinate;
	private String coordinatePly;
	private String userId;
	private String userName;
	private String portrait;
	private Timestamp createTime;
	
	private String landXmTask;//土地征收（亩）总任务数（计算而得）
	private String houseXmTask;//房屋征收（m2）总任务数（计算而得）
	private String mountainXmTask;//坟山迁移（座）总任务数（计算而得）
	private String landCRate;//土地征收（亩）百分比
	
	private String nowCycle;//当前时间-最新工作进度（时间）
	private String isOverdue;//是否超期：0不超期，1超期
	
	private String landType;//征地类型
	
	public ViewXmLandproj() {
		super();
	}

	public ViewXmLandproj(Integer id, String landNumber, String type,
			String name, String laYear, String cycle, String approval,
			String landTotal, String landSurplus, String landCurrent,
			String houseTotal, String houseSurplus, String houseCurrent,
			String mountainTotal, String mountainSurplus,
			String mountainCurrent, String largeInvest, String owner,
			String address, String coordinate, String coordinatePly,
			String userId, String userName, String portrait,
			Timestamp createTime, String landXmTask, String houseXmTask,
			String mountainXmTask, String landCRate, String nowCycle,
			String isOverdue, String landType) {
		super();
		this.id = id;
		this.landNumber = landNumber;
		this.type = type;
		this.name = name;
		this.laYear = laYear;
		this.cycle = cycle;
		this.approval = approval;
		this.landTotal = landTotal;
		this.landSurplus = landSurplus;
		this.landCurrent = landCurrent;
		this.houseTotal = houseTotal;
		this.houseSurplus = houseSurplus;
		this.houseCurrent = houseCurrent;
		this.mountainTotal = mountainTotal;
		this.mountainSurplus = mountainSurplus;
		this.mountainCurrent = mountainCurrent;
		this.largeInvest = largeInvest;
		this.owner = owner;
		this.address = address;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.landXmTask = landXmTask;
		this.houseXmTask = houseXmTask;
		this.mountainXmTask = mountainXmTask;
		this.landCRate = landCRate;
		this.nowCycle = nowCycle;
		this.isOverdue = isOverdue;
		this.landType = landType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLandNumber() {
		return landNumber;
	}

	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLaYear() {
		return laYear;
	}

	public void setLaYear(String laYear) {
		this.laYear = laYear;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getLandTotal() {
		return landTotal;
	}

	public void setLandTotal(String landTotal) {
		this.landTotal = landTotal;
	}

	public String getLandSurplus() {
		return landSurplus;
	}

	public void setLandSurplus(String landSurplus) {
		this.landSurplus = landSurplus;
	}

	public String getLandCurrent() {
		return landCurrent;
	}

	public void setLandCurrent(String landCurrent) {
		this.landCurrent = landCurrent;
	}

	public String getHouseTotal() {
		return houseTotal;
	}

	public void setHouseTotal(String houseTotal) {
		this.houseTotal = houseTotal;
	}

	public String getHouseSurplus() {
		return houseSurplus;
	}

	public void setHouseSurplus(String houseSurplus) {
		this.houseSurplus = houseSurplus;
	}

	public String getHouseCurrent() {
		return houseCurrent;
	}

	public void setHouseCurrent(String houseCurrent) {
		this.houseCurrent = houseCurrent;
	}

	public String getMountainTotal() {
		return mountainTotal;
	}

	public void setMountainTotal(String mountainTotal) {
		this.mountainTotal = mountainTotal;
	}

	public String getMountainSurplus() {
		return mountainSurplus;
	}

	public void setMountainSurplus(String mountainSurplus) {
		this.mountainSurplus = mountainSurplus;
	}

	public String getMountainCurrent() {
		return mountainCurrent;
	}

	public void setMountainCurrent(String mountainCurrent) {
		this.mountainCurrent = mountainCurrent;
	}

	public String getLargeInvest() {
		return largeInvest;
	}

	public void setLargeInvest(String largeInvest) {
		this.largeInvest = largeInvest;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getLandXmTask() {
		return landXmTask;
	}

	public void setLandXmTask(String landXmTask) {
		this.landXmTask = landXmTask;
	}

	public String getHouseXmTask() {
		return houseXmTask;
	}

	public void setHouseXmTask(String houseXmTask) {
		this.houseXmTask = houseXmTask;
	}

	public String getMountainXmTask() {
		return mountainXmTask;
	}

	public void setMountainXmTask(String mountainXmTask) {
		this.mountainXmTask = mountainXmTask;
	}

	public String getLandCRate() {
		return landCRate;
	}

	public void setLandCRate(String landCRate) {
		this.landCRate = landCRate;
	}

	public String getNowCycle() {
		return nowCycle;
	}

	public void setNowCycle(String nowCycle) {
		this.nowCycle = nowCycle;
	}

	public String getIsOverdue() {
		return isOverdue;
	}

	public void setIsOverdue(String isOverdue) {
		this.isOverdue = isOverdue;
	}

	public String getLandType() {
		return landType;
	}

	public void setLandType(String landType) {
		this.landType = landType;
	}

}
