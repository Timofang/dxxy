package com.gxwzu.sysVO;

import java.sql.Timestamp;
import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.system.model.user.User;

public class ViewXmChild {
	private Integer id;
	private String xm_number;
	private String xm_child_number;
	private String xm_child_name;
	private String unit_id;
	private String user_name;
	private String unit_parent;
	private Integer issue;
	private String requires;
	private Integer file_count;
	private String file_name;
	private String materials_id;
	private String publish_time;
	private String finish_time;
	private String dead_line;
	private Integer status;
	private Integer acceptstatus;
	private String xm_name;
	private List<User> listUser;
	private List<Unit> listUnit;
	private User user;
	private Unit unit;

	private ViewXm projectModel;
	private ViewXmChild projectChildModel;
	private List<ViewXmChild> projectChildListModel;
	private List<ViewPsInfoModel> listPs;
	
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public String getFinish_time() {
		return finish_time;
	}

	public void setFinish_time(String finish_time) {
		this.finish_time = finish_time;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

	public List<Unit> getListUnit() {
		return listUnit;
	}

	public void setListUnit(List<Unit> listUnit) {
		this.listUnit = listUnit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXm_number() {
		return xm_number;
	}

	public void setXm_number(String xm_number) {
		this.xm_number = xm_number;
	}

	public String getXm_child_number() {
		return xm_child_number;
	}

	public void setXm_child_number(String xm_child_number) {
		this.xm_child_number = xm_child_number;
	}

	public String getXm_child_name() {
		return xm_child_name;
	}

	public void setXm_child_name(String xm_child_name) {
		this.xm_child_name = xm_child_name;
	}

	public String getUnit_id() {
		return unit_id;
	}

	public void setUnit_id(String unit_id) {
		this.unit_id = unit_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUnit_parent() {
		return unit_parent;
	}

	public void setUnit_parent(String unit_parent) {
		this.unit_parent = unit_parent;
	}

	public Integer getIssue() {
		return issue;
	}

	public void setIssue(Integer issue) {
		this.issue = issue;
	}

	public String getRequires() {
		return requires;
	}

	public void setRequires(String requires) {
		this.requires = requires;
	}

	public Integer getFile_count() {
		return file_count;
	}

	public void setFile_count(Integer file_count) {
		this.file_count = file_count;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getMaterials_id() {
		return materials_id;
	}

	public void setMaterials_id(String materials_id) {
		this.materials_id = materials_id;
	}

	public String getPublish_time() {
		return publish_time;
	}

	public void setPublish_time(String publish_time) {
		this.publish_time = publish_time;
	}

	public String getDead_line() {
		return dead_line;
	}

	public void setDead_line(String dead_line) {
		this.dead_line = dead_line;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getAcceptstatus() {
		return acceptstatus;
	}

	public void setAcceptstatus(Integer acceptstatus) {
		this.acceptstatus = acceptstatus;
	}

	public String getXm_name() {
		return xm_name;
	}

	public void setXm_name(String xm_name) {
		this.xm_name = xm_name;
	}

	public ViewXmChild(){
		super();
	}
	
	public ViewXmChild(Integer id, 
			String xm_number, 
			String xm_child_number, 
			String xm_child_name,
			String unit_id, 
			String user_name, 
			String unit_parent,
					   Integer issue,
			String requires,
					   Integer file_count,
			String file_name,
			String materials_id,
					   String publish_time,
					   String dead_line,
					   Integer status,
					   Integer acceptstatus,
			String xm_name,ViewXm projectModel,ViewXmChild projectChildModel)
	{
		super();
		this.id = id;
		this. xm_number = xm_number;
		this. xm_child_number = xm_child_number;
		this. xm_child_name = xm_child_name;
		this. unit_id = unit_id;
		this. user_name = user_name;
		this. unit_parent = unit_parent;
		this. issue = issue;
		this. requires = requires;
		this. file_count = file_count;
		this. file_name = file_name;
		this. materials_id = materials_id;
		this. publish_time = publish_time;
		this. dead_line = dead_line;
		this. status = status;
		this. acceptstatus = acceptstatus;
		this. xm_name = xm_name;
		this.projectModel=projectModel;
		this.projectChildModel=projectChildModel;
	}

	public ViewXm getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ViewXm projectModel) {
		this.projectModel = projectModel;
	}

	public ViewXmChild getProjectChildModel() {
		return projectChildModel;
	}

	public void setProjectChildModel(ViewXmChild projectChildModel) {
		this.projectChildModel = projectChildModel;
	}

	public List<ViewXmChild> getProjectChildListModel() {
		return projectChildListModel;
	}

	public void setProjectChildListModel(List<ViewXmChild> projectChildListModel) {
		this.projectChildListModel = projectChildListModel;
	}

	public List<ViewPsInfoModel> getListPs() {
		return listPs;
	}

	public void setListPs(List<ViewPsInfoModel> listPs) {
		this.listPs = listPs;
	}
}
