package com.gxwzu.sysVO;

public class ViewInvest implements java.io.Serializable {

	private Integer id;
	private String xmNumber;
	private String investment;
	private String payment;
	private String investYear;
	private String investMonth;
	private Integer userId;
	private String userName;
	private String createTime;
	
	public ViewInvest() {
		super();
	}

	public ViewInvest(Integer id, String xmNumber, String investment,
			String payment, String investYear, String investMonth,
			Integer userId, String userName, String createTime) {
		super();
		this.id = id;
		this.xmNumber = xmNumber;
		this.investment = investment;
		this.payment = payment;
		this.investYear = investYear;
		this.investMonth = investMonth;
		this.userId = userId;
		this.userName = userName;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getInvestment() {
		return investment;
	}

	public void setInvestment(String investment) {
		this.investment = investment;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getInvestYear() {
		return investYear;
	}

	public void setInvestYear(String investYear) {
		this.investYear = investYear;
	}

	public String getInvestMonth() {
		return investMonth;
	}

	public void setInvestMonth(String investMonth) {
		this.investMonth = investMonth;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	
}