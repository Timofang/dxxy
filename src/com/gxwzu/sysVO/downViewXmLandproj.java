package com.gxwzu.sysVO;

import java.math.BigInteger;
import java.util.List;

import com.gxwzu.core.model.ViewbaseModel;

public class downViewXmLandproj extends ViewbaseModel{

	@Override
	public void total() {
		
	}

	@Override
	public void sort() {
		
		List list=super.getData();
		
		for(int i=0;list!=null&&i<list.size();i++){
			Object[]oc=(Object[])list.get(i);
			
			String landType = (String)oc[30];
			String strLandType = "";
			if(landType != null && !"".equals(landType)){
				if(landType.contains("01")){
					strLandType += "分片区；";
				}
				if(landType.contains("02")){
					strLandType += "片区外重点；";
				}
				if(landType.contains("03")){
					strLandType += "片区外统筹；";
				}
				if(landType.contains("04")){
					strLandType += "征拆大会战；";
				}
			}
			
			String strIsOverdue = "";
			if("0".equals((BigInteger)oc[29]+"")){
				strIsOverdue = "未超期";
			}
			if("1".equals((BigInteger)oc[29]+"")){
				strIsOverdue = "超期";
			}
			
			Object[] sortOc = { oc[3], oc[2], oc[5], strLandType,
					oc[11], oc[12], oc[13], oc[14], oc[15], oc[16], oc[17], oc[18], oc[19],
					oc[24], oc[25], oc[26], oc[27], oc[6],
					oc[7], oc[8], oc[4], strIsOverdue };

			super.getSortdata().add(sortOc);
			
		}
	}

	@Override
	public void convert() {
		
	}
}
