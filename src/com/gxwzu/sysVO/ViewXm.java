package com.gxwzu.sysVO;

import java.sql.Timestamp;
import java.util.List;

import com.gxwzu.business.model.Unit;
import com.gxwzu.system.model.user.User;

public class ViewXm {
	private Integer id;
	private String name;
	private String username;
	private String xm_number;
	private String publish_time;
	private String  dead_line;
	private String receiver;
	private String type;
	private String receiver_unit;
	private Integer file_count;
	private String file_name;
	private Integer status;
	private String requires;
	private String finish_time;
	
	private List<User> listUser;
	private List<Unit> listUnit;
	private List<ViewPsInfoModel> listPs;
	private User user;
	private Unit unit;
	
	public ViewXm(){
		super();
	}
	
	
	public ViewXm(Integer id, String name, String username, String xm_number, String publish_time,
				  String dead_line, String receiver, String type,String receiver_unit, Integer file_count, String file_name, Integer status,String requires)
	{
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.xm_number = xm_number;
		this.publish_time = publish_time;
		this.dead_line = dead_line;
		this.receiver = receiver;
		this.type = type;
		this.setReceiver_unit(receiver_unit);
		this.file_count = file_count;
		this.file_name = file_name;
		this.status = status;
		this.requires = requires;
		
	}
	
	
	public String getFinish_time() {
		return finish_time;
	}


	public void setFinish_time(String finish_time) {
		this.finish_time = finish_time;
	}


	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Unit> getListUnit() {
		return listUnit;
	}

	public void setListUnit(List<Unit> listUnit) {
		this.listUnit = listUnit;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getXm_number() {
		return xm_number;
	}

	public void setXm_number(String xm_number) {
		this.xm_number = xm_number;
	}

	public String getPublish_time() {
		return publish_time;
	}

	public void setPublish_time(String publish_time) {
		this.publish_time = publish_time;
	}

	public String getDead_line() {
		return dead_line;
	}

	public void setDead_line(String dead_line) {
		this.dead_line = dead_line;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFile_count() {
		return file_count;
	}

	public void setFile_count(Integer file_count) {
		this.file_count = file_count;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRequires() {
		return requires;
	}

	public void setRequires(String requires) {
		this.requires = requires;
	}

	public String getReceiver_unit() {
		return receiver_unit;
	}

	public void setReceiver_unit(String receiver_unit) {
		this.receiver_unit = receiver_unit;
	}

	@Override
	public String toString() {
		return "ViewXm [id=" + id + ", name=" + name + ", username=" + username + ", xm_number=" + xm_number
				+ ", publish_time=" + publish_time + ", dead_line=" + dead_line + ", receiver=" + receiver + ", type="
				+ type + ", receiver_unit=" + receiver_unit + ", file_count=" + file_count + ", file_name=" + file_name
				+ ", status=" + status + ", requires=" + requires + ", listUser=" + listUser + ", listUnit=" + listUnit
				+ "]";
	}


	public List<ViewPsInfoModel> getListPs() {
		return listPs;
	}


	public void setListPs(List<ViewPsInfoModel> listPs) {
		this.listPs = listPs;
	}

	
	

}
