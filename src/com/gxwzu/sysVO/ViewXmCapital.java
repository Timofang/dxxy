package com.gxwzu.sysVO;

public class ViewXmCapital {

	private Integer id;
	private String xmNumber;
	
	private String investTotal;//总投投资
	private String investSource;//资金来源
	private String investPlan;//当年计划投资
	private String investComplete;//去年完成投资
	
	private String sInvestment;//当年完成投资（资金进度表统计）
	private String cRate;//投资完成率
	
	public ViewXmCapital() {
		super();
	}

	public ViewXmCapital(Integer id, String xmNumber, String investTotal,
			String investSource, String investPlan, String investComplete,
			String sInvestment, String cRate) {
		super();
		this.id = id;
		this.xmNumber = xmNumber;
		this.investTotal = investTotal;
		this.investSource = investSource;
		this.investPlan = investPlan;
		this.investComplete = investComplete;
		this.sInvestment = sInvestment;
		this.cRate = cRate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getInvestTotal() {
		return investTotal;
	}

	public void setInvestTotal(String investTotal) {
		this.investTotal = investTotal;
	}

	public String getInvestSource() {
		return investSource;
	}

	public void setInvestSource(String investSource) {
		this.investSource = investSource;
	}

	public String getInvestPlan() {
		return investPlan;
	}

	public void setInvestPlan(String investPlan) {
		this.investPlan = investPlan;
	}

	public String getInvestComplete() {
		return investComplete;
	}

	public void setInvestComplete(String investComplete) {
		this.investComplete = investComplete;
	}

	public String getsInvestment() {
		return sInvestment;
	}

	public void setsInvestment(String sInvestment) {
		this.sInvestment = sInvestment;
	}

	public String getcRate() {
		return cRate;
	}

	public void setcRate(String cRate) {
		this.cRate = cRate;
	}
	
}
