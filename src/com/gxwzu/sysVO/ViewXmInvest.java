package com.gxwzu.sysVO;

import java.util.List;

public class ViewXmInvest {

	private String allPay;
	private List<ViewInvest> listViewInvest;
	
	public ViewXmInvest() {
		super();
	}

	public ViewXmInvest(String allPay, List<ViewInvest> listViewInvest) {
		super();
		this.allPay = allPay;
		this.listViewInvest = listViewInvest;
	}

	public String getAllPay() {
		return allPay;
	}

	public void setAllPay(String allPay) {
		this.allPay = allPay;
	}

	public List<ViewInvest> getListViewInvest() {
		return listViewInvest;
	}

	public void setListViewInvest(List<ViewInvest> listViewInvest) {
		this.listViewInvest = listViewInvest;
	}
	
}
