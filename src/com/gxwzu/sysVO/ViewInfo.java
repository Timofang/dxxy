package com.gxwzu.sysVO;

public class ViewInfo {

	private Integer id;
	private String xmNumber;
	private String xmYear;
	private String name;
	private String scale;
	private String period;
	private String cycle;
	private String owner;
	private String county;
	private String startTime;
	private String completionTime;
	private String address;
	private String industry;
	private String isState;
	private String isCompleted;
	private String budgetType;
	private String projectType;
	private String userId;
	private String userName;
	private String portrait;
	private String createTime;
	
	private String coordinate;
	private String coordinatePly;
	
	public ViewInfo() {
		super();
	}

	public ViewInfo(Integer id, String xmNumber, String xmYear, String name,
			String scale, String period, String cycle, String owner,
			String county, String startTime, String completionTime,
			String address, String industry, String isState,
			String isCompleted, String budgetType, String projectType,
			String userId, String userName, String portrait, String createTime,
			String coordinate, String coordinatePly) {
		super();
		this.id = id;
		this.xmNumber = xmNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.scale = scale;
		this.period = period;
		this.cycle = cycle;
		this.owner = owner;
		this.county = county;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.address = address;
		this.industry = industry;
		this.isState = isState;
		this.isCompleted = isCompleted;
		this.budgetType = budgetType;
		this.projectType = projectType;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(String completionTime) {
		this.completionTime = completionTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}

	public String getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}

	
}