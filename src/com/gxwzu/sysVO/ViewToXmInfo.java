package com.gxwzu.sysVO;

import java.sql.Timestamp;

public class ViewToXmInfo {

	private Integer toLandprojid;//中间表的ID
	private Integer infoid;//征地进度的ID
	private String xmNumber;
	private String landNumber;
	
	private String xmYear;
	private String name;
	private String scale;
	private String period;
	private String cycle;
	private String owner;
	private String county;
	private Timestamp startTime;
	private Timestamp completionTime;
	private String address;
	private String industry;
	private String isState;
	private String isCompleted;
	private String budgetType;
	private String projectType;
	private String userId;
	private String userName;
	private String portrait;
	private Timestamp createTime;
	
	private String coordinate;
	private String coordinatePly;
	
	public ViewToXmInfo() {
		super();
	}

	public ViewToXmInfo(Integer toLandprojid, Integer infoid, String xmNumber,
			String landNumber, String xmYear, String name, String scale,
			String period, String cycle, String owner, String county,
			Timestamp startTime, Timestamp completionTime, String address,
			String industry, String isState, String isCompleted,
			String budgetType, String projectType, String userId,
			String userName, String portrait, Timestamp createTime,
			String coordinate, String coordinatePly) {
		super();
		this.toLandprojid = toLandprojid;
		this.infoid = infoid;
		this.xmNumber = xmNumber;
		this.landNumber = landNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.scale = scale;
		this.period = period;
		this.cycle = cycle;
		this.owner = owner;
		this.county = county;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.address = address;
		this.industry = industry;
		this.isState = isState;
		this.isCompleted = isCompleted;
		this.budgetType = budgetType;
		this.projectType = projectType;
		this.userId = userId;
		this.userName = userName;
		this.portrait = portrait;
		this.createTime = createTime;
		this.coordinate = coordinate;
		this.coordinatePly = coordinatePly;
	}

	public Integer getToLandprojid() {
		return toLandprojid;
	}

	public void setToLandprojid(Integer toLandprojid) {
		this.toLandprojid = toLandprojid;
	}

	public Integer getInfoid() {
		return infoid;
	}

	public void setInfoid(Integer infoid) {
		this.infoid = infoid;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getLandNumber() {
		return landNumber;
	}

	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(Timestamp completionTime) {
		this.completionTime = completionTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}

	public String getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoordinatePly() {
		return coordinatePly;
	}

	public void setCoordinatePly(String coordinatePly) {
		this.coordinatePly = coordinatePly;
	}
	
	
}
