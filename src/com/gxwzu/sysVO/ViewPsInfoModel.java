package com.gxwzu.sysVO;

import java.util.List;

public class ViewPsInfoModel {


	private String xmNumber;
	private String instructionsPerson;
	private String instructionsContent;
	private String instructionsTime;
	private List<ViewXm> listXm;

	public ViewPsInfoModel() {
		super();
	}

	public ViewPsInfoModel(String xmNumber,
			String instructionsPerson,
			String instructions_conte,
			String instructions_time) {
		super();
		this.xmNumber = xmNumber;
		this.instructionsPerson = instructionsPerson;
		this.instructionsContent = instructionsContent;
		this.instructionsTime = instructionsTime;

	}

	public List<ViewXm> getListXm() {
		return listXm;
	}

	public void setListXm(List<ViewXm> listXm) {
		this.listXm = listXm;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getInstructionsPerson() {
		return instructionsPerson;
	}

	public void setInstructionsPerson(String instructionsPerson) {
		this.instructionsPerson = instructionsPerson;
	}

	public String getInstructionsContent() {
		return instructionsContent;
	}

	public void setInstructionsContent(String instructionsContent) {
		this.instructionsContent = instructionsContent;
	}

	public String getInstructionsTime() {
		return instructionsTime;
	}

	public void setInstructionsTime(String instructionsTime) {
		this.instructionsTime = instructionsTime;
	}
}
