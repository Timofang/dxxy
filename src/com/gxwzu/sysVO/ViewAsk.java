package com.gxwzu.sysVO;

import java.sql.Timestamp;

public class ViewAsk {

	private String xmNumber;
	private String xmYear;
	private String name;
	private Timestamp startTime;
	private String budgetType;
	private String xmSourceNumber;
	private String xsName;
	private Integer peopleId;
	private String people;
	private String peopleTel;
	private String xsLevel;//响应等级
	private String xsIndustry;//相关产业
	private String xsArea;//所属区域
	
	private String scheduleContent01;//最新落实情况
	private String scheduleContent02;//存在的问题和困难
	private String scheduleContent03;//下一步工作计划
	
	private String dutyPeople08;//责任领导
	private String dutyPeople09;//具体跟踪人及联系方式
	
	private String nowCycle;
	private String isOverdue;
	
	private String isClose;//是否办结：00已办结，01未办结
	
	public ViewAsk() {
		super();
	}

	public ViewAsk(String xmNumber, String xmYear, String name,
			Timestamp startTime, String budgetType, String xmSourceNumber,
			String xsName, Integer peopleId, String people, String peopleTel,
			String xsLevel,String xsIndustry,String xsArea,String scheduleContent01, String scheduleContent02,
			String scheduleContent03, String dutyPeople08, String dutyPeople09, 
			String nowCycle, String isOverdue, String isClose) {
		super();
		this.xmNumber = xmNumber;
		this.xmYear = xmYear;
		this.name = name;
		this.startTime = startTime;
		this.budgetType = budgetType;
		this.xmSourceNumber = xmSourceNumber;
		this.xsName = xsName;
		this.peopleId = peopleId;
		this.people = people;
		this.peopleTel = peopleTel;
		this.xsLevel = xsLevel;
		this.xsIndustry = xsIndustry;
		this.xsArea = xsArea;
		this.scheduleContent01 = scheduleContent01;
		this.scheduleContent02 = scheduleContent02;
		this.scheduleContent03 = scheduleContent03;
		this.dutyPeople08 = dutyPeople08;
		this.dutyPeople09 = dutyPeople09;
		this.nowCycle = nowCycle;
		this.isOverdue = isOverdue;
		this.isClose = isClose;
	}

	public String getXmNumber() {
		return xmNumber;
	}

	public void setXmNumber(String xmNumber) {
		this.xmNumber = xmNumber;
	}

	public String getXmYear() {
		return xmYear;
	}

	public void setXmYear(String xmYear) {
		this.xmYear = xmYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public String getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}

	public String getXmSourceNumber() {
		return xmSourceNumber;
	}

	public void setXmSourceNumber(String xmSourceNumber) {
		this.xmSourceNumber = xmSourceNumber;
	}

	public String getXsName() {
		return xsName;
	}

	public void setXsName(String xsName) {
		this.xsName = xsName;
	}

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public String getPeople() {
		return people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	public String getPeopleTel() {
		return peopleTel;
	}

	public void setPeopleTel(String peopleTel) {
		this.peopleTel = peopleTel;
	}

	public String getScheduleContent01() {
		return scheduleContent01;
	}

	public void setScheduleContent01(String scheduleContent01) {
		this.scheduleContent01 = scheduleContent01;
	}

	public String getScheduleContent02() {
		return scheduleContent02;
	}

	public void setScheduleContent02(String scheduleContent02) {
		this.scheduleContent02 = scheduleContent02;
	}

	public String getScheduleContent03() {
		return scheduleContent03;
	}

	public void setScheduleContent03(String scheduleContent03) {
		this.scheduleContent03 = scheduleContent03;
	}

	public String getDutyPeople08() {
		return dutyPeople08;
	}

	public void setDutyPeople08(String dutyPeople08) {
		this.dutyPeople08 = dutyPeople08;
	}

	public String getDutyPeople09() {
		return dutyPeople09;
	}

	public void setDutyPeople09(String dutyPeople09) {
		this.dutyPeople09 = dutyPeople09;
	}

	public String getNowCycle() {
		return nowCycle;
	}

	public void setNowCycle(String nowCycle) {
		this.nowCycle = nowCycle;
	}

	public String getIsOverdue() {
		return isOverdue;
	}

	public void setIsOverdue(String isOverdue) {
		this.isOverdue = isOverdue;
	}

	public String getIsClose() {
		return isClose;
	}

	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}

	public String getXsLevel() {
		return xsLevel;
	}

	public void setXsLevel(String xsLevel) {
		this.xsLevel = xsLevel;
	}

	public String getXsIndustry() {
		return xsIndustry;
	}

	public void setXsIndustry(String xsIndustry) {
		this.xsIndustry = xsIndustry;
	}

	public String getXsArea() {
		return xsArea;
	}

	public void setXsArea(String xsArea) {
		this.xsArea = xsArea;
	}
	
	
	
}
